@extends('layouts.admin.app')
@section('content')
    <div class="card card-custom">
        <div class="card-header">
            <h3 class="card-title">
                Base Controls
            </h3>
            <div class="card-toolbar">
                <div class="example-tools justify-content-center">
                    <span class="example-toggle" data-toggle="tooltip" title="View code"></span>
                    <span class="example-copy" data-toggle="tooltip" title="Copy code"></span>
                </div>
            </div>
        </div>
        <!--begin::Form-->
        <form action="{{ route('admin.user.update', $user->id) }}" method="POST" enctype="multipart/form-data">
            @method('PUT')
            @csrf
            <div class="card-body">
                <div class="form-group mb-8">
                    <div class="mb-3">
                        <label for="image" class="form-label">Post Image</label>
                        <input class="form-control @error('image') is-invalid @enderror" type="file" id="image"
                            name="image" value="{{asset('image/'.$user->image) }}">
                        @error('image')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label>Name <span class="text-danger">*</span></label>
                        <input name="name" type="text" class="form-control" placeholder="Enter name"
                            value="{{ $user->name }}" />
                    </div>
                    <div class="form-group">
                        <label>Email address <span class="text-danger">*</span></label>
                        <input name="email" type="email" class="form-control" placeholder="Enter email"
                            value="{{ $user->email }}" />
                        <span class="form-text text-muted">We'll never share your email with anyone else.</span>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Password <span class="text-danger">*</span></label>
                        <input name="password" type="password" class="form-control" id="exampleInputPassword1"
                            placeholder="Password" />
                    </div>
                </div>
                <div class="form-group mb-8">
                    <div class="form-group">
                        <label>Phone <span class="text-danger">*</span></label>
                        <input name="phone" type="number" class="form-control" placeholder="Phone"
                            value="{{ $user->phone }}" />
                    </div>
                </div>
                <div class="form-group mb-8">
                    <div class="form-group">
                        <label>Address <span class="text-danger">*</span></label>
                        <input name="address" type="address" class="form-control" placeholder="Address"
                            value="{{ $user->address }}" />
                    </div>
                </div>
                <div class="form-group mb-8">
                    <div class="form-group">
                        <label>Birth Date <span class="text-danger">*</span></label>
                        <input name="birt_date" type="date" class="form-control" placeholder="Birth Date"
                            value="{{ $user->birth_date }}" />
                    </div>
                </div>
                <div class="form-group mb-8">
                    <div class="form-group">
                        <label>Birth Place <span class="text-danger">*</span></label>
                        <input name="birth_place" type="birthplace" class="form-control" placeholder="Birth Place"
                            value="{{ $user->birth_place }}" />
                    </div>
                </div>
                <div class="form-group mb-8">
                    <div class="form-group">
                        <label>Jenis Kelamin<span class="text-danger">*</span></label>
                        <input name="jk" type="birthplace" class="form-control" placeholder="jk"
                            value="{{ $user->jk }}" />
                    </div>
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-success mr-2">Update</button>
                    <a class="btn btn-secondary" href="{{ route('admin.user.index') }}">Cancel</a>
                </div>
        </form>
        </form>
        <!--end::Form-->
    </div>
@stop
