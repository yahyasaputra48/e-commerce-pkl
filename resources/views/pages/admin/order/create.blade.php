@extends('layouts.admin.app')

@section('content')
    <div class="row">
        <div class="col-md-6">
            <button type="button" class="btn btn-info mb-2">
                <span class="badge bg-light text-danger">
                    1
                </span>
            </button>
        </div>
    </div>

    <section class="section">
        <div class="row">
            <div class="col-lg-8">

                <div class="card">
                    <div class="card-body">
                    <div class="row">
                        <div class="col-md-8 mt-2">
                            <h4 class="mt-4"><b>Daftar Pesanan</b> </h4>
                        </div>
                        <div class="col-md-4 mt-3">
                            <div class="btn btn-sm btn-outline-danger text-center">Hapus</div>
                        </div>
                    </div>
                    <table class="table table-responsive mt-3 mb-2">
                        {{-- @for ($i = 1; $i <= 5; $i++)
                        <tr class="fs-5">
                            <td>nama product 1dgliasf</td>
                                    <td><input type="number" class="form-control text-center" value="1">
                            </td>
                            <td>
                                <span>Rp.250.000</span>
                            </td>
                            <td>
                                <button class="btn-close "></button>
                            </td>
                        </tr>
                        @endfor --}}
                        <thead>
                            <th>name</th>
                            <th>quantity</th>
                            <th>price</th>
                            <th>subtotal</th>
                            <th></th>
                        </thead>
                        <tbody class="product-items">

                        </tbody>
                    </table>
                    <center class="my-2">
                        <a href="#">+ Biaya Tambahan </a>
                    </center>
                    <hr class="m-0">
                    <div class="row d-lex justify-content-between">
                        <div class="col-md-4 my-2">
                            <b>
                                <strong>
                                    <h4>Total</h4>
                                </strong>
                            </b>
                        </div>
                        <div class="col-md-8 my-2 text-end">
                            <h4>Rp.25.000,00</h4>
                        </div>
                    </div>
                    <hr class="m-0">
                    <div class="row">
                        <div class="col-md-6 mt-3">
                            <div class=" btn-lg btn-info text-center">Simpan</div>
                        </div>

                        <div class="col-md-6 mt-3">
                            <div class=" btn-lg btn-danger text-center">Bayar</div>
                        </div>
                    </div>
                    </div>
                </div>

            </div>

            <div class="col-lg-4">
                <div class="card">
                    <div class="card-body">

                        <!-- Bordered Tabs -->
                        <ul class="nav nav-tabs nav-tabs-bordered d-flex" id="borderedTabJustified" role="tablist">
                            <li class="nav-item" role="presentation">
                                <button class="nav-link active" id="makanan-ringan-tab" data-bs-toggle="tab"
                                    data-bs-target="#bordered-makanan-ringan" type="button" role="tab"
                                    aria-controls="makanan-ringan" aria-selected="true">Makana Ringan</button>
                            </li>
                            <li class="nav-item" role="presentation">
                                <button class="nav-link" id="minuman-tab" data-bs-toggle="tab"
                                    data-bs-target="#bordered-minuman" type="button" role="tab" aria-controls="minuman"
                                    aria-selected="false">Minuman</button>
                            </li>
                            <li class="nav-item" role="presentation">
                                <button class="nav-link" id="makanan-berat-tab" data-bs-toggle="tab"
                                    data-bs-target="#bordered-makanan-berat" type="button" role="tab"
                                    aria-controls="makanan-berat" aria-selected="false">Makana Berat</button>
                            </li>
                        </ul>

                        <div class="tab-content pt-2" id="borderedTabContent">
                            <div class="tab-pane  active" id="bordered-makanan-ringan" role="tabpanel"
                                aria-labelledby="makanan-ringan-tab">

                                @foreach ($products as $product)
                                    <div class="col-md-3 col-lg-3 col-sm-3 image-responsive product"
                                    data-name="{{ $product->name }}" data-id="{{ $product->id }}"
                                    data-price="{{ $product->price }}" data-stok="{{ $product->stok }}">
                                        <div class="mx-1 my-2">
                                            @if ($product->image)
                                                <img style="width:200px; height:80px;" class=""
                                                    src="{{ asset('storage/' . $product->image) }}" alt=" ">
                                            @else
                                                <img style="width:200px; " class="image-responsive"
                                                    src="{{ asset('img/ppnull.jpg') }}" alt="">
                                            @endif
                                            <span>{{ $product->name }}</span>
                                            <span>{{ $product->price }}</span>
                                        </div>
                                    </div>
                                @endforeach

                            </div>

                            <div class="tab-pane " id="bordered-minuman" role="tabpanel" aria-labelledby="minuman-tab">

                                @foreach ($products as $product)
                                    <div class="col-md-3 col-lg-3 col-sm-3 image-responsive">
                                        <div class="mx-1 my-2">
                                            <input style="position:absolute;" type="checkbox">
                                            <img style="width:200px;" src="{{ asset('img/ppnull.jpg') }}" alt=""
                                                class="">
                                            <center> milk shake</center>
                                        </div>
                                    </div>
                                @endforeach

                            </div>

                            <div class="tab-pane " id="bordered-makanan-berat" role="tabpanel"
                                aria-labelledby="makanan-berat-tab">

                                <div class="col-md-3 col-lg-3 col-sm-3 image-responsive">
                                    <div class="mx-1 my-2">
                                        <input style="position:absolute;" type="checkbox">
                                        <img style="width:200px;" src="{{ asset('img/ppnull.jpg') }}" alt=""
                                            class="">
                                        <center>sangu ayam pcel</center>
                                    </div>
                                </div>

                            </div>

                        </div><!-- End Bordered Tabs -->

                    </div>
                </div>
            </div>

        </div>

    </section>

    <script>
        // loadProductItems();
        // localStorage.setItem("carts", "");
        localStorage.removeItem("carts");
        $(".product").click(function() {
            var index = $(".product").index(this)
            var id = $(".product").eq(index).data("id");
            var name = $(".product").eq(index).data("name");
            var price = $(".product").eq(index).data("price");
            var stok = $(".product").eq(index).data("stok");
            var carts = localStorage.getItem("carts") == null ? [] : JSON.parse(localStorage.getItem("carts"));

            var cart = {};
            cart.id = id
            cart.name = name
            cart.price = price
            cart.stok = stok
            // carts.push(cart);

            // localStorage.setItem("carts", JSON.stringify(carts));

            // console.log(JSON.parse(localStorage.getItem("carts")));
            var html = `<tr class="fs-5">
                            <td>
                                <span>${cart.name}</span>
                                <input type="hidden" value="${cart.name}" name="name" readonly>
                            </td>
                                <td><input type="number" class="cart-qty form-control text-center " value="1">
                            </td>
                            <td>
                                <span>${cart.price}</span>
                                <input type="hidden" value="${cart.price}" name="price" class="cart-price" readonly>
                            </td>
                            <td>
                                <span class="cart-subtotal">${cart.price}</span>
                                <input type="hidden" name="subtotal" class="subtotal" value="${cart.price}">
                            </td>
                            <td>
                                <button class="btn-close"></button>
                            </td>
                        </tr>`;
            $(".product-items").append(html);
            // loadProductItems();
        });
        // var carts = JSON.parse(localStorage.getItem("carts"));

        function loadProductItems() {
            let carts = JSON.parse(localStorage.getItem("carts"));
            let html = '';

            for (const cart of carts) {
                html += `<tr class="fs-5">
                            <td>
                                <span>${cart.name}</span>
                                <input type="hidden" value="${cart.name}" name="name" readonly>
                            </td>
                                <td><input type="number" class="cart-qty form-control text-center " value="1">
                            </td>
                            <td>
                                <span>${cart.price}</span>
                                <input type="hidden" value="${cart.price}" name="price" class="cart-price" readonly>
                            </td>
                            <td>
                                <span class="cart-subtotal">${cart.price}</span>
                                <input type="hidden" name="subtotal" class="subtotal" value="${cart.price}">
                            </td>
                            <td>
                                <button class="btn-close"></button>
                            </td>
                        </tr>`;
            }

            $('.product-items').html(html)
        }

        $(document).on('keyup','.cart-qty',function( e ) {
            var index   = $(".cart-qty").index(this)
            console.log(index);
            var qty     = $(".cart-qty").eq(index).val();
            var price   = $(".cart-price").eq(index).val();
            var subtotal= qty * price;
            $(".cart-subtotal").eq(index).html(subtotal)
        });

        $(document).on('click','.btn-close',function( e ) {
            var index   = $(".btn-close").index(this)
            $(".btn-close").eq(index).parent().parent().remove();
        });


    </script>
    <span></span>
@endsection
