@extends('layouts.admin.app')
@section('content')
    <div class="card-header border-0 py-5">
        <h3 class="card-title align-items-start flex-column">
            <span class="card-label font-weight-bolder text-dark">List Order</span>
        </h3>
        <div class="card-toolbar">
            <a href="{{ route('admin.ordercreate') }}" class="btn btn-success font-weight-bolder font-size-sm">Create</a>
        </div>
    </div>
    <div class="card-header border-0 py-5">
        <div class="tab-content">
            <!--begin::Table-->
            <div class=>
                <table class="table table-head-custom table-head-bg table-borderless table-vertical-center">
                    <thead>
                        <tr class="text-left text-uppercase">
                            <th style="min-width: 80px">User Id</th>
                            <th style="min-width: 80px">Order Number</th>
                            <th style="min-width: 80px">Order Date</th>
                            <th style="min-width: 80px">Status</th>
                            <th style="min-width: 80px">Total</th>
                            <th style="min-width: 80px">Payment</th>
                            <th style="min-width: 80px">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($orders as $data)
                            <tr>
                                <td>{{ $data->user->name }}</td>
                                <td>{{ $data->order_number }}</td>
                                <td>{{ $data->order_date }}</td>
                                <td>{{ $data->status }}</td>
                                <td>{{ $data->total }}</td>
                                <td>{{ $data->proof_of_payment }}</td>
                                <td>
                                    <!-- Example single danger button -->
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-success dropdown-toggle"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Action
                                        </button>
                                        <div class="dropdown-menu">
                                            <a class="dropdown-item"
                                                href="{{ route('admin.orderdetail', $data->id) }}">Action</a>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <!--end::Table-->
        </div>
    </div>
@endsection
