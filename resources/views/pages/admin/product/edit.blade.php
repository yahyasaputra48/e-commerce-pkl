@extends('layouts.admin.app')
@section('content')
    <div class="card card-custom">
        <div class="card-header">
            <h3 class="card-title">
                Base Controls
            </h3>
            <div class="card-toolbar">
                <div class="example-tools justify-content-center">
                    <span class="example-toggle" data-toggle="tooltip" title="View code"></span>
                    <span class="example-copy" data-toggle="tooltip" title="Copy code"></span>
                </div>
            </div>
        </div>
        <!--begin::Form-->
        <form action="{{ route('admin.product.update', $product->id) }}" method="post">
            @csrf
            @method('put')
            <div class="card-body">
                <div class="form-group mb-8">
                    <div class="mb-3">
                        <label for="image" class="form-label">Post Image</label>
                        <input class="form-control @error('image') is-invalid @enderror" type="file" id="image"
                            name="image" value="{{asset('image/'.$product   ->image) }}">
                        @error('image')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label>Category <span class="text-danger">*</span></label>
                        <select name="menu_category_id" class="form-control">
                            <option value="{{ $product->menu_category_id }}" disabled selected>Choose Category</option>
                            @foreach ($menu_categories as $category)
                                <option value="{{ $category->id }}" {{ $product->menu_category_id == $category->id ? 'selected' : '' }}>Makanan </option>
                            @endforeach
                        </select>
                        @error('category_id')
                            <span class="form-text text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label>Name <span class="text-danger">*</span></label>
                        <input name="name" type="text" class="form-control" placeholder="Enter name" value="{{ $product->name }}"/>
                        @error('name')
                            <span class="form-text text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label>Price <span class="text-danger">*</span></label>
                        <input name="price" type="number" class="form-control" placeholder="Enter price"  value="{{ $product->price }}"/>
                        @error('price')
                            <span class="form-text text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label>Stock <span class="text-danger">*</span></label>
                        <input name="stock" type="number" class="form-control" placeholder="Enter stok"  value="{{ $product->stock }}"/>
                        @error('stock')
                            <span class="form-text text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label>Description <span class="text-danger">*</span></label>
                        <input name="description" type="text" class="form-control" placeholder="Enter description"  value="{{ $product->description }}"/>
                        @error('description')
                            <span class="form-text text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label>Unit <span class="text-danger">*</span></label>
                        <input name="unit" type="text" class="form-control" placeholder="Enter unit"  value="{{ $product->unit }}"/>
                        @error('description')
                            <span class="form-text text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary mr-2">Submit</button>
                    <a class="btn btn-secondary" href="{{ Route('admin.product.index') }}">Cancel</a>
                </div>
        </form>
        <!--end::Form-->
    </div>
@endsection
