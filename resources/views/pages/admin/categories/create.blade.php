@extends('layouts.admin.app')
@section('content')
    <div class="card card-custom">
        <div class="card-header">
            <h3 class="card-title">
                Kategori
            </h3>
            <div class="card-toolbar">
                <div class="example-tools justify-content-center">
                    <span class="example-toggle" data-toggle="tooltip" title="View code"></span>
                    <span class="example-copy" data-toggle="tooltip" title="Copy code"></span>
                </div>
            </div>
        </div>
        <!--begin::Form-->
        <form action="{{ route('admin.categories.store') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="card-body">
                <div class="form-group mb-8">
                    <div class=>
                    </div>
                    <div class="mb-3">
                        <label for="image" class="form-label">Post Image</label>
                        <input class="form-control @error('image') is-invalid @enderror" type="file" id="image"
                            name="image" required>
                        @error('image')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label>Nama<span class="text-danger">*</span></label>
                        <input type="text" class="form-control" name="name" placeholder="isi kategori" required />
                    </div>
                    <div class="form-group">
                    </div>
                    <div class="form-group">
                        <div class="form-group">
                        </div>
                        <div class="card-footer">
                            <button type="sumbit" class="btn btn-primary mr-2">Submit</button>
                            <a class="btn btn-secondary" href="{{ Route('admin.categories.index') }}">Cancel</a>
                        </div>
        </form>
        <!--end::Form-->
    </div>
@endsection
