@extends('layouts.admin.app')
@section('content')
    {{-- <div>
    <table class="table table-bordered">
        <tr>
            <th>No</th>
            <th>Nis</th>
            <th>Nama</th>
            <th>Rombel</th>
            <th>Rayon</th>
            <th width="280px">Action</th>
        </tr>
</div> --}}
    <div class="card-header border-0 py-5">
        <h3 class="card-title align-items-start flex-column">
            <span class="card-label font-weight-bolder text-dark">Kategori</span>
        </h3>
        <div class="card-toolbar">
            <a href="{{ route('admin.categories.create') }}"
                class="btn btn-success font-weight-bolder font-size-sm">Create</a>
        </div>
    </div>
    <div class="card-header border-0 py-5">
        <div class="tab-content">
            <!--begin::Table-->
            <div class=>
                <table class="table table-head-custom table-head-bg table-borderless table-vertical-center">
                    <thead>
                        <tr class="text-left text-uppercase">
                            <th style="min-width: 80px">Image </th>
                            <th style="min-width: 80px">Nama</th>
                            <th style="min-width: 80px">Action</th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach ($categories as $data)
                            <tr>
                                <td><img src="{{ asset('category/'.$data->image) }}" class="card-img" style="width: 110px" alt="" /></td>
                                <td>{{ $data->name }}</td>
                                <td>
                                    <div class="row">
                                        <div class="col-">
                                            <a class="btn btn-primary"
                                                href="{{ route('admin.categories.edit', $data) }}">Edit</a>
                                        </div>
                                        <div class="col-6">
                                            <form action="{{ route('admin.categories.destroy', $data) }}"
                                                method="POST" class="d-inline">
                                                @method('delete')
                                                @csrf
                                                 <button type="submit" class="btn btn-danger">Delete</button>
                                            </form>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <!--end::Table-->
        </div>
    </div>
    {{-- <div class="card card-custom">
    <div class="card-header">
     <h3 class="card-title">
      Textual HTML5 Inputs
     </h3>
    </div>
    <!--begin::Form-->
    <form>
     <div class="card-body">
      <div class="form-group mb-8">
      <div class="form-group row">
       <label  class="col-2 col-form-label">Name</label>
       <div class="col-10">
        <input class="form-control" type="text" id="example-text-input"/>
       </div>
      </div>
      <div class="form-group row">
       <label for="example-search-input" class="col-2 col-form-label">Email</label>
       <div class="col-10">
        <input class="form-control" type="search" id="example-search-input"/>
       </div>
      </div>
      <div class="form-group row">
        <label for="example-password-input" class="col-2 col-form-label">Password</label>
        <div class="col-10">
         <input class="form-control" type="password" id="example-password-input"/>
        </div>
       </div>
      <div class="form-group row">
       <label for="example-tel-input" class="col-2 col-form-label">Phone</label>
       <div class="col-10">
        <input class="form-control" type="tel" id="example-tel-input"/>
       </div>
      </div>
      <div class="form-group row">
       <label for="example-password-input" class="col-2 col-form-label">Address</label>
       <div class="col-10">
        <input class="form-control" type="password"id="example-password-input"/>
       </div>
      </div>
      <div class="form-group row">
       <label for="example-date-input" class="col-2 col-form-label">Birth Date</label>
       <div class="col-10">
        <input class="form-control" type="date" id="example-date-input"/>
       </div>
      </div>
      <div class="form-group row">
       <label for="example-month-input" class="col-2 col-form-label">Birth Place</label>
       <div class="col-10">
        <input class="form-control" type="text" id="example-month-input"/>
       </div>
      </div>
     </div>
     <div class="card-footer">
      <div class="row">
       <div class="col-2">
       </div>
       <div class="col-10">
        <button type="reset" class="btn btn-success mr-2">Submit</button>
        <button type="reset" class="btn btn-secondary">Cancel</button>
       </div>
      </div>
     </div>
    </form>
   </div>
</div> --}}
@stop
