@extends('layouts.admin.app')
@section('content')
    <div class="card-header border-0 py-5">
        <h3 class="card-title align-items-start flex-column">
            <span class="card-label font-weight-bolder text-dark">Order By User</span>
        </h3>
        <div class="col-md-12">
            <div class="form-group row">
                <div class="col-sm-2-ml-1">
                    <label for="from" class="col-form-label">Date From:</label>
                </div>
                <div class="col-sm-3">
                    <input type="date" class="form-control input-sm w-100" id="from" name="from"  required>
                </div>
                <div class="col-sm-2-ml-1">
                    <label for="to" class="col-form-label ">Date to:</label>
                </div>
                <div class="col-sm-3">
                    <input type="date" class="form-control input-sm w-100" id="to" name="to" required>
                </div>
                <div class="col-sm-2">
                    <button type="submit" class="btn btn-primary"><i class="flaticon-search-magnifier-interface-symbol"></i></button>
                    <a href="{{ route('admin.exportuser') }}" class="btn btn-primary"><i class="flaticon-technology ml-1"></i></a>
                </div>
            </div>
        </div>
    </div>
    <div class="card-header border-0 py-5">
        <div class="tab-content">
            <!--begin::Table-->
            <div class="table-responsive">
                <table class="table table-head-custom table-head-bg table-borderless table-vertical-center">
                    <thead>
                        <tr class="text-left text-uppercase">
                            <th style="min-width: 20px">No</th>
                            <th style="min-width: 80px">Tgl</th>
                            <th style="min-width: 100px">Nama Customer</th>
                            <th style="min-width: 100px">Nama Produk</th>
                            <th style="min-width: 100px">Categori</th>
                            <th style="min-width: 50px">Pesanan</th>
                            <th style="min-width: 50px">Jumlah Terjual</th>
                            <th style="min-width: 100px">Total</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($orders as $data)
                            <tr>
                                <td>{{$data->id}}</td>
                                <td>{{$data->order_date}}</td>
                                <td>{{$data->user_id}}</td>
                                <td>{{$data->status}}</td>
                                <td>{{$data->proof_of_payment}}</td>
                                <td>{{$data->order_number}}</td>
                                <td>{{$data->order_number}}</td>
                                <td>{{$data->total}}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="d-flex justify-content-center align-items-center flex-wrap">
                <div class="d-flex flex-wrap mr-3">
                    <a href="#" class="btn btn-icon btn-sm btn-light-primary mr-2 my-1">
                        <i class="ki ki-bold-arrow-back icon-xs"></i>
                    </a>
                    <a href="#" class="btn btn-icon btn-sm border-0 btn-hover-primary active mr-2 my-1">1</a>
                    <a href="#" class="btn btn-icon btn-sm border-0 btn-hover-primary mr-2 my-1">2</a>
                    <a href="#" class="btn btn-icon btn-sm border-0 btn-hover-primary mr-2 my-1">3</a>
                    <a href="#" class="btn btn-icon btn-sm btn-light-primary mr-2 my-1">
                        <i class="ki ki-bold-arrow-next icon-xs"></i>
                    </a>
                </div>
            </div>
            <!--end::Table-->
        </div>
    </div>
@stop
