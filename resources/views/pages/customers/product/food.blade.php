<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link href="{{ asset('assets/plugins/global/plugins.bundle.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ asset('assets/plugins/custom/prismjs/prismjs.bundle.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ asset('assets/css/style.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href='https://unpkg.com/boxicons@2.1.2/css/boxicons.min.css' rel='stylesheet'>
    <link rel="stylesheet" href="{{ asset('assets/plugins/bootstrap/dist/css/bootstrap.min.css')}}" />
    <link rel="stylesheet" href="{{ asset('assets/style/main.css') }}" />
    <link rel="shortcut icon" href="{{ asset('assets/media/logos/faviconhealthy.png') }}" />
    <title>MAKANAN | HEALTY SHOP</title>
</head>
<body>
    @include('layouts.user.navbar')
      <!-- PRODUK FOOD -->
      <div class="container menu-wrapper">
        <section class="kategori-food">
            <div class="container">
              <div class="row align-items-center">
                <div class="col-6">
                  <h4>Produk Makanan</h4>
                </div>
              </div>
              <div class="row row-card mt-3 align-items-stretch">
                <div class="col-lg-3 col-6">
                  <div class="card p-3 p-sm-3">
                    <img src="assets/media/products/24.png" class="img-fluid m-auto" alt="" />
                    <div class="card-body mt-3 p-1">
                      <h5 class="title">Martabak Keju kurma</h5>
                      {{-- <div class="rating">
                        <i class="bx bxs-star text-warning"></i>
                        <i class="bx bxs-star text-warning"></i>
                        <i class="bx bxs-star text-warning"></i>
                        <i class="bx bxs-star-half text-warning"></i>
                        <i class="bx bxs-star text-warning"></i>
                      </div> --}}
                        <h6 class="m-0 price">Rp 200.000</h6>
                        <div class="cart d-flex align-items-center mt-3 justify-content-between">
                                <a href="#" class="btn btn-primary mt-2">Detail</a>
                                <a href="detail.html" class="btn btn-danger mt-2"><i class="bx bx-cart-alt"></i></a>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-lg-3 col-6">
                    <div class="card p-3 p-sm-3">
                      <img src="assets/media/products/23.jpeg" class="img-fluid m-auto" alt="" />
                      <div class="card-body mt-3 p-1">
                        <h5 class="title">Martabak Telor</h5>
                        <h6 class="m-0 price">Rp 240.000</h6>
                        <div class="cart d-flex align-items-center mt-3 justify-content-between">
                            <a href="#" class="btn btn-primary mt-2">Detail</a>
                            <a href="detail.html" class="btn btn-danger mt-2"><i class="bx bx-cart-alt"></i></a>
                        </div>
                      </div>
                    </div>
                  </div>
                <div class="col-lg-3 col-6">
                    <div class="card p-3 p-sm-3">
                      <img src="assets/media/products/25.jpeg" class="img-fluid m-auto" alt="" />
                      <div class="card-body mt-3 p-1">
                        <h5 class="title">Nasi Goreng Palembang</h5>
                        <h6 class="m-0 price">Rp 240.000</h6>
                        <div class="cart d-flex align-items-center mt-3 justify-content-between">
                            <a href="#" class="btn btn-primary mt-2">Detail</a>
                            <a href="detail.html" class="btn btn-danger mt-2"><i class="bx bx-cart-alt"></i></a>
                        </div>
                      </div>
                    </div>
                  </div>
                <div class="col-lg-3 col-6">
                    <div class="card p-3 p-sm-3">
                      <img src="assets/media/products/26.jpeg" class="img-fluid m-auto" alt="" />
                      <div class="card-body mt-3 p-1">
                        <h5 class="title">Pempek</h5>
                        <h6 class="m-0 price">Rp 240.000</h6>
                        <div class="cart d-flex align-items-center mt-3 justify-content-between">
                            <a href="#" class="btn btn-primary mt-2">Detail</a>
                            <a href="detail.html" class="btn btn-danger mt-2"><i class="bx bx-cart-alt"></i></a>
                        </div>
                      </div>
                    </div>
                  </div>
              </div>
            </div>
        </section>
      </div>
      <script>
        var HOST_URL = "https://preview.keenthemes.com/metronic/theme/html/tools/preview";
    </script>
    <!--begin::Global Config(global config for global JS scripts)-->
    <script>
        var KTAppSettings = {
            "breakpoints": {
                "sm": 576,
                "md": 768,
                "lg": 992,
                "xl": 1200,
                "xxl": 1400
            },
            "colors": {
                "theme": {
                    "base": {
                        "white": "#ffffff",
                        "primary": "#3699FF",
                        "secondary": "#E5EAEE",
                        "success": "#1BC5BD",
                        "info": "#8950FC",
                        "warning": "#FFA800",
                        "danger": "#F64E60",
                        "light": "#E4E6EF",
                        "dark": "#181C32"
                    },
                    "light": {
                        "white": "#ffffff",
                        "primary": "#E1F0FF",
                        "secondary": "#EBEDF3",
                        "success": "#C9F7F5",
                        "info": "#EEE5FF",
                        "warning": "#FFF4DE",
                        "danger": "#FFE2E5",
                        "light": "#F3F6F9",
                        "dark": "#D6D6E0"
                    },
                    "inverse": {
                        "white": "#ffffff",
                        "primary": "#ffffff",
                        "secondary": "#3F4254",
                        "success": "#ffffff",
                        "info": "#ffffff",
                        "warning": "#ffffff",
                        "danger": "#ffffff",
                        "light": "#464E5F",
                        "dark": "#ffffff"
                    }
                },
                "gray": {
                    "gray-100": "#F3F6F9",
                    "gray-200": "#EBEDF3",
                    "gray-300": "#E4E6EF",
                    "gray-400": "#D1D3E0",
                    "gray-500": "#B5B5C3",
                    "gray-600": "#7E8299",
                    "gray-700": "#5E6278",
                    "gray-800": "#3F4254",
                    "gray-900": "#181C32"
                }
            },
            "font-family": "Poppins"
        };
    </script>
    <!--end::Global Config-->
    <!--begin::Global Theme Bundle(used by all pages)-->
    <script src="{{ asset('assets/plugins/global/plugins.bundle.js') }}"></script>
    <script src="{{ asset('assets/plugins/custom/prismjs/prismjs.bundle.js') }}"></script>
    <script src="{{ asset('assets/js/scripts.bundle.js') }}"></script>
    <!--end::Global Theme Bundle-->
    <!--begin::Page Scripts(used by this page)-->
    <script src="{{ asset('assets/js/pages/widgets.js') }}"></script>
    <script src="{{ asset('assets/js/pages/custom/profile/profile.js') }}"></script>
      <script src="{{ asset('assets/plugins/bootstrap/dist/js/bootstrap.bundle.min.js') }}"></script>
</body>
</html>
