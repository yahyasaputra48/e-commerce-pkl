<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="ECommerce Shopping Cart" />
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
		<link rel="canonical" href="https://keenthemes.com/metronic" />
		<!--begin::Fonts-->
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
		<!--end::Fonts-->
		<!--begin::Global Theme Styles(used by all pages)-->
		<link href="{{ asset('assets/plugins/global/plugins.bundle.css') }}" rel="stylesheet" type="text/css" />
		<link href="{{ asset('assets/plugins/custom/prismjs/prismjs.bundle.css') }}" rel="stylesheet" type="text/css" />
		<link href="{{ asset('assets/css/style.bundle.css') }}" rel="stylesheet" type="text/css" />
		<!--end::Global Theme Styles-->
		<!--begin::Layout Themes(used by all pages)-->
		<link href="{{ asset('assets/css/themes/layout/header/base/light.css') }}" rel="stylesheet" type="text/css" />
		<link href="{{ asset('assets/css/themes/layout/header/menu/light.css') }}" rel="stylesheet" type="text/css" />
		<link href="{{ asset('assets/css/themes/layout/brand/dark.css') }}" rel="stylesheet" type="text/css" />
		<link href="{{ asset('assets/css/themes/layout/aside/dark.css') }}" rel="stylesheet" type="text/css" />
    <link href='https://unpkg.com/boxicons@2.1.2/css/boxicons.min.css' rel='stylesheet'>
    <link rel="stylesheet" href="{{ asset('assets/plugins/bootstrap/dist/css/bootstrap.min.css')}}" />
    <link rel="stylesheet" href="{{ asset('assets/style/main.css') }}" />
    <link rel="shortcut icon" href="{{ asset('assets/media/logos/faviconhealthy.png') }}" />
    <title>KERANJANG | HEALTY SHOP</title>
</head>
<body>
    @include('layouts.user.navbar')
    <div class="container menu-wrapper">
        <!-- Shopping cart table -->
        <div class="card">
            <div class="card-header">
                <h2>Keranjang</h2>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                  <table class="table table-bordered m-0">
                    <thead>
                      <tr>
                        <!-- Set columns width -->
                        <th class="text-center py-3 px-4" style="min-width: 400px;">Product Name &amp; Details</th>
                        <th class="text-right py-3 px-4" style="width: 100px;">Price</th>
                        <th class="text-center py-3 px-4" style="width: 120px;">Quantity</th>
                        <th class="text-right py-3 px-4" style="width: 100px;">Total</th>
                        <th class="text-center align-middle py-3 px-0" style="width: 40px;"><a href="#" class="shop-tooltip float-none text-light" title="" data-original-title="Clear cart"><i class="ino ion-md-trash"></i></a></th>
                      </tr>
                    </thead>
                    <tbody>

                      <tr>
                        <td class="p-4">
                          <div class="media align-items-center">
                            <img src="https://bootdey.com/img/Content/avatar/avatar1.png" class="d-block ui-w-40 ui-bordered mr-4" alt="">
                            <div class="media-body">
                              <a href="#" class="d-block text-dark">Product 1</a>
                              <small>
                                <span class="text-muted">Deskripsi:</span> mkmkmkm &nbsp;
                              </small>
                            </div>
                          </div>
                        </td>
                        <td class="text-right font-weight-semibold align-middle p-4">$57.55</td>
                        <td class="text-center align-middle">
                            <input type="button" onclick="decrementValue()" value="-" />
                            <input class="text-center" type="text" name="quantity" value="1" maxlength="2" max="10" size="1" id="number" />
                            <input type="button" onclick="incrementValue()" value="+" />
                        </td>
                        <td class="text-right font-weight-semibold align-middle p-4">$115.1</td>
                        <td class="text-center align-middle px-0"><a href="#" class="shop-tooltip close float-none text-danger" title="" data-original-title="Remove">×</a></td>
                      </tr>
                    </tbody>
                  </table>
                </div>
                <!-- / Shopping cart table -->

                <div class="d-flex flex-wrap justify-content-between align-items-center pb-4">
                  <div class="mt-4">
                    <label class="text-muted font-weight-normal">Promocode</label>
                    <input type="text" placeholder="ABC" class="form-control">
                  </div>
                  <div class="d-flex">
                    <div class="text-right mt-4 mr-5">
                      <label class="text-muted font-weight-normal m-0">Discount</label>
                      <div class="text-large"><strong>$20</strong></div>
                    </div>
                    <div class="text-right mt-4">
                      <label class="text-muted font-weight-normal m-0">Total price</label>
                      <div class="text-large"><strong>$1164.65</strong></div>
                    </div>
                  </div>
                </div>

                <div class="float-right">
                  <button type="button" class="btn btn-lg btn-default md-btn-flat mt-2 mr-3">Back to shopping</button>
                  <button type="button" class="btn btn-lg btn-primary mt-2">Checkout</button>
                </div>

              </div>
          </div>
      </div>
      {{-- <div class="container menu-wrapper">
        <div class="flex-row-fluid">
            <!--begin::Section-->
            <div class="card card-custom gutter-b">
                <!--begin::Header-->
                <div class="card-header flex-wrap border-0 pt-6 pb-0">
                    <h3 class="card-title align-items-start flex-column">
                        <span class="card-label font-weight-bolder font-size-h3 text-dark">Keranjang Saya</span>
                    </h3>
                    <div class="card-toolbar">
                        <div class="dropdown dropdown-inline">
                            <a href="{{ route('user.index') }}" class="btn btn-danger p-2 ">
                                <i class="flaticon2-delete ml-1"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <!--end::Header-->
                <div class="card-body">
                    <!--begin::Shopping Cart-->
                        <table class="table">
                            <!--begin::Cart Header-->
                            <thead>
                                <tr>
                                    <th>Produk</th>
                                    <th class="text-center">Harga Satuan</th>
                                    <th class="text-center">Kuantitas</th>
                                    <th class="text-center">Total Harga</th>
                                    <th class="text-center">Aksi</th>
                                </tr>
                            </thead>
                            <!--end::Cart Header-->
                            @foreach ($products as $data)
                            <tbody>
                                <!--begin::Cart Content-->
                                <tr>
                                    <td class="d-flex align-items-center font-weight-bolder">
                                        <!--begin::Symbol-->
                                        <input type="checkbox" name="#" class="mt-2 me-2" id="#" value="#" onclick="checkCheckbox()" checked>
                                        <div class="symbol symbol-60 bg-light flex-shrink-0 mr-4 bg-light">
                                            <div class="symbol-label" style="background-image: url('assets/media/products/11.png')"></div>
                                        </div>
                                        <!--end::Symbol-->
                                        <a href="#" class="text-dark text-hover-primary">{{$data->name}}</a>
                                    </td>
                                    <td class="text-center align-middle font-weight-bolder font-size-h5">Rp. {{$data->price}}</td>
                                    <td class="text-center align-middle">
                                        <a href="javascript:;" class="btn btn-xs btn-light-success btn-icon mr-2">
                                            <i class="ki ki-minus icon-xs"></i>
                                        </a>
                                        <span class="mr-2 font-weight-bolder">1</span>
                                        <a href="javascript:;" class="btn btn-xs btn-light-success btn-icon">
                                            <i class="ki ki-plus icon-xs"></i>
                                        </a>
                                    </td>
                                    <td class="text-center align-middle font-weight-bolder font-size-h5">Rp. {{$data->total}}</td>
                                    <td class="text-center align-middle">
                                        <form action="{{ route('user.cart', $data->id) }}" method="POST"
                                            class="d-inline">
                                            @method('get')
                                            @csrf
                                            <button type="sumbit" class="btn btn-danger p-2" onclick="">
                                                <i class="flaticon2-trash ml-1"></i>
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="d-flex align-items-center font-weight-bolder">
                                        <!--begin::Symbol-->
                                        <input type="checkbox" name="#" class="mt-2 me-2" id="#" value="#" onclick="checkCheckbox()" checked>
                                        <div class="symbol symbol-60 bg-light flex-shrink-0 mr-4 bg-light">
                                            <div class="symbol-label" style="background-image: url('assets/media/products/1.png')"></div>
                                        </div>
                                        <!--end::Symbol-->
                                        <a href="#" class="text-dark text-hover-primary m">Jam Tangan</a>
                                    </td>
                                    <td class="text-center align-middle font-weight-bolder font-size-h5">Rp. 200.000</td>
                                    <td class="text-center align-middle">
                                        {{-- <a class="input-group" style="width:100px">
                                            <button class="input-group-text decrement-btn update">-</button>
                                            <input type="text" class="form-control text-center input-qty bg-white" value="">
                                            <button class="input-group-text increment-btn">+</button>
                                        </a> --}}
                                        <a href="javascript:;" class="btn btn-xs btn-light-success btn-icon mr-2">
                                            <i class="ki ki-minus icon-xs"></i>
                                        </a>
                                        <span class="mr-2 font-weight-bolder">1</span>
                                        <a href="javascript:;" class="btn btn-xs btn-light-success btn-icon">
                                            <i class="ki ki-plus icon-xs"></i>
                                        </a>
                                    </td>
                                    <td class="text-center align-middle font-weight-bolder font-size-h5">Rp. 200.000</td>
                                    <td class="text-center align-middle">
                                        <button type="button" class="btn btn-danger p-2" onclick="">
                                            <i class="flaticon2-trash ml-1"></i>
                                        </button>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="border-0 pt-10">
                                        <form>
                                            <div class="form-group row">
                                                <div class="col-lg-6 col-md-12 col-sm-6">
                                                    <h5 class="text-cs">Total Harga :</h5>
                                                </div>
                                                <div class="col-lg-6">
                                                    <span id="total-all-price" class="h4 fw-700 text-gr">Rp. 400.000</span>
                                                </div>
                                                </div>
                                            </div>
                                        </form>
                                    </td>
                                    <td colspan="5" class="border-0 text-right pt-10">
                                        <button type="submit" id="btn-checkout" class="btn btn-success font-weight-bolder font-size-sm">
                                            Checkout
                                        </button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <!--end::Shopping Cart-->
                </div>
            </div>
            <!--end::Section-->
            <script>var HOST_URL = "https://preview.keenthemes.com/metronic/theme/html/tools/preview";</script>
		<!--begin::Global Config(global config for global JS scripts)-->
		<script>var KTAppSettings = { "breakpoints": { "sm": 576, "md": 768, "lg": 992, "xl": 1200, "xxl": 1400 }, "colors": { "theme": { "base": { "white": "#ffffff", "primary": "#3699FF", "secondary": "#E5EAEE", "success": "#1BC5BD", "info": "#8950FC", "warning": "#FFA800", "danger": "#F64E60", "light": "#E4E6EF", "dark": "#181C32" }, "light": { "white": "#ffffff", "primary": "#E1F0FF", "secondary": "#EBEDF3", "success": "#C9F7F5", "info": "#EEE5FF", "warning": "#FFF4DE", "danger": "#FFE2E5", "light": "#F3F6F9", "dark": "#D6D6E0" }, "inverse": { "white": "#ffffff", "primary": "#ffffff", "secondary": "#3F4254", "success": "#ffffff", "info": "#ffffff", "warning": "#ffffff", "danger": "#ffffff", "light": "#464E5F", "dark": "#ffffff" } }, "gray": { "gray-100": "#F3F6F9", "gray-200": "#EBEDF3", "gray-300": "#E4E6EF", "gray-400": "#D1D3E0", "gray-500": "#B5B5C3", "gray-600": "#7E8299", "gray-700": "#5E6278", "gray-800": "#3F4254", "gray-900": "#181C32" } }, "font-family": "Poppins" };</script>
		<!--end::Global Config-->
		<!--begin::Global Theme Bundle(used by all pages)-->
		<script src="{{ asset('assets/plugins/global/plugins.bundle.js') }}"></script>
		<script src="{{ asset('assets/plugins/custom/prismjs/prismjs.bundle.js') }}"></script>
		<script src="{{ asset('assets/js/scripts.bundle.js') }}"></script>
      <script src="{{ asset('assets/plugins/bootstrap/dist/js/bootstrap.bundle.min.js') }}"></script>
      <script type="text/javascript">
        function incrementValue()
        {
            var value = parseInt(document.getElementById('number').value, 10);
            value = isNaN(value) ? 0 : value;
            if(value<10){
                value++;
                    document.getElementById('number').value = value;
            }
        }
        function decrementValue()
        {
            var value = parseInt(document.getElementById('number').value, 10);
            value = isNaN(value) ? 0 : value;
            if(value>1){
                value--;
                    document.getElementById('number').value = value;
            }

        }
        </script>
    </body>
    </html>
