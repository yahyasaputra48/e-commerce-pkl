<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
    <link href="{{ asset('assets/plugins/global/plugins.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/plugins/custom/prismjs/prismjs.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/style.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href='https://unpkg.com/boxicons@2.1.2/css/boxicons.min.css' rel='stylesheet'>
    <link rel="stylesheet" href="{{ asset('assets/plugins/bootstrap/dist/css/bootstrap.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/style/main.css') }}" />
    <link rel="shortcut icon" href="{{ asset('assets/media/logos/faviconhealthy.png') }}" />
    <title>HOME | HEALTY SHOP</title>
</head>

<body>
    @include('layouts.user.navbar')
      <!-- BANNER -->
      <div class="container menu-wrapper">
        <div id="carouselExampleIndicators" class="carousel slide" data-bs-ride="true">
            <div class="carousel-indicators">
                <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0" class="active"
                    aria-current="true" aria-label="Slide 1"></button>
                <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="1"
                    aria-label="Slide 2"></button>
                <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="2"
                    aria-label="Slide 3"></button>
                <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="3"
                    aria-label="Slide 4"></button>
            </div>
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img src="assets/media/stock-900x600/fashion.png" class="d-block w-100" alt="...">
                </div>
                <div class="carousel-item">
                    <img src="assets/media/stock-900x600/food.png" class="d-block w-100" alt="...">
                </div>
                <div class="carousel-item">
                    <img src="assets/media/stock-900x600/drink.png" class="d-block w-100" alt="...">
                </div>
                <div class="carousel-item">
                    <img src="assets/media/stock-900x600/new.png" class="d-block w-100" alt="...">
                </div>
            </div>
            <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators"
                data-bs-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Previous</span>
            </button>
            <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleIndicators"
                data-bs-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Next</span>
            </button>
        </div>

        {{-- PILIH KATEGORI --}}
        <section class="pilih-kategori">
            <div class="row mt-4">
                <h4>Kategori</h4>
                <div class="col text-center">
                    <div class="card">
                        <div class="card-body">
                            <img src="assets/media/products/minuman.png" class="img-fluid">
                        </div>
                    </div>
                    <h6 class="mt-3">MINUMAN</h6>
                </div>
                <div class="col text-center">
                    <div class="card">
                        <div class="card-body">
                            <img src="assets/media/products/otomotif.png" class="img-fluid">
                        </div>
                    </div>
                    <h6 class="mt-3">OTOMOTIF</h6>
                </div>
                <div class="col text-center">
                    <div class="card">
                        <div class="card-body">
                            <img src="assets/media/products/jam-tangan.png" class="img-fluid">
                        </div>
                    </div>
                    <h6 class="mt-3">JAM TANGAN</h6>
                </div>
                <div class="col text-center">
                    <div class="card">
                        <div class="card-body">
                            <img src="assets/media/products/handphone&aksesoris.png" class="img-fluid">
                        </div>
                    </div>
                    <h6 class="mt-3">HANDPHONE & AKSESORIS</h6>
                </div>
                <div class="col text-center">
                    <div class="card">
                        <div class="card-body">
                            <img src="assets/media/products/sepatu.png" class="img-fluid">
                        </div>
                    </div>
                    <h6 class="mt-3">SEPATU</h6>
                </div>
                <div class="col text-center">
                    <div class="card">
                        <div class="card-body">
                            <img src="assets/media/products/pakaian.png" class="img-fluid">
                        </div>
                    </div>
                    <h6 class="mt-3">PAKAIAN</h6>
                </div>
            </div>
        </section>

        <!-- PRODUK FOOD -->
        <section class="popular">
            <div class="container">
              <div class="row align-items-center mt-4">
                <div class="col-6">
                  <h4>All Produk</h4>
                </div>
              </div>
              <div class="row row-card mt-3 align-items-stretch">
                @foreach ( $products as $data )
                <div class="col-lg-3 col-6">
                    <div class="card p-3 p-sm-3 mt-4">
                      <img src="{{ asset('image/'.$data->image) }}"class="img-fluid m-auto" alt="" />
                      <div   12 class="card-body mt-3 p-1">
                        <h5 class="title">{{$data->name}}</h5>
                          <h6 class="m-0 price">Rp. {{$data->price}}</h6>
                          <div class="cart d-flex align-items-center mt-3 justify-content-between">
                              <a href="{{ route('details', $data->id) }}" class="btn btn-primary mt-2">Detail</a>
                              <a href="detail.html" class="btn btn-danger mt-2"><i class="bx bx-cart-alt"></i></a>
                        </div>
                      </div>
                    </div>
                  </div>
                @endforeach
                {{-- <div class="col-lg-3 col-6">
                    <div class="card p-3 p-sm-3">
                      <img src="assets/media/products/23.jpeg" class="img-fluid m-auto" alt="" />
                      <div class="card-body mt-3 p-1">
                        <h5 class="title">Martabak Telor</h5>
                        <h6 class="m-0 price">Rp 240.000</h6>
                        <div class="cart d-flex align-items-center mt-3 justify-content-between">
                            <a href="#" class="btn btn-primary mt-2" type="button">Detail</a>
                            <a href="detail.html" class="btn btn-danger mt-2" type="button"><i class="bx bx-cart-alt"></i></a>
                        </div>
                      </div>
                    </div>
                </div>

                <div class="row row-card mt-3 align-items-stretch">
                        @foreach ($products as $data )
                    <div class="col-lg-3 col-6">
                        <div class="card p-3 p-sm-3">
                            <img src="{{ asset('image/'.$data->image) }}" class="card-img" height="300px" alt="" />
                            <div class="card-body mt-3 p-1">
                                <h5 class="title">{{$data->name}}</h5>
                                <h6 class="m-0 price">Rp {{$data->price}}</h6>
                                <div class="cart d-flex align-items-center mt-3 justify-content-between">
                                    <a href="#" class="btn btn-primary mt-2" data-bs-target="#product{{$data->id}}"
                                        data-bs-toggle="modal">Detail</a>
                                    <a href="detail.html" class="btn btn-danger mt-2"><i
                                            class="bx bx-cart-alt"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal fade" id="product{{$data->id}}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Detail Produk</h5>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                </div>
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-md-5">
                                            <img src="{{ asset('image/'.$data->image) }}" class="img-fluid" alt="">
                                        </div>
                                        <div class="col-md-7">
                                            <h2>
                                                <strong>{{$data->name}}</strong>
                                            </h2>
                                            <h4>
                                                Rp. {{$data->price}}
                                            </h4>
                                            <div class="col">
                                                <table class="table table-borderless">
                                                    <tr>
                                                        <td>Stok</td>
                                                        <td>:</td>
                                                        <td>{{$data->stock}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Kategori</td>
                                                        <td>:</td>
                                                        <td>{{$data->menu_category_id}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Deskripsi</td>
                                                        <td>:</td>
                                                        <td>{{$data->description}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Kuantitas</td>
                                                        <td>:</td>
                                                        <td class="input-group">
                                                            <div class="input-group-prepend">
                                                                <button type="button" class="btn btn-secondary btn-min"
                                                                    onclick="updateQuantity(-1)">
                                                                    <b>-</b>
                                                                </button>
                                                            </div>
                                                            <input type="number" name="quantity" id="quantity"
                                                                class="form-control
                                            form-control-sm text-center "
                                                                min="1" value="1" onchange="checkQuantity()">
                                                            <input type="hidden" name="product_id" value="21">
                                                            <div class="input-group-append">
                                                                <button type="button" class="btn btn-primary btn-plus"
                                                                    onclick="updateQuantity(1)">
                                                                    <b>+</b>
                                                                </button>
                                                            </div>
                                                        </td>
                                                    <tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <a href="detail.html" class="btn btn-primary">Checkout</i></a>
                                        <a href="detail.html" class="btn btn-danger"><i class="bx bx-cart-alt"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                  </div> --}}
              </div>
            </div>
        </section>
    </div>
    <!-- PRODUCT DRINK -->
    <div class="container menu-wrapper">
        <section class="popular">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-6">
                        <h4>Produk Minuman</h4>
                    </div>
                    <div class="col-6 text-end">
                        <a href="#" class="btn-first">Lihat Semua...</a>
                    </div>
                </div>
                <div class="row row-card mt-3 align-items-stretch">
                    <div class="col-lg-3 col-6">
                        <div class="card p-3 p-sm-3">
                            <img src="assets/media/products/27.jpg" class="img-fluid m-auto" alt="" />
                            <div class="card-body mt-3 p-1">
                                <h5 class="title">Anggur Merah Ketan Hitam</h5>
                                <h6 class="m-0 price">Rp 30.000</h6>
                                <div class="cart d-flex align-items-center mt-3 justify-content-between">
                                    <a href="" class="btn btn-primary mt-2">Detail</a>
                                    <a href="detail.html" class="btn btn-danger mt-2"><i
                                            class="bx bx-cart-alt"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-6">
                        <div class="card p-3 p-sm-3">
                            <img src="assets/media/products/28.jpg" class="img-fluid m-auto" alt="" />
                            <div class="card-body mt-3 p-1">
                                <h5 class="title">FreshTea 12 pcs</h5>
                                <h6 class="m-0 price">Rp 50.000</h6>
                                <div class="cart d-flex align-items-center mt-3 justify-content-between">
                                    <a href="#" class="btn btn-primary mt-2">Detail</a>
                                    <a href="detail.html" class="btn btn-danger mt-2"><i
                                            class="bx bx-cart-alt"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-6">
                        <div class="card p-3 p-sm-3">
                            <img src="assets/media/products/29.jpg" class="img-fluid m-auto" alt="" />
                            <div class="card-body mt-3 p-1">
                                <h5 class="title">Sprit 250 ML</h5>
                                <h6 class="m-0 price">Rp 10.000</h6>
                                <div class="cart d-flex align-items-center mt-3 justify-content-between">
                                    <a href="#" class="btn btn-primary mt-2">Detail</a>
                                    <a href="detail.html" class="btn btn-danger mt-2"><i
                                            class="bx bx-cart-alt"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-6">
                        <div class="card p-3 p-sm-3">
                            <img src="assets/media/products/30.jpg" class="img-fluid m-auto" alt="" />
                            <div class="card-body mt-3 p-1">
                                <h5 class="title">Teh Pucuk</h5>
                                <h6 class="m-0 price">Rp 5.000</h6>
                                <div class="cart d-flex align-items-center mt-3 justify-content-between">
                                    <a href="#" class="btn btn-primary mt-2">Detail</a>
                                    <a href="detail.html" class="btn btn-danger mt-2"><i
                                            class="bx bx-cart-alt"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <!-- PRODUCT FASHION -->
    <div class="container menu-wrapper">
        <section class="popular">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-6">
                        <h4>Produk Sepatu</h4>
                    </div>
                    <div class="col-6 text-end">
                        <a href="#" class="btn-first">Lihat Semua...</a>
                    </div>
                </div>
                <div class="row row-card mt-3 align-items-stretch mb-6">
                    <div class="col-lg-3 col-6">
                        <div class="card p-3 p-sm-3">
                            <img src="assets/media/products/13.png" class="img-fluid m-auto" alt="" />
                            <div class="card-body mt-3 p-1">
                                <h5 class="title">Sepatu Nike</h5>
                                <h6 class="m-0 price">Rp 120.000</h6>
                                <div class="cart d-flex align-items-center mt-3 justify-content-between">
                                    <a href="#" class="btn btn-primary mt-2">Detail</a>
                                    <a href="detail.html" class="btn btn-danger mt-2"><i
                                            class="bx bx-cart-alt"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-6">
                        <div class="card p-3 p-sm-3">
                            <img src="assets/media/products/10.png" class="img-fluid m-auto" alt="" />
                            <div class="card-body mt-3 p-1">
                                <h5 class="title">Sepatu Nike Putih</h5>
                                <h6 class="m-0 price">Rp 100.000</h6>
                                <div class="cart d-flex align-items-center mt-3 justify-content-between">
                                    <a href="#" class="btn btn-primary mt-2">Detail</a>
                                    <a href="detail.html" class="btn btn-danger mt-2"><i
                                            class="bx bx-cart-alt"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-6">
                        <div class="card p-3 p-sm-3">
                            <img src="assets/media/products/11.png" class="img-fluid m-auto" alt="" />
                            <div class="card-body mt-3 p-1">
                                <h5 class="title">Sepatu Nike RGB</h5>
                                <h6 class="m-0 price">Rp 250.000</h6>
                                <div class="cart d-flex align-items-center mt-3 justify-content-between">
                                    <a href="#" class="btn btn-primary mt-2">Detail</a>
                                    <a href="detail.html" class="btn btn-danger mt-2"><i
                                            class="bx bx-cart-alt"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-6">
                        <div class="card p-3 p-sm-3">
                            <img src="assets/media/products/12.png" class="img-fluid m-auto" alt="" />
                            <div class="card-body mt-3 p-1">
                                <h5 class="title">NIKE G32</h5>
                                <h6 class="m-0 price">Rp 70.000</h6>
                                <div class="cart d-flex align-items-center mt-3 justify-content-between">
                                    <a href="#" class="btn btn-primary mt-2">Detail</a>
                                    <a href="detail.html" class="btn btn-danger mt-2"><i
                                            class="bx bx-cart-alt"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
  </div>
</div>
<div class="footer bg-white py-4 d-flex flex-lg-column mt-3 bg-footer" id="kt_footer">
    <div class="kt-container kt-container--fluid">
      <div class="kt-footer__copyright">
        <footer class="footer text-center">2022 © HEALTHYSHOP</footer>
      </div>
    </div>
</div>
  <script>
    var HOST_URL = "https://preview.keenthemes.com/metronic/theme/html/tools/preview";
</script>
<!--begin::Global Config(global config for global JS scripts)-->
<script>
    var KTAppSettings = {
        "breakpoints": {
            "sm": 576,
            "md": 768,
            "lg": 992,
            "xl": 1200,
            "xxl": 1400
        },
        "colors": {
            "theme": {
                "base": {
                    "white": "#ffffff",
                    "primary": "#3699FF",
                    "secondary": "#E5EAEE",
                    "success": "#1BC5BD",
                    "info": "#8950FC",
                    "warning": "#FFA800",
                    "danger": "#F64E60",
                    "light": "#E4E6EF",
                    "dark": "#181C32"
                },
                "light": {
                    "white": "#ffffff",
                    "primary": "#E1F0FF",
                    "secondary": "#EBEDF3",
                    "success": "#C9F7F5",
                    "info": "#EEE5FF",
                    "warning": "#FFF4DE",
                    "danger": "#FFE2E5",
                    "light": "#F3F6F9",
                    "dark": "#D6D6E0"
                },
                "gray": {
                    "gray-100": "#F3F6F9",
                    "gray-200": "#EBEDF3",
                    "gray-300": "#E4E6EF",
                    "gray-400": "#D1D3E0",
                    "gray-500": "#B5B5C3",
                    "gray-600": "#7E8299",
                    "gray-700": "#5E6278",
                    "gray-800": "#3F4254",
                    "gray-900": "#181C32"
                }
            },
            "font-family": "Poppins"
        };
    </script>
    <!--end::Global Config-->
    <!--begin::Global Theme Bundle(used by all pages)-->
    <script src="{{ asset('assets/plugins/global/plugins.bundle.js') }} "></script>
    <script src="{{ asset('assets/plugins/custom/prismjs/prismjs.bundle.js') }}"></script>
    <script src="{{ asset('assets/js/scripts.bundle.js') }}"></script>
    <!--end::Global Theme Bundle-->
    <!--begin::Page Scripts(used by this page)-->
    <script src="{{ asset('assets/js/pages/widgets.js') }}"></script>
    <script src="{{ asset('assets/js/pages/custom/profile/profile.js') }}"></script>
    <!--end::Page Scripts-->
    <script src="{{ asset('assets/plugins/bootstrap/dist/js/bootstrap.bundle.min.js') }}"></script>
</body>

</html>
