<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="canonical" href="https://keenthemes.com/metronic" />

    <!--begin::Fonts-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
    <!--end::Fonts-->
    <!--begin::Page Vendors Styles(used by this page)-->
    <link href="{{ asset('assets/plugins/custom/fullcalendar/fullcalendar.bundle.css') }}" rel="stylesheet"
        type="text/css" />
    <!--end::Page Vendors Styles-->
    <!--begin::Global Theme Styles(used by all pages)-->
    <link href="{{ asset('assets/plugins/global/plugins.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/plugins/custom/prismjs/prismjs.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/style.bundle.css') }}" rel="stylesheet" type="text/css" />
    <!--end::Global Theme Styles-->
    <!--begin::Layout Themes(used by all pages)-->
    <link href="{{ asset('assets/css/themes/layout/header/base/light.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/themes/layout/header/menu/light.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/themes/layout/brand/dark.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/themes/layout/aside/dark.css') }}" rel="stylesheet" type="text/css" />
    <!--end::Layout Themes-->
    <link href='https://unpkg.com/boxicons@2.1.2/css/boxicons.min.css' rel='stylesheet'>
    <link rel="stylesheet" href="{{ asset('assets/plugins/bootstrap/dist/css/bootstrap.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/style/main.css') }}" />
    <link rel="shortcut icon" href="{{ asset('assets/media/logos/faviconhealthy.png') }}" />
    <title>Detail Produk | HEALTY SHOP</title>
</head>

<body>
    @include('layouts.user.navbar')
    <div class="container menu-wrapper">
        <div class="row mt-3">
            <!-- colom 1 -->
            <div class="col-md-6">
                <div id="carouselExampleIndicators" class="carousel slide" data-bs-ride="true">
                    <div class="carousel-indicators">
                        <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0"
                            class="active" aria-current="true" aria-label="Slide 1"></button>
                        <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="1"
                            aria-label="Slide 2"></button>
                        <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="2"
                            aria-label="Slide 3"></button>
                    </div>
                    <div class="carousel-inner" style="width: 520px">
                        <div class="carousel-item active">
                            <img src={{ asset('image/'.$products->image) }} class="d-block w-100 rounded float-start"
                                alt="...">
                        </div>
                        <div class="carousel-item">
                            <img src="assets/media/products/27.jpg" class="d-block w-100 rounded float-start"
                                alt="...">
                        </div>
                        <div class="carousel-item">
                            <img src={{ asset('image/'.$products->image) }} class="d-block w-100 rounded float-start"
                                alt="...">
                        </div> --}}
                    </div>
                    <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators"
                        data-bs-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="visually-hidden">Previous</span>
                    </button>
                    <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleIndicators"
                        data-bs-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="visually-hidden">Next</span>
                    </button>
                </div>
            </div>
            <!-- colom 2 -->
            <div class="col-md-5">
                <h1 class="text-judul-halaman">{{$products->name}}</h1>
                <svg width="14" height="12" viewBox="0 0 14 12" fill="none"
                    xmlns="http://www.w3.org/2000/svg">
                    <g clip-path="url(#clip0_54_44)">
                        <path
                            d="M6.07735 0.417179L4.54689 3.5203L1.12267 4.01952C0.508605 4.10858 0.262511 4.86561 0.707824 5.2992L3.18517 7.71326L2.59923 11.1234C2.49376 11.7398 3.14298 12.2015 3.68673 11.9133L6.75001 10.3031L9.81329 11.9133C10.357 12.1992 11.0063 11.7398 10.9008 11.1234L10.3149 7.71326L12.7922 5.2992C13.2375 4.86561 12.9914 4.10858 12.3774 4.01952L8.95314 3.5203L7.42267 0.417179C7.14845 -0.135945 6.35392 -0.142977 6.07735 0.417179Z"
                            fill="#FFC400" />
                    </g>
                    <defs>
                        <clipPath id="clip0_54_44">
                            <rect width="13.5" height="12" fill="white" />
                        </clipPath>
                    </defs>
                </svg>
                <svg width="14" height="12" viewBox="0 0 14 12" fill="none"
                    xmlns="http://www.w3.org/2000/svg">
                    <g clip-path="url(#clip0_54_44)">
                        <path
                            d="M6.07735 0.417179L4.54689 3.5203L1.12267 4.01952C0.508605 4.10858 0.262511 4.86561 0.707824 5.2992L3.18517 7.71326L2.59923 11.1234C2.49376 11.7398 3.14298 12.2015 3.68673 11.9133L6.75001 10.3031L9.81329 11.9133C10.357 12.1992 11.0063 11.7398 10.9008 11.1234L10.3149 7.71326L12.7922 5.2992C13.2375 4.86561 12.9914 4.10858 12.3774 4.01952L8.95314 3.5203L7.42267 0.417179C7.14845 -0.135945 6.35392 -0.142977 6.07735 0.417179Z"
                            fill="#FFC400" />
                    </g>
                    <defs>
                        <clipPath id="clip0_54_44">
                            <rect width="13.5" height="12" fill="white" />
                        </clipPath>
                    </defs>
                </svg>
                <svg width="14" height="12" viewBox="0 0 14 12" fill="none"
                    xmlns="http://www.w3.org/2000/svg">
                    <g clip-path="url(#clip0_54_44)">
                        <path
                            d="M6.07735 0.417179L4.54689 3.5203L1.12267 4.01952C0.508605 4.10858 0.262511 4.86561 0.707824 5.2992L3.18517 7.71326L2.59923 11.1234C2.49376 11.7398 3.14298 12.2015 3.68673 11.9133L6.75001 10.3031L9.81329 11.9133C10.357 12.1992 11.0063 11.7398 10.9008 11.1234L10.3149 7.71326L12.7922 5.2992C13.2375 4.86561 12.9914 4.10858 12.3774 4.01952L8.95314 3.5203L7.42267 0.417179C7.14845 -0.135945 6.35392 -0.142977 6.07735 0.417179Z"
                            fill="#FFC400" />
                    </g>
                    <defs>
                        <clipPath id="clip0_54_44">
                            <rect width="13.5" height="12" fill="white" />
                        </clipPath>
                    </defs>
                </svg>
                <svg width="14" height="12" viewBox="0 0 14 12" fill="none"
                    xmlns="http://www.w3.org/2000/svg">
                    <g clip-path="url(#clip0_54_44)">
                        <path
                            d="M6.07735 0.417179L4.54689 3.5203L1.12267 4.01952C0.508605 4.10858 0.262511 4.86561 0.707824 5.2992L3.18517 7.71326L2.59923 11.1234C2.49376 11.7398 3.14298 12.2015 3.68673 11.9133L6.75001 10.3031L9.81329 11.9133C10.357 12.1992 11.0063 11.7398 10.9008 11.1234L10.3149 7.71326L12.7922 5.2992C13.2375 4.86561 12.9914 4.10858 12.3774 4.01952L8.95314 3.5203L7.42267 0.417179C7.14845 -0.135945 6.35392 -0.142977 6.07735 0.417179Z"
                            fill="#FFC400" />
                    </g>
                    <defs>
                        <clipPath id="clip0_54_44">
                            <rect width="13.5" height="12" fill="white" />
                        </clipPath>
                    </defs>
                </svg>
                <svg width="13" height="12" viewBox="0 0 13 12" fill="none"
                    xmlns="http://www.w3.org/2000/svg">
                    <g clip-path="url(#clip0_54_71)">
                        <path
                            d="M11.919 4.01977L8.48845 3.52031L6.95541 0.417422C6.81759 0.140156 6.54877 0 6.27994 0C6.01275 0 5.7458 0.138281 5.60752 0.417422L4.074 3.52008L0.64322 4.01906C0.0279852 4.10812 -0.218577 4.86492 0.227438 5.29852L2.70924 7.71258L2.12189 11.1223C2.03845 11.6093 2.42728 12 2.86275 12C2.9783 12 3.09713 11.9726 3.21127 11.9121L6.28041 10.3024L9.34931 11.9126C9.46322 11.9723 9.58181 11.9995 9.69689 11.9995C10.1328 11.9995 10.5224 11.6102 10.4389 11.123L9.85228 7.71305L12.3345 5.29945C12.7808 4.86586 12.5342 4.10883 11.919 4.01977ZM9.06572 6.90727L8.64103 7.32023L8.74134 7.90336L9.19884 10.5623L6.80517 9.30633L6.28064 9.03117L6.28134 1.59703L7.47736 4.01789L7.73939 4.54828L8.32556 4.63359L11.0028 5.02336L9.06572 6.90727Z"
                            fill="#FFC400" />
                    </g>
                    <defs>
                        <clipPath id="clip0_54_71">
                            <rect width="12.5625" height="12" fill="white" />
                        </clipPath>
                    </defs>
                </svg>
                4.5
                <h3>
                    Rp. {{$products->price}}
                </h3>
                <p class="text-deskripsi mt-3 mb-5">
                </p>

                <div class="row">
                    <div class="col-md-12">
                        <table class="table table-borderless h5 d-flex flex-start">
                            <tr>
                                <td>Stok</td>
                                <td>:</td>
                                <td>{{$products->stock}}</td>
                            </tr>
                            <tr>
                                <td>Kategori</td>
                                <td>:</td>
                                <td>{{$products->category}}</td>
                            </tr>
                            <tr>
                                <td>Berat</td>
                                <td>:</td>
                                <td>500 ml</td>
                            </tr>
                            <tr>
                                <td>Kuantitas</td>
                                <td>:</td>
                                <td>
                                    <div class="input-group w-auto align-items-center">
                                        <input type="button" value="-"
                                            class="button-minus border rounded-circle  icon-shape icon-sm mx-1 "
                                            data-field="quantity">
                                        <input type="number" step="1" max="10" value="1"
                                            name="quantity" class="quantity-field border-0 text-center w-25">
                                        <input type="button" value="+"
                                            class="button-plus border rounded-circle icon-shape icon-sm "
                                            data-field="quantity">
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <td>
                            <button class="btn btn-danger mr-2"><i class="bx bx-cart-alt"></i>Masukkan
                                Keranjang</button>
                            <a class="btn btn-success mr-2">Beli Sekarang</a>
                        </td>
                    </div>
                </div>
            </div>
            <div class="col-md-1">
                <a href="{{ route('user.index') }}"><i class="ki ki-close text-danger"></i></a>
            </div>

            <!-- Deskription -->
            <div class="row mt-5">
                <div class="col-12">
                    <h3 class="text-judul">Deskripsi Produk</h3>
                    <p class="text-deskripsi">
                        {{$products->description}}
                    </p>
                </div>
            </div>

            <!-- Review -->
            <div class="col-md-12">
                <div class="row mt-5">
                    <div class="row align-items-center mt-4">
                        <div class="col-6">
                          <h4>Ulasan</h4>
                        </div>
                        <div class="col-6 text-end">
                            <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
                                Beri Ulasan
                            </button>
                        </div>
                    </div>
                    <!-- Modal -->
                    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                      <div class="modal-dialog">
                        <div class="modal-content">
                          <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                          </div>
                             <div class="modal-body">
                                <div class="rating-css">
                                    <div class="star-icon">
                                        <input type="radio" value="1" name="product_rating" checked id="rating1">
                                        <label for="rating1" class="fa fa-star"></label>
                                        <input type="radio" value="2" name="product_rating" id="rating2">
                                        <label for="rating2" class="fa fa-star"></label>
                                        <input type="radio" value="3" name="product_rating" id="rating3">
                                        <label for="rating3" class="fa fa-star"></label>
                                        <input type="radio" value="4" name="product_rating" id="rating4">
                                        <label for="rating4" class="fa fa-star"></label>
                                        <input type="radio" value="5" name="product_rating" id="rating5">
                                        <label for="rating5" class="fa fa-star"></label>
                                    </div>
                                </div>
                             </div>
                             <div class="col-md-12 mt-4 mb-2 text-center">
                                <label class="form-label">Ulasan Anda</label>
                                <textarea class="form-control" id="alamat-lengkap" rows="5" name="state" placeholder="Ceritakan tentang produk ini"></textarea>
                             </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-primary">Submit</button>
                          </div>
                        </div>
                      </div>
                    </div>
                </div>
            </div>

            <div class="row pt-5 pb-5 border-bottom">
                <div class="col-md-3">
                    <div class="row">
                        <div class="col-md-5"><img src="assets/media/users/100_4.jpg" class="rounded-circle"></div>
                        <div class="col mt-4">Wayan Yanti Putri<br><small>Kemarin</small></div>
                    </div>
                </div>
                <div class="col-md-9">
                    <svg width="14" height="12" viewBox="0 0 14 12" fill="none"
                        xmlns="http://www.w3.org/2000/svg">
                        <g clip-path="url(#clip0_54_44)">
                            <path
                                d="M6.07735 0.417179L4.54689 3.5203L1.12267 4.01952C0.508605 4.10858 0.262511 4.86561 0.707824 5.2992L3.18517 7.71326L2.59923 11.1234C2.49376 11.7398 3.14298 12.2015 3.68673 11.9133L6.75001 10.3031L9.81329 11.9133C10.357 12.1992 11.0063 11.7398 10.9008 11.1234L10.3149 7.71326L12.7922 5.2992C13.2375 4.86561 12.9914 4.10858 12.3774 4.01952L8.95314 3.5203L7.42267 0.417179C7.14845 -0.135945 6.35392 -0.142977 6.07735 0.417179Z"
                                fill="#FFC400" />
                        </g>
                        <defs>
                            <clipPath id="clip0_54_44">
                                <rect width="13.5" height="12" fill="white" />
                            </clipPath>
                        </defs>
                    </svg>
                    <svg width="14" height="12" viewBox="0 0 14 12" fill="none"
                        xmlns="http://www.w3.org/2000/svg">
                        <g clip-path="url(#clip0_54_44)">
                            <path
                                d="M6.07735 0.417179L4.54689 3.5203L1.12267 4.01952C0.508605 4.10858 0.262511 4.86561 0.707824 5.2992L3.18517 7.71326L2.59923 11.1234C2.49376 11.7398 3.14298 12.2015 3.68673 11.9133L6.75001 10.3031L9.81329 11.9133C10.357 12.1992 11.0063 11.7398 10.9008 11.1234L10.3149 7.71326L12.7922 5.2992C13.2375 4.86561 12.9914 4.10858 12.3774 4.01952L8.95314 3.5203L7.42267 0.417179C7.14845 -0.135945 6.35392 -0.142977 6.07735 0.417179Z"
                                fill="#FFC400" />
                        </g>
                        <defs>
                            <clipPath id="clip0_54_44">
                                <rect width="13.5" height="12" fill="white" />
                            </clipPath>
                        </defs>
                    </svg>
                    <svg width="14" height="12" viewBox="0 0 14 12" fill="none"
                        xmlns="http://www.w3.org/2000/svg">
                        <g clip-path="url(#clip0_54_44)">
                            <path
                                d="M6.07735 0.417179L4.54689 3.5203L1.12267 4.01952C0.508605 4.10858 0.262511 4.86561 0.707824 5.2992L3.18517 7.71326L2.59923 11.1234C2.49376 11.7398 3.14298 12.2015 3.68673 11.9133L6.75001 10.3031L9.81329 11.9133C10.357 12.1992 11.0063 11.7398 10.9008 11.1234L10.3149 7.71326L12.7922 5.2992C13.2375 4.86561 12.9914 4.10858 12.3774 4.01952L8.95314 3.5203L7.42267 0.417179C7.14845 -0.135945 6.35392 -0.142977 6.07735 0.417179Z"
                                fill="#FFC400" />
                        </g>
                        <defs>
                            <clipPath id="clip0_54_44">
                                <rect width="13.5" height="12" fill="white" />
                            </clipPath>
                        </defs>
                    </svg>
                    <svg width="14" height="12" viewBox="0 0 14 12" fill="none"
                        xmlns="http://www.w3.org/2000/svg">
                        <g clip-path="url(#clip0_54_44)">
                            <path
                                d="M6.07735 0.417179L4.54689 3.5203L1.12267 4.01952C0.508605 4.10858 0.262511 4.86561 0.707824 5.2992L3.18517 7.71326L2.59923 11.1234C2.49376 11.7398 3.14298 12.2015 3.68673 11.9133L6.75001 10.3031L9.81329 11.9133C10.357 12.1992 11.0063 11.7398 10.9008 11.1234L10.3149 7.71326L12.7922 5.2992C13.2375 4.86561 12.9914 4.10858 12.3774 4.01952L8.95314 3.5203L7.42267 0.417179C7.14845 -0.135945 6.35392 -0.142977 6.07735 0.417179Z"
                                fill="#FFC400" />
                        </g>
                        <defs>
                            <clipPath id="clip0_54_44">
                                <rect width="13.5" height="12" fill="white" />
                            </clipPath>
                        </defs>
                    </svg>
                    <svg width="14" height="12" viewBox="0 0 14 12" fill="none"
                        xmlns="http://www.w3.org/2000/svg">
                        <g clip-path="url(#clip0_54_44)">
                            <path
                                d="M6.07735 0.417179L4.54689 3.5203L1.12267 4.01952C0.508605 4.10858 0.262511 4.86561 0.707824 5.2992L3.18517 7.71326L2.59923 11.1234C2.49376 11.7398 3.14298 12.2015 3.68673 11.9133L6.75001 10.3031L9.81329 11.9133C10.357 12.1992 11.0063 11.7398 10.9008 11.1234L10.3149 7.71326L12.7922 5.2992C13.2375 4.86561 12.9914 4.10858 12.3774 4.01952L8.95314 3.5203L7.42267 0.417179C7.14845 -0.135945 6.35392 -0.142977 6.07735 0.417179Z"
                                fill="#FFC400" />
                        </g>
                        <defs>
                            <clipPath id="clip0_54_44">
                                <rect width="13.5" height="12" fill="white" />
                            </clipPath>
                        </defs>
                    </svg>
                    5
                    <p class="text-deskripsi">Paket sudah diterima, packing rapi, terima kasih.</p>
                </div>
            </div>
            <tr>
                <div class="row pt-5 pb-5 border-bottom">
                    <div class="col-md-3">
                        <div class="row">
                            <div class="col-md-5"><img src="assets/media/users/100_4.jpg" class="rounded-circle"></div>
                            <div class="col mt-4">Wayan Yanti Putri<br><small>Kemarin</small></div>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <svg width="14" height="12" viewBox="0 0 14 12" fill="none"
                            xmlns="http://www.w3.org/2000/svg">
                            <g clip-path="url(#clip0_54_44)">
                                <path
                                    d="M6.07735 0.417179L4.54689 3.5203L1.12267 4.01952C0.508605 4.10858 0.262511 4.86561 0.707824 5.2992L3.18517 7.71326L2.59923 11.1234C2.49376 11.7398 3.14298 12.2015 3.68673 11.9133L6.75001 10.3031L9.81329 11.9133C10.357 12.1992 11.0063 11.7398 10.9008 11.1234L10.3149 7.71326L12.7922 5.2992C13.2375 4.86561 12.9914 4.10858 12.3774 4.01952L8.95314 3.5203L7.42267 0.417179C7.14845 -0.135945 6.35392 -0.142977 6.07735 0.417179Z"
                                    fill="#FFC400" />
                            </g>
                            <defs>
                                <clipPath id="clip0_54_44">
                                    <rect width="13.5" height="12" fill="white" />
                                </clipPath>
                            </defs>
                        </svg>
                        <svg width="14" height="12" viewBox="0 0 14 12" fill="none"
                            xmlns="http://www.w3.org/2000/svg">
                            <g clip-path="url(#clip0_54_44)">
                                <path
                                    d="M6.07735 0.417179L4.54689 3.5203L1.12267 4.01952C0.508605 4.10858 0.262511 4.86561 0.707824 5.2992L3.18517 7.71326L2.59923 11.1234C2.49376 11.7398 3.14298 12.2015 3.68673 11.9133L6.75001 10.3031L9.81329 11.9133C10.357 12.1992 11.0063 11.7398 10.9008 11.1234L10.3149 7.71326L12.7922 5.2992C13.2375 4.86561 12.9914 4.10858 12.3774 4.01952L8.95314 3.5203L7.42267 0.417179C7.14845 -0.135945 6.35392 -0.142977 6.07735 0.417179Z"
                                    fill="#FFC400" />
                            </g>
                            <defs>
                                <clipPath id="clip0_54_44">
                                    <rect width="13.5" height="12" fill="white" />
                                </clipPath>
                            </defs>
                        </svg>
                        <svg width="14" height="12" viewBox="0 0 14 12" fill="none"
                            xmlns="http://www.w3.org/2000/svg">
                            <g clip-path="url(#clip0_54_44)">
                                <path
                                    d="M6.07735 0.417179L4.54689 3.5203L1.12267 4.01952C0.508605 4.10858 0.262511 4.86561 0.707824 5.2992L3.18517 7.71326L2.59923 11.1234C2.49376 11.7398 3.14298 12.2015 3.68673 11.9133L6.75001 10.3031L9.81329 11.9133C10.357 12.1992 11.0063 11.7398 10.9008 11.1234L10.3149 7.71326L12.7922 5.2992C13.2375 4.86561 12.9914 4.10858 12.3774 4.01952L8.95314 3.5203L7.42267 0.417179C7.14845 -0.135945 6.35392 -0.142977 6.07735 0.417179Z"
                                    fill="#FFC400" />
                            </g>
                            <defs>
                                <clipPath id="clip0_54_44">
                                    <rect width="13.5" height="12" fill="white" />
                                </clipPath>
                            </defs>
                        </svg>
                        <svg width="14" height="12" viewBox="0 0 14 12" fill="none"
                            xmlns="http://www.w3.org/2000/svg">
                            <g clip-path="url(#clip0_54_44)">
                                <path
                                    d="M6.07735 0.417179L4.54689 3.5203L1.12267 4.01952C0.508605 4.10858 0.262511 4.86561 0.707824 5.2992L3.18517 7.71326L2.59923 11.1234C2.49376 11.7398 3.14298 12.2015 3.68673 11.9133L6.75001 10.3031L9.81329 11.9133C10.357 12.1992 11.0063 11.7398 10.9008 11.1234L10.3149 7.71326L12.7922 5.2992C13.2375 4.86561 12.9914 4.10858 12.3774 4.01952L8.95314 3.5203L7.42267 0.417179C7.14845 -0.135945 6.35392 -0.142977 6.07735 0.417179Z"
                                    fill="#FFC400" />
                            </g>
                            <defs>
                                <clipPath id="clip0_54_44">
                                    <rect width="13.5" height="12" fill="white" />
                                </clipPath>
                            </defs>
                        </svg>
                        <svg width="14" height="12" viewBox="0 0 14 12" fill="none"
                            xmlns="http://www.w3.org/2000/svg">
                            <g clip-path="url(#clip0_54_44)">
                                <path
                                    d="M6.07735 0.417179L4.54689 3.5203L1.12267 4.01952C0.508605 4.10858 0.262511 4.86561 0.707824 5.2992L3.18517 7.71326L2.59923 11.1234C2.49376 11.7398 3.14298 12.2015 3.68673 11.9133L6.75001 10.3031L9.81329 11.9133C10.357 12.1992 11.0063 11.7398 10.9008 11.1234L10.3149 7.71326L12.7922 5.2992C13.2375 4.86561 12.9914 4.10858 12.3774 4.01952L8.95314 3.5203L7.42267 0.417179C7.14845 -0.135945 6.35392 -0.142977 6.07735 0.417179Z"
                                    fill="#FFC400" />
                            </g>
                            <defs>
                                <clipPath id="clip0_54_44">
                                    <rect width="13.5" height="12" fill="white" />
                                </clipPath>
                            </defs>
                        </svg>
                        5
                        <p class="text-deskripsi">Paket sudah diterima, packing rapi, terima kasih.</p>
                    </div>
                </div>
            <tr>
                <div class="row pt-5 pb-5 border-bottom">
                    <div class="col-md-3">
                        <div class="row">
                            <div class="col-md-5"><img src="assets/media/users/100_4.jpg" class="rounded-circle"></div>
                            <div class="col mt-4">Wayan Yanti Putri<br><small>Kemarin</small></div>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <svg width="14" height="12" viewBox="0 0 14 12" fill="none"
                            xmlns="http://www.w3.org/2000/svg">
                            <g clip-path="url(#clip0_54_44)">
                                <path
                                    d="M6.07735 0.417179L4.54689 3.5203L1.12267 4.01952C0.508605 4.10858 0.262511 4.86561 0.707824 5.2992L3.18517 7.71326L2.59923 11.1234C2.49376 11.7398 3.14298 12.2015 3.68673 11.9133L6.75001 10.3031L9.81329 11.9133C10.357 12.1992 11.0063 11.7398 10.9008 11.1234L10.3149 7.71326L12.7922 5.2992C13.2375 4.86561 12.9914 4.10858 12.3774 4.01952L8.95314 3.5203L7.42267 0.417179C7.14845 -0.135945 6.35392 -0.142977 6.07735 0.417179Z"
                                    fill="#FFC400" />
                            </g>
                            <defs>
                                <clipPath id="clip0_54_44">
                                    <rect width="13.5" height="12" fill="white" />
                                </clipPath>
                            </defs>
                        </svg>
                        <svg width="14" height="12" viewBox="0 0 14 12" fill="none"
                            xmlns="http://www.w3.org/2000/svg">
                            <g clip-path="url(#clip0_54_44)">
                                <path
                                    d="M6.07735 0.417179L4.54689 3.5203L1.12267 4.01952C0.508605 4.10858 0.262511 4.86561 0.707824 5.2992L3.18517 7.71326L2.59923 11.1234C2.49376 11.7398 3.14298 12.2015 3.68673 11.9133L6.75001 10.3031L9.81329 11.9133C10.357 12.1992 11.0063 11.7398 10.9008 11.1234L10.3149 7.71326L12.7922 5.2992C13.2375 4.86561 12.9914 4.10858 12.3774 4.01952L8.95314 3.5203L7.42267 0.417179C7.14845 -0.135945 6.35392 -0.142977 6.07735 0.417179Z"
                                    fill="#FFC400" />
                            </g>
                            <defs>
                                <clipPath id="clip0_54_44">
                                    <rect width="13.5" height="12" fill="white" />
                                </clipPath>
                            </defs>
                        </svg>
                        <svg width="14" height="12" viewBox="0 0 14 12" fill="none"
                            xmlns="http://www.w3.org/2000/svg">
                            <g clip-path="url(#clip0_54_44)">
                                <path
                                    d="M6.07735 0.417179L4.54689 3.5203L1.12267 4.01952C0.508605 4.10858 0.262511 4.86561 0.707824 5.2992L3.18517 7.71326L2.59923 11.1234C2.49376 11.7398 3.14298 12.2015 3.68673 11.9133L6.75001 10.3031L9.81329 11.9133C10.357 12.1992 11.0063 11.7398 10.9008 11.1234L10.3149 7.71326L12.7922 5.2992C13.2375 4.86561 12.9914 4.10858 12.3774 4.01952L8.95314 3.5203L7.42267 0.417179C7.14845 -0.135945 6.35392 -0.142977 6.07735 0.417179Z"
                                    fill="#FFC400" />
                            </g>
                            <defs>
                                <clipPath id="clip0_54_44">
                                    <rect width="13.5" height="12" fill="white" />
                                </clipPath>
                            </defs>
                        </svg>
                        <svg width="14" height="12" viewBox="0 0 14 12" fill="none"
                            xmlns="http://www.w3.org/2000/svg">
                            <g clip-path="url(#clip0_54_44)">
                                <path
                                    d="M6.07735 0.417179L4.54689 3.5203L1.12267 4.01952C0.508605 4.10858 0.262511 4.86561 0.707824 5.2992L3.18517 7.71326L2.59923 11.1234C2.49376 11.7398 3.14298 12.2015 3.68673 11.9133L6.75001 10.3031L9.81329 11.9133C10.357 12.1992 11.0063 11.7398 10.9008 11.1234L10.3149 7.71326L12.7922 5.2992C13.2375 4.86561 12.9914 4.10858 12.3774 4.01952L8.95314 3.5203L7.42267 0.417179C7.14845 -0.135945 6.35392 -0.142977 6.07735 0.417179Z"
                                    fill="#FFC400" />
                            </g>
                            <defs>
                                <clipPath id="clip0_54_44">
                                    <rect width="13.5" height="12" fill="white" />
                                </clipPath>
                            </defs>
                        </svg>
                        <svg width="14" height="12" viewBox="0 0 14 12" fill="none"
                            xmlns="http://www.w3.org/2000/svg">
                            <g clip-path="url(#clip0_54_44)">
                                <path
                                    d="M6.07735 0.417179L4.54689 3.5203L1.12267 4.01952C0.508605 4.10858 0.262511 4.86561 0.707824 5.2992L3.18517 7.71326L2.59923 11.1234C2.49376 11.7398 3.14298 12.2015 3.68673 11.9133L6.75001 10.3031L9.81329 11.9133C10.357 12.1992 11.0063 11.7398 10.9008 11.1234L10.3149 7.71326L12.7922 5.2992C13.2375 4.86561 12.9914 4.10858 12.3774 4.01952L8.95314 3.5203L7.42267 0.417179C7.14845 -0.135945 6.35392 -0.142977 6.07735 0.417179Z"
                                    fill="#FFC400" />
                            </g>
                            <defs>
                                <clipPath id="clip0_54_44">
                                    <rect width="13.5" height="12" fill="white" />
                                </clipPath>
                            </defs>
                        </svg>
                        5
                        <p class="text-deskripsi">Paket sudah diterima, packing rapi, terima kasih.</p>
                    </div>
                </div>

                <div class="container menu-wrapper">
                    <section class="popular">
                        <div class="container">
                            <div class="row align-items-center">
                                <div class="col-6">
                                    <h4>Rekomendasi</h4>
                                </div>
                            </div>
                            <div class="row row-card mt-3 align-items-stretch mb-6">
                                <div class="col-lg-3 col-6">
                                    <div class="card p-3 p-sm-3">
                                        <img src="assets/media/products/13.png" class="img-fluid m-auto"
                                            alt="" />
                                        <div class="card-body mt-3 p-1">
                                            <h5 class="title">Sepatu Nike</h5>
                                            <h6 class="m-0 price">Rp 120.000</h6>
                                            <div class="cart d-flex align-items-center mt-3 justify-content-between">
                                                <a href="#" class="btn btn-primary mt-2">Detail</a>
                                                <a href="detail.html" class="btn btn-danger mt-2"><i
                                                        class="bx bx-cart-alt"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-6">
                                    <div class="card p-3 p-sm-3">
                                        <img src="assets/media/products/10.png" class="img-fluid m-auto"
                                            alt="" />
                                        <div class="card-body mt-3 p-1">
                                            <h5 class="title">Sepatu Nike Putih</h5>
                                            <h6 class="m-0 price">Rp 100.000</h6>
                                            <div class="cart d-flex align-items-center mt-3 justify-content-between">
                                                <a href="#" class="btn btn-primary mt-2">Detail</a>
                                                <a href="detail.html" class="btn btn-danger mt-2"><i
                                                        class="bx bx-cart-alt"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-6">
                                    <div class="card p-3 p-sm-3">
                                        <img src="assets/media/products/11.png" class="img-fluid m-auto"
                                            alt="" />
                                        <div class="card-body mt-3 p-1">
                                            <h5 class="title">Sepatu Nike RGB</h5>
                                            <h6 class="m-0 price">Rp 250.000</h6>
                                            <div class="cart d-flex align-items-center mt-3 justify-content-between">
                                                <a href="#" class="btn btn-primary mt-2">Detail</a>
                                                <a href="#" class="btn btn-danger mt-2"><i
                                                        class="bx bx-cart-alt"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-6">
                                    <div class="card p-3 p-sm-3">
                                        <img src="assets/media/products/12.png" class="img-fluid m-auto"
                                            alt="" />
                                        <div class="card-body mt-3 p-1">
                                            <h5 class="title">NIKE G32</h5>
                                            <h6 class="m-0 price">Rp 70.000</h6>
                                            <div class="cart d-flex align-items-center mt-3 justify-content-between">
                                                <a href="#" class="btn btn-primary mt-2">Detail</a>
                                                <a href="detail.html" class="btn btn-danger mt-2"><i
                                                        class="bx bx-cart-alt"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
                <div class="footer bg-white py-4 d-flex flex-lg-column mt-3 bg-footer" id="kt_footer">
                    <div class="kt-container kt-container--fluid">
                        <div class="kt-footer__copyright">
                            <footer class="footer text-center">2022 © HEALTHYSHOP</footer>
                        </div>
                    </div>
                </div>
        </div>
        <script>
            var HOST_URL = "https://preview.keenthemes.com/metronic/theme/html/tools/preview";
        </script>
        <!--begin::Global Config(global config for global JS scripts)-->
        <script>
            var KTAppSettings = {
                "breakpoints": {
                    "sm": 576,
                    "md": 768,
                    "lg": 992,
                    "xl": 1200,
                    "xxl": 1400
                },
                "colors": {
                    "theme": {
                        "base": {
                            "white": "#ffffff",
                            "primary": "#3699FF",
                            "secondary": "#E5EAEE",
                            "success": "#1BC5BD",
                            "info": "#8950FC",
                            "warning": "#FFA800",
                            "danger": "#F64E60",
                            "light": "#E4E6EF",
                            "dark": "#181C32"
                        },
                        "light": {
                            "white": "#ffffff",
                            "primary": "#E1F0FF",
                            "secondary": "#EBEDF3",
                            "success": "#C9F7F5",
                            "info": "#EEE5FF",
                            "warning": "#FFF4DE",
                            "danger": "#FFE2E5",
                            "light": "#F3F6F9",
                            "dark": "#D6D6E0"
                        },
                        "inverse": {
                            "white": "#ffffff",
                            "primary": "#ffffff",
                            "secondary": "#3F4254",
                            "success": "#ffffff",
                            "info": "#ffffff",
                            "warning": "#ffffff",
                            "danger": "#ffffff",
                            "light": "#464E5F",
                            "dark": "#ffffff"
                        }
                    },
                    "gray": {
                        "gray-100": "#F3F6F9",
                        "gray-200": "#EBEDF3",
                        "gray-300": "#E4E6EF",
                        "gray-400": "#D1D3E0",
                        "gray-500": "#B5B5C3",
                        "gray-600": "#7E8299",
                        "gray-700": "#5E6278",
                        "gray-800": "#3F4254",
                        "gray-900": "#181C32"
                    }
                },
                "font-family": "Poppins"
            };
        </script>
        <!--end::Global Config-->
        <!--begin::Global Theme Bundle(used by all pages)-->
        <script src="{{ asset('assets/plugins/global/plugins.bundle.js') }}"></script>
        <script src="{{ asset('assets/plugins/custom/prismjs/prismjs.bundle.js') }}"></script>
        <script src="{{ asset('assets/js/scripts.bundle.js') }}"></script>
        <!--end::Global Theme Bundle-->
        <!--begin::Page Scripts(used by this page)-->
        <script src="{{ asset('assets/js/pages/widgets.js') }}"></script>
        <script src="{{ asset('assets/js/pages/custom/profile/profile.js') }}"></script>
        <!--end::Page Scripts-->
        <script src="{{ asset('assets/plugins/bootstrap/dist/js/bootstrap.bundle.min.js') }}"></script>
        <script type="text/javascript">
            function incrementValue()
            {
                var value = parseInt(document.getElementById('number').value, 10);
                value = isNaN(value) ? 0 : value;
                if(value<10){
                    value++;
                        document.getElementById('number').value = value;
                }
            }
            function decrementValue()
            {
                var value = parseInt(document.getElementById('number').value, 10);
                value = isNaN(value) ? 0 : value;
                if(value>1){
                    value--;
                        document.getElementById('number').value = value;
                }

            }
            </script>

</body>

</html>

