<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="canonical" href="https://keenthemes.com/metronic" />

    <!--begin::Fonts-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
    <!--end::Fonts-->
    <!--begin::Page Vendors Styles(used by this page)-->
    <link href="{{ asset('assets/plugins/custom/fullcalendar/fullcalendar.bundle.css') }}" rel="stylesheet"
        type="text/css" />
    <!--end::Page Vendors Styles-->
    <!--begin::Global Theme Styles(used by all pages)-->
    <link href="{{ asset('assets/plugins/global/plugins.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/plugins/custom/prismjs/prismjs.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/style.bundle.css') }}" rel="stylesheet" type="text/css" />
    <!--end::Global Theme Styles-->
    <!--begin::Layout Themes(used by all pages)-->
    <link href="{{ asset('assets/css/themes/layout/header/base/light.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/themes/layout/header/menu/light.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/themes/layout/brand/dark.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/themes/layout/aside/dark.css') }}" rel="stylesheet" type="text/css" />
    <!--end::Layout Themes-->
    <link href='https://unpkg.com/boxicons@2.1.2/css/boxicons.min.css' rel='stylesheet'>
    <link rel="stylesheet" href="{{ asset('assets/plugins/bootstrap/dist/css/bootstrap.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/style/main.css') }}" />
    <link rel="shortcut icon" href="{{ asset('assets/media/logos/faviconhealthy.png') }}" />
    <title>Checkout | HEALTY SHOP</title>
</head>

<body>
    @include('layouts.user.navbar')
    <div class="container menu-wrapper">
    <div class="flex-row-fluid ml-lg-8">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-10">
                    <div class="card-toolbar">
                            <a type="submit" href="{{ Route('user.index') }}" class="btn btn-primary mr-2">Kembali</a>
                    <div class="text-center">
                        <img width="90" height="90" src="{{ asset('assets/media/logos/faviconhealthy.png') }}" alt="">
                        <div class="mt-3">
                        <h2>Checkout</h2>
                        <hr>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-7">
                            <h4>Alamat Tagihan</h4>
                            <hr>

                            <form class="mb-6">
                                <div class="row">
                                    <div class="col-md-6">
                                       <div class="mb-3">
                                         <label for="fullname" class="form-label">Nama Lengkap</label>
                                         <input type="text" id="fullname" class="form-control" placeholder="Nama Lengkap"/>
                                         <span class="form-text text-muted">Harap isi dengan benar </span>
                                       </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="mb-3">
                                          <label for="no-whatsapp" class="form-label">No Whatsapp</label>
                                          <input type="text" id="no-whatsapp" class="form-control" placeholder="628xxx"/>
                                          <span class="form-text text-muted">Harap isi sesuai dengan no Whatsapp anda</span>
                                        </div>
                                    </div>
                                </div> <!-- row -->

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="mb-3">
                                            <label for="provinsi" class="form-label">Provinsi</label>
                                            <select id="provinsi" class="form-select">
                                                <option disabled selected>Pilih Provinsi</option>
                                                <option>Aceh</option>
                                                <option>Bali</option>
                                                <option>Bangka Belitung</option>
                                                <option>Banten</option>
                                                <option>Bengkulu</option>
                                                <option>Jawa Tengah</option>
                                                <option>Kalimantan Tengah</option>
                                                <option>Sulawesi Tengah</option>
                                                <option>Jawa timur</option>
                                                <option>Kalimantan Timur</option>
                                                <option>Nusa Tenggara Timur</option>
                                                <option>Gorontalo</option>
                                                <option>Daerah Khusus Ibukota Jakarta</option>
                                                <option>Jambi</option>
                                                <option>Lampung</option>
                                                <option>Maluku</option>
                                                <option>Sulawesi Utara</option>
                                                <option>Sumatera Utara</option>
                                                <option>Papua</option>
                                                <option>Riau</option>
                                                <option>Kepulauan Riau</option>
                                                <option>Sulawesi Tenggara</option>
                                                <option>Kalimantan Selatan</option>
                                                <option>Sulawesi Selatan</option>
                                                <option>Sumatera Selatan</option>
                                                <option>Jawa Barat</option>
                                                <option>Kalimantan Barat</option>
                                                <option>Nusa tenggara Barat</option>
                                                <option>Papua Barat</option>
                                                <option>Sulawesi Barat</option>
                                                <option>Sumatera Barat</option>
                                                <option>Daerah Istimewa Yogyakarta</option>
                                                <option>Kalimantan Utara</option>
                                                <option>Maluku Utara</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="mb-3">
                                        <label for="kabupaten-kota" class="form-label">Kabupaten/Kota</label>
                                            <select id="kabupaten-kota" class="form-select">
                                                <option disabled selected>Pilih Kabupaten/Kota</option>
                                                <option>Kabupaten Aceh Barat</option>
                                                <option>Kabupaten Aceh Barat Daya</option>
                                                <option>Kabupaten Aceh Besar</option>
                                                <option>Kabupaten Aceh Jaya</option>
                                                <option>Kabupaten Aceh Selatan</option>
                                                <option>Kabupaten Aceh Singkil</option>
                                                <option>Kabupaten Aceh Tamiang</option>
                                                <option>Kabupaten Aceh Tenggara</option>
                                                <option>Kabupaten Aceh Timur</option>
                                                <option>Kabupaten Aceh Utara</option>
                                                <option>Kabupaten Asahan</option>
                                                <option>Kabupaten Agam</option>
                                                <option>Kabupaten Bandung</option>
                                                <option>Kabupaten Bekasi</option>
                                                <option>Kabupaten Bogor</option>
                                                <option>Kabupaten Badung</option>
                                                <option>Kabupaten Banjar</option>
                                                <option>Kabupaten Bangli</option>
                                                <option>Kabupaten Buleleng</option>
                                                <option>Kabupaten Batubara</option>
                                                <option>Kabupaten Banyumas</option>
                                                <option>Kabupaten Bener Meriah</option>
                                                <option>Kabupaten Bireuen</option>
                                                <option>Kabupaten Banyuasin</option>
                                                <option>Kabupaten Bengkalis</option>
                                                <option>Kabupaten Ciamis</option>
                                                <option>Kabupaten Cianjur</option>
                                                <option>Kabupaten Cirebon</option>
                                                <option>Kabupaten Dairi</option>
                                                <option>Kabupaten Deli Serdang</option>
                                                <option>Kabupaten Dharmasraya</option>
                                                <option>Kabupaten Empat Lawang</option>
                                                <option>Kabupaten Garut</option>
                                                <option>Kabupaten Gayo Lues</option>
                                                <option>Kabupaten Gunung Mas</option>
                                                <option>Kabupaten Gorontalo</option>
                                                <option>Kabupaten Gunungkidul</option>
                                                <option>Kabupaten Humbang Hasundutan</option>
                                                <option>Kabupaten Halmahera Barat</option>
                                                <option>Kabupaten Halmahera Tengah</option>
                                                <option>Kabupaten Halmahera Utara</option>
                                                <option>Kabupaten Halmahera Selatan</option>
                                                <option>Kabupaten Indramayu</option>
                                                <option>Kabupaten Indragiri Hilir</option>
                                                <option>Kabupaten Indragiri Hulu</option>
                                                <option>Kabupaten Jepara</option>
                                                <option>Kabupaten Karanganyar</option>
                                                <option>Kabupaten Kebumen</option>
                                                <option>Kabupaten Kendal</option>
                                                <option>Kabupaten Klaten</option>
                                                <option>Kabupaten Kolaka</option>
                                                <option>Kabupaten Kudus</option>
                                                <option>Kabupaten Karawang</option>
                                                <option>Kabupaten Kuningan</option>
                                                <option>Kabupaten Karo</option>
                                                <option>Kabupaten Kepulauan Mentawai</option>
                                                <option>Kabupaten Kampar</option>
                                                <option>Kabupaten Kepulauan Meranti</option>
                                                <option>Kabupaten Kuantan Singingi</option>
                                                <option>Kabupaten Labuhanbatu</option>
                                                <option>Kabupaten Labuhanbatu Selatan</option>
                                                <option>Kabupaten Labuhanbatu Utara</option>
                                                <option>Kabupaten Langkat</option>
                                                <option>Kabupaten Lima Puluh Kota</option>
                                                <option>Kabupaten Lahat</option>
                                                <option>Kabupaten Magelang</option>
                                                <option>Kabupaten Majalengka</option>
                                                <option>Kabupaten Muara Enim</option>
                                                <option>Kabupaten Musi Banyuasin</option>
                                                <option>Kabupaten Musi Rawas</option>
                                                <option>Kabupaten Minahasa</option>
                                                <option>Kabupaten Musi Rawas Utara</option>
                                                <option>Kabupaten Mandailing Natal</option>
                                                <option>Kabupaten Majene</option>
                                                <option>Kabupaten Mamasa</option>
                                                <option>Kabupaten Mamuju</option>
                                                <option>Kabupaten Mamuju Tengah</option>
                                                <option>Kabupaten Mamuju Utara</option>
                                                <option>Kabupaten Nias</option>
                                                <option>Kabupaten Nias Barat</option>
                                                <option>Kabupaten Nias Selatan</option>
                                                <option>Kabupaten Nias Utara</option>
                                                <option>Kabupaten Nagan Raya</option>
                                                <option>Kabupaten Ogan Ilir</option>
                                                <option>Kabupaten Ogan Komering Ilir</option>
                                                <option>Kabupaten Ogan Komering Ulu</option>
                                                <option>Kabupaten Ogan Komering Ulu Selatan</option>
                                                <option>Kabupaten Ogan Komering Ulu Timur</option>
                                                <option>Kabupaten Pati</option>
                                                <option>Kabupaten Pekalongan</option>
                                                <option>Kabupaten Pemalang</option>
                                                <option>Kabupaten Purbalingga</option>
                                                <option>Kabupaten Purworejo</option>
                                                <option>Kabupaten Pangandaran</option>
                                                <option>Kabupaten Poso</option>
                                                <option>Kabupaten Purwakarta</option>
                                                <option>Kabupaten Padang Lawas</option>
                                                <option>Kabupaten Padang Lawas Utara</option>
                                                <option>Kabupaten Pakpak Bharat</option>
                                                <option>Kabupaten Pidie</option>
                                                <option>Kabupaten Pidie Jaya</option>
                                                <option>Kabupaten Padang Pariaman</option>
                                                <option>Kabupaten Pasaman</option>
                                                <option>Kabupaten Pasaman Barat</option>
                                                <option>Kabupaten Pesisir Selatan</option>
                                                <option>Kabupaten Penukal Abab Lematang Ilir</option>
                                                <option>Kabupaten Pelalawan</option>
                                                <option>Kabupaten Rokan Hilir</option>
                                                <option>Kabupaten Rokan Hulu</option>
                                                <option>Kabupaten Raja Ampat</option>
                                                <option>Kabupaten Subang</option>
                                                <option>Kabupaten Sukabumi</option>
                                                <option>Kabupaten Sumedang</option>
                                                <option>Kabupaten Siak</option>
                                                <option>Kabupaten Sorong</option>
                                                <option>Kabupaten Samosir</option>
                                                <option>Kabupaten Serdang Bedagai</option>
                                                <option>Kabupaten Simalungun</option>
                                                <option>Kabupaten Simeulue</option>
                                                <option>Kabupaten Sijunjung</option>
                                                <option>Kabupaten Solok</option>
                                                <option>Kabupaten Solok Selatan</option>
                                                <option>Kabupaten Tegal</option>
                                                <option>Kabupaten Temanggung</option>
                                                <option>Kabupaten Tasikmalaya</option>
                                                <option>Kabupaten Tapanuli Selatan</option>
                                                <option>Kabupaten Tapanuli Tengah</option>
                                                <option>Kabupaten Tapanuli Utara</option>
                                                <option>Kabupaten Toba Samosir</option>
                                                <option>Kabupaten Tanah Datar</option>
                                                <option>Kabupaten Wonogiri</option>
                                                <option>Kabupaten Wonosobo</option>
                                                <option>Kota Administrasi Jakarta Barat</option>
                                                <option>Kota Administrasi Jakarta Pusat</option>
                                                <option>Kota Administrasi Jakarta Selatan</option>
                                                <option>Kota Administrasi Jakarta Timur</option>
                                                <option>Kota Administrasi Jakarta Utara</option>
                                                <option>Kota Ambon</option>
                                                <option>Kota Bandung</option>
                                                <option>Kota Banjar</option>
                                                <option>Kota Bekasi</option>
                                                <option>Kota Bogor</option>
                                                <option>Kota Binjai</option>
                                                <option>Kota Bitung</option>
                                                <option>Kota Bau-Bau</option>
                                                <option>Kota Batu</option>
                                                <option>Kota Blitar</option>
                                                <option>Kota Banda Aceh</option>
                                                <option>Kota Banjarbaru</option>
                                                <option>Kota Banjarmasin</option>
                                                <option>Kota Balikpapan</option>
                                                <option>Kota Bontang</option>
                                                <option>Kota Bukittinggi</option>
                                                <option>Kota Batam</option>
                                                <option>Kota Bengkulu</option>
                                                <option>Kota Bandar Lampung</option>
                                                <option>Kota Bima</option>
                                                <option>Kota Cimahi</option>
                                                <option>Kota Cirebon</option>
                                                <option>Kota Cilegon</option>
                                                <option>Kota Depok</option>
                                                <option>Kota Denpasar</option>
                                                <option>Kota Gorontalo</option>
                                                <option>Kota Gunungsitoli</option>
                                                <option>Kota Jambi</option>
                                                <option>Kota Kediri</option>
                                                <option>Kota Kendari</option>
                                                <option>Kota Kotamobagu</option>
                                                <option>Kota Lhokseumawe</option>
                                                <option>Kota Medan</option>
                                                <option>Kota Metro</option>
                                                <option>Kota Mataram</option>
                                                <option>Kota Manado</option>
                                                <option>Kota Malang</option>
                                                <option>Kota Mamuju</option>
                                                <option>Kota Palopo</option>
                                                <option>Kota Parepare</option>
                                                <option>Kota Pasuruan</option>
                                                <option>Kota Probolinggo</option>
                                                <option>Kota Palangka Raya</option>
                                                <option>Kota Padangsidempuan</option>
                                                <option>Kota Palu</option>
                                                <option>Kota Pematangsiantar</option>
                                                <option>Kota Padang</option>
                                                <option>Kota Padangpanjang</option>
                                                <option>Kota Pariaman</option>
                                                <option>Kota Payakumbuh</option>
                                                <option>Kota Pontianak</option>
                                                <option>Kota Pagar Alam</option>
                                                <option>Kota Palembang</option>
                                                <option>Kota Prabumulih</option>
                                                <option>Kota Pekanbaru</option>
                                                <option>Kota Pangkal Pinang</option>
                                                <option>Kota Sukabumi</option>
                                                <option>Kota Sabang</option>
                                                <option>Kota Surabaya</option>
                                                <option>Kota Singkawang</option>
                                                <option>Kota Samarinda</option>
                                                <option>Kota Subulussalam</option>
                                                <option>Kota Sibolga</option>
                                                <option>Kota Sawahlunto</option>
                                                <option>Kota Solok</option>
                                                <option>Kota Sungai Penuh</option>
                                                <option>Kota Serang</option>
                                                <option>Kota Ternate</option>
                                                <option>Kota Tidore Kepulauan</option>
                                                <option>Kota Tegal</option>
                                                <option>Kota Tasikmalaya</option>
                                                <option>Kota Tanjungbalai</option>
                                                <option>Kota Tarakan</option>
                                                <option>Kota Tual</option>
                                                <option>Kota Tebing Tinggi</option>
                                                <option>Kota Tanjung Pinang</option>
                                                <option>Kota Tangerang</option>
                                                <option>Kota Tangerang Selatan</option>
                                                <option>Kota Yogyakarta</option>

                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="mb-3">
                                        <label for="kecamatan" class="form-label">Kecamatan</label>
                                            <select id="kecamatan" class="form-select">
                                                <option disabled selected>Pilih Kecamatan</option>
                                                <option>	Kec. Kepulauan Seribu Utara	</option>
                                                <option>	Kec. Tanah Abang	</option>
                                                <option>	Kec. Menteng	</option>
                                                <option>	Kec. Senen	</option>
                                                <option>	Kec. Johar Baru	</option>
                                                <option>	Kec. Cempaka Putih	</option>
                                                <option>	Kec. Kemayoran	</option>
                                                <option>	Kec. Sawah Besar	</option>
                                                <option>	Kec. Gambir	</option>
                                                <option>	Kec. Penjaringan	</option>
                                                <option>	Kec. Pademangan	</option>
                                                <option>	Kec. Tanjung Priok	</option>
                                                <option>	Kec. Koja	</option>
                                                <option>	Kec. Kelapa Gading	</option>
                                                <option>	Kec. Cilincing	</option>
                                                <option>	Kec. Kembangan	</option>
                                                <option>	Kec. Kebon Jeruk	</option>
                                                <option>	Kec. Palmerah	</option>
                                                <option>	Kec. Grogol Petamburan	</option>
                                                <option>	Kec. Tambora	</option>
                                                <option>	Kec. Taman Sari	</option>
                                                <option>	Kec. Cengkareng	</option>
                                                <option>	Kec. Kali Deres	</option>
                                                <option>	Kec. Jagakarsa	</option>
                                                <option>	Kec. Pasar Minggu	</option>
                                                <option>	Kec. Cilandak	</option>
                                                <option>	Kec. Pesanggrahan	</option>
                                                <option>	Kec. Kebayoran Lama	</option>
                                                <option>	Kec. Kebayoran Baru	</option>
                                                <option>	Kec. Mampang Prapatan	</option>
                                                <option>	Kec. Pancoran	</option>
                                                <option>	Kec. Tebet	</option>
                                                <option>	Kec. Setia Budi	</option>
                                                <option>	Kec. Pasar Rebo	</option>
                                                <option>	Kec. Ciracas	</option>
                                                <option>	Kec. Cipayung	</option>
                                                <option>	Kec. Makasar	</option>
                                                <option>	Kec. Kramat Jati	</option>
                                                <option>	Kec. Jatinegara	</option>
                                                <option>	Kec. Duren Sawit	</option>
                                                <option>	Kec. Cakung	</option>
                                                <option>	Kec. Pulo Gadung	</option>
                                                <option>	Kec. Matraman	</option>
                                                <option>	Kec. Nanggung	</option>
                                                <option>	Kec. Leuwiliang	</option>
                                                <option>	Kec. Pamijahan	</option>
                                                <option>	Kec. Cibungbulang	</option>
                                                <option>	Kec. Ciampea	</option>
                                                <option>	Kec. Dramaga	</option>
                                                <option>	Kec. Ciomas	</option>
                                                <option>	Kec. Cijeruk	</option>
                                                <option>	Kec. Caringin	</option>
                                                <option>	Kec. Ciawi	</option>
                                                <option>	Kec. Cisarua	</option>
                                                <option>	Kec. Megamendung	</option>
                                                <option>	Kec. Sukaraja	</option>
                                                <option>	Kec. Babakan Madang	</option>
                                                <option>	Kec. Sukamakmur	</option>
                                                <option>	Kec. Cariu	</option>
                                                <option>	Kec. Jonggol	</option>
                                                <option>	Kec. Cileungsi	</option>
                                                <option>	Kec. Gunungputri	</option>
                                                <option>	Kec. Citeureup	</option>
                                                <option>	Kec. Cibinong	</option>
                                                <option>	Kec. Bojong Gede	</option>
                                                <option>	Kec. Kemang	</option>
                                                <option>	Kec. Parung	</option>
                                                <option>	Kec. Gunung Sindur	</option>
                                                <option>	Kec. Rumpin	</option>
                                                <option>	Kec. Cigudeg	</option>
                                                <option>	Kec. Jasinga	</option>
                                                <option>	Kec. Tenjo	</option>
                                                <option>	Kec. Parungpanjang	</option>
                                                <option>	Kec. Tamansari	</option>
                                                <option>	Kec. Ciseeng	</option>
                                                <option>	Kec. Kelapa Nunggal	</option>
                                                <option>	Kec. Sukajaya	</option>
                                                <option>	Kec. Ranca Bungur	</option>
                                                <option>	Kec. Tanjung Sari	</option>
                                                <option>	Kec. Tajurhalang	</option>
                                                <option>	Kec. Cigombong	</option>
                                                <option>	Kec. Leuwisadeng	</option>
                                                <option>	Kec. Tenjolaya	</option>
                                                <option>	Kec. Ciemas	</option>
                                                <option>	Kec. Ciracap	</option>
                                                <option>	Kec. Surade	</option>
                                                <option>	Kec. Jampang Kulon	</option>
                                                <option>	Kec. Kalibunder	</option>
                                                <option>	Kec. Tegalbuleud	</option>
                                                <option>	Kec. Cidolog	</option>
                                                <option>	Kec. Sagaranten	</option>
                                                <option>	Kec. Pabuaran	</option>
                                                <option>	Kec. Lengkong	</option>
                                                <option>	Kec. Pelabuhan Ratu	</option>
                                                <option>	Kec. Warung Kiara	</option>
                                                <option>	Kec. Jampang Tengah	</option>
                                                <option>	Kec. Cikembar	</option>
                                                <option>	Kec. Nyalindung	</option>
                                                <option>	Kec. Gegerbitung	</option>
                                                <option>	Kec. Sukaraja	</option>
                                                <option>	Kec. Sukabumi	</option>
                                                <option>	Kec. Kadudampit	</option>
                                                <option>	Kec. Cisaat	</option>
                                                <option>	Kec. Cibadak	</option>
                                                <option>	Kec. Nagrak	</option>
                                                <option>	Kec. Cicurug	</option>
                                                <option>	Kec. Cidahu	</option>
                                                <option>	Kec. Parakansalak	</option>
                                                <option>	Kec. Parungkuda	</option>
                                                <option>	Kec. Kalapa Nunggal	</option>
                                                <option>	Kec. Cikidang	</option>
                                                <option>	Kec. Cisolok	</option>
                                                <option>	Kec. Kabandungan	</option>
                                                <option>	Kec. Gunung Guruh	</option>
                                                <option>	Kec. Cikakak	</option>
                                                <option>	Kec. Bantar Gadung	</option>
                                                <option>	Kec. Cicantayan	</option>
                                                <option>	Kec. Simpenan	</option>
                                                <option>	Kec. Kebon Pedes	</option>
                                                <option>	Kec. Cidadap	</option>
                                                <option>	Kec. Cibitung	</option>
                                                <option>	Kec. Curugkembar	</option>
                                                <option>	Kec. Purabaya	</option>
                                                <option>	Kec. Cireunghas	</option>
                                                <option>	Kec. Sukalarang	</option>
                                                <option>	Kec. Caringin	</option>
                                                <option>	Kec. Bojong Genteng	</option>
                                                <option>	Kec. Waluran	</option>
                                                <option>	Kec. Cimanggu	</option>
                                                <option>	Kec. Ciambar	</option>
                                                <option>	Kec. Agrabinta	</option>
                                                <option>	Kec. Sindang Barang	</option>
                                                <option>	Kec. Cidaun	</option>
                                                <option>	Kec. Naringgul	</option>
                                                <option>	Kec. Cibinong	</option>
                                                <option>	Kec. Tanggeung	</option>
                                                <option>	Kec. Kadupandak	</option>
                                                <option>	Kec. Takokak	</option>
                                                <option>	Kec. Sukanagara	</option>
                                                <option>	Kec. Pagelaran	</option>
                                                <option>	Kec. Campaka	</option>
                                                <option>	Kec. Cibeber	</option>
                                                <option>	Kec. Warungkondang	</option>
                                                <option>	Kec. Cilaku	</option>
                                                <option>	Kec. Sukaluyu	</option>
                                                <option>	Kec. Ciranjang	</option>
                                                <option>	Kec. Mande	</option>
                                                <option>	Kec. Karang Tengah	</option>
                                                <option>	Kec. Cianjur	</option>
                                                <option>	Kec. Cugenang	</option>
                                                <option>	Kec. Pacet	</option>
                                                <option>	Kec. Sukaresmi	</option>
                                                <option>	Kec. Cikalong Kulon	</option>
                                                <option>	Kec. Bojong Picung	</option>
                                                <option>	Kec. Campaka Mulya	</option>
                                                <option>	Kec. Cikadu	</option>
                                                <option>	Kec. Leles	</option>
                                                <option>	Kec. Cijati	</option>
                                                <option>	Kec. Gekbrong	</option>
                                                <option>	Kec. Cipanas	</option>
                                                <option>	Kec. Haurwangi	</option>
                                                <option>	Kec. Pasirkuda	</option>
                                                <option>	Kec. Ciwidey	</option>
                                                <option>	Kec. Pasirjambu	</option>
                                                <option>	Kec. Cimaung	</option>
                                                <option>	Kec. Pangalengan	</option>
                                                <option>	Kec. Kertasari	</option>
                                                <option>	Kec. Pacet	</option>
                                                <option>	Kec. Ibun	</option>
                                                <option>	Kec. Paseh	</option>
                                                <option>	Kec. Cikancung	</option>
                                                <option>	Kec. Cicalengka	</option>
                                                <option>	Kec. Rancaekek	</option>
                                                <option>	Kec. Majalaya	</option>
                                                <option>	Kec. Ciparay	</option>
                                                <option>	Kec. Bale Endah	</option>
                                                <option>	Kec. Arjasari	</option>
                                                <option>	Kec. Banjaran	</option>
                                                <option>	Kec. Pameungpeuk	</option>
                                                <option>	Kec. Ketapang	</option>
                                                <option>	Kec. Soreang	</option>
                                                <option>	Kec. Marga Asih	</option>
                                                <option>	Kec. Margahayu	</option>
                                                <option>	Kec. Dayeuhkolot	</option>
                                                <option>	Kec. Bojongsoang	</option>
                                                <option>	Kec. Cileunyi	</option>
                                                <option>	Kec. Cilengkrang	</option>
                                                <option>	Kec. Cimenyan	</option>
                                                <option>	Kec. Rancabali	</option>
                                                <option>	Kec. Nagreg	</option>
                                                <option>	Kec. Solokan Jeruk	</option>
                                                <option>	Kec. Cangkuang	</option>
                                                <option>	Kec. Kutawaringin	</option>
                                                <option>	Kec. Jatinangor	</option>
                                                <option>	Kec. Cimanggung	</option>
                                                <option>	Kec. Tanjungsari	</option>
                                                <option>	Kec. Rancakalong	</option>
                                                <option>	Kec. Sumedang Selatan	</option>
                                                <option>	Kec. Sumedang Utara	</option>
                                                <option>	Kec. Situraja	</option>
                                                <option>	Kec. Darmaraja	</option>
                                                <option>	Kec. Cibugel	</option>
                                                <option>	Kec. Wado	</option>
                                                <option>	Kec. Tomo	</option>
                                                <option>	Kec. Ujung Jaya	</option>
                                                <option>	Kec. Conggeang	</option>
                                                <option>	Kec. Paseh	</option>
                                                <option>	Kec. Cimalaka	</option>
                                                <option>	Kec. Tanjungkerta	</option>
                                                <option>	Kec. Buah Dua	</option>
                                                <option>	Kec. Ganeas	</option>
                                                <option>	Kec. Jati Gede	</option>
                                                <option>	Kec. Pamulihan	</option>
                                                <option>	Kec. Cisitu	</option>
                                                <option>	Kec. Jatinunggal	</option>
                                                <option>	Kec. Cisarua	</option>
                                                <option>	Kec. Tanjungmedar	</option>
                                                <option>	Kec. Surian	</option>
                                                <option>	Kec. Sukasari	</option>
                                                <option>	Kec. Talegong	</option>
                                                <option>	Kec. Cisewu	</option>
                                                <option>	Kec. Bungbulang	</option>
                                                <option>	Kec. Pamulihan	</option>
                                                <option>	Kec. Pakenjeng	</option>
                                                <option>	Kec. Cikelet	</option>
                                                <option>	Kec. Pameungpeuk	</option>
                                                <option>	Kec. Cibalong	</option>
                                                <option>	Kec. Cisompet	</option>
                                                <option>	Kec. Peundeuy	</option>
                                                <option>	Kec. Singajaya	</option>
                                                <option>	Kec. Cikajang	</option>
                                                <option>	Kec. Banjarwangi	</option>
                                                <option>	Kec. Cilawu	</option>
                                                <option>	Kec. Bayongbong	</option>
                                                <option>	Kec. Cisurupan	</option>
                                                <option>	Kec. Samarang	</option>
                                                <option>	Kec. Garut Kota	</option>
                                                <option>	Kec. Karangpawitan	</option>
                                                <option>	Kec. Wanaraja	</option>
                                                <option>	Kec. Sukawening	</option>
                                                <option>	Kec. Banyuresmi	</option>
                                                <option>	Kec. Leles	</option>
                                                <option>	Kec. Leuwigoong	</option>
                                                <option>	Kec. Cibatu	</option>
                                                <option>	Kec. Cibiuk	</option>
                                                <option>	Kec. Kadungora	</option>
                                                <option>	Kec. Blubur Limbangan	</option>
                                                <option>	Kec. Selaawi	</option>
                                                <option>	Kec. Malangbong	</option>
                                                <option>	Kec. Mekarmukti	</option>
                                                <option>	Kec. Caringin	</option>
                                                <option>	Kec. Cihurip	</option>
                                                <option>	Kec. Sukaresmi	</option>
                                                <option>	Kec. Pasirwangi	</option>
                                                <option>	Kec. Karangtengah	</option>
                                                <option>	Kec. Kersamanah	</option>
                                                <option>	Kec. Tarogong Kaler	</option>
                                                <option>	Kec. Tarogong Kidul	</option>
                                                <option>	Kec. Cigedug	</option>
                                                <option>	Kec. Sucinaraja	</option>
                                                <option>	Kec. Pangatikan	</option>
                                                <option>	Kec. Cipatujah	</option>
                                                <option>	Kec. Karangnunggal	</option>
                                                <option>	Kec. Cikalong	</option>
                                                <option>	Kec. Panca Tengah	</option>
                                                <option>	Kec. Cikatomas	</option>
                                                <option>	Kec. Cibalong	</option>
                                                <option>	Kec. Bantarkalong	</option>
                                                <option>	Kec. Bojong Gambir	</option>
                                                <option>	Kec. Sodonghilir	</option>
                                                <option>	Kec. Taraju	</option>
                                                <option>	Kec. Salawu	</option>
                                                <option>	Kec. Tanjungjaya	</option>
                                                <option>	Kec. Sukaraja	</option>
                                                <option>	Kec. Salopa	</option>
                                                <option>	Kec. Cineam	</option>
                                                <option>	Kec. Manonjaya	</option>
                                                <option>	Kec. Singaparna	</option>
                                                <option>	Kec. Cigalontang	</option>
                                                <option>	Kec. Leuwisari	</option>
                                                <option>	Kec. Cisayong	</option>
                                                <option>	Kec. Rajapolah	</option>
                                                <option>	Kec. Jamanis	</option>
                                                <option>	Kec. Ciawi	</option>
                                                <option>	Kec. Pagerageung	</option>
                                                <option>	Kec. Parung Ponteng	</option>
                                                <option>	Kec. Sariwangi	</option>
                                                <option>	Kec. Sukaratu	</option>
                                                <option>	Kec. Sukarame	</option>
                                                <option>	Kec. Bojong Asih	</option>
                                                <option>	Kec. Culamega	</option>
                                                <option>	Kec. Puspahiang	</option>
                                                <option>	Kec. Jatiwaras	</option>
                                                <option>	Kec. Mangunreja	</option>
                                                <option>	Kec. Gunung Tanjung	</option>
                                                <option>	Kec. Karang Jaya	</option>
                                                <option>	Kec. Pada Kembang	</option>
                                                <option>	Kec. Sukahening	</option>
                                                <option>	Kec. Kadipaten	</option>
                                                <option>	Kec. Sukaresik	</option>
                                                <option>	Kec. Cimerak	</option>
                                                <option>	Kec. Cijulang	</option>
                                                <option>	Kec. Cigugur	</option>
                                                <option>	Kec. Langkaplancar	</option>
                                                <option>	Kec. Parigi	</option>
                                                <option>	Kec. Sidamulih	</option>
                                                <option>	Kec. Pangandaran	</option>
                                                <option>	Kec. Kalipucang	</option>
                                                <option>	Kec. Padaherang	</option>
                                                <option>	Kec. Banjarsari	</option>
                                                <option>	Kec. Lakbok	</option>
                                                <option>	Kec. Pamarican	</option>
                                                <option>	Kec. Cidolog	</option>
                                                <option>	Kec. Cimaragas	</option>
                                                <option>	Kec. Cijeungjing	</option>
                                                <option>	Kec. Cisaga	</option>
                                                <option>	Kec. Tambaksari	</option>
                                                <option>	Kec. Rancah	</option>
                                                <option>	Kec. Rajadesa	</option>
                                                <option>	Kec. Sukadana	</option>
                                                <option>	Kec. Ciamis	</option>
                                                <option>	Kec. Cikoneng	</option>
                                                <option>	Kec. Cihaurbeuti	</option>
                                                <option>	Kec. Sadananya	</option>
                                                <option>	Kec. Cipaku	</option>
                                                <option>	Kec. Jatinagara	</option>
                                                <option>	Kec. Panawangan	</option>
                                                <option>	Kec. Kawali	</option>
                                                <option>	Kec. Panjalu	</option>
                                                <option>	Kec. Panumbangan	</option>
                                                <option>	Kec. Panjalu Utara/Sukamantri	</option>
                                                <option>	Kec. Sindangkasih	</option>
                                                <option>	Kec. Purwadadi	</option>
                                                <option>	Kec. Baregbeg	</option>
                                                <option>	Kec. Lumbung	</option>
                                                <option>	Kec. Mangunjaya	</option>
                                                <option>	Sukamantri	</option>
                                                <option>	Padaherang	</option>
                                                <option>	Kalipucang	</option>
                                                <option>	Pangandaran	</option>
                                                <option>	Sidamulih	</option>
                                                <option>	Parigi	</option>
                                                <option>	Cimerak	</option>
                                                <option>	Cigugur	</option>
                                                <option>	Langkaplancar	</option>
                                                <option>	Mangunjaya	</option>
                                                <option>	Kec. Darma	</option>
                                                <option>	Kec. Kadugede	</option>
                                                <option>	Kec. Ciniru	</option>
                                                <option>	Kec. Selajambe	</option>
                                                <option>	Kec. Subang	</option>
                                                <option>	Kec. Ciwaru	</option>
                                                <option>	Kec. Cibingbin	</option>
                                                <option>	Kec. Luragung	</option>
                                                <option>	Kec. Cidahu	</option>
                                                <option>	Kec. Ciawigebang	</option>
                                                <option>	Kec. Lebakwangi	</option>
                                                <option>	Kec. Garawangi	</option>
                                                <option>	Kec. Kuningan	</option>
                                                <option>	Kec. Cigugur	</option>
                                                <option>	Kec. Kramatmulya	</option>
                                                <option>	Kec. Jalaksana	</option>
                                                <option>	Kec. Cilimus	</option>
                                                <option>	Kec. Mandirancan	</option>
                                                <option>	Kec. Pasawahan	</option>
                                                <option>	Kec. Pancalang	</option>
                                                <option>	Kec. Cipicung	</option>
                                                <option>	Kec. Kalimanggis	</option>
                                                <option>	Kec. Japara	</option>
                                                <option>	Kec. Karangkancana	</option>
                                                <option>	Kec. Nusaherang	</option>
                                                <option>	Kec. Cilebak	</option>
                                                <option>	Kec. Hantara	</option>
                                                <option>	Kec. Cimahi	</option>
                                                <option>	Kec. Cibeureum	</option>
                                                <option>	Kec. Sindang Agung	</option>
                                                <option>	Kec. Maleber	</option>
                                                <option>	Kec. Ciganda Mekar	</option>
                                                <option>	Kec. Lemahsugih	</option>
                                                <option>	Kec. Bantarujeg	</option>
                                                <option>	Kec. Cikijing	</option>
                                                <option>	Kec. Talaga	</option>
                                                <option>	Kec. Argapura	</option>
                                                <option>	Kec. Maja	</option>
                                                <option>	Kec. Majalengka	</option>
                                                <option>	Kec. Cigasong	</option>
                                                <option>	Kec. Sukahaji	</option>
                                                <option>	Kec. Rajagaluh	</option>
                                                <option>	Kec. Sindangwangi	</option>
                                                <option>	Kec. Leuwimunding	</option>
                                                <option>	Kec. Palasah	</option>
                                                <option>	Kec. Jatiwangi	</option>
                                                <option>	Kec. Dawuan	</option>
                                                <option>	Kec. Panyingkiran	</option>
                                                <option>	Kec. Kadipaten	</option>
                                                <option>	Kec. Kertajati	</option>
                                                <option>	Kec. Jatitujuh	</option>
                                                <option>	Kec. Ligung	</option>
                                                <option>	Kec. Sumberjaya	</option>
                                                <option>	Kec. Banjaran	</option>
                                                <option>	Kec. Cingambul	</option>
                                                <option>	Kec. Mala usma	</option>
                                                <option>	Kec. Sindang	</option>
                                                <option>	Kec. Kasokandel	</option>
                                                <option>	Kec. Waled	</option>
                                                <option>	Kec. Ciledug	</option>
                                                <option>	Kec. Losari	</option>
                                                <option>	Kec. Babakan	</option>
                                                <option>	Kec. Karang Sembung	</option>
                                                <option>	Kec. Lemah Abang	</option>
                                                <option>	Kec. Sedong	</option>
                                                <option>	Kec. Astana Japura	</option>
                                                <option>	Kec. Mundu	</option>
                                                <option>	Kec. Beber	</option>
                                                <option>	Kec. Sumber	</option>
                                                <option>	Kec. Palimanan	</option>
                                                <option>	Kec. Plumbon	</option>
                                                <option>	Kec. Weru	</option>
                                                <option>	Kec. Kapetakan	</option>
                                                <option>	Kec. Klangenan	</option>
                                                <option>	Kec. Arjawinangun	</option>
                                                <option>	Kec. Ciwaringin	</option>
                                                <option>	Kec. Susukan	</option>
                                                <option>	Kec. Gegesik	</option>
                                                <option>	Kec. Susukan Lebak	</option>
                                                <option>	Kec. Pabedilan	</option>
                                                <option>	Kec. Dukupuntang	</option>
                                                <option>	Kec. Panguragan	</option>
                                                <option>	Kec. Kaliwedi	</option>
                                                <option>	Kec. Pangenan	</option>
                                                <option>	Kec. Gebang	</option>
                                                <option>	Kec. Depok	</option>
                                                <option>	Kec. Kedawung	</option>
                                                <option>	Kec. Karang Wereng	</option>
                                                <option>	Kec. Talun	</option>
                                                <option>	Kec. Gunung Jati	</option>
                                                <option>	Kec. Pasaleman	</option>
                                                <option>	Kec. Pabuaran	</option>
                                                <option>	Kec. Tengah Tani	</option>
                                                <option>	Kec. Plered	</option>
                                                <option>	Kec. Gempol	</option>
                                                <option>	Kec. Greged	</option>
                                                <option>	Kec. Suranenggala	</option>
                                                <option>	Kec. Jamblang	</option>
                                                <option>	Kec. Haurgeulis	</option>
                                                <option>	Kec. Kroya	</option>
                                                <option>	Kec. Gabuswetan	</option>
                                                <option>	Kec. Cikedung	</option>
                                                <option>	Kec. Lelea	</option>
                                                <option>	Kec. Bangodua	</option>
                                                <option>	Kec. Widasari	</option>
                                                <option>	Kec. Kertasemaya	</option>
                                                <option>	Kec. Krangkeng	</option>
                                                <option>	Kec. Karangampel	</option>
                                                <option>	Kec. Juntinyuat	</option>
                                                <option>	Kec. Sliyeg	</option>
                                                <option>	Kec. Jatibarang	</option>
                                                <option>	Kec. Balongan	</option>
                                                <option>	Kec. Indramayu	</option>
                                                <option>	Kec. Sindang	</option>
                                                <option>	Kec. Lohbener	</option>
                                                <option>	Kec. Losarang	</option>
                                                <option>	Kec. Kandanghaur	</option>
                                                <option>	Kec. Bongas	</option>
                                                <option>	Kec. Anjatan	</option>
                                                <option>	Kec. Sukra	</option>
                                                <option>	Kec. Arahan	</option>
                                                <option>	Kec. Cantigi	</option>
                                                <option>	Kec. Gantar	</option>
                                                <option>	Kec. Terisi	</option>
                                                <option>	Kec. Sukagumiwang	</option>
                                                <option>	Kec. Kedokan Bunder	</option>
                                                <option>	Kec. Pasekan	</option>
                                                <option>	Kec. Tukdana	</option>
                                                <option>	Kec. Patrol	</option>
                                                <option>	Kec. Sagalaherang	</option>
                                                <option>	Kec. Jalancagak	</option>
                                                <option>	Kec. Cisalak	</option>
                                                <option>	Kec. Tanjung Siang	</option>
                                                <option>	Kec. Cijambe	</option>
                                                <option>	Kec. Cibogo	</option>
                                                <option>	Kec. Subang	</option>
                                                <option>	Kec. Kalijati	</option>
                                                <option>	Kec. Cipeundeuy	</option>
                                                <option>	Kec. Pabuaran	</option>
                                                <option>	Kec. Patokbeusi	</option>
                                                <option>	Kec. Purwadadi	</option>
                                                <option>	Kec. Cikaum	</option>
                                                <option>	Kec. Pagaden	</option>
                                                <option>	Kec. Cipunagara	</option>
                                                <option>	Kec. Compreng	</option>
                                                <option>	Kec. Binong	</option>
                                                <option>	Kec. Ciasem	</option>
                                                <option>	Kec. Pamanukan	</option>
                                                <option>	Kec. Pusakanagara	</option>
                                                <option>	Kec. Legon Kulon	</option>
                                                <option>	Kec. Blanakan	</option>
                                                <option>	Kec. Dawuan	</option>
                                                <option>	Kec. Serang Panjang	</option>
                                                <option>	Kec. Kasomalang	</option>
                                                <option>	Kec. Tambakdahan	</option>
                                                <option>	Kec. Pagaden Barat	</option>
                                                <option>	Kec. Pusakajaya	</option>
                                                <option>	Kec. Ciater	</option>
                                                <option>	Kec. Sukasari	</option>
                                                <option>	Kec. Jatiluhur	</option>
                                                <option>	Kec. Maniis	</option>
                                                <option>	Kec. Tegalwaru	</option>
                                                <option>	Kec. Plered	</option>
                                                <option>	Kec. Sukatani	</option>
                                                <option>	Kec. Darangdan	</option>
                                                <option>	Kec. Bojong	</option>
                                                <option>	Kec. Wanayasa	</option>
                                                <option>	Kec. Pasawahan	</option>
                                                <option>	Kec. Purwakarta	</option>
                                                <option>	Kec. Campaka	</option>
                                                <option>	Kec. Sukasari	</option>
                                                <option>	Kec. Kiarapedes	</option>
                                                <option>	Kec. Babakancikao	</option>
                                                <option>	Kec. Cibatu	</option>
                                                <option>	Kec. Bungursari	</option>
                                                <option>	Kec. Pondok Salam	</option>
                                                <option>	Kec. Pangkalan	</option>
                                                <option>	Kec. Ciampel	</option>
                                                <option>	Kec. Klari	</option>
                                                <option>	Kec. Cikampek	</option>
                                                <option>	Kec. Tirtamulya	</option>
                                                <option>	Kec. Jatisari	</option>
                                                <option>	Kec. Lemahabang	</option>
                                                <option>	Kec. Telagasari	</option>
                                                <option>	Kec. Karawang	</option>
                                                <option>	Kec. Rawamerta	</option>
                                                <option>	Kec. Tempuran	</option>
                                                <option>	Kec. Kutawaluya	</option>
                                                <option>	Kec. Rengasdengklok	</option>
                                                <option>	Kec. Pedes	</option>
                                                <option>	Kec. Cibuaya	</option>
                                                <option>	Kec. Tirtajaya	</option>
                                                <option>	Kec. Batujaya	</option>
                                                <option>	Kec. Pakisjaya	</option>
                                                <option>	Kec. Majalaya	</option>
                                                <option>	Kec. Jayakerta	</option>
                                                <option>	Kec. Cilamaya Kulon	</option>
                                                <option>	Kec. Banyusari	</option>
                                                <option>	Kec. Kotabaru	</option>
                                                <option>	Kec. Cilamaya Wetan	</option>
                                                <option>	Kec. Purwasari	</option>
                                                <option>	Kec. Teluk Jambe Barat	</option>
                                                <option>	Kec. Teluk Jambe Timur	</option>
                                                <option>	Kec. Karawang Timur	</option>
                                                <option>	Kec. Tegalwaru	</option>
                                                <option>	Kec. Cilebar	</option>
                                                <option>	Kec. Karawang Barat	</option>
                                                <option>	Kec. Setu	</option>
                                                <option>	Kec. Cibarusah	</option>
                                                <option>	Kec. Kedung Waringin	</option>
                                                <option>	Kec. Cibitung	</option>
                                                <option>	Kec. Bebelan	</option>
                                                <option>	Kec. Taruma Jaya	</option>
                                                <option>	Kec. Tembelang	</option>
                                                <option>	Kec. Sukatani	</option>
                                                <option>	Kec. Pebayuran	</option>
                                                <option>	Kec. Cabangbungin	</option>
                                                <option>	Kec. Muara Gembong	</option>
                                                <option>	Kec. Tambun Selatan	</option>
                                                <option>	Kec. Tambun Utara	</option>
                                                <option>	Kec. Cikarang Barat	</option>
                                                <option>	Kec. Karang Bahagia	</option>
                                                <option>	Kec. Cikarang Utara	</option>
                                                <option>	Kec. Cikarang Selatan	</option>
                                                <option>	Kec. Cikarang Timur	</option>
                                                <option>	Kec. Bojong Mangu	</option>
                                                <option>	Kec. Cikarang Pusat	</option>
                                                <option>	Kec. Sukakarya	</option>
                                                <option>	Kec. Sukawangi	</option>
                                                <option>	Kec. Serang Baru	</option>
                                                <option>	Kec. Rongga	</option>
                                                <option>	Kec. Gununghalu	</option>
                                                <option>	Kec. Sindangkerta	</option>
                                                <option>	Kec. Cililin	</option>
                                                <option>	Kec. Cihampelas	</option>
                                                <option>	Kec. Cipongkor	</option>
                                                <option>	Kec. Batujajar	</option>
                                                <option>	Kec. Cipatat	</option>
                                                <option>	Kec. Padalarang	</option>
                                                <option>	Kec. Ngamprah	</option>
                                                <option>	Kec. Parongpong	</option>
                                                <option>	Kec. Lembang	</option>
                                                <option>	Kec. Cisarua	</option>
                                                <option>	Kec. Cikalong Wetan	</option>
                                                <option>	Kec. Cipeundeuy	</option>
                                                <option>	Kec. Bandung Kulon	</option>
                                                <option>	Kec. Babakan Ciparay	</option>
                                                <option>	Kec. Bojong Loa Kaler	</option>
                                                <option>	Kec. Bojong Loa Kidul	</option>
                                                <option>	Kec. Astananyar	</option>
                                                <option>	Kec. Regol	</option>
                                                <option>	Kec. Lengkong	</option>
                                                <option>	Kec. Bandung Kidul	</option>
                                                <option>	Kec. Buah Batu	</option>
                                                <option>	Kec. Rancasari	</option>
                                                <option>	Kec. Cibiru	</option>
                                                <option>	Kec. Ujungberung	</option>
                                                <option>	Kec. Arcamanik	</option>
                                                <option>	Kec. Kiaracondong	</option>
                                                <option>	Kec. Batununggal	</option>
                                                <option>	Kec. Sumur Bandung	</option>
                                                <option>	Kec. Andir	</option>
                                                <option>	Kec. Cicendo	</option>
                                                <option>	Kec. Bandung Wetan	</option>
                                                <option>	Kec. Cibeunying Kidul	</option>
                                                <option>	Kec. Cibeunying Kaler	</option>
                                                <option>	Kec. Coblong	</option>
                                                <option>	Kec. Sukajadi	</option>
                                                <option>	Kec. Sukasari	</option>
                                                <option>	Kec. Cidadap	</option>
                                                <option>	Kec. Gedebage	</option>
                                                <option>	Kec. Panyileukan	</option>
                                                <option>	Kec. Cinambo	</option>
                                                <option>	Kec. Mandalajati	</option>
                                                <option>	Kec. Antapani	</option>
                                                <option>	Kec. Kota Bogor Selatan	</option>
                                                <option>	Kec. Kota Bogor Timur	</option>
                                                <option>	Kec. Kota Bogor Utara	</option>
                                                <option>	Kec. Kota Bogor Tengah	</option>
                                                <option>	Kec. Kota Bogor Barat	</option>
                                                <option>	Kec. Tanah Sereal	</option>
                                                <option>	Kec. Baros	</option>
                                                <option>	Kec. Citamiang	</option>
                                                <option>	Kec. Warudoyong	</option>
                                                <option>	Kec. Gunung Puyuh	</option>
                                                <option>	Kec. Cikole	</option>
                                                <option>	Kec. Lembur Situ	</option>
                                                <option>	Kec. Cibeureum	</option>
                                                <option>	Kec. Harjamukti	</option>
                                                <option>	Kec. Lemahwungkuk	</option>
                                                <option>	Kec. Pekalipan	</option>
                                                <option>	Kec. Kesambi	</option>
                                                <option>	Kec. Kejaksan	</option>
                                                <option>	Kec. Pondokgede	</option>
                                                <option>	Kec. Jatiasih	</option>
                                                <option>	Kec. Bantargebang	</option>
                                                <option>	Kec. Bekasi Timur	</option>
                                                <option>	Kec. Bekasi Selatan	</option>
                                                <option>	Kec. Bekasi Barat	</option>
                                                <option>	Kec. Bekasi Utara	</option>
                                                <option>	Kec. Jati Sampurna	</option>
                                                <option>	Kec. Medan Satria	</option>
                                                <option>	Kec. Rawalumbu	</option>
                                                <option>	Kec. Mustika Jaya	</option>
                                                <option>	Kec. Pondok Melati	</option>
                                                <option>	Kec. Sawangan	</option>
                                                <option>	Kec. Pancoran Mas	</option>
                                                <option>	Kec. Sukmajaya	</option>
                                                <option>	Kec. Cimanggis	</option>
                                                <option>	Kec. Beji	</option>
                                                <option>	Kec. Limo	</option>
                                                <option>	Kec. Cipayung	</option>
                                                <option>	Kec. Cilodong	</option>
                                                <option>	Kec. Cinere	</option>
                                                <option>	Kec. Tapos	</option>
                                                <option>	Kec. Bojongsari	</option>
                                                <option>	Kec. Cimahi Selatan	</option>
                                                <option>	Kec. Cimahi Tengah	</option>
                                                <option>	Kec. Cimahi Utara	</option>
                                                <option>	Kec. Cibeureum	</option>
                                                <option>	Kec. Tamansari	</option>
                                                <option>	Kec. Kawalu	</option>
                                                <option>	Kec. Mangkubumi	</option>
                                                <option>	Kec. Indihiang	</option>
                                                <option>	Kec. Cipedes	</option>
                                                <option>	Kec. Cihideung	</option>
                                                <option>	Kec. Tawang	</option>
                                                <option>	Kec. Purbaratu	</option>
                                                <option>	Kec. Bungursari	</option>
                                                <option>	Kec. Banjar	</option>
                                                <option>	Kec. Purwaharja	</option>
                                                <option>	Kec. Pataruman	</option>
                                                <option>	Kec. Langensari	</option>
                                                <option>	Kec. Dayeuhluhur	</option>
                                                <option>	Kec. Wanareja	</option>
                                                <option>	Kec. Majenang	</option>
                                                <option>	Kec. Cimanggu	</option>
                                                <option>	Kec. Karangpucung	</option>
                                                <option>	Kec. Cipari	</option>
                                                <option>	Kec. Sidareja	</option>
                                                <option>	Kec. Kedungreja	</option>
                                                <option>	Kec. Patimuan	</option>
                                                <option>	Kec. Gandrungmangu	</option>
                                                <option>	Kec. Bantarsari	</option>
                                                <option>	Kec. Kawunganten	</option>
                                                <option>	Kec. Kampung Laut	</option>
                                                <option>	Kec. Jeruklegi	</option>
                                                <option>	Kec. Kesugihan	</option>
                                                <option>	Kec. Adipala	</option>
                                                <option>	Kec. Maos	</option>
                                                <option>	Kec. Sampang	</option>
                                                <option>	Kec. Kroya	</option>
                                                <option>	Kec. Binangun	</option>
                                                <option>	Kec. Nusawungu	</option>
                                                <option>	Kec. Cilacap Selatan	</option>
                                                <option>	Kec. Cilacap Tengah	</option>
                                                <option>	Kec. Cilacap Utara	</option>
                                                <option>	Kec. Lumbir	</option>
                                                <option>	Kec. Wangon	</option>
                                                <option>	Kec. Jatilawang	</option>
                                                <option>	Kec. Rawalo	</option>
                                                <option>	Kec. Kebasen	</option>
                                                <option>	Kec. Kemranjen	</option>
                                                <option>	Kec. Sumpiuh	</option>
                                                <option>	Kec. Tambak	</option>
                                                <option>	Kec. Somagede	</option>
                                                <option>	Kec. Kalibagor	</option>
                                                <option>	Kec. Banyumas	</option>
                                                <option>	Kec. Patikraja	</option>
                                                <option>	Kec. Purwojati	</option>
                                                <option>	Kec. Ajibarang	</option>
                                                <option>	Kec. Gumelar	</option>
                                                <option>	Kec. Pekuncen	</option>
                                                <option>	Kec. Cilongok	</option>
                                                <option>	Kec. Karanglewas	</option>
                                                <option>	Kec. Kedung Banteng	</option>
                                                <option>	Kec. Baturaden	</option>
                                                <option>	Kec. Sumbang	</option>
                                                <option>	Kec. Kembaran	</option>
                                                <option>	Kec. Sokaraja	</option>
                                                <option>	Kec. Purwokerto Selatan	</option>
                                                <option>	Kec. Purwokerto Barat	</option>
                                                <option>	Kec. Purwokerto Timur	</option>
                                                <option>	Kec. Purwokerto Utara	</option>
                                                <option>	Kec. Kemangkon	</option>
                                                <option>	Kec. Bukateja	</option>
                                                <option>	Kec. Kejobong	</option>
                                                <option>	Kec. Pengadegan	</option>
                                                <option>	Kec. Kaligondang	</option>
                                                <option>	Kec. Purbalingga	</option>
                                                <option>	Kec. Kalimanah	</option>
                                                <option>	Kec. Padamara	</option>
                                                <option>	Kec. Kutasari	</option>
                                                <option>	Kec. Bojongsari	</option>
                                                <option>	Kec. Mrebet	</option>
                                                <option>	Kec. Bobotsari	</option>
                                                <option>	Kec. Karangreja	</option>
                                                <option>	Kec. Karanganyar	</option>
                                                <option>	Kec. Karangmoncol	</option>
                                                <option>	Kec. Rembang	</option>
                                                <option>	Kec. Karangjambu	</option>
                                                <option>	Kec. Kertanegara	</option>
                                                <option>	Kec. Susukan	</option>
                                                <option>	Kec. Purworejo/ Klampok	</option>
                                                <option>	Kec. Mandiraja	</option>
                                                <option>	Kec. Purwonegara	</option>
                                                <option>	Kec. Bawang	</option>
                                                <option>	Kec. Banjarnegara	</option>
                                                <option>	Kec. Sigaluh	</option>
                                                <option>	Kec. Madukara	</option>
                                                <option>	Kec. Banjarmangu	</option>
                                                <option>	Kec. Wanadadi	</option>
                                                <option>	Kec. Rakit	</option>
                                                <option>	Kec. Punggelan	</option>
                                                <option>	Kec. Karangkobar	</option>
                                                <option>	Kec. Pagentan	</option>
                                                <option>	Kec. Pejawaran	</option>
                                                <option>	Kec. Batur	</option>
                                                <option>	Kec. Wanayasa	</option>
                                                <option>	Kec. Kalibening	</option>
                                                <option>	Kec. Pandan Arum	</option>
                                                <option>	Kec. Pagedongan	</option>
                                                <option>	Kec. Ayah	</option>
                                                <option>	Kec. Buayan	</option>
                                                <option>	Kec. Puring	</option>
                                                <option>	Kec. Petanahan	</option>
                                                <option>	Kec. Klirong	</option>
                                                <option>	Kec. Bulupesantren	</option>
                                                <option>	Kec. Ambal	</option>
                                                <option>	Kec. Mirit	</option>
                                                <option>	Kec. Prembun	</option>
                                                <option>	Kec. Kutowinangun	</option>
                                                <option>	Kec. Alian	</option>
                                                <option>	Kec. Kebumen	</option>
                                                <option>	Kec. Pejagoan	</option>
                                                <option>	Kec. Sruweng	</option>
                                                <option>	Kec. Adimulyo	</option>
                                                <option>	Kec. Kuwarasan	</option>
                                                <option>	Kec. Rowokele	</option>
                                                <option>	Kec. Sempor	</option>
                                                <option>	Kec. Gombong	</option>
                                                <option>	Kec. Karanganyar	</option>
                                                <option>	Kec. Karangganyam	</option>
                                                <option>	Kec. Sadang	</option>
                                                <option>	Kec. Bonorowo	</option>
                                                <option>	Kec. Padureso	</option>
                                                <option>	Kec. Poncowarno	</option>
                                                <option>	Kec. Karangsambung	</option>
                                                <option>	Kec. Grabag	</option>
                                                <option>	Kec. Ngombol	</option>
                                                <option>	Kec. Purwodadi	</option>
                                                <option>	Kec. Bagelen	</option>
                                                <option>	Kec. Kaligesing	</option>
                                                <option>	Kec. Purworejo	</option>
                                                <option>	Kec. Banyu Urip	</option>
                                                <option>	Kec. Bayan	</option>
                                                <option>	Kec. Kutoarjo	</option>
                                                <option>	Kec. Butuh	</option>
                                                <option>	Kec. Pituruh	</option>
                                                <option>	Kec. Kemiri	</option>
                                                <option>	Kec. Bruno	</option>
                                                <option>	Kec. Gebang	</option>
                                                <option>	Kec. Loano	</option>
                                                <option>	Kec. Bener	</option>
                                                <option>	Kec. Wadaslintang	</option>
                                                <option>	Kec. Kepil	</option>
                                                <option>	Kec. Sapuran	</option>
                                                <option>	Kec. Kaliwiro	</option>
                                                <option>	Kec. Leksono	</option>
                                                <option>	Kec. Selomerto	</option>
                                                <option>	Kec. Kalikajar	</option>
                                                <option>	Kec. Kertek	</option>
                                                <option>	Kec. Wonosobo	</option>
                                                <option>	Kec. Watumalang	</option>
                                                <option>	Kec. Mojotengah	</option>
                                                <option>	Kec. Garung	</option>
                                                <option>	Kec. Kejajar	</option>
                                                <option>	Kec. Sukoharjo	</option>
                                                <option>	Kec. Kalibawang	</option>
                                                <option>	Kec. Salaman	</option>
                                                <option>	Kec. Borobudur	</option>
                                                <option>	Kec. Ngluwar	</option>
                                                <option>	Kec. Salam	</option>
                                                <option>	Kec. Srumbung	</option>
                                                <option>	Kec. Dukun	</option>
                                                <option>	Kec. Muntilan	</option>
                                                <option>	Kec. Mungkid	</option>
                                                <option>	Kec. Sawangan	</option>
                                                <option>	Kec. Candimulyo	</option>
                                                <option>	Kec. Martoyudan	</option>
                                                <option>	Kec. Tempuran	</option>
                                                <option>	Kec. Kajoran	</option>
                                                <option>	Kec. Kaliangkrik	</option>
                                                <option>	Kec. Bandongan	</option>
                                                <option>	Kec. Windusari	</option>
                                                <option>	Kec. Secang	</option>
                                                <option>	Kec. Tegalrejo	</option>
                                                <option>	Kec. Pakis	</option>
                                                <option>	Kec. Grabag	</option>
                                                <option>	Kec. Ngablak	</option>
                                                <option>	Kec. Selo	</option>
                                                <option>	Kec. Ampel	</option>
                                                <option>	Kec. Cepogo	</option>
                                                <option>	Kec. Musuk	</option>
                                                <option>	Kec. Boyolali	</option>
                                                <option>	Kec. Mojosongo	</option>
                                                <option>	Kec. Teras	</option>
                                                <option>	Kec. Sawit	</option>
                                                <option>	Kec. Banyudono	</option>
                                                <option>	Kec. Sambi	</option>
                                                <option>	Kec. Ngemplak	</option>
                                                <option>	Kec. Nogosari	</option>
                                                <option>	Kec. Simo	</option>
                                                <option>	Kec. Karanggede	</option>
                                                <option>	Kec. Klego	</option>
                                                <option>	Kec. Andong	</option>
                                                <option>	Kec. Kemusu	</option>
                                                <option>	Kec. Wonosegoro	</option>
                                                <option>	Kec. Juwangi	</option>
                                                <option>	Kec. Prambanan	</option>
                                                <option>	Kec. Gantiwarno	</option>
                                                <option>	Kec. Wedi	</option>
                                                <option>	Kec. Bayat	</option>
                                                <option>	Kec. Cawas	</option>
                                                <option>	Kec. Trucuk	</option>
                                                <option>	Kec. Kalikotes	</option>
                                                <option>	Kec. Kebonarum	</option>
                                                <option>	Kec. Jogonalan	</option>
                                                <option>	Kec. Manisrenggo	</option>
                                                <option>	Kec. Karangnongko	</option>
                                                <option>	Kec. Ngawen	</option>
                                                <option>	Kec. Ceper	</option>
                                                <option>	Kec. Pedan	</option>
                                                <option>	Kec. Karangdowo	</option>
                                                <option>	Kec. Juwiring	</option>
                                                <option>	Kec. Wonosari	</option>
                                                <option>	Kec. Delanggu	</option>
                                                <option>	Kec. Polanharjo	</option>
                                                <option>	Kec. Karanganom	</option>
                                                <option>	Kec. Tulung	</option>
                                                <option>	Kec. Jatinom	</option>
                                                <option>	Kec. Kemalang	</option>
                                                <option>	Kec. Klaten Selatan	</option>
                                                <option>	Kec. Klaten Tengah	</option>
                                                <option>	Kec. Klaten Utara	</option>
                                                <option>	Kec. Weru	</option>
                                                <option>	Kec. Bulu	</option>
                                                <option>	Kec. Tawangsari	</option>
                                                <option>	Kec. Sukoharjo	</option>
                                                <option>	Kec. Nguter	</option>
                                                <option>	Kec. Bendosari	</option>
                                                <option>	Kec. Polokarto	</option>
                                                <option>	Kec. Mojolaban	</option>
                                                <option>	Kec. Grogol	</option>
                                                <option>	Kec. Baki	</option>
                                                <option>	Kec. Gatak	</option>
                                                <option>	Kec. Kartasura	</option>
                                                <option>	Kec. Pracimantoro	</option>
                                                <option>	Kec. Paranggupito	</option>
                                                <option>	Kec. Giritontro	</option>
                                                <option>	Kec. Giriwoyo	</option>
                                                <option>	Kec. Batuwarno	</option>
                                                <option>	Kec. Karangtengah	</option>
                                                <option>	Kec. Tirtomoyo	</option>
                                                <option>	Kec. Nguntoronadi	</option>
                                                <option>	Kec. Baturetno	</option>
                                                <option>	Kec. Eromoko	</option>
                                                <option>	Kec. Wuryantoro	</option>
                                                <option>	Kec. Manyaran	</option>
                                                <option>	Kec. Selogiri	</option>
                                                <option>	Kec. Wonogiri	</option>
                                                <option>	Kec. Ngadirojo	</option>
                                                <option>	Kec. Sidoharjo	</option>
                                                <option>	Kec. Jatiroto	</option>
                                                <option>	Kec. Kismantoro	</option>
                                                <option>	Kec. Purwantoro	</option>
                                                <option>	Kec. Bulukerto	</option>
                                                <option>	Kec. Slogohimo	</option>
                                                <option>	Kec. Jatisrono	</option>
                                                <option>	Kec. Jatipurno	</option>
                                                <option>	Kec. Girimarto	</option>
                                                <option>	Kec. Puhpelem	</option>
                                                <option>	Kec. Jatipuro	</option>
                                                <option>	Kec. Jatiyoso	</option>
                                                <option>	Kec. Jumapolo	</option>
                                                <option>	Kec. Jumantono	</option>
                                                <option>	Kec. Matesih	</option>
                                                <option>	Kec. Tawangmangu	</option>
                                                <option>	Kec. Ngargoyoso	</option>
                                                <option>	Kec. Karangpandan	</option>
                                                <option>	Kec. Karanganyar	</option>
                                                <option>	Kec. Tasikmadu	</option>
                                                <option>	Kec. Jaten	</option>
                                                <option>	Kec. Colomadu	</option>
                                                <option>	Kec. Gondangrejo	</option>
                                                <option>	Kec. Kebakkramat	</option>
                                                <option>	Kec. Mojogedang	</option>
                                                <option>	Kec. Kerjo	</option>
                                                <option>	Kec. Jenawi	</option>
                                                <option>	Kec. Kalijambe	</option>
                                                <option>	Kec. Plupuh	</option>
                                                <option>	Kec. Masaran	</option>
                                                <option>	Kec. Kedawung	</option>
                                                <option>	Kec. Sambirejo	</option>
                                                <option>	Kec. Gondang	</option>
                                                <option>	Kec. Sambung Macan	</option>
                                                <option>	Kec. Ngrampal	</option>
                                                <option>	Kec. Karangmalang	</option>
                                                <option>	Kec. Sragen	</option>
                                                <option>	Kec. Sidoharjo	</option>
                                                <option>	Kec. Tanon	</option>
                                                <option>	Kec. Gemolong	</option>
                                                <option>	Kec. Miri	</option>
                                                <option>	Kec. Sumberlawang	</option>
                                                <option>	Kec. Mondokan	</option>
                                                <option>	Kec. Sukodono	</option>
                                                <option>	Kec. Gesi	</option>
                                                <option>	Kec. Tangen	</option>
                                                <option>	Kec. Jenar	</option>
                                                <option>	Kec. Kedungjati	</option>
                                                <option>	Kec. Karangrayung	</option>
                                                <option>	Kec. Penawangan	</option>
                                                <option>	Kec. Toroh	</option>
                                                <option>	Kec. Geyer	</option>
                                                <option>	Kec. Pulokulon	</option>
                                                <option>	Kec. Kradenan	</option>
                                                <option>	Kec. Gabus	</option>
                                                <option>	Kec. Ngaringan	</option>
                                                <option>	Kec. Wirosari	</option>
                                                <option>	Kec. Tawangharjo	</option>
                                                <option>	Kec. Grobogan	</option>
                                                <option>	Kec. Purwodadi	</option>
                                                <option>	Kec. Brati	</option>
                                                <option>	Kec. Klambu	</option>
                                                <option>	Kec. Godong	</option>
                                                <option>	Kec. Gubug	</option>
                                                <option>	Kec. Tegowanu	</option>
                                                <option>	Kec. Tanggungharjo	</option>
                                                <option>	Kec. Jati	</option>
                                                <option>	Kec. Randublatung	</option>
                                                <option>	Kec. Kradenan	</option>
                                                <option>	Kec. Kedungtuban	</option>
                                                <option>	Kec. Cepu	</option>
                                                <option>	Kec. Sambong	</option>
                                                <option>	Kec. Jiken	</option>
                                                <option>	Kec. Bogorejo	</option>
                                                <option>	Kec. Jepon	</option>
                                                <option>	Kec. Kota Blora	</option>
                                                <option>	Kec. Banjarejo	</option>
                                                <option>	Kec. Tunjungan	</option>
                                                <option>	Kec. Japah	</option>
                                                <option>	Kec. Ngawen	</option>
                                                <option>	Kec. Kunduran	</option>
                                                <option>	Kec. Todanan	</option>
                                                <option>	Kec. Sumber	</option>
                                                <option>	Kec. Bulu	</option>
                                                <option>	Kec. Gunem	</option>
                                                <option>	Kec. Sale	</option>
                                                <option>	Kec. Sarang	</option>
                                                <option>	Kec. Sedan	</option>
                                                <option>	Kec. Pamotan	</option>
                                                <option>	Kec. Sulang	</option>
                                                <option>	Kec. Kaliori	</option>
                                                <option>	Kec. Rembang	</option>
                                                <option>	Kec. Pancur	</option>
                                                <option>	Kec. Kragan	</option>
                                                <option>	Kec. Sluke	</option>
                                                <option>	Kec. Lasem	</option>
                                                <option>	Kec. Sukolilo	</option>
                                                <option>	Kec. Kayen	</option>
                                                <option>	Kec. Tambakromo	</option>
                                                <option>	Kec. Winong	</option>
                                                <option>	Kec. Pucakwangi	</option>
                                                <option>	Kec. Jaken	</option>
                                                <option>	Kec. Batangan	</option>
                                                <option>	Kec. Juwana	</option>
                                                <option>	Kec. Jakenan	</option>
                                                <option>	Kec. Pati	</option>
                                                <option>	Kec. Gabus	</option>
                                                <option>	Kec. Margorejo	</option>
                                                <option>	Kec. Gembong	</option>
                                                <option>	Kec. Tlogowungu	</option>
                                                <option>	Kec. Wedarijaksa	</option>
                                                <option>	Kec. Trangkil	</option>
                                                <option>	Kec. Margoyoso	</option>
                                                <option>	Kec. Gunung Wungkal	</option>
                                                <option>	Kec. Cluwak	</option>
                                                <option>	Kec. Tayu	</option>
                                                <option>	Kec. Dukuhseti	</option>
                                                <option>	Kec. Kaliwungu	</option>
                                                <option>	Kec. Kota Kudus	</option>
                                                <option>	Kec. Jati	</option>
                                                <option>	Kec. Undaan	</option>
                                                <option>	Kec. Mejobo	</option>
                                                <option>	Kec. Jekulo	</option>
                                                <option>	Kec. Bae	</option>
                                                <option>	Kec. Gebog	</option>
                                                <option>	Kec. Dawe	</option>
                                                <option>	Kec. Kedung	</option>
                                                <option>	Kec. Pecangaan	</option>
                                                <option>	Kec. Welahan	</option>
                                                <option>	Kec. Mayong	</option>
                                                <option>	Kec. Nalumsari	</option>
                                                <option>	Kec. Batealit	</option>
                                                <option>	Kec. Tahunan	</option>
                                                <option>	Kec. Jepara	</option>
                                                <option>	Kec. Mlonggo	</option>
                                                <option>	Kec. Bangsri	</option>
                                                <option>	Kec. Keling	</option>
                                                <option>	Kec. Karimunjawa	</option>
                                                <option>	Kec. Kalinyamatan	</option>
                                                <option>	Kec. Kembang	</option>
                                                <option>	Kec. Donorojo	</option>
                                                <option>	Kec. Pakis Aji	</option>
                                                <option>	Kec. Mranggen	</option>
                                                <option>	Kec. Karangawen	</option>
                                                <option>	Kec. Guntur	</option>
                                                <option>	Kec. Sayung	</option>
                                                <option>	Kec. Karang Tengah	</option>
                                                <option>	Kec. Bonang	</option>
                                                <option>	Kec. Demak	</option>
                                                <option>	Kec. Wonosalam	</option>
                                                <option>	Kec. Dempet	</option>
                                                <option>	Kec. Gajah	</option>
                                                <option>	Kec. Karanganyar	</option>
                                                <option>	Kec. Mijen	</option>
                                                <option>	Kec. Wedung	</option>
                                                <option>	Kec. Kebonagung	</option>
                                                <option>	Kec. Getasan	</option>
                                                <option>	Kec. Tengaran	</option>
                                                <option>	Kec. Susukan	</option>
                                                <option>	Kec. Suruh	</option>
                                                <option>	Kec. Pabelan	</option>
                                                <option>	Kec. Tuntang	</option>
                                                <option>	Kec. Banyubiru	</option>
                                                <option>	Kec. Jambu	</option>
                                                <option>	Kec. Sumowono	</option>
                                                <option>	Kec. Ambarawa	</option>
                                                <option>	Kec. Bawen	</option>
                                                <option>	Kec. Bringin	</option>
                                                <option>	Kec. Pringapus	</option>
                                                <option>	Kec. Bergas	</option>
                                                <option>	Kec. Kaliwungu	</option>
                                                <option>	Kec. Bancak	</option>
                                                <option>	Kec. Ungaran Barat	</option>
                                                <option>	Kec. Ungaran Timur	</option>
                                                <option>	Kec. Bandungan	</option>
                                                <option>	Kec. Parakan	</option>
                                                <option>	Kec. Bulu	</option>
                                                <option>	Kec. Temanggung	</option>
                                                <option>	Kec. Tembarak	</option>
                                                <option>	Kec. Kranggan	</option>
                                                <option>	Kec. Pringsurat	</option>
                                                <option>	Kec. Kaloran	</option>
                                                <option>	Kec. Kandangan	</option>
                                                <option>	Kec. Kedu	</option>
                                                <option>	Kec. Ngadirejo	</option>
                                                <option>	Kec. Jumo	</option>
                                                <option>	Kec. Candiroto	</option>
                                                <option>	Kec. Tretep	</option>
                                                <option>	Kec. Kledung	</option>
                                                <option>	Kec. Bansari	</option>
                                                <option>	Kec. Tlogomulyo	</option>
                                                <option>	Kec. Selopampang	</option>
                                                <option>	Kec. Gemawang	</option>
                                                <option>	Kec. Bejen	</option>
                                                <option>	Kec. Wonoboyo	</option>
                                                <option>	Kec. Plantungan	</option>
                                                <option>	Kec. Sukorejo	</option>
                                                <option>	Kec. Pegeruyung	</option>
                                                <option>	Kec. Patean	</option>
                                                <option>	Kec. Singorojo	</option>
                                                <option>	Kec. Limbangan	</option>
                                                <option>	Kec. Boja	</option>
                                                <option>	Kec. Kaliwungu	</option>
                                                <option>	Kec. Brangsong	</option>
                                                <option>	Kec. Pegandon	</option>
                                                <option>	Kec. Gemuh	</option>
                                                <option>	Kec. Waleri	</option>
                                                <option>	Kec. Rowosari	</option>
                                                <option>	Kec. Kangkung	</option>
                                                <option>	Kec. Cipiring	</option>
                                                <option>	Kec. Patebon	</option>
                                                <option>	Kec. Kota Kendal	</option>
                                                <option>	Kec. Ngampel	</option>
                                                <option>	Kec. Ringinarum	</option>
                                                <option>	Kec. Kaliwungu Selatan	</option>
                                                <option>	Kec. Wonotunggal	</option>
                                                <option>	Kec. Bandar	</option>
                                                <option>	Kec. Blado	</option>
                                                <option>	Kec. Reban	</option>
                                                <option>	Kec. Bawang	</option>
                                                <option>	Kec. Tersono	</option>
                                                <option>	Kec. Gringsing	</option>
                                                <option>	Kec. Limpung	</option>
                                                <option>	Kec. Subah	</option>
                                                <option>	Kec. Tulis	</option>
                                                <option>	Kec. Batang	</option>
                                                <option>	Kec. Warung Asem	</option>
                                                <option>	Kec. Banyuputih	</option>
                                                <option>	Kec. Pecalungan	</option>
                                                <option>	Kec. Kandeman	</option>
                                                <option>	Kec. Kandang Serang	</option>
                                                <option>	Kec. Peninggaran	</option>
                                                <option>	Kec. Lebakbarang	</option>
                                                <option>	Kec. Petungkriono	</option>
                                                <option>	Kec. Talun	</option>
                                                <option>	Kec. Doro	</option>
                                                <option>	Kec. Karanganyar	</option>
                                                <option>	Kec. Kajen	</option>
                                                <option>	Kec. Kesesi	</option>
                                                <option>	Kec. Sragi	</option>
                                                <option>	Kec. Bojong	</option>
                                                <option>	Kec. Wonopringgo	</option>
                                                <option>	Kec. Kedungwuni	</option>
                                                <option>	Kec. Buaran	</option>
                                                <option>	Kec. Tirto	</option>
                                                <option>	Kec. Wiradesa	</option>
                                                <option>	Kec. Siwalan	</option>
                                                <option>	Kec. Karangdadap	</option>
                                                <option>	Kec. Wonokerto	</option>
                                                <option>	Kec. Moga	</option>
                                                <option>	Kec. Pulosari	</option>
                                                <option>	Kec. Belik	</option>
                                                <option>	Kec. Watukumpul	</option>
                                                <option>	Kec. Bodeh	</option>
                                                <option>	Kec. Bantarbolang	</option>
                                                <option>	Kec. Randudongkel	</option>
                                                <option>	Kec. Pemalang	</option>
                                                <option>	Kec. Taman	</option>
                                                <option>	Kec. Petarukan	</option>
                                                <option>	Kec. Ampelgading	</option>
                                                <option>	Kec. Comal	</option>
                                                <option>	Kec. Ulujami	</option>
                                                <option>	Kec. Warungpring	</option>
                                                <option>	Kec. Margasari	</option>
                                                <option>	Kec. Bumijawa	</option>
                                                <option>	Kec. Bojong	</option>
                                                <option>	Kec. Balapulang	</option>
                                                <option>	Kec. Pagerbarang	</option>
                                                <option>	Kec. Lebaksiu	</option>
                                                <option>	Kec. Jatinegara	</option>
                                                <option>	Kec. Kedung Banteng	</option>
                                                <option>	Kec. Pangkah	</option>
                                                <option>	Kec. Slawi	</option>
                                                <option>	Kec. Dukuhwaru	</option>
                                                <option>	Kec. Adiwerna	</option>
                                                <option>	Kec. Dukuhturi	</option>
                                                <option>	Kec. Talang	</option>
                                                <option>	Kec. Tarub	</option>
                                                <option>	Kec. Kramat	</option>
                                                <option>	Kec. Suradadi	</option>
                                                <option>	Kec. Warureja	</option>
                                                <option>	Kec. Salem	</option>
                                                <option>	Kec. Bantarkawung	</option>
                                                <option>	Kec. Bumiayu	</option>
                                                <option>	Kec. Paguyangan	</option>
                                                <option>	Kec. Sirampog	</option>
                                                <option>	Kec. Tonjong	</option>
                                                <option>	Kec. Larangan	</option>
                                                <option>	Kec. Ketanggungan	</option>
                                                <option>	Kec. Banjarharjo	</option>
                                                <option>	Kec. Losari	</option>
                                                <option>	Kec. Tanjung	</option>
                                                <option>	Kec. Kersana	</option>
                                                <option>	Kec. Bulakamba	</option>
                                                <option>	Kec. Wanasari	</option>
                                                <option>	Kec. Songgom	</option>
                                                <option>	Kec. Jatibarang	</option>
                                                <option>	Kec. Brebes	</option>
                                                <option>	Kec. Magelang Selatan	</option>
                                                <option>	Kec. Magelang Utara	</option>
                                                <option>	Kec. Magelang Tengah	</option>
                                                <option>	Kec. Laweyan	</option>
                                                <option>	Kec. Serengan	</option>
                                                <option>	Kec. Pasarkliwon	</option>
                                                <option>	Kec. Jebres	</option>
                                                <option>	Kec. Banjarsari	</option>
                                                <option>	Kec. Argomulyo	</option>
                                                <option>	Kec. Tingkir	</option>
                                                <option>	Kec. Sidomukti	</option>
                                                <option>	Kec. Sidorejo	</option>
                                                <option>	Kec. Mijen	</option>
                                                <option>	Kec. Gunung Pati	</option>
                                                <option>	Kec. Banyumanik	</option>
                                                <option>	Kec. Gajah Mungkur	</option>
                                                <option>	Kec. Semarang Selatan	</option>
                                                <option>	Kec. Candisari	</option>
                                                <option>	Kec. Tembalang	</option>
                                                <option>	Kec. Pedurungan	</option>
                                                <option>	Kec. Genuk	</option>
                                                <option>	Kec. Gayamsari	</option>
                                                <option>	Kec. Semarang Timur	</option>
                                                <option>	Kec. Semarang Tengah	</option>
                                                <option>	Kec. Semarang Utara	</option>
                                                <option>	Kec. Semarang Barat	</option>
                                                <option>	Kec. Tugu	</option>
                                                <option>	Kec. Ngaliyan	</option>
                                                <option>	Kec. Pekalongan Barat	</option>
                                                <option>	Kec. Pekalongan Timur	</option>
                                                <option>	Kec. Pekalongan Selatan	</option>
                                                <option>	Kec. Pekalongan Utara	</option>
                                                <option>	Kec. Tegal Selatan	</option>
                                                <option>	Kec. Tegal Timur	</option>
                                                <option>	Kec. Tegal Barat	</option>
                                                <option>	Kec. Margadana	</option>
                                                <option>	Kec. Srandakan	</option>
                                                <option>	Kec. Sanden	</option>
                                                <option>	Kec. Kretek	</option>
                                                <option>	Kec. Pundong	</option>
                                                <option>	Kec. Bambang Lipuro	</option>
                                                <option>	Kec. Pandak	</option>
                                                <option>	Kec. Bantul	</option>
                                                <option>	Kec. Jetis	</option>
                                                <option>	Kec. Imogiri	</option>
                                                <option>	Kec. Dlingo	</option>
                                                <option>	Kec. Pleret	</option>
                                                <option>	Kec. Piyungan	</option>
                                                <option>	Kec. Banguntapan	</option>
                                                <option>	Kec. Sewon	</option>
                                                <option>	Kec. Kasihan	</option>
                                                <option>	Kec. Pajangan	</option>
                                                <option>	Kec. Sedayu	</option>
                                                <option>	Kec. Moyudan	</option>
                                                <option>	Kec. Minggir	</option>
                                                <option>	Kec. Seyegan	</option>
                                                <option>	Kec. Godean	</option>
                                                <option>	Kec. Gamping	</option>
                                                <option>	Kec. Mlati	</option>
                                                <option>	Kec. Depok	</option>
                                                <option>	Kec. Berbah	</option>
                                                <option>	Kec. Prambanan	</option>
                                                <option>	Kec. Kalasan	</option>
                                                <option>	Kec. Ngemplak	</option>
                                                <option>	Kec. Ngaglik	</option>
                                                <option>	Kec. Sleman	</option>
                                                <option>	Kec. Tempel	</option>
                                                <option>	Kec. Turi	</option>
                                                <option>	Kec. Pekem	</option>
                                                <option>	Kec. Cangkringan	</option>
                                                <option>	Kec. Panggang	</option>
                                                <option>	Kec. Paliyan	</option>
                                                <option>	Kec. Sapto Sari	</option>
                                                <option>	Kec. Tepus	</option>
                                                <option>	Kec. Rongkop	</option>
                                                <option>	Kec. Semanu	</option>
                                                <option>	Kec. Ponjong	</option>
                                                <option>	Kec. Karangmojo	</option>
                                                <option>	Kec. Wonosari	</option>
                                                <option>	Kec. Playen	</option>
                                                <option>	Kec. Patuk	</option>
                                                <option>	Kec. Gedang Sari	</option>
                                                <option>	Kec. Nglipar	</option>
                                                <option>	Kec. Ngawen	</option>
                                                <option>	Kec. Semin	</option>
                                                <option>	Kec. Purwosari	</option>
                                                <option>	Kec. Girisubo	</option>
                                                <option>	Kec. Tanjungsari	</option>
                                                <option>	Kec. Temon	</option>
                                                <option>	Kec. Wates	</option>
                                                <option>	Kec. Panjatan	</option>
                                                <option>	Kec. Galur	</option>
                                                <option>	Kec. Lendah	</option>
                                                <option>	Kec. Sentolo	</option>
                                                <option>	Kec. Pengasih	</option>
                                                <option>	Kec. Kokap	</option>
                                                <option>	Kec. Girimulyo	</option>
                                                <option>	Kec. Nanggulan	</option>
                                                <option>	Kec. Kalibawang	</option>
                                                <option>	Kec. Samigaluh	</option>
                                                <option>	Kec. Mantrijeron	</option>
                                                <option>	Kec. Kraton	</option>
                                                <option>	Kec. Mergangsan	</option>
                                                <option>	Kec. Umbulharjo	</option>
                                                <option>	Kec. Kotagede	</option>
                                                <option>	Kec. Gondokusuman	</option>
                                                <option>	Kec. Danurejan	</option>
                                                <option>	Kec. Pakualaman	</option>
                                                <option>	Kec. Gondomanan	</option>
                                                <option>	Kec. Ngampilan	</option>
                                                <option>	Kec. Wirobrajan	</option>
                                                <option>	Kec. Gedongtengen	</option>
                                                <option>	Kec. Jetis	</option>
                                                <option>	Kec. Tegalrejo	</option>
                                                <option>	Kec. Wringin Anom	</option>
                                                <option>	Kec. Driyorejo	</option>
                                                <option>	Kec. Kedamean	</option>
                                                <option>	Kec. Menganti	</option>
                                                <option>	Kec. Cerme	</option>
                                                <option>	Kec. Benjeng	</option>
                                                <option>	Kec. Balong Panggang	</option>
                                                <option>	Kec. Duduk Sampeyan	</option>
                                                <option>	Kec. Kebomas	</option>
                                                <option>	Kec. Gresik	</option>
                                                <option>	Kec. Manyar	</option>
                                                <option>	Kec. Bungah	</option>
                                                <option>	Kec. Sidayu	</option>
                                                <option>	Kec. Dukun	</option>
                                                <option>	Kec. Panceng	</option>
                                                <option>	Kec. Ujung Pangkah	</option>
                                                <option>	Kec. Sangkapura	</option>
                                                <option>	Kec. Tambak	</option>
                                                <option>	Kec. Tarik	</option>
                                                <option>	Kec. Prambon	</option>
                                                <option>	Kec. Krembung	</option>
                                                <option>	Kec. Porong	</option>
                                                <option>	Kec. Jabon	</option>
                                                <option>	Kec. Tanggulangin	</option>
                                                <option>	Kec. Candi	</option>
                                                <option>	Kec. Tulangan	</option>
                                                <option>	Kec. Wonoayu	</option>
                                                <option>	Kec. Sukodono	</option>
                                                <option>	Kec. Sidoarjo	</option>
                                                <option>	Kec. Buduran	</option>
                                                <option>	Kec. Sedati	</option>
                                                <option>	Kec. Waru	</option>
                                                <option>	Kec. Gedangan	</option>
                                                <option>	Kec. Taman	</option>
                                                <option>	Kec. Krian	</option>
                                                <option>	Kec. Balong Bendo	</option>
                                                <option>	Kec. Jatirejo	</option>
                                                <option>	Kec. Gondang	</option>
                                                <option>	Kec. Pacet	</option>
                                                <option>	Kec. Trawas	</option>
                                                <option>	Kec. Ngoro	</option>
                                                <option>	Kec. Pungging	</option>
                                                <option>	Kec. Kutorejo	</option>
                                                <option>	Kec. Mojosari	</option>
                                                <option>	Kec. Bangsal	</option>
                                                <option>	Kec. Dlanggu	</option>
                                                <option>	Kec. Puri	</option>
                                                <option>	Kec. Trowulan	</option>
                                                <option>	Kec. Sooko	</option>
                                                <option>	Kec. Gedek	</option>
                                                <option>	Kec. Kemlagi	</option>
                                                <option>	Kec. Jetis	</option>
                                                <option>	Kec. Dawar Blandong	</option>
                                                <option>	Kec. Mojoanyar	</option>
                                                <option>	Kec. Bandar Kedung Mulyo	</option>
                                                <option>	Kec. Perak	</option>
                                                <option>	Kec. Gudo	</option>
                                                <option>	Kec. Diwek	</option>
                                                <option>	Kec. Ngoro	</option>
                                                <option>	Kec. Mojowarno	</option>
                                                <option>	Kec. Bareng	</option>
                                                <option>	Kec. Wonosalam	</option>
                                                <option>	Kec. Mojoagung	</option>
                                                <option>	Kec. Somobito	</option>
                                                <option>	Kec. Jogo Roto	</option>
                                                <option>	Kec. Peterongan	</option>
                                                <option>	Kec. Jombang	</option>
                                                <option>	Kec. Megaluh	</option>
                                                <option>	Kec. Tembelang	</option>
                                                <option>	Kec. Kesamben	</option>
                                                <option>	Kec. Kudu	</option>
                                                <option>	Kec. Ploso	</option>
                                                <option>	Kec. Kabuh	</option>
                                                <option>	Kec. Plandaan	</option>
                                                <option>	Kec. Ngusikan	</option>
                                                <option>	Kec. Margomulyo	</option>
                                                <option>	Kec. Ngraho	</option>
                                                <option>	Kec. Tambakrejo	</option>
                                                <option>	Kec. Ngambon	</option>
                                                <option>	Kec. Bubulan	</option>
                                                <option>	Kec. Temayang	</option>
                                                <option>	Kec. Sugihwaras	</option>
                                                <option>	Kec. Kedungadem	</option>
                                                <option>	Kec. Kepoh Baru	</option>
                                                <option>	Kec. Baureno	</option>
                                                <option>	Kec. Kanor	</option>
                                                <option>	Kec. Sumberrejo	</option>
                                                <option>	Kec. Balen	</option>
                                                <option>	Kec. Sukosewu	</option>
                                                <option>	Kec. Kapas	</option>
                                                <option>	Kec. Bojonegoro	</option>
                                                <option>	Kec. Trucuk	</option>
                                                <option>	Kec. Dander	</option>
                                                <option>	Kec. Ngasem	</option>
                                                <option>	Kec. Kalitidu	</option>
                                                <option>	Kec. Malo	</option>
                                                <option>	Kec. Purwosari	</option>
                                                <option>	Kec. Padangan	</option>
                                                <option>	Kec. Kasiman	</option>
                                                <option>	Kec. Kedewan	</option>
                                                <option>	Kec. Gondang	</option>
                                                <option>	Kec. Sekar	</option>
                                                <option>	Kec. Kenduruan	</option>
                                                <option>	Kec. Bangilan	</option>
                                                <option>	Kec. Senori	</option>
                                                <option>	Kec. Singgahan	</option>
                                                <option>	Kec. Montong	</option>
                                                <option>	Kec. Parengan	</option>
                                                <option>	Kec. Soko	</option>
                                                <option>	Kec. Rengel	</option>
                                                <option>	Kec. Plumpang	</option>
                                                <option>	Kec. Widang	</option>
                                                <option>	Kec. Palang	</option>
                                                <option>	Kec. Semanding	</option>
                                                <option>	Kec. Tuban	</option>
                                                <option>	Kec. Jenu	</option>
                                                <option>	Kec. Merakurak	</option>
                                                <option>	Kec. Kerek	</option>
                                                <option>	Kec. Tambakboyo	</option>
                                                <option>	Kec. Jatirogo	</option>
                                                <option>	Kec. Bancar	</option>
                                                <option>	Kec. Grabagan	</option>
                                                <option>	Kec. Sukorame	</option>
                                                <option>	Kec. Bluluk	</option>
                                                <option>	Kec. Ngimbang	</option>
                                                <option>	Kec. Sambeng	</option>
                                                <option>	Kec. Mantup	</option>
                                                <option>	Kec. Kambang Bahu	</option>
                                                <option>	Kec. Sugio	</option>
                                                <option>	Kec. Kedungpring	</option>
                                                <option>	Kec. Modo	</option>
                                                <option>	Kec. Babat	</option>
                                                <option>	Kec. Pucuk	</option>
                                                <option>	Kec. Sukodadi	</option>
                                                <option>	Kec. Lamongan	</option>
                                                <option>	Kec. Tikung	</option>
                                                <option>	Kec. Deket	</option>
                                                <option>	Kec. Glagah	</option>
                                                <option>	Kec. Karangbinangun	</option>
                                                <option>	Kec. Turi	</option>
                                                <option>	Kec. Kalitengah	</option>
                                                <option>	Kec. Karang Geneng	</option>
                                                <option>	Kec. Sekaran	</option>
                                                <option>	Kec. Maduran	</option>
                                                <option>	Kec. Laren	</option>
                                                <option>	Kec. Solokuro	</option>
                                                <option>	Kec. Paciran	</option>
                                                <option>	Kec. Brondong	</option>
                                                <option>	Kec. Sarirejo	</option>
                                                <option>	Kec. Kebonsari	</option>
                                                <option>	Kec. Geger	</option>
                                                <option>	Kec. Dolopo	</option>
                                                <option>	Kec. Dagangan	</option>
                                                <option>	Kec. Wungu	</option>
                                                <option>	Kec. Kare	</option>
                                                <option>	Kec. Gemarang	</option>
                                                <option>	Kec. Saradan	</option>
                                                <option>	Kec. Pilangkenceng	</option>
                                                <option>	Kec. Mejayan	</option>
                                                <option>	Kec. Wonoasri	</option>
                                                <option>	Kec. Balerejo	</option>
                                                <option>	Kec. Madiun	</option>
                                                <option>	Kec. Sawahan	</option>
                                                <option>	Kec. Jiwan	</option>
                                                <option>	Kec. Sine	</option>
                                                <option>	Kec. Ngrambe	</option>
                                                <option>	Kec. Jogorogo	</option>
                                                <option>	Kec. Kendal	</option>
                                                <option>	Kec. Geneng	</option>
                                                <option>	Kec. Kwadungan	</option>
                                                <option>	Kec. Pangkur	</option>
                                                <option>	Kec. Karangjati	</option>
                                                <option>	Kec. Bringin	</option>
                                                <option>	Kec. Padas	</option>
                                                <option>	Kec. Ngawi	</option>
                                                <option>	Kec. Paron	</option>
                                                <option>	Kec. Kedunggalar	</option>
                                                <option>	Kec. Pitu	</option>
                                                <option>	Kec. Widodaren	</option>
                                                <option>	Kec. Mantingan	</option>
                                                <option>	Kec. Karanganyar	</option>
                                                <option>	Kec. Gerih	</option>
                                                <option>	Kec. Kasreman	</option>
                                                <option>	Kec. Poncol	</option>
                                                <option>	Kec. Parang	</option>
                                                <option>	Kec. Lembeyan	</option>
                                                <option>	Kec. Takeran	</option>
                                                <option>	Kec. Kawedanan	</option>
                                                <option>	Kec. Magetan	</option>
                                                <option>	Kec. Plaosan	</option>
                                                <option>	Kec. Panekan	</option>
                                                <option>	Kec. Sukomoro	</option>
                                                <option>	Kec. Bendo	</option>
                                                <option>	Kec. Maospati	</option>
                                                <option>	Kec. Karangrejo	</option>
                                                <option>	Kec. Barat	</option>
                                                <option>	Kec. Kartoharjo	</option>
                                                <option>	Kec. Karas	</option>
                                                <option>	Kec. Ngariboyo	</option>
                                                <option>	Kec. Nguntoronadi	</option>
                                                <option>	Kec. Sidorejo	</option>
                                                <option>	Kec. Ngrayun	</option>
                                                <option>	Kec. Slahung	</option>
                                                <option>	Kec. Bungkal	</option>
                                                <option>	Kec. Sambit	</option>
                                                <option>	Kec. Sawoo	</option>
                                                <option>	Kec. Sooko	</option>
                                                <option>	Kec. Pulung	</option>
                                                <option>	Kec. Mlarak	</option>
                                                <option>	Kec. Siman	</option>
                                                <option>	Kec. Jetis	</option>
                                                <option>	Kec. Balong	</option>
                                                <option>	Kec. Kauman	</option>
                                                <option>	Kec. Jambon	</option>
                                                <option>	Kec. Badegan	</option>
                                                <option>	Kec. Sampung	</option>
                                                <option>	Kec. Sukorejo	</option>
                                                <option>	Kec. Ponorogo	</option>
                                                <option>	Kec. Babadan	</option>
                                                <option>	Kec. Jenangan	</option>
                                                <option>	Kec. Ngebel	</option>
                                                <option>	Kec. Pudak	</option>
                                                <option>	Kec. Donorejo	</option>
                                                <option>	Kec. Punung	</option>
                                                <option>	Kec. Pringkuku	</option>
                                                <option>	Kec. Pacitan	</option>
                                                <option>	Kec. Kebon Agung	</option>
                                                <option>	Kec. Arjosari	</option>
                                                <option>	Kec. Nawangan	</option>
                                                <option>	Kec. Bandar	</option>
                                                <option>	Kec. Tegalombo	</option>
                                                <option>	Kec. Tulakan	</option>
                                                <option>	Kec. Ngadirojo	</option>
                                                <option>	Kec. Sudimoro	</option>
                                                <option>	Kec. Kras	</option>
                                                <option>	Kec. Ringinrejo	</option>
                                                <option>	Kec. Ngancar	</option>
                                                <option>	Kec. Kepung	</option>
                                                <option>	Kec. Puncu	</option>
                                                <option>	Kec. Ploso Klaten	</option>
                                                <option>	Kec. Wates	</option>
                                                <option>	Kec. Kandat	</option>
                                                <option>	Kec. Ngadiluwih	</option>
                                                <option>	Kec. Mojo	</option>
                                                <option>	Kec. Semen	</option>
                                                <option>	Kec. Banyakan	</option>
                                                <option>	Kec. Tarokan	</option>
                                                <option>	Kec. Grogol	</option>
                                                <option>	Kec. Gampengrejo	</option>
                                                <option>	Kec. Gurah	</option>
                                                <option>	Kec. Pagu	</option>
                                                <option>	Kec. Papar	</option>
                                                <option>	Kec. Plemahan	</option>
                                                <option>	Kec. Purwoasri	</option>
                                                <option>	Kec. Kunjang	</option>
                                                <option>	Kec. Pare	</option>
                                                <option>	Kec. Kandangan	</option>
                                                <option>	Kec. Kayen Kidul	</option>
                                                <option>	Kec. Ngasem	</option>
                                                <option>	Kec. Badas	</option>
                                                <option>	Kec. Sawahan	</option>
                                                <option>	Kec. Ngetos	</option>
                                                <option>	Kec. Berbek	</option>
                                                <option>	Kec. Loceret	</option>
                                                <option>	Kec. Pace	</option>
                                                <option>	Kec. Tanjunganom	</option>
                                                <option>	Kec. Prambon	</option>
                                                <option>	Kec. Ngronggot	</option>
                                                <option>	Kec. Kertosono	</option>
                                                <option>	Kec. Patianrowo	</option>
                                                <option>	Kec. Baron	</option>
                                                <option>	Kec. Gondang	</option>
                                                <option>	Kec. Sukomoro	</option>
                                                <option>	Kec. Nganjuk	</option>
                                                <option>	Kec. Bagor	</option>
                                                <option>	Kec. Wilangan	</option>
                                                <option>	Kec. Rejoso	</option>
                                                <option>	Kec. Ngluyu	</option>
                                                <option>	Kec. Lengkong	</option>
                                                <option>	Kec. Jatikalen	</option>
                                                <option>	Kec. Bakung	</option>
                                                <option>	Kec. Wonotirto	</option>
                                                <option>	Kec. Panggungrejo	</option>
                                                <option>	Kec. Wates	</option>
                                                <option>	Kec. Binangun	</option>
                                                <option>	Kec. Sutojayan	</option>
                                                <option>	Kec. Kademangan	</option>
                                                <option>	Kec. Kanigoro	</option>
                                                <option>	Kec. Talun	</option>
                                                <option>	Kec. Selopuro	</option>
                                                <option>	Kec. Kesamben	</option>
                                                <option>	Kec. Selorejo	</option>
                                                <option>	Kec. Doko	</option>
                                                <option>	Kec. Wlingi	</option>
                                                <option>	Kec. Gandusari	</option>
                                                <option>	Kec. Garum	</option>
                                                <option>	Kec. Nglegok	</option>
                                                <option>	Kec. Sanankulon	</option>
                                                <option>	Kec. Ponggok	</option>
                                                <option>	Kec. Srengat	</option>
                                                <option>	Kec. Wonodadi	</option>
                                                <option>	Kec. Udanawu	</option>
                                                <option>	Kec. Besuki	</option>
                                                <option>	Kec. Bandung	</option>
                                                <option>	Kec. Pakel	</option>
                                                <option>	Kec. Campur Darat	</option>
                                                <option>	Kec. Tanggung Gunung	</option>
                                                <option>	Kec. Kalidawir	</option>
                                                <option>	Kec. Pucang Laban	</option>
                                                <option>	Kec. Rejotangan	</option>
                                                <option>	Kec. Ngunut	</option>
                                                <option>	Kec. Sumbergempol	</option>
                                                <option>	Kec. Boyolangu	</option>
                                                <option>	Kec. Tulungagung	</option>
                                                <option>	Kec. Kedungwaru	</option>
                                                <option>	Kec. Ngantru	</option>
                                                <option>	Kec. Karangrejo	</option>
                                                <option>	Kec. Kauman	</option>
                                                <option>	Kec. Gondang	</option>
                                                <option>	Kec. Pagerwojo	</option>
                                                <option>	Kec. Sendang	</option>
                                                <option>	Kec. Panggul	</option>
                                                <option>	Kec. Munjungan	</option>
                                                <option>	Kec. Watulimo	</option>
                                                <option>	Kec. Kampak	</option>
                                                <option>	Kec. Dongko	</option>
                                                <option>	Kec. Pule	</option>
                                                <option>	Kec. Karangan	</option>
                                                <option>	Kec. Gandusari	</option>
                                                <option>	Kec. Durenan	</option>
                                                <option>	Kec. Pogalan	</option>
                                                <option>	Kec. Trenggalek	</option>
                                                <option>	Kec. Tugu	</option>
                                                <option>	Kec. Bendungan	</option>
                                                <option>	Kec. Suruh	</option>
                                                <option>	Kec. Donomulyo	</option>
                                                <option>	Kec. Kalipare	</option>
                                                <option>	Kec. Pagak	</option>
                                                <option>	Kec. Bantur	</option>
                                                <option>	Kec. Gedangan	</option>
                                                <option>	Kec. Sumber Manjing Wetan	</option>
                                                <option>	Kec. Dampit	</option>
                                                <option>	Kec. Tirto Yudo	</option>
                                                <option>	Kec. Ampelgading	</option>
                                                <option>	Kec. Poncokusumo	</option>
                                                <option>	Kec. Wajak	</option>
                                                <option>	Kec. Turen	</option>
                                                <option>	Kec. Pagelaran	</option>
                                                <option>	Kec. Gondanglegi	</option>
                                                <option>	Kec. Bululawang	</option>
                                                <option>	Kec. Kepanjen	</option>
                                                <option>	Kec. Sumberpucung	</option>
                                                <option>	Kec. Kromengan	</option>
                                                <option>	Kec. Wonosari	</option>
                                                <option>	Kec. Ngajum	</option>
                                                <option>	Kec. Wagir	</option>
                                                <option>	Kec. Pakisaji	</option>
                                                <option>	Kec. Tajinan	</option>
                                                <option>	Kec. Tumpang	</option>
                                                <option>	Kec. Pakis	</option>
                                                <option>	Kec. Jabung	</option>
                                                <option>	Kec. Lawang	</option>
                                                <option>	Kec. Singosari	</option>
                                                <option>	Kec. Karangploso	</option>
                                                <option>	Kec. Dau	</option>
                                                <option>	Kec. Pujon	</option>
                                                <option>	Kec. Ngantang	</option>
                                                <option>	Kec. Kasembon	</option>
                                                <option>	Kec. Purwodadi	</option>
                                                <option>	Kec. Tutur	</option>
                                                <option>	Kec. Puspo	</option>
                                                <option>	Kec. Tosari	</option>
                                                <option>	Kec. Lumbang	</option>
                                                <option>	Kec. Pasrepan	</option>
                                                <option>	Kec. Kejayan	</option>
                                                <option>	Kec. Wonorejo	</option>
                                                <option>	Kec. Purwosari	</option>
                                                <option>	Kec. Prigen	</option>
                                                <option>	Kec. Sukorejo	</option>
                                                <option>	Kec. Pandaan	</option>
                                                <option>	Kec. Gempol	</option>
                                                <option>	Kec. Beji	</option>
                                                <option>	Kec. Bangil	</option>
                                                <option>	Kec. Rembang	</option>
                                                <option>	Kec. Kraton	</option>
                                                <option>	Kec. Pohjentrek	</option>
                                                <option>	Kec. Gondang Wetan	</option>
                                                <option>	Kec. Rejoso	</option>
                                                <option>	Kec. Winongan	</option>
                                                <option>	Kec. Grati	</option>
                                                <option>	Kec. Lekok	</option>
                                                <option>	Kec. Nguling	</option>
                                                <option>	Kec. Sukapura	</option>
                                                <option>	Kec. Sumber	</option>
                                                <option>	Kec. Kuripan	</option>
                                                <option>	Kec. Bantaran	</option>
                                                <option>	Kec. Leces	</option>
                                                <option>	Kec. Tegal Siwalan	</option>
                                                <option>	Kec. Banyu Anyar	</option>
                                                <option>	Kec. Tiris	</option>
                                                <option>	Kec. Krucil	</option>
                                                <option>	Kec. Gading	</option>
                                                <option>	Kec. Pakuniran	</option>
                                                <option>	Kec. Kota Anyar	</option>
                                                <option>	Kec. Paiton	</option>
                                                <option>	Kec. Besuk	</option>
                                                <option>	Kec. Kraksaan	</option>
                                                <option>	Kec. Krejengan	</option>
                                                <option>	Kec. Pajarakan	</option>
                                                <option>	Kec. Maron	</option>
                                                <option>	Kec. Gending	</option>
                                                <option>	Kec. Dringu	</option>
                                                <option>	Kec. Wonomerto	</option>
                                                <option>	Kec. Lumbang	</option>
                                                <option>	Kec. Tongas	</option>
                                                <option>	Kec. Sumberasih	</option>
                                                <option>	Kec. Tempursari	</option>
                                                <option>	Kec. Pronojiwo	</option>
                                                <option>	Kec. Candipuro	</option>
                                                <option>	Kec. Pasirian	</option>
                                                <option>	Kec. Tempeh	</option>
                                                <option>	Kec. Kunir	</option>
                                                <option>	Kec. Yosowilangun	</option>
                                                <option>	Kec. Rowokangkung	</option>
                                                <option>	Kec. Tekung	</option>
                                                <option>	Kec. Lumajang	</option>
                                                <option>	Kec. Pasrujambe	</option>
                                                <option>	Kec. Senduro	</option>
                                                <option>	Kec. Padang	</option>
                                                <option>	Kec. Sukodono	</option>
                                                <option>	Kec. Jatiroto	</option>
                                                <option>	Kec. Randuagung	</option>
                                                <option>	Kec. Kedungjajang	</option>
                                                <option>	Kec. Gucialit	</option>
                                                <option>	Kec. Klakah	</option>
                                                <option>	Kec. Ranuyoso	</option>
                                                <option>	Kec. Sumbersuko	</option>
                                                <option>	Kec. Maesan	</option>
                                                <option>	Kec. Grujugan	</option>
                                                <option>	Kec. Tamanan	</option>
                                                <option>	Kec. Pujer	</option>
                                                <option>	Kec. Tlogosari	</option>
                                                <option>	Kec. Sukosari	</option>
                                                <option>	Kec. Tapen	</option>
                                                <option>	Kec. Wonosari	</option>
                                                <option>	Kec. Tenggarang	</option>
                                                <option>	Kec. Bondowoso	</option>
                                                <option>	Kec. Curahdami	</option>
                                                <option>	Kec. Pakem	</option>
                                                <option>	Kec. Wringin	</option>
                                                <option>	Kec. Tegalampel	</option>
                                                <option>	Kec. Klabang	</option>
                                                <option>	Kec. Prajekan	</option>
                                                <option>	Kec. Cermee	</option>
                                                <option>	Kec. Binakal	</option>
                                                <option>	Kec. Sumber Wringin	</option>
                                                <option>	Kec. Sempol	</option>
                                                <option>	Kec. Jambesari Darus Sholah	</option>
                                                <option>	Kec. Taman Krocok	</option>
                                                <option>	Kec. Botolinggo	</option>
                                                <option>	Kec. Sumber Malang	</option>
                                                <option>	Kec. Jatibanteng	</option>
                                                <option>	Kec. Banyuglugur	</option>
                                                <option>	Kec. Besuki	</option>
                                                <option>	Kec. Suboh	</option>
                                                <option>	Kec. Mlandingan	</option>
                                                <option>	Kec. Bungatan	</option>
                                                <option>	Kec. Kendit	</option>
                                                <option>	Kec. Panarukan	</option>
                                                <option>	Kec. Situbondo	</option>
                                                <option>	Kec. Mangaran	</option>
                                                <option>	Kec. Panji	</option>
                                                <option>	Kec. Kapongan	</option>
                                                <option>	Kec. Arjasa	</option>
                                                <option>	Kec. Jangkar	</option>
                                                <option>	Kec. Asembagus	</option>
                                                <option>	Kec. Banyuputih	</option>
                                                <option>	Kec. Kencong	</option>
                                                <option>	Kec. Gumuk Mas	</option>
                                                <option>	Kec. Puger	</option>
                                                <option>	Kec. Wuluhan	</option>
                                                <option>	Kec. Ambulu	</option>
                                                <option>	Kec. Tempurejo	</option>
                                                <option>	Kec. Silo	</option>
                                                <option>	Kec. Mayang	</option>
                                                <option>	Kec. Mumbulsari	</option>
                                                <option>	Kec. Jenggawah	</option>
                                                <option>	Kec. Ajung	</option>
                                                <option>	Kec. Rambipuji	</option>
                                                <option>	Kec. Balung	</option>
                                                <option>	Kec. Umbulsari	</option>
                                                <option>	Kec. Semboro	</option>
                                                <option>	Kec. Jombang	</option>
                                                <option>	Kec. Sumberbaru	</option>
                                                <option>	Kec. Tanggul	</option>
                                                <option>	Kec. Bangsalsari	</option>
                                                <option>	Kec. Panti	</option>
                                                <option>	Kec. Sukorambi	</option>
                                                <option>	Kec. Arjasa	</option>
                                                <option>	Kec. Pakusari	</option>
                                                <option>	Kec. Kalisat	</option>
                                                <option>	Kec. Ledok Ombo	</option>
                                                <option>	Kec. Sumberjambe	</option>
                                                <option>	Kec. Sukowono	</option>
                                                <option>	Kec. Jelbuk	</option>
                                                <option>	Kec. Kaliwates	</option>
                                                <option>	Kec. Sumbersari	</option>
                                                <option>	Kec. Patrang	</option>
                                                <option>	Kec. Pesanggaran	</option>
                                                <option>	Kec. Bangorejo	</option>
                                                <option>	Kec. Purwoharjo	</option>
                                                <option>	Kec. Tegaldlimo	</option>
                                                <option>	Kec. Muncar	</option>
                                                <option>	Kec. Cluring	</option>
                                                <option>	Kec. Gambiran	</option>
                                                <option>	Kec. Glenmore	</option>
                                                <option>	Kec. Kalibaru	</option>
                                                <option>	Kec. Genteng	</option>
                                                <option>	Kec. Srono	</option>
                                                <option>	Kec. Rogojampi	</option>
                                                <option>	Kec. Kabat	</option>
                                                <option>	Kec. Singojuruh	</option>
                                                <option>	Kec. Sempu	</option>
                                                <option>	Kec. Songgon	</option>
                                                <option>	Kec. Glagah	</option>
                                                <option>	Kec. Banyuwangi	</option>
                                                <option>	Kec. Giri	</option>
                                                <option>	Kec. Kalipuro	</option>
                                                <option>	Kec. Wongsorejo	</option>
                                                <option>	Kec. Licin	</option>
                                                <option>	Kec. Tegalsari	</option>
                                                <option>	Kec. Siliragung	</option>
                                                <option>	Kec. Tlanakan	</option>
                                                <option>	Kec. Pademawu	</option>
                                                <option>	Kec. Galis	</option>
                                                <option>	Kec. Larangan	</option>
                                                <option>	Kec. Pamekasan	</option>
                                                <option>	Kec. Proppo	</option>
                                                <option>	Kec. Palengaan	</option>
                                                <option>	Kec. Pegantenan	</option>
                                                <option>	Kec. Kadur	</option>
                                                <option>	Kec. Pakong	</option>
                                                <option>	Kec. Waru	</option>
                                                <option>	Kec. Batu Marmer	</option>
                                                <option>	Kec. Pasean	</option>
                                                <option>	Kec. Sreseh	</option>
                                                <option>	Kec. Torjun	</option>
                                                <option>	Kec. Sampang	</option>
                                                <option>	Kec. Camplong	</option>
                                                <option>	Kec. Omben	</option>
                                                <option>	Kec. Kedungdung	</option>
                                                <option>	Kec. Jrengik	</option>
                                                <option>	Kec. Tambelangan	</option>
                                                <option>	Kec. Banyuates	</option>
                                                <option>	Kec. Robatal	</option>
                                                <option>	Kec. Ketapang	</option>
                                                <option>	Kec. Sokobanah	</option>
                                                <option>	Kec. Karangpenang	</option>
                                                <option>	Kec. Pangarengan	</option>
                                                <option>	Kec. Pragaan	</option>
                                                <option>	Kec. Bluto	</option>
                                                <option>	Kec. Saronggi	</option>
                                                <option>	Kec. Giligenteng	</option>
                                                <option>	Kec. Talango	</option>
                                                <option>	Kec. Kalianget	</option>
                                                <option>	Kec. Kota Sumenep	</option>
                                                <option>	Kec. Lenteng	</option>
                                                <option>	Kec. Ganding	</option>
                                                <option>	Kec. Guluk Guluk	</option>
                                                <option>	Kec. Pasongsongan	</option>
                                                <option>	Kec. Ambunten	</option>
                                                <option>	Kec. Rubaru	</option>
                                                <option>	Kec. Dasuk	</option>
                                                <option>	Kec. Manding	</option>
                                                <option>	Kec. Batuputih	</option>
                                                <option>	Kec. Gapura	</option>
                                                <option>	Kec. Batang Batang	</option>
                                                <option>	Kec. Dungkek	</option>
                                                <option>	Kec. Nonggunong	</option>
                                                <option>	Kec. Gayam	</option>
                                                <option>	Kec. Ra As	</option>
                                                <option>	Kec. Sapeken	</option>
                                                <option>	Kec. Arjasa	</option>
                                                <option>	Kec. Masalembu	</option>
                                                <option>	Kec. Batuan	</option>
                                                <option>	Kec. Kangayan	</option>
                                                <option>	Kec. Kamal	</option>
                                                <option>	Kec. Labang	</option>
                                                <option>	Kec. Kwanyar	</option>
                                                <option>	Kec. Modung	</option>
                                                <option>	Kec. Blega	</option>
                                                <option>	Kec. Konang	</option>
                                                <option>	Kec. Galis	</option>
                                                <option>	Kec. Tanah Merah	</option>
                                                <option>	Kec. Tragah	</option>
                                                <option>	Kec. Socah	</option>
                                                <option>	Kec. Bangkalan	</option>
                                                <option>	Kec. Burneh	</option>
                                                <option>	Kec. Arosbaya	</option>
                                                <option>	Kec. Geger	</option>
                                                <option>	Kec. Kokop	</option>
                                                <option>	Kec. Tanjungbumi	</option>
                                                <option>	Kec. Sepulu	</option>
                                                <option>	Kec. Klampis	</option>
                                                <option>	Kec. Karang Pilang	</option>
                                                <option>	Kec. Jambangan	</option>
                                                <option>	Kec. Gayungan	</option>
                                                <option>	Kec. Wonocolo	</option>
                                                <option>	Kec. Tenggilis Mejoyo	</option>
                                                <option>	Kec. Gununganyar	</option>
                                                <option>	Kec. Rungkut	</option>
                                                <option>	Kec. Sukolilo	</option>
                                                <option>	Kec. Mulyorejo	</option>
                                                <option>	Kec. Gubeng	</option>
                                                <option>	Kec. Wonokromo	</option>
                                                <option>	Kec. Dukuh Pakis	</option>
                                                <option>	Kec. Wiyung	</option>
                                                <option>	Kec. Lakarsantri	</option>
                                                <option>	Kec. Tandes	</option>
                                                <option>	Kec. Sukomanunggal	</option>
                                                <option>	Kec. Sawahan	</option>
                                                <option>	Kec. Tegal Sari	</option>
                                                <option>	Kec. Genteng	</option>
                                                <option>	Kec. Tambaksari	</option>
                                                <option>	Kec. Kenjeran	</option>
                                                <option>	Kec. Simokerto	</option>
                                                <option>	Kec. Semampir	</option>
                                                <option>	Kec. Pabean Cantian	</option>
                                                <option>	Kec. Bubutan	</option>
                                                <option>	Kec. Krembangan	</option>
                                                <option>	Kec. Asemrowo	</option>
                                                <option>	Kec. Benowo	</option>
                                                <option>	Kec. Bulak	</option>
                                                <option>	Kec. Pakal	</option>
                                                <option>	Kec. Sambi Kerep	</option>
                                                <option>	Kec. Kedungkandang	</option>
                                                <option>	Kec. Sukun	</option>
                                                <option>	Kec. Klojen	</option>
                                                <option>	Kec. Blimbing	</option>
                                                <option>	Kec. Lowokwaru	</option>
                                                <option>	Kec. Manguharjo	</option>
                                                <option>	Kec. Taman	</option>
                                                <option>	Kec. Kartoharjo	</option>
                                                <option>	Kec. Mojoroto	</option>
                                                <option>	Kec. Kota Kediri	</option>
                                                <option>	Kec. Pesantren	</option>
                                                <option>	Kec. Prajurit Kulon	</option>
                                                <option>	Kec. Magersari	</option>
                                                <option>	Kec. Sukorejo	</option>
                                                <option>	Kec. Kepanjen Kidul	</option>
                                                <option>	Kec. Sanan Wetan	</option>
                                                <option>	Kec. Gadingrejo	</option>
                                                <option>	Kec. Purworejo	</option>
                                                <option>	Kec. Bugul Kidul	</option>
                                                <option>	Panggungrejo	</option>
                                                <option>	Kec. Kademangan	</option>
                                                <option>	Kec. Wonoasih	</option>
                                                <option>	Kec. Mayangan	</option>
                                                <option>	Kec. Kedopok	</option>
                                                <option>	Kec. Kanigaran	</option>
                                                <option>	Kec. Batu	</option>
                                                <option>	Kec. Junrejo	</option>
                                                <option>	Kec. Bumiaji	</option>
                                                <option>	Kec. Lhoong	</option>
                                                <option>	Kec. Lho Nga	</option>
                                                <option>	Kec. Indrapuri	</option>
                                                <option>	Kec. Seulimeum	</option>
                                                <option>	Kec. Mesjid Raya	</option>
                                                <option>	Kec. Darussalam	</option>
                                                <option>	Kec. Kuta Baro	</option>
                                                <option>	Kec. Montasik	</option>
                                                <option>	Kec. Ingin Jaya	</option>
                                                <option>	Kec. Suka Makmur	</option>
                                                <option>	Kec. Darul Imarah	</option>
                                                <option>	Kec. Peukan Bada	</option>
                                                <option>	Kec. Pulo Aceh	</option>
                                                <option>	Kec. Leupung	</option>
                                                <option>	Kec. Kuta Malaka	</option>
                                                <option>	Kec. Leumbah Seulewah	</option>
                                                <option>	Kec. Kota Jantho	</option>
                                                <option>	Kec. Baitussalam	</option>
                                                <option>	Kec. Krung Barona Jaya	</option>
                                                <option>	Kec. Simpang Tiga	</option>
                                                <option>	Kec. Kuta Cot Glie	</option>
                                                <option>	Kec. Darul Kamal	</option>
                                                <option>	Kec. Blang Bintang	</option>
                                                <option>	Kec. Geumpang	</option>
                                                <option>	Kec. Glumpang Tiga	</option>
                                                <option>	Kec. Mutiara	</option>
                                                <option>	Kec. Tiro/Truseb	</option>
                                                <option>	Kec. Tangse	</option>
                                                <option>	Kec. Sakti	</option>
                                                <option>	Kec. Mila	</option>
                                                <option>	Kec. Padang Tiji	</option>
                                                <option>	Kec. Delima	</option>
                                                <option>	Kec. Indrajaya	</option>
                                                <option>	Kec. Peukan Baru	</option>
                                                <option>	Kec. Kembang Tanjung	</option>
                                                <option>	Kec. Simpang Tiga	</option>
                                                <option>	Kec. Kota Sigli	</option>
                                                <option>	Kec. Pidie	</option>
                                                <option>	Kec. Batee	</option>
                                                <option>	Kec. Muara Tiga	</option>
                                                <option>	Kec. Mane	</option>
                                                <option>	Kec. Grong-Grong	</option>
                                                <option>	Kec. Mutiara Timur	</option>
                                                <option>	Kec. Glupang Baro	</option>
                                                <option>	Kec. Keumala	</option>
                                                <option>	Kec. Titeuae	</option>
                                                <option>	Kec. Sawang	</option>
                                                <option>	Kec. Nisam	</option>
                                                <option>	Kec. Kuta Makmur	</option>
                                                <option>	Kec. Syamtalira Bayu	</option>
                                                <option>	Kec. Meurah Mulia	</option>
                                                <option>	Kec. Matangkuli	</option>
                                                <option>	Kec. Cot Girek	</option>
                                                <option>	Kec. Tanah Jambo Aye	</option>
                                                <option>	Kec. Seunudon	</option>
                                                <option>	Kec. Baktiya	</option>
                                                <option>	Kec. Tanah Luas	</option>
                                                <option>	Kec. Samudera	</option>
                                                <option>	Kec. Syamtalira Aron	</option>
                                                <option>	Kec. Tanah Pasir	</option>
                                                <option>	Kec. Langkahan	</option>
                                                <option>	Kec. Baktiya Barat	</option>
                                                <option>	Kec. Simpang Keramat	</option>
                                                <option>	Kec. Nibong	</option>
                                                <option>	Kec. Paya Bakong	</option>
                                                <option>	Kec. Muara Batu	</option>
                                                <option>	Kec. Dewantara	</option>
                                                <option>	Kec. Lhoksukon	</option>
                                                <option>	Kec. Lapang	</option>
                                                <option>	Kec. Pirak Timu	</option>
                                                <option>	Kec. Geureudong Pase	</option>
                                                <option>	Kec. Banda Baro	</option>
                                                <option>	Kec. Nisam Antara	</option>
                                                <option>	Kec. Serba Jadi	</option>
                                                <option>	Kec. Birem Bayeun	</option>
                                                <option>	Kec. Rantau Selamat	</option>
                                                <option>	Kec. Peureulak	</option>
                                                <option>	Kec. Ranto Peureulak	</option>
                                                <option>	Kec. Idi Rayeuk	</option>
                                                <option>	Kec. Darul Aman	</option>
                                                <option>	Kec. Nurussalam	</option>
                                                <option>	Kec. Julok	</option>
                                                <option>	Kec. Pante Beudari	</option>
                                                <option>	Kec. Simpang Ulim	</option>
                                                <option>	Kec. Sungai Raya	</option>
                                                <option>	Kec. Peureulak Timur	</option>
                                                <option>	Kec. Peureulak Barat	</option>
                                                <option>	Kec. Banda Alam	</option>
                                                <option>	Kec. Idi Tunong	</option>
                                                <option>	Kec. Peudawa	</option>
                                                <option>	Kec. Indra Makmur	</option>
                                                <option>	Kec. Madat	</option>
                                                <option>	Kec. Simpang Jernih	</option>
                                                <option>	Kec. Darul Ihsan	</option>
                                                <option>	Kec. Peunaron	</option>
                                                <option>	Kec. Idi Timur	</option>
                                                <option>	Kec. Darul Falah	</option>
                                                <option>	Kec. Lingge	</option>
                                                <option>	Kec. Bintang	</option>
                                                <option>	Kec. Pegasing	</option>
                                                <option>	Kec. Bebesen	</option>
                                                <option>	Kec. Silih Nara	</option>
                                                <option>	Kec. Kuta Panang	</option>
                                                <option>	Kec. Ketol	</option>
                                                <option>	Kec. Celala	</option>
                                                <option>	Kec. Kebayakan	</option>
                                                <option>	Kec. Laut Tawar	</option>
                                                <option>	Kec. Atu Lintang	</option>
                                                <option>	Kec. Jagong Jeget	</option>
                                                <option>	Kec. Bies	</option>
                                                <option>	Kec. Rusip Antara	</option>
                                                <option>	Kec. Johan Pahlawan	</option>
                                                <option>	Kec. Samatiga	</option>
                                                <option>	Kec. Woyla	</option>
                                                <option>	Kec. Kaway XVI	</option>
                                                <option>	Kec. Sungai Mas	</option>
                                                <option>	Kec. Bubon	</option>
                                                <option>	Kec. Arongan Lambalek	</option>
                                                <option>	Kec. Meureubo	</option>
                                                <option>	Kec. Pantee Ceureumen	</option>
                                                <option>	Kec. Woyla Barat	</option>
                                                <option>	Kec. Woyla Timur	</option>
                                                <option>	Kec. Panton Reu	</option>
                                                <option>	Kec. Trumon	</option>
                                                <option>	Kec. Bakongan	</option>
                                                <option>	Kec. Kluet Selatan	</option>
                                                <option>	Kec. Kluet Utara	</option>
                                                <option>	Kec. Tapak Tuan	</option>
                                                <option>	Kec. Samadua	</option>
                                                <option>	Kec. Sawang	</option>
                                                <option>	Kec. Meukek	</option>
                                                <option>	Kec. Labuhan Haji	</option>
                                                <option>	Kec. Pasie Raja	</option>
                                                <option>	Kec. Trumon Timur	</option>
                                                <option>	Kec. Kluet Timur	</option>
                                                <option>	Kec. Bakongan Timur	</option>
                                                <option>	Kec. Kluet Tengah	</option>
                                                <option>	Kec. Labuhan Haji Barat	</option>
                                                <option>	Kec. Labuhan Haji Timur	</option>
                                                <option>	Kec. Kota Bahagia	</option>
                                                <option>	Kec. Trumon Tengah	</option>
                                                <option>	Kec. Lawe Alas	</option>
                                                <option>	Kec. Lawe Sigala-Gala	</option>
                                                <option>	Kec. Bambel	</option>
                                                <option>	Kec. Babussalam	</option>
                                                <option>	Kec. Badar	</option>
                                                <option>	Kec. Darul Hasanah	</option>
                                                <option>	Kec. Babul Makmur	</option>
                                                <option>	Kec. Lawe Bulan	</option>
                                                <option>	Kec. Bukit Tusam	</option>
                                                <option>	Kec. Semadam	</option>
                                                <option>	Kec. Babul Rahmat	</option>
                                                <option>	Kec. Ketambe	</option>
                                                <option>	Kec. Deleng Pokhkisen	</option>
                                                <option>	Kec. Lawe Sumur	</option>
                                                <option>	Kec. Tanoh Alas	</option>
                                                <option>	Kec. Lueser	</option>
                                                <option>	Kec. Teupah Selatan	</option>
                                                <option>	Kec. Simeulue Timur	</option>
                                                <option>	Kec. Simeulue Tengah	</option>
                                                <option>	Kec. Salang	</option>
                                                <option>	Kec. Simeulue Barat	</option>
                                                <option>	Kec. Teupah Barat	</option>
                                                <option>	Kec. Teluk Dalam	</option>
                                                <option>	Kec. Alafan	</option>
                                                <option>	Kec. Samalanga	</option>
                                                <option>	Kec. Pandrah	</option>
                                                <option>	Kec. Jeunib	</option>
                                                <option>	Kec. Peudada	</option>
                                                <option>	Kec. Juli	</option>
                                                <option>	Kec. Jeumpa	</option>
                                                <option>	Kec. Jangka	</option>
                                                <option>	Kec. Peusangan	</option>
                                                <option>	Kec. Makmur	</option>
                                                <option>	Kec. Ganda Pura	</option>
                                                <option>	Kec. Simpang Mamplam	</option>
                                                <option>	Kec. Peulimbang	</option>
                                                <option>	Kec. Kota Juang	</option>
                                                <option>	Kec. Kuala	</option>
                                                <option>	Kec. Peusangan Selatan	</option>
                                                <option>	Kec. Peusangan Siblah Krueng	</option>
                                                <option>	Kec. Kuta Blang	</option>
                                                <option>	Kec. Pulau Banyak	</option>
                                                <option>	Kec. Singkil	</option>
                                                <option>	Kec. Simpang Kanan	</option>
                                                <option>	Kec. Singkil Utara	</option>
                                                <option>	Kec. Gunung Mariah	</option>
                                                <option>	Kec. Danau Paris	</option>
                                                <option>	Kec. Suro Makmur	</option>
                                                <option>	Kec. Kuta Baharu	</option>
                                                <option>	Kec. Singkohor	</option>
                                                <option>	Kec. Kuala Baru	</option>
                                                <option>	Pulau Banyak Barat	</option>
                                                <option>	Kec. Tamiang Hulu	</option>
                                                <option>	Kec. Kejuruan Muda	</option>
                                                <option>	Kec. Kota Kuala Simpang	</option>
                                                <option>	Kec. Seruway	</option>
                                                <option>	Kec. Bendahara	</option>
                                                <option>	Kec. Karang Baru	</option>
                                                <option>	Kec. Manyak Payed	</option>
                                                <option>	Kec. Rantau	</option>
                                                <option>	Kec. Bandar Mulya	</option>
                                                <option>	Kec. Bandar Pusaka	</option>
                                                <option>	Kec. Tenggulun	</option>
                                                <option>	Kec. Sekerak	</option>
                                                <option>	Kec. Darul Makmur	</option>
                                                <option>	Kec. Kuala	</option>
                                                <option>	Kec. Beutong	</option>
                                                <option>	Kec. Seunagan	</option>
                                                <option>	Kec. Seunagan Timur	</option>
                                                <option>	Kec. Kuala Pesisir	</option>
                                                <option>	Kec. Tadu Raya	</option>
                                                <option>	Kec. Suka Makmue	</option>
                                                <option>	Kec. Tripa Makmur	</option>
                                                <option>	Kec. Teunom	</option>
                                                <option>	Kec. Krueng Sabee	</option>
                                                <option>	Kec. Setia Bakti	</option>
                                                <option>	Kec. Sampoiniet	</option>
                                                <option>	Kec. Jaya	</option>
                                                <option>	Kec. Panga	</option>
                                                <option>	Kec. Indra Jaya	</option>
                                                <option>	Kec. Darul Hikmah	</option>
                                                <option>	Kec. Pasie Raya	</option>
                                                <option>	Kec. Manggeng	</option>
                                                <option>	Kec. Tangan-Tangan	</option>
                                                <option>	Kec. Blang Pidie	</option>
                                                <option>	Kec. Susoh	</option>
                                                <option>	Kec. Kuala Batee	</option>
                                                <option>	Kec. Babah Rot	</option>
                                                <option>	Kec. Lembah Sabil	</option>
                                                <option>	Kec. Setia Bakti	</option>
                                                <option>	Kec. Jeumpa	</option>
                                                <option>	Kec. Blang Kejeran	</option>
                                                <option>	Kec. Kuta Panjang	</option>
                                                <option>	Kec. Rikit Gaib	</option>
                                                <option>	Kec. Terangon	</option>
                                                <option>	Kec. Pinding	</option>
                                                <option>	Kec. Blang Jerango	</option>
                                                <option>	Kec. Puteri Betung	</option>
                                                <option>	Kec. Dabung Gelang	</option>
                                                <option>	Kec. Blang Pegayon	</option>
                                                <option>	Kec. Pantan Cuaca	</option>
                                                <option>	Kec. Tripe Jaya	</option>
                                                <option>	Kec. Timang Gajah	</option>
                                                <option>	Kec. Bukit	</option>
                                                <option>	Kec. Bandar	</option>
                                                <option>	Kec. Permata	</option>
                                                <option>	Kec. Pintu Rime Gayo	</option>
                                                <option>	Kec. Wih Pesam	</option>
                                                <option>	Kec. Syiah Utama	</option>
                                                <option>	Bener Kelipah	</option>
                                                <option>	Mesidah	</option>
                                                <option>	Gajah Putih	</option>
                                                <option>	Kec. Meureudu	</option>
                                                <option>	Kec. Ulim	</option>
                                                <option>	Kec. Jangka Buya	</option>
                                                <option>	Kec. Bandar Dua	</option>
                                                <option>	Kec. Meurah Dua	</option>
                                                <option>	Kec. Bandar Baru	</option>
                                                <option>	Kec. Panteraja	</option>
                                                <option>	Kec. Trienggadeng	</option>
                                                <option>	Kec. Sukajaya	</option>
                                                <option>	Kec. Sukakarya	</option>
                                                <option>	Kec. Meuraxa	</option>
                                                <option>	Kec. Baiturrahman	</option>
                                                <option>	Kec. Kuta Alam	</option>
                                                <option>	Kec. Syiah Kuala	</option>
                                                <option>	Kec. Kuta Raja	</option>
                                                <option>	Kec. Ulee Kareng	</option>
                                                <option>	Kec. Lueng Bata	</option>
                                                <option>	Kec. Banda Raya	</option>
                                                <option>	Kec. Jaya Baru	</option>
                                                <option>	Kec. Blang Mangat	</option>
                                                <option>	Kec. Muara Dua	</option>
                                                <option>	Kec. Banda Sakti	</option>
                                                <option>	Kec. Muara Satu	</option>
                                                <option>	Kec. Langsa Timur	</option>
                                                <option>	Kec. Langsa Barat	</option>
                                                <option>	Kec. Langsa Kota	</option>
                                                <option>	Kec. Langsa Lama	</option>
                                                <option>	Kec. Langsa Baro	</option>
                                                <option>	Simpang Kiri	</option>
                                                <option>	Penanggalan	</option>
                                                <option>	Rundeng	</option>
                                                <option>	Sultan Daulat	</option>
                                                <option>	Longkib	</option>
                                                <option>	Kec. Gunung Meriah	</option>
                                                <option>	Kec. Sinembah Tanjung Muda Hul	</option>
                                                <option>	Kec. Sibolangit	</option>
                                                <option>	Kec. KutaIimbaru	</option>
                                                <option>	Kec. Pancur Batu	</option>
                                                <option>	Kec. Namo Rambe	</option>
                                                <option>	Kec. Sibiru-biru	</option>
                                                <option>	Kec. Bangun Purba	</option>
                                                <option>	Kec. Galang	</option>
                                                <option>	Kec. Tanjung Morawa	</option>
                                                <option>	Kec. Patumbak	</option>
                                                <option>	Kec. Deli Tua	</option>
                                                <option>	Kec. Sunggal	</option>
                                                <option>	Kec. Hamparan Perak	</option>
                                                <option>	Kec. Labuhan Deli	</option>
                                                <option>	Kec. Percut Sei Tuan	</option>
                                                <option>	Kec. Batang Kuis	</option>
                                                <option>	Kec. Pantai Labu	</option>
                                                <option>	Kec. Beringin	</option>
                                                <option>	Kec. Lubuk Pakam	</option>
                                                <option>	Kec. Pagar Marbau	</option>
                                                <option>	STM Hilir	</option>
                                                <option>	Kec. Bohorok	</option>
                                                <option>	Kec. Salapian	</option>
                                                <option>	Kec. Sei Bingai	</option>
                                                <option>	Kec. Kuala	</option>
                                                <option>	Kec. Selesai	</option>
                                                <option>	Kec. Binjai	</option>
                                                <option>	Kec. Stabat	</option>
                                                <option>	Kec. Wampu	</option>
                                                <option>	Kec. Batang Serangan	</option>
                                                <option>	Kec. Sawit Seberang	</option>
                                                <option>	Kec. Padang Tualang	</option>
                                                <option>	Kec. Hinai	</option>
                                                <option>	Kec. Secanggang	</option>
                                                <option>	Kec. Tanjung Pura	</option>
                                                <option>	Kec. Gebang	</option>
                                                <option>	Kec. Sei Lepan	</option>
                                                <option>	Kec. Babalan	</option>
                                                <option>	Kec. Berandan Barat	</option>
                                                <option>	Kec. Besitang	</option>
                                                <option>	Kec. Pangkalan Susu	</option>
                                                <option>	Kec. Serapit	</option>
                                                <option>	Kec. Kutambaru	</option>
                                                <option>	Kec. Pematang Jaya	</option>
                                                <option>	Kec. Mardinding	</option>
                                                <option>	Kec. Laubaleng	</option>
                                                <option>	Kec. Tiga Binanga	</option>
                                                <option>	Kec. Juhar	</option>
                                                <option>	Kec. Munte	</option>
                                                <option>	Kec. Kuta Buluh	</option>
                                                <option>	Kec. Payung	</option>
                                                <option>	Kec. Simpang Empat	</option>
                                                <option>	Kec. Kabanjahe	</option>
                                                <option>	Kec. Berastagi	</option>
                                                <option>	Kec. Tiga Panah	</option>
                                                <option>	Kec. Merek	</option>
                                                <option>	Kec. Barus Jahe	</option>
                                                <option>	Kec. Tiga Binanga	</option>
                                                <option>	Kec. Naman Teran	</option>
                                                <option>	Kec. Merdeka	</option>
                                                <option>	Kec. Dolat Rayat	</option>
                                                <option>	Tiganderket	</option>
                                                <option>	Kec. Silimakuta	</option>
                                                <option>	Kec. Purba	</option>
                                                <option>	Kec. Dolok Pardamean	</option>
                                                <option>	Kec. Sidamanik	</option>
                                                <option>	Kec. Girsang Simpangan Bolon	</option>
                                                <option>	Kec. Tanah Jawa	</option>
                                                <option>	Kec. Dolok Panribuan	</option>
                                                <option>	Kec. Jorlang Hataran	</option>
                                                <option>	Kec. Pane	</option>
                                                <option>	Kec. Raya	</option>
                                                <option>	Kec. Dolok Silau	</option>
                                                <option>	Kec. Silau Kahean	</option>
                                                <option>	Kec. Raya Kahean	</option>
                                                <option>	Kec. Tapian Dolok	</option>
                                                <option>	Kec. Dolok Batu Nanggar	</option>
                                                <option>	Kec. Siantar	</option>
                                                <option>	Kec. Hutabayu Raja	</option>
                                                <option>	Kec. Pematang Bandar	</option>
                                                <option>	Kec. Bandar	</option>
                                                <option>	Kec. Bosar Maligas	</option>
                                                <option>	Kec. Ujung Padang	</option>
                                                <option>	Kec. Panombeian Pane	</option>
                                                <option>	Kec. Gunung Malela	</option>
                                                <option>	Kec. Gunung Maligas	</option>
                                                <option>	Kec. Bandar Huluan	</option>
                                                <option>	Kec. Bandar Masilam	</option>
                                                <option>	Kec. Hatonduhan	</option>
                                                <option>	Kec. Jawa Maraja Bah Jambi	</option>
                                                <option>	Kec. Haranggaol Horison	</option>
                                                <option>	Kec. Pematang Sidamanik	</option>
                                                <option>	Pamatang Silima Huta	</option>
                                                <option>	Kec. Sidikalang	</option>
                                                <option>	Kec. Parbuluan	</option>
                                                <option>	Kec. Sumbul	</option>
                                                <option>	Kec. Pegangan Hilir	</option>
                                                <option>	Kec. Siempat Nempu Hulu	</option>
                                                <option>	Kec. Siempat Nempu	</option>
                                                <option>	Kec. Silima Pungga-Pungga	</option>
                                                <option>	Kec. Siempat Nempu Hilir	</option>
                                                <option>	Kec. Tigalingga	</option>
                                                <option>	Kec. Tanah Pinem	</option>
                                                <option>	Kec. Lae Parira	</option>
                                                <option>	Kec. Gunung Stember	</option>
                                                <option>	Kec. Berampu	</option>
                                                <option>	Kec. Sitinjo	</option>
                                                <option>	Silahi Sabungan	</option>
                                                <option>	Merek	</option>
                                                <option>	Kec. Bandar Pasir Mandoge	</option>
                                                <option>	Kec. Bandar Pulau	</option>
                                                <option>	Kec. Pulau Rakyat	</option>
                                                <option>	Kec. Sei Kepayang	</option>
                                                <option>	Kec. Tanjung Balai	</option>
                                                <option>	Kec. Simpang Empat	</option>
                                                <option>	Kec. Air Batu	</option>
                                                <option>	Kec. Buntu Pane	</option>
                                                <option>	Kec. Meranti	</option>
                                                <option>	Kec. Air Joman	</option>
                                                <option>	Kec. Aek Kuasan	</option>
                                                <option>	Kec. Kisaran Barat	</option>
                                                <option>	Kec. Kisaran Timur	</option>
                                                <option>	Kec. Aek Songsongan	</option>
                                                <option>	Kec. Rahuning	</option>
                                                <option>	Kec. Aek Ledong	</option>
                                                <option>	Kec. Sei Kepayang Barat	</option>
                                                <option>	Kec. Sei Kepayang Timur	</option>
                                                <option>	Kec. Teluk Dalam	</option>
                                                <option>	Kec. Sei dadap	</option>
                                                <option>	Kec. Tinggi Raja	</option>
                                                <option>	Kec. Setia Janji	</option>
                                                <option>	Kec. Pulo Bandring	</option>
                                                <option>	Kec. Rawang Panca Arga	</option>
                                                <option>	Kec. Silo Laut	</option>
                                                <option>	Kec. Bilah Hulu	</option>
                                                <option>	Kec. Pangkatan	</option>
                                                <option>	Kec. Bilah Barat	</option>
                                                <option>	Kec. Bilah Hilir	</option>
                                                <option>	Kec. Panai Hulu	</option>
                                                <option>	Kec. Panai Tengah	</option>
                                                <option>	Kec. Panai Hilir	</option>
                                                <option>	Kec. Rantau Selatan	</option>
                                                <option>	Kec. Rantau Utara	</option>
                                                <option>	Kec. Parmonangan	</option>
                                                <option>	Kec. Adian Koting	</option>
                                                <option>	Kec. Sipoholon	</option>
                                                <option>	Kec. Tarutung	</option>
                                                <option>	Kec. Pahae Jae	</option>
                                                <option>	Kec. Pahae Julu	</option>
                                                <option>	Kec. Pangaribuan	</option>
                                                <option>	Kec. Garoga	</option>
                                                <option>	Kec. Sipahutar	</option>
                                                <option>	Kec. Siborong-Borong	</option>
                                                <option>	Kec. Pagaran	</option>
                                                <option>	Kec. Muara	</option>
                                                <option>	Kec. Purbatua	</option>
                                                <option>	Kec. Simangumban	</option>
                                                <option>	Kec. Siatas Barita	</option>
                                                <option>	Kec. Lumut	</option>
                                                <option>	Kec. Sibabangun	</option>
                                                <option>	Kec. Pandan	</option>
                                                <option>	Kec. Tapian Nauli	</option>
                                                <option>	Kec. Kolang	</option>
                                                <option>	Kec. Sorkam	</option>
                                                <option>	Kec. Barus	</option>
                                                <option>	Kec. Manduamas	</option>
                                                <option>	Kec. Sosor Gadong	</option>
                                                <option>	Kec. Sorkam Barat	</option>
                                                <option>	Kec. Andam Dewi	</option>
                                                <option>	Kec. Badiri	</option>
                                                <option>	Kec. Sitahuis	</option>
                                                <option>	Kec. Sirandorung	</option>
                                                <option>	Kec. Tukka	</option>
                                                <option>	Kec. Pinang Sori	</option>
                                                <option>	Kec. Sukabangun	</option>
                                                <option>	Kec. Sarudik	</option>
                                                <option>	Kec. Barus Utara	</option>
                                                <option>	Pasaribu Tobing	</option>
                                                <option>	Kec. Batang Angkola	</option>
                                                <option>	Kec. Batang Toru	</option>
                                                <option>	Kec. Sipirok	</option>
                                                <option>	Kec. Arse	</option>
                                                <option>	Kec. Saipar Dolok Hole	</option>
                                                <option>	Kec. Marancar	</option>
                                                <option>	Kec. Sayur Matinggi	</option>
                                                <option>	Kec. Aek Bilah	</option>
                                                <option>	Kec. Muaro Batang Toru	</option>
                                                <option>	Angkola Barat	</option>
                                                <option>	Angkola Sangkunur	</option>
                                                <option>	Angkola Selatan	</option>
                                                <option>	Angkola Timur	</option>
                                                <option>	Tantom Angkola	</option>
                                                <option>	Kec. Idano Gawo	</option>
                                                <option>	Kec. Gido	</option>
                                                <option>	Kec. Hiliduho	</option>
                                                <option>	Kec. Bawalato	</option>
                                                <option>	Kec. Ulugawo	</option>
                                                <option>	Kec. Ma U	</option>
                                                <option>	Kec. Somolo-Molo	</option>
                                                <option>	Kec. Hili Serangkai	</option>
                                                <option>	Kec. Botomuzoi	</option>
                                                <option>	Sogaeadu	</option>
                                                <option>	Kec. Batahan	</option>
                                                <option>	Kec. Batang Natal	</option>
                                                <option>	Kec. Kotanopan	</option>
                                                <option>	Kec. Muara Sipongi	</option>
                                                <option>	Kec. Panyabungan Kota	</option>
                                                <option>	Kec. Natal	</option>
                                                <option>	Kec. Muara Batang Gadis	</option>
                                                <option>	Kec. Siabu	</option>
                                                <option>	Kec. Panyabungan Utara	</option>
                                                <option>	Kec. Panyabungan Barat	</option>
                                                <option>	Kec. Panyabungan Timur	</option>
                                                <option>	Kec. Panyabungan Selatan	</option>
                                                <option>	Kec. Bukit Malintang	</option>
                                                <option>	Kec. Lembah Sorik Merapi	</option>
                                                <option>	Kec. Ulu Pungut	</option>
                                                <option>	Kec. Tambangan	</option>
                                                <option>	Kec. Langga Bayu	</option>
                                                <option>	Kec. Balige	</option>
                                                <option>	Kec. Lagu Boti	</option>
                                                <option>	Kec. Habinsaran	</option>
                                                <option>	Kec. Silaen	</option>
                                                <option>	Kec. Porsea	</option>
                                                <option>	Kec. Lumban Julu	</option>
                                                <option>	Kec. Uluan	</option>
                                                <option>	Kec. Pintu Pohan Meranti	</option>
                                                <option>	Kec. Ajibata	</option>
                                                <option>	Kec. Borbor	</option>
                                                <option>	Kec. Tampahan	</option>
                                                <option>	Kec. Nassau	</option>
                                                <option>	Kec. Sigumpar	</option>
                                                <option>	Kec. Siantar Narumonda	</option>
                                                <option>	Kec. Parmaksian	</option>
                                                <option>	Kec. Bonatua Lunasi	</option>
                                                <option>	Kec. Pulau-Pulau Batu	</option>
                                                <option>	Kec. Teluk Dalam	</option>
                                                <option>	Kec. Amandraya	</option>
                                                <option>	Kec. Lahusa	</option>
                                                <option>	Kec. Gomo	</option>
                                                <option>	Kec. Lolomatua	</option>
                                                <option>	Kec. Lolowa`U	</option>
                                                <option>	Kec. Hibala	</option>
                                                <option>	Kec. Susua	</option>
                                                <option>	Kec. Maniamolo	</option>
                                                <option>	Kec. Hilimegai	</option>
                                                <option>	Kec. Toma	</option>
                                                <option>	Kec. Mazino	</option>
                                                <option>	Kec. Umbunasi	</option>
                                                <option>	Kec. Aramo	</option>
                                                <option>	Kec. Pulau-Pulau Batu Timur	</option>
                                                <option>	Kec. Mazo	</option>
                                                <option>	Kec. Fanayama	</option>
                                                <option>	Kec. Salak	</option>
                                                <option>	Kec. Kerajaan	</option>
                                                <option>	Kec. Sitelutali Urang Jehe	</option>
                                                <option>	Kec. Sitelutali Urang Jehe	</option>
                                                <option>	Kec. Pangindar	</option>
                                                <option>	Kec. Pergetteng-getteng Sengku	</option>
                                                <option>	Kec. Tinada	</option>
                                                <option>	Kec. Siempat Rube	</option>
                                                <option>	Sitelu Tali Urang Julu	</option>
                                                <option>	Kec. Pakkat	</option>
                                                <option>	Kec. Onan Ganjang	</option>
                                                <option>	Kec. Lintong Nihuta	</option>
                                                <option>	Kec. Dolok Sanggul	</option>
                                                <option>	Kec. Parlilitan	</option>
                                                <option>	Kec. Pollung	</option>
                                                <option>	Kec. Paranginan	</option>
                                                <option>	Kec. Bakti Raja	</option>
                                                <option>	Kec. Sijamapolang	</option>
                                                <option>	Kec. Tarabintang	</option>
                                                <option>	Kec. Harian	</option>
                                                <option>	Kec. Sianjur Mula Mula	</option>
                                                <option>	Kec. Onan Runggu Timur	</option>
                                                <option>	Kec. Palipi	</option>
                                                <option>	Kec. Pangururan	</option>
                                                <option>	Kec. Simanindo	</option>
                                                <option>	Kec. Nainggolan	</option>
                                                <option>	Kec. Ronggur Nihuta	</option>
                                                <option>	Kec. Sitiotio	</option>
                                                <option>	Kec. Kotarih	</option>
                                                <option>	Kec. Dolok Masihul	</option>
                                                <option>	Kec. Sipispis	</option>
                                                <option>	Kec. Dolok Merawan	</option>
                                                <option>	Kec. Tebing Tinggi	</option>
                                                <option>	Kec. Bandar Khalifah	</option>
                                                <option>	Kec. Tanjung Beringin	</option>
                                                <option>	Kec. Teluk Mengkudu	</option>
                                                <option>	Kec. Sei Rampah	</option>
                                                <option>	Kec. Perbaungan	</option>
                                                <option>	Kec. Pantai Cermin	</option>
                                                <option>	Kec. Silinda	</option>
                                                <option>	Kec. Bintang Bayu	</option>
                                                <option>	Kec. Serbajadi	</option>
                                                <option>	Kec. Tebing Syahbandar	</option>
                                                <option>	Kec. Sei Bamban	</option>
                                                <option>	Kec. Pegajahan	</option>
                                                <option>	Kec. Medang Deras	</option>
                                                <option>	Kec. Air Putih	</option>
                                                <option>	Kec. Lima Puluh	</option>
                                                <option>	Kec. Sei Balai	</option>
                                                <option>	Kec. Sei Suka	</option>
                                                <option>	Kec. Talawi	</option>
                                                <option>	Kec. Tanjung Tiram	</option>
                                                <option>	Kec. Padang Bolak Julu	</option>
                                                <option>	Kec. Padang Bolak	</option>
                                                <option>	Kec. Halongonan	</option>
                                                <option>	Kec. Dolok Sxigompulon	</option>
                                                <option>	Kec. Portibi	</option>
                                                <option>	Kec. Simangambat	</option>
                                                <option>	Kec. Batang Onang	</option>
                                                <option>	Kec. Dolok	</option>
                                                <option>	Hulu Sihapas	</option>
                                                <option>	Kec. Barumun	</option>
                                                <option>	Kec. Sosa	</option>
                                                <option>	Kec. Barumun Tengah	</option>
                                                <option>	Kec. Batang Lubu Sutam.	</option>
                                                <option>	Kec. Huta Raja Tinggi	</option>
                                                <option>	Kec. Lubuk Barumun	</option>
                                                <option>	Kec. Huristak	</option>
                                                <option>	Kec. Ulu Barumun	</option>
                                                <option>	Kec. Sosopan	</option>
                                                <option>	Kec. Barumun Selatan	</option>
                                                <option>	Kec. Aek Nabara Barumun	</option>
                                                <option>	Kec. Na IX-X	</option>
                                                <option>	Kec. Aek Natas	</option>
                                                <option>	Kec. Aek Kuo	</option>
                                                <option>	Kec. Kualuh Hilir	</option>
                                                <option>	Kec. Kualuh Selatan	</option>
                                                <option>	Kec. Kualuh Hulu	</option>
                                                <option>	Kec. Kualuh Leidong	</option>
                                                <option>	Kec. Marbau	</option>
                                                <option>	Kec. Sungai Kanan	</option>
                                                <option>	Kec. Torgamba	</option>
                                                <option>	Kec. Kota Pinang	</option>
                                                <option>	Kec. Silangkitang	</option>
                                                <option>	Kec. Kampung Rakyat	</option>
                                                <option>	Kec. Lolofitu Moi	</option>
                                                <option>	Kec. Sirombu	</option>
                                                <option>	Kec. Lahomi	</option>
                                                <option>	Kec. Mandrehe	</option>
                                                <option>	Kec. Mandrehe Barat	</option>
                                                <option>	Kec. Moro O	</option>
                                                <option>	Kec. Mandrehe Barat	</option>
                                                <option>	Kec. Ulo Moro O	</option>
                                                <option>	Kec. Tuhemberua	</option>
                                                <option>	Kec. Lotu	</option>
                                                <option>	Kec. Sitolu Ori	</option>
                                                <option>	Kec. Sawo	</option>
                                                <option>	Kec. Alasa	</option>
                                                <option>	Kec. Namohalu Esiwa	</option>
                                                <option>	Kec. Alasa Talu Muzoi	</option>
                                                <option>	Kec. Tugala Oyo	</option>
                                                <option>	Kec. Lahewa	</option>
                                                <option>	Kec. Afulu	</option>
                                                <option>	Kec. Lahewa Timur	</option>
                                                <option>	Kec. Medan Tuntungan	</option>
                                                <option>	Kec. Medan Johor	</option>
                                                <option>	Kec. Medan Amplas	</option>
                                                <option>	Kec. Medan Denai	</option>
                                                <option>	Kec. Medan Area	</option>
                                                <option>	Kec. Medan Kota	</option>
                                                <option>	Kec. Medan Maimun	</option>
                                                <option>	Kec. Medan Polonia	</option>
                                                <option>	Kec. Medan Baru	</option>
                                                <option>	Kec. Medan Selayang	</option>
                                                <option>	Kec. Medan Sunggal	</option>
                                                <option>	Kec. Medan Helvetia	</option>
                                                <option>	Kec. Medan Petisah	</option>
                                                <option>	Kec. Medan Barat	</option>
                                                <option>	Kec. Medan Timur	</option>
                                                <option>	Kec. Medan Perjuangan	</option>
                                                <option>	Kec. Medan Tembung	</option>
                                                <option>	Kec. Medan Deli	</option>
                                                <option>	Kec. Medan Labuhan	</option>
                                                <option>	Kec. Medan Marelan	</option>
                                                <option>	Kec. Medan Kota Belawan	</option>
                                                <option>	Kec. Binjai Selatan	</option>
                                                <option>	Kec. Binjai Kota	</option>
                                                <option>	Kec. Binjai Timur	</option>
                                                <option>	Kec. Binjai Utara	</option>
                                                <option>	Kec. Binjai Barat	</option>
                                                <option>	Kec. Padang Hulu	</option>
                                                <option>	Kec. Rambutan	</option>
                                                <option>	Kec. Padang Hilir	</option>
                                                <option>	Kec. Bajenis	</option>
                                                <option>	Kec. Tebing Tinggi Kota	</option>
                                                <option>	Kec. Siantar Marihat	</option>
                                                <option>	Kec. Siantar Selatan	</option>
                                                <option>	Kec. Siantar Barat	</option>
                                                <option>	Kec. Siantar Utara	</option>
                                                <option>	Kec. Siantar Timur	</option>
                                                <option>	Kec. Siantar Martoba	</option>
                                                <option>	Kec. Siantar Marimbun	</option>
                                                <option>	Kec. Siantar Sitalasari	</option>
                                                <option>	Kec. Datuk Bandar	</option>
                                                <option>	Kec. Tanjung Balai Selatan	</option>
                                                <option>	Kec. Tanjung Balai Utara	</option>
                                                <option>	Kec. S. Tualang Raso	</option>
                                                <option>	Kec. Teluk Nibung	</option>
                                                <option>	Kec. Datuk Bandar Timur	</option>
                                                <option>	Kec. Sibolga Utara	</option>
                                                <option>	Kec. Sibolga Kota	</option>
                                                <option>	Kec. Sibolga Selatan	</option>
                                                <option>	Kec. Sibolga Sambas	</option>
                                                <option>	Kec. Padang Sidimpuan Selatan	</option>
                                                <option>	Kec. Padang Sidimpuan Utara	</option>
                                                <option>	Kec. Padang Sidimpuan Batu Nad	</option>
                                                <option>	Kec. Padang Sidimpuan Hutaimba	</option>
                                                <option>	Kec. Padang Sidimpuan Tenggara	</option>
                                                <option>	Kec. Padang Sidimpuan Angkola	</option>
                                                <option>	Kec. Gunung Sitoli Idanoi	</option>
                                                <option>	Kec. Gunung Sitoli Alo Oa	</option>
                                                <option>	Kec. Gunung Sitoli	</option>
                                                <option>	Kec. Gunung Sitoli Selatan	</option>
                                                <option>	Kec. Gunung Sitoli Barat	</option>
                                                <option>	Kec. Gunung Sitoli Utara	</option>
                                                <option>	Kec. Tanjung Mutiara	</option>
                                                <option>	Kec. Lubuk Basung	</option>
                                                <option>	Kec. Tanjung Raya	</option>
                                                <option>	Kec. Matur	</option>
                                                <option>	Kec. IV Koto	</option>
                                                <option>	Kec. IV Angkat Candung	</option>
                                                <option>	Kec. Baso	</option>
                                                <option>	Kec. Tilatang Kamang	</option>
                                                <option>	Kec. Palembayan	</option>
                                                <option>	Kec. Palupuh	</option>
                                                <option>	Kec. Sungai Pua	</option>
                                                <option>	Kec. Candung	</option>
                                                <option>	Kec. Kamang Magek	</option>
                                                <option>	Kec. Banuhampu	</option>
                                                <option>	Kec. Ampek Angkek	</option>
                                                <option>	Kec. Malalak	</option>
                                                <option>	Ampek Nagari	</option>
                                                <option>	Kec. Bonjol	</option>
                                                <option>	Kec. Lubuk Sikaping	</option>
                                                <option>	Kec. II Koto	</option>
                                                <option>	Kec. Panti	</option>
                                                <option>	Kec. III Nagari	</option>
                                                <option>	Kec. Rao	</option>
                                                <option>	Kec. Mapat Tunggul	</option>
                                                <option>	Kec. Mapat Tunggul Selatan	</option>
                                                <option>	Kec. Simpang Alahan Mati	</option>
                                                <option>	Kec. Padang Gelugur	</option>
                                                <option>	Kec. Rao Utara	</option>
                                                <option>	Kec. Rao Selatan	</option>
                                                <option>	Kec. Payakumbuh	</option>
                                                <option>	Kec. Luak	</option>
                                                <option>	Kec. Harau	</option>
                                                <option>	Kec. Guguak	</option>
                                                <option>	Kec. Suliki	</option>
                                                <option>	Kec. Gunuang Omeh	</option>
                                                <option>	Kec. Kapur IX	</option>
                                                <option>	Kec. Pangkalan Koto Baru	</option>
                                                <option>	Kec. Bukkt Barisan	</option>
                                                <option>	Kec. Mungka	</option>
                                                <option>	Kec. Akabiluru	</option>
                                                <option>	Kec. Situjuah Limo Nagari	</option>
                                                <option>	Kec. Lareh Sago Halaban	</option>
                                                <option>	Kec. Pantai Cermin	</option>
                                                <option>	Kec. Lembah Gumanti	</option>
                                                <option>	Kec. Payung Sekaki	</option>
                                                <option>	Kec. Lembang Jaya	</option>
                                                <option>	Kec. Gunung Talang	</option>
                                                <option>	Kec. Bukit Sundi	</option>
                                                <option>	Kec. Kubung	</option>
                                                <option>	Kec. IX Koto Sungai Lasi	</option>
                                                <option>	Kec. X Koto Diatas	</option>
                                                <option>	Kec. X Koto Singkarak	</option>
                                                <option>	Kec. Junjung Sirih	</option>
                                                <option>	Kec. Hiliran Gumanti	</option>
                                                <option>	Kec. Tigo Lurah	</option>
                                                <option>	Kec. Danau Kembar	</option>
                                                <option>	Kec. Batang Anai	</option>
                                                <option>	Kec. Lubuk Alung	</option>
                                                <option>	Kec. Ulakan Tapakis	</option>
                                                <option>	Kec. Nan Sabaris	</option>
                                                <option>	Kec. 2 x 11 VI Lingkung	</option>
                                                <option>	Kec. VII Koto Sungai Sarik	</option>
                                                <option>	Kec. V Koto Kampung Dalam	</option>
                                                <option>	Kec. Sungai Limau	</option>
                                                <option>	Kec. Sungai Geringging	</option>
                                                <option>	Kec. IV Koto Aur Malintang	</option>
                                                <option>	Kec. Batang Gasan	</option>
                                                <option>	Kec. V Koto Timur	</option>
                                                <option>	Kec. Patamuan	</option>
                                                <option>	Kec. Padang Sago	</option>
                                                <option>	Kec. 2 x 11 Kayu Tanam	</option>
                                                <option>	Kec. Sintuk Toboh Gadang	</option>
                                                <option>	Kec. VI Lingkung	</option>
                                                <option>	Kec. Lunang Silaut	</option>
                                                <option>	Kec. Basa IV Balai Tapan	</option>
                                                <option>	Kec. Pancung Soal	</option>
                                                <option>	Kec. Linggo Saribaganti	</option>
                                                <option>	Kec. Ranah Pesisir	</option>
                                                <option>	Kec. Lengayang	</option>
                                                <option>	Kec. Sutera	</option>
                                                <option>	Kec. Batang Kapas	</option>
                                                <option>	Kec. IV Jurai	</option>
                                                <option>	Kec. Bayang	</option>
                                                <option>	Kec. Koto XI Terusan	</option>
                                                <option>	Kec. IV Bayang Utara	</option>
                                                <option>	Kec. Sepuluh Koto	</option>
                                                <option>	Kec. Batipuh	</option>
                                                <option>	Kec. Pariangan	</option>
                                                <option>	Kec. Rambatan	</option>
                                                <option>	Kec. Lima Kaum	</option>
                                                <option>	Kec. Tanjung Mas	</option>
                                                <option>	Kec. Padang Ganting	</option>
                                                <option>	Kec. Lintau Buo	</option>
                                                <option>	Kec. Sungayang	</option>
                                                <option>	Kec. Sungai Tarab	</option>
                                                <option>	Kec. Salimpaung	</option>
                                                <option>	Kec. Batipuh Selatan	</option>
                                                <option>	Kec. Lintau Buo Utara	</option>
                                                <option>	Kec. Tanjung Baru	</option>
                                                <option>	Kec. Kamang Baru	</option>
                                                <option>	Kec. Tanjung Gadang	</option>
                                                <option>	Kec. Sijunjung	</option>
                                                <option>	Kec. IV Nagari	</option>
                                                <option>	Kec. Kupitan	</option>
                                                <option>	Kec. Koto Tujuh	</option>
                                                <option>	Kec. Sumpur Kudus	</option>
                                                <option>	Kec. Lubuk Tarok	</option>
                                                <option>	Kec. Pagai Utara	</option>
                                                <option>	Kec. Sipora Selatan	</option>
                                                <option>	Kec. Siberut Selatan	</option>
                                                <option>	Kec. Siberut Utara	</option>
                                                <option>	Kec. Siberut Barat	</option>
                                                <option>	Kec. Siberut Barat Daya	</option>
                                                <option>	Kec. Siberut Tengah	</option>
                                                <option>	Kec. Sipora Utara	</option>
                                                <option>	Kec. Sikakap	</option>
                                                <option>	Kec. Pagai Selatan	</option>
                                                <option>	Siberut Barat	</option>
                                                <option>	Kec. Sangir	</option>
                                                <option>	Kec. Sungai Pagu	</option>
                                                <option>	Kec. Koto Parik Gadang Diateh	</option>
                                                <option>	Kec. Sangir Jujuhan	</option>
                                                <option>	Kec. Sangir Batanghari	</option>
                                                <option>	Kec. Pauah Duo	</option>
                                                <option>	Kec. Sangir Balai Janggo	</option>
                                                <option>	Kec. Sungai Rumbai	</option>
                                                <option>	Kec. Koto Baru	</option>
                                                <option>	Kec. Sitiung	</option>
                                                <option>	Kec. Pulau Punjung	</option>
                                                <option>	Kec. Sembilan Koto	</option>
                                                <option>	Kec. Timpeh	</option>
                                                <option>	Kec. Koto Salak	</option>
                                                <option>	Kec. Tiumang	</option>
                                                <option>	Kec. Padang Laweh	</option>
                                                <option>	Kec. Asam Jujuhan	</option>
                                                <option>	Kec. Koto Besar	</option>
                                                <option>	Kec. Sungai Beremas	</option>
                                                <option>	Kec. Ranah Batahan	</option>
                                                <option>	Kec. Lembah Melintang	</option>
                                                <option>	Kec. Gunung Tuleh	</option>
                                                <option>	Kec. Pasaman	</option>
                                                <option>	Kec. Kinali	</option>
                                                <option>	Kec. Talamau	</option>
                                                <option>	Kec. Koto Balingka	</option>
                                                <option>	Kec. Luhak Nan Duo	</option>
                                                <option>	Kec. Sasak Ranah Pesisir	</option>
                                                <option>	Kec. Sungai Aur	</option>
                                                <option>	Kec. Guguk Panjang	</option>
                                                <option>	Kec. Mandiangin Koto Selayan	</option>
                                                <option>	Kec. Aur Birugo Tigo Baleh	</option>
                                                <option>	Kec. Bungus Teluk Kabung	</option>
                                                <option>	Kec. Lubuk Kilangan	</option>
                                                <option>	Kec. Lubuk Begalung	</option>
                                                <option>	Kec. Padang Selatan	</option>
                                                <option>	Kec. Padang Timur	</option>
                                                <option>	Kec. Padang Barat	</option>
                                                <option>	Kec. Padang Utara	</option>
                                                <option>	Kec. Nanggalo	</option>
                                                <option>	Kec. Kuranji	</option>
                                                <option>	Kec. Pauh	</option>
                                                <option>	Kec. Koto Tangah	</option>
                                                <option>	Kec. Padang Panjang Barat	</option>
                                                <option>	Kec. Padang Panjang Timur	</option>
                                                <option>	Kec. Silungkang	</option>
                                                <option>	Kec. Lembah Segar	</option>
                                                <option>	Kec. Barangin	</option>
                                                <option>	Kec. Talawi	</option>
                                                <option>	Kec. Lubuk Sikarah	</option>
                                                <option>	Kec. Tanjung Harapan	</option>
                                                <option>	Kec. Payakumbuh Barat	</option>
                                                <option>	Kec. Payakumbuh Timur	</option>
                                                <option>	Kec. Payakumbuh Utara	</option>
                                                <option>	Kec. Lamposi Tigo Nagori	</option>
                                                <option>	Kec. Payakumbuh Selatan	</option>
                                                <option>	Kec. Pariaman Selatan	</option>
                                                <option>	Kec. Pariaman Tengah	</option>
                                                <option>	Kec. Pariaman Utara	</option>
                                                <option>	Kec. Pariaman Timur	</option>
                                                <option>	Kec. XIII Koto Kampar	</option>
                                                <option>	Kec. Kampar Kiri	</option>
                                                <option>	Kec. Kampar	</option>
                                                <option>	Kec. Tambang	</option>
                                                <option>	Kec. Bangkinang	</option>
                                                <option>	Kec. Bangkinang Barat	</option>
                                                <option>	Kec. Siak Hulu	</option>
                                                <option>	Kec. Tapung	</option>
                                                <option>	Kec. Kampar Kiri Hulu	</option>
                                                <option>	Kec. Kampar Kiri Hilir	</option>
                                                <option>	Kec. Tapung Hulu	</option>
                                                <option>	Kec. Tapung Hilir	</option>
                                                <option>	Kec. Tapung Kiri	</option>
                                                <option>	Kec. Salo	</option>
                                                <option>	Kec. Rumbio Jaya	</option>
                                                <option>	Kec. Bangkinang Seberang	</option>
                                                <option>	Kec. Perhentian Raja	</option>
                                                <option>	Kec. Kampar Timur	</option>
                                                <option>	Kec. Kampar Utara	</option>
                                                <option>	Kec. Kampar Kiri Tengah	</option>
                                                <option>	Kec. Gunung Sahilan	</option>
                                                <option>	Koto Kampar Hulu	</option>
                                                <option>	Kec. Mandau	</option>
                                                <option>	Kec. Bengkalis	</option>
                                                <option>	Kec. Bantan	</option>
                                                <option>	Kec. Bukit Batu	</option>
                                                <option>	Kec. Rupat	</option>
                                                <option>	Kec. Rupat Utara	</option>
                                                <option>	Kec. Siak Kecil	</option>
                                                <option>	Kec. Pinggir	</option>
                                                <option>	Kec. Peranap	</option>
                                                <option>	Kec. Pasir Penyu	</option>
                                                <option>	Kec. Kelayang	</option>
                                                <option>	Kec. Seberida	</option>
                                                <option>	Kec. Rengat	</option>
                                                <option>	Kec. Rengat Barat	</option>
                                                <option>	Kec. Lirik	</option>
                                                <option>	Kec. Batang Gansal	</option>
                                                <option>	Kec. Batang Cenaku	</option>
                                                <option>	Kec. Batang Peranap	</option>
                                                <option>	Kec. Lubuk Batu Jaya	</option>
                                                <option>	Kec. Sei Lala	</option>
                                                <option>	Kec. Rakit Kulim	</option>
                                                <option>	Kec. Kuala Cenaku	</option>
                                                <option>	Kec. Keritang	</option>
                                                <option>	Kec. Reteh	</option>
                                                <option>	Kec. Enok	</option>
                                                <option>	Kec. Tanah Merah	</option>
                                                <option>	Kec. Kuala Indragiri	</option>
                                                <option>	Kec. Tembilahan	</option>
                                                <option>	Kec. Tempuling	</option>
                                                <option>	Kec. Batang Tuaka	</option>
                                                <option>	Kec. Gaung Anak Serka	</option>
                                                <option>	Kec. Gaung	</option>
                                                <option>	Kec. Mandah	</option>
                                                <option>	Kec. Kateman	</option>
                                                <option>	Kec. Kemuning	</option>
                                                <option>	Kec. Pelangiran	</option>
                                                <option>	Kec. Pulau Burung	</option>
                                                <option>	Kec. Teluk Blengkong	</option>
                                                <option>	Kec. Tembilahan Hulu	</option>
                                                <option>	Kec. Concong	</option>
                                                <option>	Kec. Kempas	</option>
                                                <option>	Kec. Sungai Batang	</option>
                                                <option>	Kec. Langgam	</option>
                                                <option>	Kec. Pangkalan Kuras	</option>
                                                <option>	Kec. Bunut	</option>
                                                <option>	Kec. Kuala Kampar	</option>
                                                <option>	Kec. Pangkalan Kerinci	</option>
                                                <option>	Kec. Ukui	</option>
                                                <option>	Kec. Pangkalan Lesung	</option>
                                                <option>	Kec. Kerumutan	</option>
                                                <option>	Kec. Pelalawan	</option>
                                                <option>	Kec. Teluk Meranti	</option>
                                                <option>	Kec. Bandar Sei Kijang	</option>
                                                <option>	Kec. Bandar Petalangan	</option>
                                                <option>	Kec. Ujung Batu	</option>
                                                <option>	Kec. Rokan IV Koto	</option>
                                                <option>	Kec. Rambah	</option>
                                                <option>	Kec. Tembusai	</option>
                                                <option>	Kec. Kepenuhan	</option>
                                                <option>	Kec. Kunto Darussalam	</option>
                                                <option>	Kec. Rambah Samo	</option>
                                                <option>	Kec. Rambah Hilir	</option>
                                                <option>	Kec. Tembusai Utara	</option>
                                                <option>	Kec. Bangun Purba	</option>
                                                <option>	Kec. Tandun	</option>
                                                <option>	Kec. Kabun	</option>
                                                <option>	Kec. Bonai Darussalam	</option>
                                                <option>	Kec. Pagaran Tapah Darussalam	</option>
                                                <option>	Kec. Pendalian IV Koto	</option>
                                                <option>	Kec. Kepenuhan Hulu	</option>
                                                <option>	Kec. Kubu	</option>
                                                <option>	Kec. Bangko	</option>
                                                <option>	Kec. Tanah Putih	</option>
                                                <option>	Kec. Rimba Melintang	</option>
                                                <option>	Kec. Bagan Sinembah	</option>
                                                <option>	Kec. Pasir Limau Kapas	</option>
                                                <option>	Kec. Sinaboi	</option>
                                                <option>	Kec. Tanah Putih Tanjung Melaw	</option>
                                                <option>	Kec. Pujud	</option>
                                                <option>	Kec. Bangko Pusako	</option>
                                                <option>	Kec. Simpang Kanan	</option>
                                                <option>	Kec. Batu Hampar	</option>
                                                <option>	Kec. Rantau Kopar	</option>
                                                <option>	Kec. Minas	</option>
                                                <option>	Kec. Siak	</option>
                                                <option>	Kec. Sungai Apit	</option>
                                                <option>	Kec. Tualang	</option>
                                                <option>	Kec. Kerinci Kanan	</option>
                                                <option>	Kec. Dayun	</option>
                                                <option>	Kec. Bunga Raya	</option>
                                                <option>	Kec. Sungai Mandau	</option>
                                                <option>	Kec. Kandis	</option>
                                                <option>	Kec. Lubuk Dalam	</option>
                                                <option>	Kec. Koto Gasip	</option>
                                                <option>	Kec. Pusako	</option>
                                                <option>	Kec. Sabak Auh	</option>
                                                <option>	Kec. Sungai Mempura	</option>
                                                <option>	Mempura	</option>
                                                <option>	Kec. Kuantan Mudik	</option>
                                                <option>	Kec. Kuantan Tengah	</option>
                                                <option>	Kec. Benai	</option>
                                                <option>	Kec. Singingi	</option>
                                                <option>	Kec. Kuantan Hilir	</option>
                                                <option>	Kec. Cerenti	</option>
                                                <option>	Kec. Pangean	</option>
                                                <option>	Kec. Logas Tanah Darat	</option>
                                                <option>	Kec. Inuman	</option>
                                                <option>	Kec. Singingi Hilir	</option>
                                                <option>	Kec. Hulu Kuantan	</option>
                                                <option>	Kec. Gunung Toar	</option>
                                                <option>	Kec. Merbau	</option>
                                                <option>	Kec. Rangsang	</option>
                                                <option>	Kec. Rangsang Barat	</option>
                                                <option>	Kec. Tebing Tinggi	</option>
                                                <option>	Kec. Tebing Tinggi Barat	</option>
                                                <option>	Kec. Tasik Putri Puyu	</option>
                                                <option>	Kec. Pulau Merbau	</option>
                                                <option>	Kec. Tebing Tinggi Timur	</option>
                                                <option>	Kec. Rangsang Pesisir	</option>
                                                <option>	Kec. Tampan	</option>
                                                <option>	Kec. Bukit Raya	</option>
                                                <option>	Kec. Lima Puluh	</option>
                                                <option>	Kec. Sail	</option>
                                                <option>	Kec. Pekanbaru Kota	</option>
                                                <option>	Kec. Sukajadi	</option>
                                                <option>	Kec. Senapelan	</option>
                                                <option>	Kec. Rumbai	</option>
                                                <option>	Kec. Tenayan Raya	</option>
                                                <option>	Kec. Marpoyan Damai	</option>
                                                <option>	Kec. Rumbai Pesisir	</option>
                                                <option>	Kec. Payung Sekaki	</option>
                                                <option>	Kec. Bukit Kapur	</option>
                                                <option>	Kec. Dumai Barat	</option>
                                                <option>	Kec. Dumai Timur	</option>
                                                <option>	Kec. Medang Kampai	</option>
                                                <option>	Kec. Sungai Sembilan	</option>
                                                <option>	Kec. Dumai Kota	</option>
                                                <option>	Kec. Dumai Selatan	</option>
                                                <option>	Kec. Mersan	</option>
                                                <option>	Kec. Muara Tembesi	</option>
                                                <option>	Kec. Batin XXIV	</option>
                                                <option>	Kec. Muara Bulian	</option>
                                                <option>	Kec. Pemayung	</option>
                                                <option>	Kec. Maro Sebo Ulu	</option>
                                                <option>	Kec. Bajubang	</option>
                                                <option>	Kec. Maro Sebo Ilir	</option>
                                                <option>	Kec. Tanah Tumbuh	</option>
                                                <option>	Kec. Tanah Sepenggal	</option>
                                                <option>	Kec. Jujuhan	</option>
                                                <option>	Kec. Rantau Pandan	</option>
                                                <option>	Kec. Pasar Muara Bungo	</option>
                                                <option>	Kec. Pelepat	</option>
                                                <option>	Kec. Pelepat Ilir	</option>
                                                <option>	Kec. Limbur Lubuk Mengkuang	</option>
                                                <option>	Kec. Bathin II Babeko	</option>
                                                <option>	Kec. Muko Muko Batin VII	</option>
                                                <option>	Kec. Bungo Dani	</option>
                                                <option>	Kec. Bathin III	</option>
                                                <option>	Kec. Bathin III Ulu	</option>
                                                <option>	Kec. Tanah Sepenggal Luas	</option>
                                                <option>	Kec. Bathin II Pelayang	</option>
                                                <option>	Kec. Jujuhan Ilir	</option>
                                                <option>	Kec. Rimbo Tengah	</option>
                                                <option>	Tanah Sepenggal Lintas	</option>
                                                <option>	Kec. Batang Asai	</option>
                                                <option>	Kec. Limun	</option>
                                                <option>	Kec. Sarolangon	</option>
                                                <option>	Kec. Pelawan	</option>
                                                <option>	Kec. Pauh	</option>
                                                <option>	Kec. Mandiangin	</option>
                                                <option>	Kec. Air Hitam	</option>
                                                <option>	Kec. Bathin VIII	</option>
                                                <option>	Kec. Singkut	</option>
                                                <option>	Kec. Cermin Nan Gadang	</option>
                                                <option>	Kec. Tungkal Ulu	</option>
                                                <option>	Kec. Pengabuan	</option>
                                                <option>	Kec. Tungkal Ilir	</option>
                                                <option>	Kec. Betara	</option>
                                                <option>	Kec. Merlung	</option>
                                                <option>	Kec. Batang Asam	</option>
                                                <option>	Kec. Tebing Tinggi	</option>
                                                <option>	Kec. Renah Mendalu	</option>
                                                <option>	Kec. Muara Papalik	</option>
                                                <option>	Kec. Senyerang	</option>
                                                <option>	Kec. Bram Itam	</option>
                                                <option>	Kec. Seberang Kota	</option>
                                                <option>	Kec. Kuala Betara	</option>
                                                <option>	Kec. Gunung Raya	</option>
                                                <option>	Kec. Batang Merangin	</option>
                                                <option>	Kec. Keliling Danau	</option>
                                                <option>	Kec. Danau Kerinci	</option>
                                                <option>	Kec. Sitinjau Laut	</option>
                                                <option>	Kec. Air Hangat	</option>
                                                <option>	Kec. Gunung Kerinci	</option>
                                                <option>	Kec. Kayu Aro	</option>
                                                <option>	Kec. Air Hangat Timur	</option>
                                                <option>	Kec. Gunung Tujuh	</option>
                                                <option>	Kec. Siulak	</option>
                                                <option>	Kec. Depati Tujuh	</option>
                                                <option>	Kec. Rimbo Bujang	</option>
                                                <option>	Kec. Tebo Ilir	</option>
                                                <option>	Kec. Tebo Tengah	</option>
                                                <option>	Kec. Tebo Ulu	</option>
                                                <option>	Kec. Sumay	</option>
                                                <option>	Kec. VII Koto	</option>
                                                <option>	Kec. Rimbo Ilir	</option>
                                                <option>	Kec. Rimbo Ulu	</option>
                                                <option>	Kec. Tengah Ilir	</option>
                                                <option>	Kec. Serai Serumpun	</option>
                                                <option>	Kec. VII Koto Ilir	</option>
                                                <option>	Kec. Muara Tabir	</option>
                                                <option>	Kec. Jambi Luar Kota	</option>
                                                <option>	Kec. Mestong	</option>
                                                <option>	Kec. Kumpeh Ulu	</option>
                                                <option>	Kec. Sekernan	</option>
                                                <option>	Kec. Maro Sebo	</option>
                                                <option>	Kec. Kumpeh	</option>
                                                <option>	Kec. Sungai Bahar	</option>
                                                <option>	Kec. Sungai Gelam	</option>
                                                <option>	Kec. Muara Sabak	</option>
                                                <option>	Kec. Mendahara	</option>
                                                <option>	Kec. Dendang	</option>
                                                <option>	Kec. Nipah Panjang	</option>
                                                <option>	Kec. Rantau Rasau	</option>
                                                <option>	Kec. Sadu	</option>
                                                <option>	Kec. Mendahara Ulu	</option>
                                                <option>	Kec. Geragai	</option>
                                                <option>	Kec. Muara Sabak Barat	</option>
                                                <option>	Kec. Kuala Jambi	</option>
                                                <option>	Kec. Berbak	</option>
                                                <option>	Muara Sabak Timur	</option>
                                                <option>	Kec. Jangkat	</option>
                                                <option>	Kec. Bangko	</option>
                                                <option>	Kec. Pamenang	</option>
                                                <option>	Kec. Muara Siau	</option>
                                                <option>	Kec. Sungai Manau	</option>
                                                <option>	Kec. Tabir	</option>
                                                <option>	Kec. Tabir Ulu	</option>
                                                <option>	Kec. Lembah Masuarai	</option>
                                                <option>	Kec. Tabir Selatan	</option>
                                                <option>	Kec. Bangko Barat	</option>
                                                <option>	Kec. Nalo Tantan	</option>
                                                <option>	Kec. Batang Masumai	</option>
                                                <option>	Kec. Pamenang Barat	</option>
                                                <option>	Kec. Tabir Ilir	</option>
                                                <option>	Kec. Tabir Timur	</option>
                                                <option>	Kec. Renah Pemparap	</option>
                                                <option>	Kec. Pangkalan Jambu	</option>
                                                <option>	Kec. Sungai Tenang	</option>
                                                <option>	Kec. Tiang Pumpung	</option>
                                                <option>	Kec. Renah Pamenang	</option>
                                                <option>	Kec. Pamenang Selatan	</option>
                                                <option>	Kec. Tabir Lintas	</option>
                                                <option>	Kec. Margo Tabir	</option>
                                                <option>	Kec. Tabir Barat	</option>
                                                <option>	Kec. Kota Baru	</option>
                                                <option>	Kec. Jambi Selatan	</option>
                                                <option>	Kec. Jelutung	</option>
                                                <option>	Kec. Pasar Jambi	</option>
                                                <option>	Kec. Telanai Pura	</option>
                                                <option>	Kec. Danau Teluk	</option>
                                                <option>	Kec. Pelayangan	</option>
                                                <option>	Kec. Jambi Timur	</option>
                                                <option>	Kec. Tanah Kampung	</option>
                                                <option>	Kec. Sungai Penuh	</option>
                                                <option>	Kec. Hamparan Rawang	</option>
                                                <option>	Kec. Pesisir Bukit	</option>
                                                <option>	Kec. Kumun Debai	</option>
                                                <option>	Kec. Sanga Desa	</option>
                                                <option>	Kec. Babat Toman	</option>
                                                <option>	Kec. Sungai Keruh	</option>
                                                <option>	Kec. Sekayu	</option>
                                                <option>	Kec. Sungai Lilin	</option>
                                                <option>	Kec. Bayung Lencir	</option>
                                                <option>	Kec. Lais	</option>
                                                <option>	Kec. Batanghari Leko	</option>
                                                <option>	Kec. Keluang	</option>
                                                <option>	Kec. Plakat Tinggi	</option>
                                                <option>	Kec. Lalan	</option>
                                                <option>	Kec. Tungkal Jaya	</option>
                                                <option>	Kec. Lawang Wetan	</option>
                                                <option>	Kec. Babat Supat	</option>
                                                <option>	Kec. Lempuing	</option>
                                                <option>	Kec. Mesuji	</option>
                                                <option>	Kec. Tulung Selapan	</option>
                                                <option>	Kec. Pedamaran	</option>
                                                <option>	Kec. Tanjung Lubuk	</option>
                                                <option>	Kec. Kota Ayu Agung	</option>
                                                <option>	Kec. Sirah Pulau Padang	</option>
                                                <option>	Kec. Pampangan	</option>
                                                <option>	Kec. Air Sugihan	</option>
                                                <option>	Kec. Cengal	</option>
                                                <option>	Kec. Jejawi	</option>
                                                <option>	Kec. Sungai Menang	</option>
                                                <option>	Kec. Lempuing Jaya	</option>
                                                <option>	Kec. Mesuji Makmur	</option>
                                                <option>	Kec. Mesuji Raya	</option>
                                                <option>	Kec. Pedamaran Timur	</option>
                                                <option>	Kec. Teluk Gelam	</option>
                                                <option>	Kec. Pangkalan Lapam	</option>
                                                <option>	Kec. Sosoh Buay Rayap	</option>
                                                <option>	Kec. Pengandonan	</option>
                                                <option>	Kec. Peninjauan	</option>
                                                <option>	Kec. Lubuk Batang	</option>
                                                <option>	Kec. Ulu Ogan	</option>
                                                <option>	Kec. Semidang Aji	</option>
                                                <option>	Kec. Lengkiti	</option>
                                                <option>	Kec. Baturaja Timur	</option>
                                                <option>	Kec. Baturaja Barat	</option>
                                                <option>	Kec. Sinar Peninjauan	</option>
                                                <option>	Kec. Lubuk Raja	</option>
                                                <option>	Kec. Muara Jaya	</option>
                                                <option>	Kec. Semende Darat Laut	</option>
                                                <option>	Kec. Tanjung Agung	</option>
                                                <option>	Kec. Lawang Kidul	</option>
                                                <option>	Kec. Muara Enim	</option>
                                                <option>	Kec. Gunung Megang	</option>
                                                <option>	Kec. Talang Ubi	</option>
                                                <option>	Kec. Gelumbang	</option>
                                                <option>	Kec. Sungai Rotan	</option>
                                                <option>	Kec. Lembak	</option>
                                                <option>	Kec. Benakat	</option>
                                                <option>	Kec. Ujan Mas	</option>
                                                <option>	Kec. Penukal	</option>
                                                <option>	Kec. Penukal Utara	</option>
                                                <option>	Kec. Tanah Abang	</option>
                                                <option>	Kec. Lubai	</option>
                                                <option>	Kec. Rambang	</option>
                                                <option>	Kec. Semende Darat Ulu	</option>
                                                <option>	Kec. Semende Darat Tengah	</option>
                                                <option>	Kec. Rambang Dangku	</option>
                                                <option>	Kec. Abab	</option>
                                                <option>	Kec. Kelekar	</option>
                                                <option>	Kec. Muara Belida	</option>
                                                <option>	Abab	</option>
                                                <option>	Talang Ubi	</option>
                                                <option>	Penukal Utara	</option>
                                                <option>	Tanah Abang	</option>
                                                <option>	Penukal	</option>
                                                <option>	Kec. Kota Agung	</option>
                                                <option>	Kec. Pulau Pinang	</option>
                                                <option>	Kec. Jarai	</option>
                                                <option>	Kec. Kikim Timur	</option>
                                                <option>	Kec. Lahat	</option>
                                                <option>	Kec. Mulak Ulu	</option>
                                                <option>	Kec. Pajar Bulan	</option>
                                                <option>	Kec. Kikim Selatan	</option>
                                                <option>	Kec. Kikim Barat	</option>
                                                <option>	Kec. Kikim Tengah	</option>
                                                <option>	Kec. Tanjung Sakti Pumi	</option>
                                                <option>	Kec. Tanjung Sakti Pumu	</option>
                                                <option>	Kec. Merapi Barat	</option>
                                                <option>	Kec. Pseksu	</option>
                                                <option>	Kec. Gumay Talang	</option>
                                                <option>	Kec. Pagar Gunung	</option>
                                                <option>	Kec. Merapi Timur	</option>
                                                <option>	Kec. Gumay Ulu	</option>
                                                <option>	Kec. Merapi Selatan	</option>
                                                <option>	kec. Tanjung Tebat	</option>
                                                <option>	Kec. Muara Payang	</option>
                                                <option>	Kec. Rawas Ulu	</option>
                                                <option>	Kec. Rupit	</option>
                                                <option>	Kec. STL Ulu Terawas	</option>
                                                <option>	Kec. Tugu Mulyo	</option>
                                                <option>	Kec. Muara Beliti	</option>
                                                <option>	Kec. Jayaloka	</option>
                                                <option>	Kec. Muara Kelingi	</option>
                                                <option>	Kec. Muara Lakitan	</option>
                                                <option>	Kec. Megang Sakti	</option>
                                                <option>	Kec. Rawas Ilir	</option>
                                                <option>	Kec. Purwodadi	</option>
                                                <option>	Kec. Selangit	</option>
                                                <option>	Kec. Karang Jaya	</option>
                                                <option>	Kec. Karang Dapo	</option>
                                                <option>	Kec. Bulan Tengah Suku Ulu	</option>
                                                <option>	Kec. Ulu Rawas	</option>
                                                <option>	Kec. Nibung	</option>
                                                <option>	Kec. Tiang Pumpung Kepungut	</option>
                                                <option>	Kec. Sumber Harta	</option>
                                                <option>	Kec. Tanah Negeri	</option>
                                                <option>	Kec. Suka Karya	</option>
                                                <option>	Kec. Rantau Bayur	</option>
                                                <option>	Kec. Talang Kelapa	</option>
                                                <option>	Kec. Banyuasin III	</option>
                                                <option>	Kec. Betung	</option>
                                                <option>	Kec. Makarti Jaya	</option>
                                                <option>	Kec. Banyuasin I	</option>
                                                <option>	Kec. Banyuasin II	</option>
                                                <option>	Kec. Rambutan	</option>
                                                <option>	Kec. Muara Telang	</option>
                                                <option>	Kec. Muara Padang	</option>
                                                <option>	Kec. Tanjung Lago	</option>
                                                <option>	Kec. Muara Sugihan	</option>
                                                <option>	Kec. Air Salek	</option>
                                                <option>	Kec. Tungkal Ilir	</option>
                                                <option>	Kec. Suak Tapeh	</option>
                                                <option>	Kec. Pulau Rimau	</option>
                                                <option>	Kec. Sembawa	</option>
                                                <option>	Kec. Martapura	</option>
                                                <option>	Kec. Buay Madang	</option>
                                                <option>	Kec. Cempaka	</option>
                                                <option>	Kec. Madang Suku I	</option>
                                                <option>	Kec. Madang Suku II	</option>
                                                <option>	Kec. Belitang I	</option>
                                                <option>	Kec. Belitang II	</option>
                                                <option>	Kec. Belitang III	</option>
                                                <option>	Kec. Buay Pemuka Peliung	</option>
                                                <option>	Kec. Semendawai Suku III	</option>
                                                <option>	Kec. Bunga Mayang	</option>
                                                <option>	Kec. Buay Madang Timur	</option>
                                                <option>	Kec. Madang Suku III	</option>
                                                <option>	Kec. Semendawai Barat	</option>
                                                <option>	Kec. Semendawai Timur	</option>
                                                <option>	Kec. Jayapura	</option>
                                                <option>	Kec. Belitang Jaya	</option>
                                                <option>	Kec. Belitang Madang Raya	</option>
                                                <option>	Kec. Belitang Mulia	</option>
                                                <option>	Kec. Buay Pemuka Bangsa Raja	</option>
                                                <option>	Belitang	</option>
                                                <option>	Kec. Banding Agung	</option>
                                                <option>	Kec. Pulau Beringin	</option>
                                                <option>	Kec. Muaradua Kisam	</option>
                                                <option>	Kec. Muaradua	</option>
                                                <option>	Kec. Simpang	</option>
                                                <option>	Kec. Buay Sandang Aji	</option>
                                                <option>	Kec. Buay Runjung	</option>
                                                <option>	Kec. Buay Pemaca	</option>
                                                <option>	Kec. Mekakau Ilir	</option>
                                                <option>	Kec. Kisam Tinggi	</option>
                                                <option>	Kec. Kisam Ilir	</option>
                                                <option>	Kec. Buay Pematang Ribu Ranau	</option>
                                                <option>	Kec. Warkuk Ranau Selatan	</option>
                                                <option>	Kec. Runjung Agung	</option>
                                                <option>	Kec. Sungai Are	</option>
                                                <option>	Kec. Sidang Danau	</option>
                                                <option>	Kec. Buana Pemaca	</option>
                                                <option>	Kec. Tiga Dihaji	</option>
                                                <option>	Kec. Buay Rawan	</option>
                                                <option>	Kec. Tanjung Raja	</option>
                                                <option>	Kec. Muara Kuang	</option>
                                                <option>	Kec. Tanjung Batu	</option>
                                                <option>	Kec. Indralaya	</option>
                                                <option>	Kec. Pemulutan	</option>
                                                <option>	Kec. Rantau Alai	</option>
                                                <option>	Kec. Rambang Kuang	</option>
                                                <option>	Kec. Lubuk Keliat	</option>
                                                <option>	Kec. Payaraman	</option>
                                                <option>	Kec. Kandis	</option>
                                                <option>	Kec. Pemulutan Selatan	</option>
                                                <option>	Kec. Pemulutan Barat	</option>
                                                <option>	Kec. Indralaya Selatan	</option>
                                                <option>	Kec. Indralaya Utara	</option>
                                                <option>	Kec. Rantau Panjang	</option>
                                                <option>	Kec. Sungai Pinang	</option>
                                                <option>	Kec. Lintang Kanan	</option>
                                                <option>	Kec. Muara Pinang	</option>
                                                <option>	Kec. Pendopo	</option>
                                                <option>	Kec. Ulu Musi	</option>
                                                <option>	Kec. Tebing Tinggi	</option>
                                                <option>	Kec. Talang Padang	</option>
                                                <option>	Kec. Pasemah Air Keruh	</option>
                                                <option>	Kec. Sikap Dalam	</option>
                                                <option>	Kec. Ilir Barat II	</option>
                                                <option>	Kec. Seberang Ulu I	</option>
                                                <option>	Kec. Seberang Ulu II	</option>
                                                <option>	Kec. Ilir Barat I	</option>
                                                <option>	Kec. Ilir Timur I	</option>
                                                <option>	Kec. Ilir Timur II	</option>
                                                <option>	Kec. Sako	</option>
                                                <option>	Kec. Sukarami	</option>
                                                <option>	Kec. Kemuning	</option>
                                                <option>	Kec. Plaju	</option>
                                                <option>	Kec. Kertapati	</option>
                                                <option>	Kec. Gandus	</option>
                                                <option>	Kec. Bukit Kecil	</option>
                                                <option>	Kec. Kalidoni	</option>
                                                <option>	Kec. Alang-Alang Lebar	</option>
                                                <option>	Kec. Sematang Borang	</option>
                                                <option>	Kec. Prabumulih Timur	</option>
                                                <option>	Kec. Prabumulih Barat	</option>
                                                <option>	Kec. Cambai	</option>
                                                <option>	Kec. Rambang Kapak Tengah	</option>
                                                <option>	Kec. Prabumulih Utara	</option>
                                                <option>	Kec. Prabumulih Selatan	</option>
                                                <option>	Kec. Lubuk Linggau Barat I	</option>
                                                <option>	Kec. Lubuk Linggau Timur I	</option>
                                                <option>	Kec. Lubuk Linggau Selatan I	</option>
                                                <option>	Kec. Lubuk Linggau Utara I	</option>
                                                <option>	Kec. Lubuk Linggau Barat II	</option>
                                                <option>	Kec. Lubuk Linggau Timur II	</option>
                                                <option>	Kec. Lubuk Linggau Selatan II	</option>
                                                <option>	Kec. Lubuk Linggau Utara II	</option>
                                                <option>	Kec. Pagar Alam Utara	</option>
                                                <option>	Kec. Pagar Alam Selatan	</option>
                                                <option>	Kec. Dempo Utara	</option>
                                                <option>	Kec. Dempo Selatan	</option>
                                                <option>	Kec. Dempo Tengah	</option>
                                                <option>	Kec. Natar	</option>
                                                <option>	Kec. Jati Agung	</option>
                                                <option>	Kec. Tanjung Bintang	</option>
                                                <option>	Kec. Katibung	</option>
                                                <option>	Kec. Sidomulyo	</option>
                                                <option>	Kec. Palas	</option>
                                                <option>	Kec. Penengahan	</option>
                                                <option>	Kec. Merbau Mataram	</option>
                                                <option>	Kec. Candipuro	</option>
                                                <option>	Kec. Rajabasa	</option>
                                                <option>	Kec. Sragi	</option>
                                                <option>	Kec. Ketapang	</option>
                                                <option>	Kec. Katibung	</option>
                                                <option>	Kec. Bakauheni	</option>
                                                <option>	Kec. Tanjung Sari	</option>
                                                <option>	Kec. Way Sulan	</option>
                                                <option>	Kec. Way Panji	</option>
                                                <option>	Kalianda	</option>
                                                <option>	Kec. Padang Ratu	</option>
                                                <option>	Kec. Kalirejo	</option>
                                                <option>	Kec. Bangunrejo	</option>
                                                <option>	Kec. Gunung Sugih	</option>
                                                <option>	Kec. Trimurjo	</option>
                                                <option>	Kec. Punggur	</option>
                                                <option>	Kec. Seputih Raman	</option>
                                                <option>	Kec. Terbanggi Besar	</option>
                                                <option>	Kec. Terusan Nunyai	</option>
                                                <option>	Kec. Seputih Mataram	</option>
                                                <option>	Kec. Seputih Banyak	</option>
                                                <option>	Kec. Rumbia	</option>
                                                <option>	Kec. Seputih Surabaya	</option>
                                                <option>	Kec. Bumi Ratu Nuban	</option>
                                                <option>	Kec. Way Pengubuan	</option>
                                                <option>	Kec. Seputih Agung	</option>
                                                <option>	Kec. Bandar Mataram	</option>
                                                <option>	Kec. Sendang Agung	</option>
                                                <option>	Kec. Anak Tuha	</option>
                                                <option>	Kec. Pubian	</option>
                                                <option>	Kec. Bandar Surabaya	</option>
                                                <option>	Kec. Bumi Nabung	</option>
                                                <option>	Kec. Way Seputih	</option>
                                                <option>	Kec. Kota Gajah	</option>
                                                <option>	Kec. Selagai Lingga	</option>
                                                <option>	Kec. Bekri	</option>
                                                <option>	Kec. Anak Ratu Aji	</option>
                                                <option>	Kec. Putra Rumbia	</option>
                                                <option>	Kec. Bukit Kemuning	</option>
                                                <option>	Kec. Tanjung Raja	</option>
                                                <option>	Kec. Abung Barat	</option>
                                                <option>	Kec. Kotabumi Kota	</option>
                                                <option>	Kec. Abung Selatan	</option>
                                                <option>	Kec. Abung Timur	</option>
                                                <option>	Kec. Sungkai Selatan	</option>
                                                <option>	Kec. Sungkai Utara	</option>
                                                <option>	Kec. Abung Tinggi	</option>
                                                <option>	Kec. Abung Tengah	</option>
                                                <option>	Kec. Kotabumi Utara	</option>
                                                <option>	Kec. Kotabumi Selatan	</option>
                                                <option>	Kec. Abung Semuli	</option>
                                                <option>	Kec. Abung Surakarta	</option>
                                                <option>	Kec. Muara Sungkai	</option>
                                                <option>	Kec. Bunga Mayang	</option>
                                                <option>	Kec. Hulu Sungkai	</option>
                                                <option>	Kec. Sungkai Tengah	</option>
                                                <option>	Kec. Abung Pekurun	</option>
                                                <option>	Kec. Sungkai Jaya	</option>
                                                <option>	Kec. Sungkai Barat	</option>
                                                <option>	Kec. Abung Kunang	</option>
                                                <option>	Kec. Blambangan Pagar	</option>
                                                <option>	Kec. Pesisir Selatan	</option>
                                                <option>	Kec. Pesisir Tengah	</option>
                                                <option>	Kec. Pesisir Utara	</option>
                                                <option>	Kec. Balik Bukit	</option>
                                                <option>	Kec. Belalau	</option>
                                                <option>	Kec. Sumber Jaya	</option>
                                                <option>	Kec. Sekincau	</option>
                                                <option>	Kec. Bengkunat	</option>
                                                <option>	Kec. Batu Brak	</option>
                                                <option>	Kec. Karyapenggawa	</option>
                                                <option>	Kec. Lemong	</option>
                                                <option>	Kec. Waytenong	</option>
                                                <option>	Kec. Sukau	</option>
                                                <option>	Kec. Suoh	</option>
                                                <option>	Kec. Gedung Surian	</option>
                                                <option>	Kec. Bengkunat Belimbing	</option>
                                                <option>	Kec. Ngambur	</option>
                                                <option>	Pagar Dewa	</option>
                                                <option>	Lumbok Seminung	</option>
                                                <option>	Kec. Banjar Agung	</option>
                                                <option>	Kec. Gedung Aji	</option>
                                                <option>	Kec. Manggala	</option>
                                                <option>	Kec. Penawartama	</option>
                                                <option>	Kec. Rawajitu Selatan	</option>
                                                <option>	Kec. Gedung Meneng	</option>
                                                <option>	Kec. Banjar Margo	</option>
                                                <option>	Kec. Penawar Aji	</option>
                                                <option>	Kec. Rawa Pitu	</option>
                                                <option>	Kec. Rawajitu Timur	</option>
                                                <option>	Kec. Meraksa Aji	</option>
                                                <option>	Kec. Gedung AJI Baru	</option>
                                                <option>	Kec. Dente Teladas	</option>
                                                <option>	Kec. Banjar Baru	</option>
                                                <option>	Kec. Menggala Timur	</option>
                                                <option>	Menggala	</option>
                                                <option>	Kec. Wonosobo	</option>
                                                <option>	Kec. Kota Agung	</option>
                                                <option>	Kec. Pulau Panggung	</option>
                                                <option>	Kec. Talang Padang	</option>
                                                <option>	Kec. Pugung	</option>
                                                <option>	Kec. Cukuh Balak	</option>
                                                <option>	Kec. Pematang Sawa	</option>
                                                <option>	Kec. Sumberejo	</option>
                                                <option>	Kec. Semaka	</option>
                                                <option>	Kec. Ulu Belu	</option>
                                                <option>	Kec. Kelumbayan	</option>
                                                <option>	Kota Agung Barat	</option>
                                                <option>	Kota Agung Timur	</option>
                                                <option>	Kec. Gisting	</option>
                                                <option>	Kec. Gunung Alip	</option>
                                                <option>	Kec. Limau	</option>
                                                <option>	Kec. Bandar Negeri Semuong	</option>
                                                <option>	Kec. Air Naningan	</option>
                                                <option>	Kec. Bulok	</option>
                                                <option>	Kec. Kelumbayan Barat	</option>
                                                <option>	Kec. Metro Kibang	</option>
                                                <option>	Kec. Batanghari	</option>
                                                <option>	Kec. Sekampung	</option>
                                                <option>	Kec. Margatiga	</option>
                                                <option>	Kec. Sekampung Udik	</option>
                                                <option>	Kec. Jabung	</option>
                                                <option>	Kec. Labuhan Maringgai	</option>
                                                <option>	Kec. Way Jepara	</option>
                                                <option>	Kec. Sukadana	</option>
                                                <option>	Kec. Pekalongan	</option>
                                                <option>	Kec. Raman Utara	</option>
                                                <option>	Kec. Purbolinggo	</option>
                                                <option>	Kec. Bumi Agung	</option>
                                                <option>	Kec. Braja Slebah	</option>
                                                <option>	Kec. Bandar Sribawono	</option>
                                                <option>	Kec. Mataram Baru	</option>
                                                <option>	Kec. Melinting	</option>
                                                <option>	Kec. Gunung Pelindung	</option>
                                                <option>	Kec. Waway Karya	</option>
                                                <option>	Kec. Pasir Sakti	</option>
                                                <option>	Kec. Labuhan Ratu	</option>
                                                <option>	Kec. Batanghari Nuban	</option>
                                                <option>	Kec. Way Bungur	</option>
                                                <option>	Kec. Marga Sekampung	</option>
                                                <option>	Kec. Banjit	</option>
                                                <option>	Kec. Baradatu	</option>
                                                <option>	Kec. Kasui	</option>
                                                <option>	Kec. Blambangan Umpu	</option>
                                                <option>	Kec. Bahuga	</option>
                                                <option>	Kec. Pakuan Ratu	</option>
                                                <option>	Kec. Gunung Labuhan	</option>
                                                <option>	Kec. Rebang Tangkas	</option>
                                                <option>	Kec. Way Tuba	</option>
                                                <option>	Kec. Negeri Agung	</option>
                                                <option>	Kec. Negara Batin	</option>
                                                <option>	Kec. Negeri Besar	</option>
                                                <option>	Kec. Buay Bahuga	</option>
                                                <option>	Kec. Bumi Agung	</option>
                                                <option>	Kec. Padang Cermin	</option>
                                                <option>	Kec. Punduh Pedada	</option>
                                                <option>	Kec. Kedondong	</option>
                                                <option>	Kec. Way Lima	</option>
                                                <option>	Kec. Gedung Tataan	</option>
                                                <option>	Kec. Negeri Katon	</option>
                                                <option>	Kec. Tegineneng	</option>
                                                <option>	Kec. Pagelaran	</option>
                                                <option>	Kec. Sukoharjo	</option>
                                                <option>	Kec. Adiluwih	</option>
                                                <option>	Kec. Banyumas	</option>
                                                <option>	Kec. Pringsewu	</option>
                                                <option>	Kec. Ambarawa	</option>
                                                <option>	Kec. Gadingrejo	</option>
                                                <option>	Kec. Pardasuka	</option>
                                                <option>	Kec. Mesuji	</option>
                                                <option>	Kec. Tanjung Raya	</option>
                                                <option>	Kec. Rawajitu Utara	</option>
                                                <option>	Kec. Mesuji Timur	</option>
                                                <option>	Kec. Simpang Pematang	</option>
                                                <option>	Kec. Way Serdang	</option>
                                                <option>	Kec. Panca Jaya	</option>
                                                <option>	Kec. Tulang Bawang Udik	</option>
                                                <option>	Kec. Tumijajar	</option>
                                                <option>	Kec. Tulang Bawang Tengah	</option>
                                                <option>	Kec. Lambu Kibang	</option>
                                                <option>	Kec. Pagar Dewa	</option>
                                                <option>	Kec. Way Kenanga	</option>
                                                <option>	Kec. Gunung Terang	</option>
                                                <option>	Kec. Gunung Agung	</option>
                                                <option>	Kec. Teluk Betung Barat	</option>
                                                <option>	Kec. Teluk Betung Selatan	</option>
                                                <option>	Kec. Panjang	</option>
                                                <option>	Kec. Tanjung Karang Timur	</option>
                                                <option>	Kec. Teluk Betung Utara	</option>
                                                <option>	Kec. Tanjung Karang Pusat	</option>
                                                <option>	Kec. Tanjung Karang Barat	</option>
                                                <option>	Kec. Kedaton	</option>
                                                <option>	Kec. Sukarame	</option>
                                                <option>	Kec. Kemiling	</option>
                                                <option>	Kec. Rajabasa	</option>
                                                <option>	Kec. Tanjung Senang	</option>
                                                <option>	Kec. Sukabumi	</option>
                                                <option>	Kec. Metro Pusat	</option>
                                                <option>	Kec. Metro Utara	</option>
                                                <option>	Kec. Metro Barat	</option>
                                                <option>	Kec. Metro Timur	</option>
                                                <option>	Kec. Metro Selatan	</option>
                                                <option>	Kec. Selakau	</option>
                                                <option>	Kec. Pemangkat	</option>
                                                <option>	Kec. Tebas	</option>
                                                <option>	Kec. Sambas	</option>
                                                <option>	Kec. Jawai	</option>
                                                <option>	Kec. Teluk Keramat	</option>
                                                <option>	Kec. Sejangkung	</option>
                                                <option>	Kec. Sajingan Besar	</option>
                                                <option>	Kec. Paloh	</option>
                                                <option>	Kec. Subah	</option>
                                                <option>	Kec. Galing	</option>
                                                <option>	Kec. Semparuk	</option>
                                                <option>	Kec. Tekarang	</option>
                                                <option>	Kec. Sajad	</option>
                                                <option>	Kec. Sebawi	</option>
                                                <option>	Kec. Jawai Selatan	</option>
                                                <option>	Kec. Tangaran	</option>
                                                <option>	Kec. Selakau Tua	</option>
                                                <option>	Kec. Salatiga	</option>
                                                <option>	Selakau Timur	</option>
                                                <option>	Kec. Siantan	</option>
                                                <option>	Kec. Sungai Pinyuh	</option>
                                                <option>	Kec. Mempawah Hilir	</option>
                                                <option>	Kec. Sungai Kunyit	</option>
                                                <option>	Kec. Toho	</option>
                                                <option>	Kec. Segedong	</option>
                                                <option>	Kec. Anjongan	</option>
                                                <option>	Kec. Sadaniang	</option>
                                                <option>	Kec. Mempawah Timur	</option>
                                                <option>	Kec. Ambawang	</option>
                                                <option>	Kec. Toba	</option>
                                                <option>	Kec. Sanggau Kapuas	</option>
                                                <option>	Kec. Mokok	</option>
                                                <option>	Kec. Jangkang	</option>
                                                <option>	Kec. Bonti	</option>
                                                <option>	Kec. Parindu	</option>
                                                <option>	Kec. Tayan Hilir	</option>
                                                <option>	Kec. Balai	</option>
                                                <option>	Kec. Tayan Hulu	</option>
                                                <option>	Kec. Kembayan	</option>
                                                <option>	Kec. Beduwai	</option>
                                                <option>	Kec. Noyan	</option>
                                                <option>	Kec. Sekayan	</option>
                                                <option>	Kec. Entikong	</option>
                                                <option>	Meliau	</option>
                                                <option>	Kapuas	</option>
                                                <option>	Kec. Nanga Serawai	</option>
                                                <option>	Kec. Ambalau	</option>
                                                <option>	Kec. Kayan Hulu	</option>
                                                <option>	Kec. Sepauk	</option>
                                                <option>	Kec. Tempunak	</option>
                                                <option>	Kec. Sungai Tebelian	</option>
                                                <option>	Kec. Sintang	</option>
                                                <option>	Kec. Dedai	</option>
                                                <option>	Kec. Kayan Hilir	</option>
                                                <option>	Kec. Kelam Permai	</option>
                                                <option>	Kec. Binjai Hulu	</option>
                                                <option>	Kec. Ketungau Hilir	</option>
                                                <option>	Kec. Ketungau Tengah	</option>
                                                <option>	Kec. Ketungau Hulu	</option>
                                                <option>	Kec. Silat Hilir	</option>
                                                <option>	Kec. Silat Hulu	</option>
                                                <option>	Kec. Hulu Gurung	</option>
                                                <option>	Kec. Bunut Hulu	</option>
                                                <option>	Kec. Mentebah	</option>
                                                <option>	Kec. Kalis	</option>
                                                <option>	Kec. Embaloh Hilir	</option>
                                                <option>	Kec. Bunut Hilir	</option>
                                                <option>	Kec. Boyan Tanjung	</option>
                                                <option>	Kec. Selimbau	</option>
                                                <option>	Kec. Suhaid	</option>
                                                <option>	Kec. Seberuang	</option>
                                                <option>	Kec. Semitau	</option>
                                                <option>	Kec. Empanang	</option>
                                                <option>	Kec. Puring Kencana	</option>
                                                <option>	Kec. Badau	</option>
                                                <option>	Kec. Batang Lupar	</option>
                                                <option>	Kec. Embaloh Hulu	</option>
                                                <option>	Kec. Hulu Kapuas	</option>
                                                <option>	Kec. Putussibau Utara	</option>
                                                <option>	Kec. Bika	</option>
                                                <option>	Kec. Jongkong	</option>
                                                <option>	Kec. Putussibau Selatan	</option>
                                                <option>	Kec. Pengkadan	</option>
                                                <option>	Kec. Danau Setarum	</option>
                                                <option>	Kec. Kendawangan	</option>
                                                <option>	Kec. Manis Mata	</option>
                                                <option>	Kec. Marau	</option>
                                                <option>	Kec. Jelai Hulu	</option>
                                                <option>	Kec. Tumbang Titi	</option>
                                                <option>	Kec. Matan Hilir Selatan	</option>
                                                <option>	Kec. Matan Hilir Utara	</option>
                                                <option>	Kec. Nanga Tayap	</option>
                                                <option>	Kec. Sandai	</option>
                                                <option>	Kec. Sungai Laur	</option>
                                                <option>	Kec. Simpang Hulu	</option>
                                                <option>	Kec. Muara Pawan	</option>
                                                <option>	Kec. Delta Pawan	</option>
                                                <option>	Kec. Simpang Dua	</option>
                                                <option>	Kec. Benua Kayong	</option>
                                                <option>	Kec. Hulu Sungai	</option>
                                                <option>	Kec. Air Upas	</option>
                                                <option>	Kec. Singkup	</option>
                                                <option>	Kec. Pemahan	</option>
                                                <option>	Kec. Sungai Melayu Rayek	</option>
                                                <option>	Kec. Sungai Raya	</option>
                                                <option>	Kec. Capkala	</option>
                                                <option>	Kec. Samalantan	</option>
                                                <option>	Kec. Bengkayang	</option>
                                                <option>	Kec. Ledo	</option>
                                                <option>	Kec. Sanggau Ledo	</option>
                                                <option>	Kec. Seluas	</option>
                                                <option>	Kec. Jagoi Babang	</option>
                                                <option>	Kec. Teriak	</option>
                                                <option>	Kec. Monterado	</option>
                                                <option>	Kec. Suti Semarang	</option>
                                                <option>	Kec. Siding	</option>
                                                <option>	Kec. Lumar	</option>
                                                <option>	Kec. Sungai Betung	</option>
                                                <option>	Kec. Sungai Raya Kepulauan	</option>
                                                <option>	Kec. Lembah Bawang	</option>
                                                <option>	Kec. Tujuh Belas	</option>
                                                <option>	Kec. Sebangki	</option>
                                                <option>	Kec. Ngabang	</option>
                                                <option>	Kec. Sengah Temila	</option>
                                                <option>	Kec. Mandor	</option>
                                                <option>	Kec. Menjalin	</option>
                                                <option>	Kec. Mempawah Hulu	</option>
                                                <option>	Kec. Menyuke	</option>
                                                <option>	Kec. Meranti	</option>
                                                <option>	Kec. Kuala Behe	</option>
                                                <option>	Kec. Air Besar	</option>
                                                <option>	Kec. Jelimpo	</option>
                                                <option>	Kec. Sompak	</option>
                                                <option>	Kec. Banyuke Hulu	</option>
                                                <option>	Kec. Nanga Mahap	</option>
                                                <option>	Kec. Nanga Taman	</option>
                                                <option>	Kec. Sekadau Hulu	</option>
                                                <option>	Kec. Sekadau Hilir	</option>
                                                <option>	Kec. Belitang	</option>
                                                <option>	Kec. Belitang Hilir	</option>
                                                <option>	Kec. Belitang Hulu	</option>
                                                <option>	Kec. Sokan	</option>
                                                <option>	Kec. Tanah Pinoh	</option>
                                                <option>	Kec. Sayan	</option>
                                                <option>	Kec. Ella Hilir	</option>
                                                <option>	Kec. Menukung	</option>
                                                <option>	Kec. Nanga Pinoh	</option>
                                                <option>	Kec. Belimbing	</option>
                                                <option>	Kec. Tanah Pinoh Barat	</option>
                                                <option>	Kec. Belimbing Hulu	</option>
                                                <option>	Kec. Pinoh Selatan	</option>
                                                <option>	Kec. Pinoh Utara	</option>
                                                <option>	Kec. Pulau Maya Karimata	</option>
                                                <option>	Kec. Simpang Hilir	</option>
                                                <option>	Kec. Sukadana	</option>
                                                <option>	Kec. Teluk Batang	</option>
                                                <option>	Kec. Seponti	</option>
                                                <option>	Kec. Kuala Mandor B	</option>
                                                <option>	Kec. Sungai Ambawang	</option>
                                                <option>	Kec. Sungai Kakap	</option>
                                                <option>	Kec. Telok Pakedai	</option>
                                                <option>	Kec. Terentang	</option>
                                                <option>	Kec. Sungai Raya	</option>
                                                <option>	Kec. Batu Ampar	</option>
                                                <option>	Kec. Kubu	</option>
                                                <option>	Kec. Rasau Jaya	</option>
                                                <option>	Kec. Pontianak Selatan	</option>
                                                <option>	Kec. Pontianak Timur	</option>
                                                <option>	Kec. Pontianak Barat	</option>
                                                <option>	Kec. Pontianak Utara	</option>
                                                <option>	Kec. Pontianak Kota	</option>
                                                <option>	Kec. Pontianak Tenggara	</option>
                                                <option>	Kec. Singkawang Selatan	</option>
                                                <option>	Kec. Singkawang Timur	</option>
                                                <option>	Kec. Singkawang Utara	</option>
                                                <option>	Kec. Singkawang Barat	</option>
                                                <option>	Kec. Singkawang Tengah	</option>
                                                <option>	Kec. Kapuas Kuala	</option>
                                                <option>	Kec. Kapuas Timur	</option>
                                                <option>	Kec. Selat	</option>
                                                <option>	Kec. Basarang	</option>
                                                <option>	Kec. Kapuas Hilir	</option>
                                                <option>	Kec. Pulau Petak	</option>
                                                <option>	Kec. Kapuas Murung	</option>
                                                <option>	Kec. Kapuas Barat	</option>
                                                <option>	Kec. Mantangai	</option>
                                                <option>	Kec. Timpah	</option>
                                                <option>	Kec. Kapuas Tengah	</option>
                                                <option>	Kec. Kapuas Hulu	</option>
                                                <option>	Kec. Dusun Hilir	</option>
                                                <option>	Kec. Jenamas	</option>
                                                <option>	Kec. Karau Kuala	</option>
                                                <option>	Kec. Dusun Selatan	</option>
                                                <option>	Kec. Dusun Utara	</option>
                                                <option>	Kec. Gunung Bintang Awai	</option>
                                                <option>	Kec. Montallat	</option>
                                                <option>	Kec. Gunung Timang	</option>
                                                <option>	Kec. Gunung Purei	</option>
                                                <option>	Kec. Teweh Timur	</option>
                                                <option>	Kec. Teweh Tengah	</option>
                                                <option>	Kec. Lahei	</option>
                                                <option>	Teweh Baru	</option>
                                                <option>	Teweh Selatan	</option>
                                                <option>	Lahei Barat	</option>
                                                <option>	Kec. Mentaya Hilir Selatan	</option>
                                                <option>	Kec. Pulau Hanaut	</option>
                                                <option>	Kec. Mentawa Baru/Ketapang	</option>
                                                <option>	Kec. Mentaya Hilir Utara	</option>
                                                <option>	Kec. Kota Besi	</option>
                                                <option>	Kec. Baamang	</option>
                                                <option>	Kec. Cempaga	</option>
                                                <option>	Kec. Parenggean	</option>
                                                <option>	Kec. Mentaya Hulu	</option>
                                                <option>	Kec. Antang Kalang	</option>
                                                <option>	Kec. Teluk Sampit	</option>
                                                <option>	Kec. Seranau	</option>
                                                <option>	Kec. Cempaga Hulu	</option>
                                                <option>	Kec. Bukit Santuei	</option>
                                                <option>	Kec. Telawang	</option>
                                                <option>	Kec. Tualan Hulu	</option>
                                                <option>	Kec. Telaga Antang	</option>
                                                <option>	Tualan Hulu	</option>
                                                <option>	Telaga Antang	</option>
                                                <option>	Kec. Kotawaringin Lama	</option>
                                                <option>	Kec. Arut Selatan	</option>
                                                <option>	Kec. Kumai	</option>
                                                <option>	Kec. Arut Utara	</option>
                                                <option>	Kec. Pangkalan Banteng	</option>
                                                <option>	Kec. Pangkalan Lada	</option>
                                                <option>	Kec. Katingan Kuala	</option>
                                                <option>	Kec. Mendawai	</option>
                                                <option>	Kec. Kampiang	</option>
                                                <option>	Kec. Tasik Payawan	</option>
                                                <option>	Kec. Katingan Hilir	</option>
                                                <option>	Kec. Tewang Sangalang Garing	</option>
                                                <option>	Kec. Pulau Malan	</option>
                                                <option>	Kec. Katingan Tengah	</option>
                                                <option>	Kec. Katingan Hulu	</option>
                                                <option>	Kec. Marikit	</option>
                                                <option>	Kec. Sanaman Mantikei	</option>
                                                <option>	Kec. Petak Malai	</option>
                                                <option>	Kec. Bukit Raya	</option>
                                                <option>	Kec. Seruyan Hilir	</option>
                                                <option>	Kec. Danau Sembuluh	</option>
                                                <option>	Kec. Hanau	</option>
                                                <option>	Kec. Seruyan Tengah	</option>
                                                <option>	Kec. Seruyan Hulu	</option>
                                                <option>	Kec. Seruyan Hilir Timur	</option>
                                                <option>	Kec. Seruyan Raya	</option>
                                                <option>	Kec. Danau Seluluk	</option>
                                                <option>	Kec. Batu Ampar	</option>
                                                <option>	Kec. Suling Tambun	</option>
                                                <option>	Kec. Sembuluh Raya	</option>
                                                <option>	Kec. Natai Kelampai	</option>
                                                <option>	Kec. Sepan Biha	</option>
                                                <option>	Kec. Seruyan Hulu Utara	</option>
                                                <option>	Natai Kelampai	</option>
                                                <option>	Sepan Biha	</option>
                                                <option>	Seruyan Hulu Utara	</option>
                                                <option>	Sembuluh Raya	</option>
                                                <option>	Kec. Jelai	</option>
                                                <option>	Kec. Sukamara	</option>
                                                <option>	Kec. Balai Riam	</option>
                                                <option>	Kec. Pantai Lunci	</option>
                                                <option>	Kec. Permata Kecubung	</option>
                                                <option>	Kec. Bulik	</option>
                                                <option>	Kec. Lamandau	</option>
                                                <option>	Kec. Delang	</option>
                                                <option>	Kec. Bulik Timur	</option>
                                                <option>	Kec. Mentobi Raya	</option>
                                                <option>	Kec. Sematu Jaya	</option>
                                                <option>	Kec. Belantikan Raya	</option>
                                                <option>	Kec. Batang Kawa	</option>
                                                <option>	Kec. Tewah	</option>
                                                <option>	Kec. Kurun	</option>
                                                <option>	Kec. Sepang Simin	</option>
                                                <option>	Kec. Rungan	</option>
                                                <option>	Kec. Manuhing	</option>
                                                <option>	Kec. Kahayan Hulu Utara	</option>
                                                <option>	Kec. Mihing Raya	</option>
                                                <option>	Kec. Damang Batu	</option>
                                                <option>	Kec. Miri Manasa	</option>
                                                <option>	Kec. Rungan Hulu	</option>
                                                <option>	Kec. Manuhing Raya	</option>
                                                <option>	Kec. Kahayan Kuala	</option>
                                                <option>	Kec. Pandih Batu	</option>
                                                <option>	Kec. Maliku	</option>
                                                <option>	Kec. Kahayan Hilir	</option>
                                                <option>	Kec. Kahayan Tengah	</option>
                                                <option>	Kec. Banama Tingan	</option>
                                                <option>	Kec. Sebangau Kuala	</option>
                                                <option>	Kec. Jabiren Raya	</option>
                                                <option>	Kec. Laung Tuhup	</option>
                                                <option>	Kec. Murung	</option>
                                                <option>	Kec. Permata Intan	</option>
                                                <option>	Kec. Tanah Siang	</option>
                                                <option>	Kec. Sumber Barito	</option>
                                                <option>	Kec. Barito Tuhup Raya	</option>
                                                <option>	Kec. Tanah Siang Selatan	</option>
                                                <option>	Kec. Sungai Babuat	</option>
                                                <option>	Kec. Seribu Riam	</option>
                                                <option>	Kec. Uut Murung	</option>
                                                <option>	Kec. Dusun Timur	</option>
                                                <option>	Kec. Benua Lima	</option>
                                                <option>	Kec. Patangkep Tutui	</option>
                                                <option>	Kec. Awang	</option>
                                                <option>	Kec. Dusun Tengah	</option>
                                                <option>	Kec. Pematang Karau	</option>
                                                <option>	Kec. Paju Epat	</option>
                                                <option>	Kec. Reren Batuah	</option>
                                                <option>	Kec. Paku	</option>
                                                <option>	Kec. Karusen Janang	</option>
                                                <option>	Kec. Pahandut	</option>
                                                <option>	Kec. Bukit Batu	</option>
                                                <option>	Kec. Sabangau	</option>
                                                <option>	Kec. Jekan Raya	</option>
                                                <option>	Kec. Rakumpit	</option>
                                                <option>	Kec. Aluh-Aluh	</option>
                                                <option>	Kec. Gambut	</option>
                                                <option>	Kec. Kertak Hanyar	</option>
                                                <option>	Kec. Sungai Tabuk	</option>
                                                <option>	Kec. Martapura	</option>
                                                <option>	Kec. Astambul	</option>
                                                <option>	Kec. Karang Intan	</option>
                                                <option>	Kec. Aranio	</option>
                                                <option>	Kec. Sungai Pinang	</option>
                                                <option>	Kec. Pengaron	</option>
                                                <option>	Kec. Mataraman	</option>
                                                <option>	Kec. Simpang Empat	</option>
                                                <option>	Kec. Beruntung Baru	</option>
                                                <option>	Kec. Martapura Barat	</option>
                                                <option>	Kec. Martapura Timur	</option>
                                                <option>	Kec. Paramasan	</option>
                                                <option>	Kec. Tatah Makmur	</option>
                                                <option>	Kec. Sambung Makmur	</option>
                                                <option>	Kec. Telaga Bauntung	</option>
                                                <option>	Kec. Panyipatan	</option>
                                                <option>	Kec. Takisung	</option>
                                                <option>	Kec. Kurau	</option>
                                                <option>	Kec. Bati-Bati	</option>
                                                <option>	Kec. Tambang Ulang	</option>
                                                <option>	Kec. Pelaihari	</option>
                                                <option>	Kec. Batu Ampar	</option>
                                                <option>	Kec. Jorong	</option>
                                                <option>	Kec. Kintap	</option>
                                                <option>	Kec. Harapan Bumi Makmur	</option>
                                                <option>	Kec. Bajuin	</option>
                                                <option>	Kec. Tabunganen	</option>
                                                <option>	Kec. Tamban	</option>
                                                <option>	Kec. Mekarsari	</option>
                                                <option>	Kec. Anjir Pasar	</option>
                                                <option>	Kec. Anjir Muara	</option>
                                                <option>	Kec. Alalak	</option>
                                                <option>	Kec. Mandastana	</option>
                                                <option>	Kec. Belawang	</option>
                                                <option>	Kec. Wanaraya	</option>
                                                <option>	Kec. Barambai	</option>
                                                <option>	Kec. Rantau Badauh	</option>
                                                <option>	Kec. Cerbon	</option>
                                                <option>	Kec. Bakumpai	</option>
                                                <option>	Kec. Marabahan	</option>
                                                <option>	Kec. Tabukan	</option>
                                                <option>	Kec. Kuripan	</option>
                                                <option>	Kec. Jejangkit	</option>
                                                <option>	Kec. Binuang	</option>
                                                <option>	Kec. Tapin Selatan	</option>
                                                <option>	Kec. Tapin Tengah	</option>
                                                <option>	Kec. Bungur	</option>
                                                <option>	Kec. Piani	</option>
                                                <option>	Kec. Lokpaikat	</option>
                                                <option>	Kec. Tapin Utara	</option>
                                                <option>	Kec. Bakarangan	</option>
                                                <option>	Kec. Candi Laras Selatan	</option>
                                                <option>	Kec. Candi Laras Utara	</option>
                                                <option>	Kec. Hatungun	</option>
                                                <option>	Kec. Salam Babaris	</option>
                                                <option>	Kec. Padang Batung	</option>
                                                <option>	Kec. Loksado	</option>
                                                <option>	Kec. Telaga Langsat	</option>
                                                <option>	Kec. Angkinang	</option>
                                                <option>	Kec. Kandangan	</option>
                                                <option>	Kec. Sungai Raya	</option>
                                                <option>	Kec. Simpur	</option>
                                                <option>	Kec. Kalumpang	</option>
                                                <option>	Kec. Daha Selatan	</option>
                                                <option>	Kec. Daha Utara	</option>
                                                <option>	Kec. Daha Barat	</option>
                                                <option>	Kec. Haruyan	</option>
                                                <option>	Kec. Batu Benawa	</option>
                                                <option>	Kec. Hantakan	</option>
                                                <option>	Kec. Batang Alai Selatan	</option>
                                                <option>	Kec. Barabai	</option>
                                                <option>	Kec. Labuan Amas Selatan	</option>
                                                <option>	Kec. Labuan Amas Utara	</option>
                                                <option>	Kec. Pendawan	</option>
                                                <option>	Kec. Batang Alai Utara	</option>
                                                <option>	Kec. Batang Alai Timur	</option>
                                                <option>	Kec. Limpasu	</option>
                                                <option>	Kec. Danau Panggang	</option>
                                                <option>	Kec. Babirik	</option>
                                                <option>	Kec. Sungai Pandan	</option>
                                                <option>	Kec. Amuntai Selatan	</option>
                                                <option>	Kec. Amuntai Tengah	</option>
                                                <option>	Kec. Banjang	</option>
                                                <option>	Kec. Amuntai Utara	</option>
                                                <option>	Kec. Paminggir	</option>
                                                <option>	Kec. Sungai Tabukan	</option>
                                                <option>	Kec. Haur Gading	</option>
                                                <option>	Kec. Banua Lawas	</option>
                                                <option>	Kec. Pugaan	</option>
                                                <option>	Kec. Kelua	</option>
                                                <option>	Kec. Muara Harus	</option>
                                                <option>	Kec. Tanta	</option>
                                                <option>	Kec. Tanjung	</option>
                                                <option>	Kec. Murung Pudak	</option>
                                                <option>	Kec. Haruai	</option>
                                                <option>	Kec. Upau	</option>
                                                <option>	Kec. Muara Uya	</option>
                                                <option>	Kec. Jaro	</option>
                                                <option>	Kec. Bintang Ara	</option>
                                                <option>	Kec. Pulau Sembilan	</option>
                                                <option>	Kec. Pulau Laut Barat	</option>
                                                <option>	Kec. Pulau Laut Selatan	</option>
                                                <option>	Kec. Pulau Laut Timur	</option>
                                                <option>	Kec. Pulau Sebuku	</option>
                                                <option>	Kec. Pulau Laut Utara	</option>
                                                <option>	Kec. Pulau Laut Tengah	</option>
                                                <option>	Kec. Kelumpang Hilir	</option>
                                                <option>	Kec. Kelumpang Barat	</option>
                                                <option>	Kec. Kelumpang Selatan	</option>
                                                <option>	Kec. Kelumpang Hulu	</option>
                                                <option>	Kec. Hampang	</option>
                                                <option>	Kec. Sungai Durian	</option>
                                                <option>	Kec. Kelumpang Tengah	</option>
                                                <option>	Kec. Kelumpang Utara	</option>
                                                <option>	Kec. Pamukan Selatan	</option>
                                                <option>	Kec. Sampanahan	</option>
                                                <option>	Kec. Pamukan Utara	</option>
                                                <option>	Kec. Pulau Laut Kepulauan	</option>
                                                <option>	Kec. Pamukan Barat	</option>
                                                <option>	Kec. Lampihong	</option>
                                                <option>	Kec. Batu Mandi	</option>
                                                <option>	Kec. Awayan	</option>
                                                <option>	Kec. Paringin	</option>
                                                <option>	Kec. Juai	</option>
                                                <option>	Kec. Halong	</option>
                                                <option>	Kec. Tebing Tinggi	</option>
                                                <option>	Kec. Paringin Selatan	</option>
                                                <option>	Kec. Kusan Hilir	</option>
                                                <option>	Kec. Sungai Loban	</option>
                                                <option>	Kec. Satui	</option>
                                                <option>	Kec. Kusan Hulu	</option>
                                                <option>	Kec. Batu Licin	</option>
                                                <option>	Kec. Simpang Empat	</option>
                                                <option>	Kec. Karang Bintang	</option>
                                                <option>	Kec. Mantewe	</option>
                                                <option>	Kec. Angsana	</option>
                                                <option>	Kec. Kuranji	</option>
                                                <option>	Kec. Banjarmasin Selatan	</option>
                                                <option>	Kec. Banjarmasin Timur	</option>
                                                <option>	Kec. Banjarmasin Barat	</option>
                                                <option>	Kec. Banjarmasin Utara	</option>
                                                <option>	Kec. Banjarmasin Tengah	</option>
                                                <option>	Kec. Landasan Ulin	</option>
                                                <option>	Kec. Cempaka	</option>
                                                <option>	Kec. Banjarbaru Utara	</option>
                                                <option>	Kec. Banjarbaru Selatan	</option>
                                                <option>	Kec. Liang Anggang	</option>
                                                <option>	Kec. Batu Sopang	</option>
                                                <option>	Kec. Tanjung Harapan	</option>
                                                <option>	Kec. Pasir Balengkong	</option>
                                                <option>	Kec. Tanah Grogot	</option>
                                                <option>	Kec. Kuaro	</option>
                                                <option>	Kec. Long Ikis	</option>
                                                <option>	Kec. Muara Komam	</option>
                                                <option>	Kec. Long Kali	</option>
                                                <option>	Kec. Muara Samu	</option>
                                                <option>	Kec. Batu Engau	</option>
                                                <option>	Kec. Semboja	</option>
                                                <option>	Kec. Muara Jawa	</option>
                                                <option>	Kec. Sanga-Sanga	</option>
                                                <option>	Kec. Loa Janan	</option>
                                                <option>	Kec. Loa Kulu	</option>
                                                <option>	Kec. Muara Muntai	</option>
                                                <option>	Kec. Muara Wis	</option>
                                                <option>	Kec. Kota Bangun	</option>
                                                <option>	Kec. Tenggarong	</option>
                                                <option>	Kec. Sebulu	</option>
                                                <option>	Kec. Tenggarong Seberang	</option>
                                                <option>	Kec. Anggana	</option>
                                                <option>	Kec. Muara Badak	</option>
                                                <option>	Kec. Marang Kayu	</option>
                                                <option>	Kec. Muara Kaman	</option>
                                                <option>	Kec. Kenohan	</option>
                                                <option>	Kec. Kembang Janggut	</option>
                                                <option>	Kec. Tabang	</option>
                                                <option>	Kec. Kelay	</option>
                                                <option>	Kec. Talisayan	</option>
                                                <option>	Kec. Biduk Biduk	</option>
                                                <option>	Kec. Pulau Derawan	</option>
                                                <option>	Kec. Sambaliung	</option>
                                                <option>	Kec. Tanjung Redeb	</option>
                                                <option>	Kec. Gunung Tabur	</option>
                                                <option>	Kec. Segah	</option>
                                                <option>	Kec. Teluk Bayur	</option>
                                                <option>	Kec. Tubalar	</option>
                                                <option>	Kec. Pulau Maratua	</option>
                                                <option>	Kec. Batu Putih	</option>
                                                <option>	Kec. Biatan	</option>
                                                <option>	Kec. Tanjung Palas	</option>
                                                <option>	Kec. Sekatak	</option>
                                                <option>	Kec. Pulau Bunyu	</option>
                                                <option>	Kec. Tanjung Palas Barat	</option>
                                                <option>	Kec. Tanjung Palas Utara	</option>
                                                <option>	Kec. Tanjung Palas Timur	</option>
                                                <option>	Kec. Tanjung Selor	</option>
                                                <option>	Kec. Tanjung Palas Tengah	</option>
                                                <option>	Kec. Peso Hilir	</option>
                                                <option>	Kec. Peso	</option>
                                                <option>	Kec. Kayan Hulu	</option>
                                                <option>	Kec. Kayan Hilir	</option>
                                                <option>	Kec. Pujungan	</option>
                                                <option>	Kec. Malinau Kota	</option>
                                                <option>	Kec. Mentarang	</option>
                                                <option>	Kec. Sungai Boh	</option>
                                                <option>	Kec. Malinau Selatan	</option>
                                                <option>	Kec. Malinau Barat	</option>
                                                <option>	Kec. Malinau Utara	</option>
                                                <option>	Kec. Kayan Selatan	</option>
                                                <option>	Kec. Bahau Hulu	</option>
                                                <option>	Kec. Mentarang Hulu	</option>
                                                <option>	Kec. Kerayan	</option>
                                                <option>	Kec. Lumbis	</option>
                                                <option>	Kec. Sembakung	</option>
                                                <option>	Kec. Nunukan	</option>
                                                <option>	Kec. Sebatik	</option>
                                                <option>	Kec. Sebuku	</option>
                                                <option>	Kec. Krayan Selatan	</option>
                                                <option>	Kec. Sebatik Barat	</option>
                                                <option>	Kec. Nunukan Selatan	</option>
                                                <option>	Nunukan	</option>
                                                <option>	Krayan Selatan	</option>
                                                <option>	Krayan	</option>
                                                <option>	Nunukan Selatan	</option>
                                                <option>	Kec. Bongan	</option>
                                                <option>	Kec. Jempang	</option>
                                                <option>	Kec. Penyinggahan	</option>
                                                <option>	Kec. Muara Pahu	</option>
                                                <option>	Kec. Muara Lawa	</option>
                                                <option>	Kec. Damai	</option>
                                                <option>	Kec. Barong Tongkok	</option>
                                                <option>	Kec. Melak	</option>
                                                <option>	Kec. Long Iram	</option>
                                                <option>	Kec. Long Hubung	</option>
                                                <option>	Kec. Long Bagun	</option>
                                                <option>	Kec. Long Pahangai	</option>
                                                <option>	Kec. Long Apari	</option>
                                                <option>	Kec. Bentian Besar	</option>
                                                <option>	Kec. Linggang Bingung	</option>
                                                <option>	Kec. Manor Bulatn	</option>
                                                <option>	Kec. Laham	</option>
                                                <option>	Kec. Nyuatan	</option>
                                                <option>	Kec. Sekolaq Darat	</option>
                                                <option>	Kec. Tering	</option>
                                                <option>	Kec. Siluq Ngurai	</option>
                                                <option>	Long Bagun	</option>
                                                <option>	Long Apari	</option>
                                                <option>	Long Pahangai	</option>
                                                <option>	Long Hubung	</option>
                                                <option>	Laham	</option>
                                                <option>	Kec. Muara Ancalong	</option>
                                                <option>	Kec. Muara Wahau	</option>
                                                <option>	Kec. Muara Bengkal	</option>
                                                <option>	Kec. Sengata Utara	</option>
                                                <option>	Kec. Sangkulirang	</option>
                                                <option>	Kec. Kaliorang	</option>
                                                <option>	Kec. Kombeng	</option>
                                                <option>	Kec. Bengalon	</option>
                                                <option>	Kec. Busang	</option>
                                                <option>	Kec. Sandaran	</option>
                                                <option>	Kec. Telen	</option>
                                                <option>	Kec. Sengata Selatan	</option>
                                                <option>	Kec. Teluk Pandan	</option>
                                                <option>	Kec. Rantau Pulung	</option>
                                                <option>	Kec. Kaubun	</option>
                                                <option>	Kec. Karangan	</option>
                                                <option>	Kec. Batu Ampar	</option>
                                                <option>	Kec. Long Mesangat	</option>
                                                <option>	Kec. Babulu	</option>
                                                <option>	Kec. Waru	</option>
                                                <option>	Kec. Penajam	</option>
                                                <option>	Kec. Sepaku	</option>
                                                <option>	Kec. Palaran	</option>
                                                <option>	Kec. Samarinda Ilir	</option>
                                                <option>	Kec. Samarinda Seberang	</option>
                                                <option>	Kec. Sungai Kunjang	</option>
                                                <option>	Kec. Samarinda Ulu	</option>
                                                <option>	Kec. Samarinda Utara	</option>
                                                <option>	Kec. Sambutan	</option>
                                                <option>	Kec. Sungai Pinang	</option>
                                                <option>	Kec. Samarinda Kota	</option>
                                                <option>	Kec. Loa Janan Ilir	</option>
                                                <option>	Kec. Balikpapan Selatan	</option>
                                                <option>	Kec. Balikpapan Timur	</option>
                                                <option>	Kec. Balikpapan Utara	</option>
                                                <option>	Kec. Balikpapan Tengah	</option>
                                                <option>	Kec. Balikpapan Barat	</option>
                                                <option>	Kec. Tarakan Timur	</option>
                                                <option>	Kec. Tarakan Tengah	</option>
                                                <option>	Kec. Tarakan Barat	</option>
                                                <option>	Kec. Tarakan Utara	</option>
                                                <option>	Kec. Bontang Selatan	</option>
                                                <option>	Kec. Bontang Utara	</option>
                                                <option>	Kec. Bontang Barat	</option>
                                                <option>	Sesayap	</option>
                                                <option>	Sesayap Hilir	</option>
                                                <option>	Tana Lia	</option>
                                                <option>	Kec. Lolayan	</option>
                                                <option>	Kec. Poigar	</option>
                                                <option>	Kec. Bolaang	</option>
                                                <option>	Kec. Lolak	</option>
                                                <option>	Kec. Sangtombolang	</option>
                                                <option>	Kec. Dumoga Utara	</option>
                                                <option>	Kec. Dumoga Barat	</option>
                                                <option>	Kec. Dumoga Timur	</option>
                                                <option>	Kec. Passi Barat	</option>
                                                <option>	Kec. Passi Timur	</option>
                                                <option>	Kec. Bilalang	</option>
                                                <option>	Kec. Sangtombolang	</option>
                                                <option>	Bolaang Timur	</option>
                                                <option>	Kec. Langowan Timur	</option>
                                                <option>	Kec. Tompaso	</option>
                                                <option>	Kec. Kawangkoan	</option>
                                                <option>	Kec. Sonder	</option>
                                                <option>	Kec. Tombariri	</option>
                                                <option>	Kec. Pineleng	</option>
                                                <option>	Kec. Tondano Timur	</option>
                                                <option>	Kec. Remboken	</option>
                                                <option>	Kec. Kakas	</option>
                                                <option>	Kec. Lembean Timur	</option>
                                                <option>	Kec. Eris	</option>
                                                <option>	Kec. Kombi	</option>
                                                <option>	Kec. Langowan Barat	</option>
                                                <option>	Kec. Tombulu	</option>
                                                <option>	Kec. Tondano Barat	</option>
                                                <option>	Kec. Tondano Utara	</option>
                                                <option>	Kec. Langowan Selatan	</option>
                                                <option>	Kec. Tondano Selatan	</option>
                                                <option>	Kec. Langowan Utara	</option>
                                                <option>	Kec. Manganitu Selatan	</option>
                                                <option>	Kec. Tamako	</option>
                                                <option>	Kec. Tabukan Selatan	</option>
                                                <option>	Kec. Tabukan Tengah	</option>
                                                <option>	Kec. Manganitu	</option>
                                                <option>	Kec. Tahuna	</option>
                                                <option>	Kec. Tabukan Utara	</option>
                                                <option>	Kec. Kendahe	</option>
                                                <option>	Kec. Tatoareng	</option>
                                                <option>	Kec. Nusa Tabukan	</option>
                                                <option>	Kec. Tabukan Selatan Tengah	</option>
                                                <option>	Kec. Tabukan Selatan Tenggara	</option>
                                                <option>	Kec. Tahuna Timur	</option>
                                                <option>	Kec. Tahuna Barat	</option>
                                                <option>	Kepulauan Marore	</option>
                                                <option>	Kec. Kabaruan	</option>
                                                <option>	Kec. Lirung	</option>
                                                <option>	Kec. Melonguane	</option>
                                                <option>	Kec. Beo	</option>
                                                <option>	Kec. Rainis	</option>
                                                <option>	Kec. Essang	</option>
                                                <option>	Kec. Nanusa	</option>
                                                <option>	Kec. Gemeh	</option>
                                                <option>	Kec. Damau	</option>
                                                <option>	Kec. Tanpa Namma	</option>
                                                <option>	Kec. Lirung Selatan	</option>
                                                <option>	Kec. Kalongan	</option>
                                                <option>	Kec. Moronge	</option>
                                                <option>	Kec. Melonguane Timur	</option>
                                                <option>	Kec. Beo Utara	</option>
                                                <option>	Kec. Beo Selatan	</option>
                                                <option>	Kec. Pulutan	</option>
                                                <option>	Kec. Essang Selatan	</option>
                                                <option>	Kec. Miangas	</option>
                                                <option>	Salibabu	</option>
                                                <option>	Kec. Modoinding	</option>
                                                <option>	Kec. Tompaso Baru	</option>
                                                <option>	Kec. Ranoyapo	</option>
                                                <option>	Kec. Motoling	</option>
                                                <option>	Kec. Tenga	</option>
                                                <option>	Kec. Amurang	</option>
                                                <option>	Kec. Tareran	</option>
                                                <option>	Kec. Kumelembuai	</option>
                                                <option>	Kec. Maesaan	</option>
                                                <option>	Kec. Amurang Barat	</option>
                                                <option>	Kec. Amurang Timur	</option>
                                                <option>	Kec. Tatapan	</option>
                                                <option>	Kec. Motoling Barat	</option>
                                                <option>	Kec. Motoling Timur	</option>
                                                <option>	Kec. Sulta	</option>
                                                <option>	Kec. Tumpaan	</option>
                                                <option>	Sinonsayang	</option>
                                                <option>	Kec. Ratatotok	</option>
                                                <option>	Kec. Pusomaen	</option>
                                                <option>	Kec. Kauditan	</option>
                                                <option>	Kec. Airmadidi	</option>
                                                <option>	Kec. Dimembe	</option>
                                                <option>	Kec. Wori	</option>
                                                <option>	Kec. Likupang Timur	</option>
                                                <option>	Kec. Kema	</option>
                                                <option>	Kec. Likupang Barat	</option>
                                                <option>	Kec. Kalawat	</option>
                                                <option>	Kec. Talawaan	</option>
                                                <option>	Kec. Likupang Selatan	</option>
                                                <option>	Kec. Bintauna	</option>
                                                <option>	Kec. Bolaang Itang Timur	</option>
                                                <option>	Kec. Bolaang Itang Barat	</option>
                                                <option>	Kec. Kaidipang	</option>
                                                <option>	Kec. Pinogaluman	</option>
                                                <option>	Kec. Sangkub	</option>
                                                <option>	Kec. Biaro	</option>
                                                <option>	Kec. Siau Barat	</option>
                                                <option>	Kec. Siau Barat Selatan	</option>
                                                <option>	Kec. Siau Barat Utara	</option>
                                                <option>	Kec. Siau Tengah	</option>
                                                <option>	Kec. Siau Timur	</option>
                                                <option>	Kec. Siau Timur Selatan	</option>
                                                <option>	Kec. Tagulandang	</option>
                                                <option>	Kec. Tagulandang Selatan	</option>
                                                <option>	Kec. Tagulandang Utara	</option>
                                                <option>	Kec. Ratatotok	</option>
                                                <option>	Kec. Pusomaen	</option>
                                                <option>	Kec. Belang	</option>
                                                <option>	Kec. Ratahan	</option>
                                                <option>	Kec. Tombatu	</option>
                                                <option>	Kec. Touluaan	</option>
                                                <option>	Kec. Touluaan Selatan	</option>
                                                <option>	Kec. Silian Raya	</option>
                                                <option>	Kec. Tombatu Timur	</option>
                                                <option>	Kec. Tombatu Utara	</option>
                                                <option>	Kec. Pasan	</option>
                                                <option>	Kec. Ratahan Timur	</option>
                                                <option>	Kec. Kotabunan	</option>
                                                <option>	Kec. Nuangan	</option>
                                                <option>	Kec.Tutuyan	</option>
                                                <option>	Kec. Modayag	</option>
                                                <option>	Kec. Modayag Barat	</option>
                                                <option>	Kec. Bolaang Uki	</option>
                                                <option>	Kec. Posigadan	</option>
                                                <option>	Kec. Pinolosian	</option>
                                                <option>	Kec. Pinolosian Timur	</option>
                                                <option>	Kec. Pinolosian Tengah	</option>
                                                <option>	Kec. Malalayang	</option>
                                                <option>	Kec. Sario	</option>
                                                <option>	Kec. Wenang	</option>
                                                <option>	Kec. Mapanget	</option>
                                                <option>	Kec. Bunaken	</option>
                                                <option>	Kec. Wanea	</option>
                                                <option>	Kec. Tikala	</option>
                                                <option>	Kec. Tuminting	</option>
                                                <option>	Kec. Singkil	</option>
                                                <option>	Kec. Ranowulu	</option>
                                                <option>	Kec. Matuari	</option>
                                                <option>	Kec. Girian	</option>
                                                <option>	Kec. Madidir	</option>
                                                <option>	Kec. Maesa	</option>
                                                <option>	Kec. Aertembaga	</option>
                                                <option>	Kec. Lembeh Utara	</option>
                                                <option>	Kec. Lembeh Selatan	</option>
                                                <option>	Kec. Tomohon Utara	</option>
                                                <option>	Kec. Tomohon Tengah	</option>
                                                <option>	Kec. Tomohon Selatan	</option>
                                                <option>	Kec. Tomohon Timur	</option>
                                                <option>	Kec. Tomohon Barat	</option>
                                                <option>	Kec. Kotamobagu Barat	</option>
                                                <option>	Kec. Kotamobagu Timur	</option>
                                                <option>	Kec. Kotamobagu Utara	</option>
                                                <option>	Kec. Kotamobagu Selatan	</option>
                                                <option>	Kec. Labobo	</option>
                                                <option>	Kec. Banggai	</option>
                                                <option>	Kec. Totikum	</option>
                                                <option>	Kec. Tinangkung	</option>
                                                <option>	Kec. Liang	</option>
                                                <option>	Kec. Bulagi	</option>
                                                <option>	Kec. Buko	</option>
                                                <option>	Kec. Bokan Kepulauan	</option>
                                                <option>	Kec. Bulagi Selatan	</option>
                                                <option>	Kec. Bangkurung	</option>
                                                <option>	Kec. Banggai Utara	</option>
                                                <option>	Kec. Banggai Tengah	</option>
                                                <option>	Kec. Banggai Selatan	</option>
                                                <option>	Kec. Selatan	</option>
                                                <option>	Kec. Tinangkung Selatan	</option>
                                                <option>	Kec. Tinangkung Utara	</option>
                                                <option>	Kec. Peling Tengah	</option>
                                                <option>	Kec. Bulagi Utara	</option>
                                                <option>	Kec. Buko Selatan	</option>
                                                <option>	Banggai Utara	</option>
                                                <option>	Banggai	</option>
                                                <option>	Labobo	</option>
                                                <option>	Bokan Kepulauan	</option>
                                                <option>	Bangkurung	</option>
                                                <option>	Banggai Tengah	</option>
                                                <option>	Banggai Selatan	</option>
                                                <option>	Totikum Selatan	</option>
                                                <option>	Kec. Banawa	</option>
                                                <option>	Kec. Labuan	</option>
                                                <option>	Kec. Sindue	</option>
                                                <option>	Kec. Sirenja	</option>
                                                <option>	Kec. Balaesang	</option>
                                                <option>	Kec. Damsol	</option>
                                                <option>	Kec. Sojol	</option>
                                                <option>	Kec. Rio Pakava	</option>
                                                <option>	Kec. Banawa Selatan	</option>
                                                <option>	Kec. Tanantovea	</option>
                                                <option>	Kec. Pinembani	</option>
                                                <option>	Kec. Banawa Tengah	</option>
                                                <option>	Kec. Sindue Tombusabora	</option>
                                                <option>	Kec. Sindue Tobata	</option>
                                                <option>	Kec. Sojol Utara	</option>
                                                <option>	Kec. Balaesang Tanjung	</option>
                                                <option>	Dompelas Sojol	</option>
                                                <option>	Kec. Pamona Selatan	</option>
                                                <option>	Kec. Lore Selatan	</option>
                                                <option>	Kec. Pamona Utara	</option>
                                                <option>	Kec. Lore Utara	</option>
                                                <option>	Kec. Poso Pesisir	</option>
                                                <option>	Kec. Lage	</option>
                                                <option>	Kec. Poso Kota	</option>
                                                <option>	Kec. Pamona Timur	</option>
                                                <option>	Kec. Lore Tengah	</option>
                                                <option>	Kec. Pamona Barat	</option>
                                                <option>	Kec. Poso Pesisir Selatan	</option>
                                                <option>	Kec. Poso Pesisir Utara	</option>
                                                <option>	Kec. Poso Kota Utara	</option>
                                                <option>	Kec. Lore Barat	</option>
                                                <option>	Kec. Poso Kota Selatan	</option>
                                                <option>	Pamona Puselemba	</option>
                                                <option>	Kec. Lore Timur	</option>
                                                <option>	Kec. Lore Peore	</option>
                                                <option>	Pamona Tenggara	</option>
                                                <option>	Kec. Toili	</option>
                                                <option>	Kec. Batui	</option>
                                                <option>	Kec. Bunta	</option>
                                                <option>	Kec. Kintom	</option>
                                                <option>	Kec. Luwuk	</option>
                                                <option>	Kec. Pagimana	</option>
                                                <option>	Kec. Lamala	</option>
                                                <option>	Kec. Balantak	</option>
                                                <option>	Kec. Bualemo	</option>
                                                <option>	Kec. Toili Barat	</option>
                                                <option>	Kec. Nuhon	</option>
                                                <option>	Kec. Luwuk Timur	</option>
                                                <option>	Kec. Masama	</option>
                                                <option>	Kec. Biau	</option>
                                                <option>	Kec. Momunu	</option>
                                                <option>	Kec. Bokat	</option>
                                                <option>	Kec. Bunobogu	</option>
                                                <option>	Kec. Paleleh	</option>
                                                <option>	Kec. Tiloan	</option>
                                                <option>	Kec. Bukal	</option>
                                                <option>	Kec. Gadung	</option>
                                                <option>	Kec. Lipunoto	</option>
                                                <option>	Kec. Karamat	</option>
                                                <option>	Kec. Paleleh Barat	</option>
                                                <option>	Lakea	</option>
                                                <option>	Kec. Dampal Selatan	</option>
                                                <option>	Kec. Dampal Utara	</option>
                                                <option>	Kec. Dondo	</option>
                                                <option>	Kec. Baolan	</option>
                                                <option>	Kec. Galang	</option>
                                                <option>	Kec. Utara Toli-Toli	</option>
                                                <option>	Kec. Ogo Deide	</option>
                                                <option>	Kec. Basidondo	</option>
                                                <option>	Kec. Lampasio	</option>
                                                <option>	Kec. Dako Pemean	</option>
                                                <option>	Kec. Menui Kepulauan	</option>
                                                <option>	Kec. Bungku Selatan	</option>
                                                <option>	Kec. Bungku Tengah	</option>
                                                <option>	Kec. Bungku Barat	</option>
                                                <option>	Kec. Lembo	</option>
                                                <option>	Kec. Mori Atas	</option>
                                                <option>	Kec. Petasia	</option>
                                                <option>	Kec. Bungku Utara	</option>
                                                <option>	Kec. Bahodopi	</option>
                                                <option>	Kec. Soyo Jaya	</option>
                                                <option>	Kec. Witaponda	</option>
                                                <option>	Kec. Mamosalato	</option>
                                                <option>	Kec. Bumi Raya	</option>
                                                <option>	Kec. Sausu	</option>
                                                <option>	Kec. Parigi	</option>
                                                <option>	Kec. Ampibabo	</option>
                                                <option>	Kec. Tinombo	</option>
                                                <option>	Kec. Tomini	</option>
                                                <option>	Kec. Moutong	</option>
                                                <option>	Kec. Bolano Lambunu	</option>
                                                <option>	Kec. Kasimbar	</option>
                                                <option>	Kec. Torue	</option>
                                                <option>	Kec. Tinombo Selatan	</option>
                                                <option>	Kec. Parigi Selatan	</option>
                                                <option>	Kec. Mepanga	</option>
                                                <option>	Kec. Malakosa	</option>
                                                <option>	Kec. Parigi Barat	</option>
                                                <option>	Kec. Parigi Utara	</option>
                                                <option>	Kec. Ribulu	</option>
                                                <option>	Kec. Siniu	</option>
                                                <option>	Kec. Palasa	</option>
                                                <option>	Kec. Taopa	</option>
                                                <option>	Kec. Parigi Tengah	</option>
                                                <option>	Balinggi	</option>
                                                <option>	Ongka Malino	</option>
                                                <option>	Bolano	</option>
                                                <option>	Lambunu	</option>
                                                <option>	Kec. Tojo	</option>
                                                <option>	Kec. Ulubongka	</option>
                                                <option>	Kec. Ampana Tete	</option>
                                                <option>	Kec. Ampana Kota	</option>
                                                <option>	Kec. Una - Una	</option>
                                                <option>	Kec. Walea Kepulauan	</option>
                                                <option>	Kec. Togean	</option>
                                                <option>	Kec. Tojo Barat	</option>
                                                <option>	Kec. Walea Besar	</option>
                                                <option>	Kec. Kulawi	</option>
                                                <option>	Kec. Pipikoro	</option>
                                                <option>	Kec. Kulawi Selatan	</option>
                                                <option>	Kec. Lindu	</option>
                                                <option>	Kec. Palolo	</option>
                                                <option>	Kec. Nokilalaki	</option>
                                                <option>	Kec. Dolo	</option>
                                                <option>	Kec. Dolo Selatan	</option>
                                                <option>	Kec. Dolo Barat	</option>
                                                <option>	Kec. Marawola	</option>
                                                <option>	Kec. Kinovaru	</option>
                                                <option>	Kec. Marawola Barat	</option>
                                                <option>	Kec. Sigibiromaru	</option>
                                                <option>	Kec. Gumbasa	</option>
                                                <option>	Kec. Tanambulava	</option>
                                                <option>	Kec. Palu Barat	</option>
                                                <option>	Kec. Palu Selatan	</option>
                                                <option>	Kec. Palu Timur	</option>
                                                <option>	Kec. Palu Utara	</option>
                                                <option>	Kec. Mandai	</option>
                                                <option>	Kec. Maros Baru	</option>
                                                <option>	Kec. Maros Utara	</option>
                                                <option>	Kec. Bantimurung	</option>
                                                <option>	Kec. Tanralili	</option>
                                                <option>	Kec. Camba	</option>
                                                <option>	Kec. Mallawa	</option>
                                                <option>	Kec. Moncongloe	</option>
                                                <option>	Kec. Turikale	</option>
                                                <option>	Kec. Marusu	</option>
                                                <option>	Kec. Lau	</option>
                                                <option>	Kec. Simbang	</option>
                                                <option>	Kec. Tompobulu	</option>
                                                <option>	Kec. Cenrana	</option>
                                                <option>	Bontoa	</option>
                                                <option>	Kec. Liukang Tangaya	</option>
                                                <option>	Kec. Liukang Kalukuang Masalim	</option>
                                                <option>	Kec. Liukang Tapabiring	</option>
                                                <option>	Kec. Pangkajene	</option>
                                                <option>	Kec. Balocci	</option>
                                                <option>	Kec. Bungoro	</option>
                                                <option>	Kec. Labakkang	</option>
                                                <option>	Kec. Ma`rang	</option>
                                                <option>	Kec. Sigeri	</option>
                                                <option>	Kec. Minasatene	</option>
                                                <option>	Kec. Tondong Tallasa	</option>
                                                <option>	Kec. Mandalle	</option>
                                                <option>	Liukang Tupabbiring Utara	</option>
                                                <option>	Kec. Bontonompo	</option>
                                                <option>	Kec. Bajeng	</option>
                                                <option>	Kec. Pallangga	</option>
                                                <option>	Kec. Somba Opu	</option>
                                                <option>	Kec. Bontomarannu	</option>
                                                <option>	Kec. Parang Loe	</option>
                                                <option>	Kec. Tinggi Moncong	</option>
                                                <option>	Kec. Bungaya	</option>
                                                <option>	Kec. Tompobulu	</option>
                                                <option>	Kec. Barombong	</option>
                                                <option>	Kec. Biring Bulu	</option>
                                                <option>	Kec. Tombolo Pao	</option>
                                                <option>	Kec. Manuju	</option>
                                                <option>	Kec. Bontolempangan	</option>
                                                <option>	Kec. Pattallassang	</option>
                                                <option>	Kec. Bontonompo Selatan	</option>
                                                <option>	Kec. Parigi	</option>
                                                <option>	Kec. Bajeng Barat	</option>
                                                <option>	Pattalasang	</option>
                                                <option>	Kec. Mangarabombang	</option>
                                                <option>	Kec. Mappakasunggu	</option>
                                                <option>	Kec. Polombangkeng Selatan	</option>
                                                <option>	Kec. Polombangkeng Utara	</option>
                                                <option>	Kec. Galesong Selatan	</option>
                                                <option>	Kec. Galesong Utara	</option>
                                                <option>	Kec. Pattallassang	</option>
                                                <option>	Kec. Sanrobone	</option>
                                                <option>	Kec. Galesong	</option>
                                                <option>	Kec. Bangkala	</option>
                                                <option>	Kec. Tamalatea	</option>
                                                <option>	Kec. Binamu	</option>
                                                <option>	Kec. Batang	</option>
                                                <option>	Kec. Kelara	</option>
                                                <option>	Kec. Bangkala Barat	</option>
                                                <option>	Kec. Bontoramba	</option>
                                                <option>	Kec. Turatea	</option>
                                                <option>	Kec. Arungkeke	</option>
                                                <option>	Kec. Rumbia	</option>
                                                <option>	Kec. Tarowang	</option>
                                                <option>	Kec. Tanete Riaja	</option>
                                                <option>	Kec. Tanete Rilau	</option>
                                                <option>	Kec. Barru	</option>
                                                <option>	Kec. Soppeng Riaja	</option>
                                                <option>	Kec. Mallusetasi	</option>
                                                <option>	Kec. Balusu	</option>
                                                <option>	Kec. Pujananting	</option>
                                                <option>	Kec. Bontocani	</option>
                                                <option>	Kec. Kahu	</option>
                                                <option>	Kec. Kajuara	</option>
                                                <option>	Kec. Selomekko	</option>
                                                <option>	Kec. Tonra	</option>
                                                <option>	Kec. Patimpeng	</option>
                                                <option>	Kec. Libureng	</option>
                                                <option>	Kec. Mare	</option>
                                                <option>	Kec. Sibulue	</option>
                                                <option>	Kec. Cina	</option>
                                                <option>	Kec. Barebbo	</option>
                                                <option>	Kec. Ponre	</option>
                                                <option>	Kec. Lappariaja	</option>
                                                <option>	Kec. Lamuru	</option>
                                                <option>	Kec. Bengo	</option>
                                                <option>	Kec. Ulaweng	</option>
                                                <option>	Kec. Palakka	</option>
                                                <option>	Kec. Awangpone	</option>
                                                <option>	Kec. Tellu Siattinge	</option>
                                                <option>	Kec. Amali	</option>
                                                <option>	Kec. Ajangale	</option>
                                                <option>	Kec. Dua Boccoe	</option>
                                                <option>	Kec. Cenrana	</option>
                                                <option>	Kec. Tanete Riattang Barat	</option>
                                                <option>	Kec. Tanete Riattang	</option>
                                                <option>	Kec. Tanete Riattang Timur	</option>
                                                <option>	Kec. Tellu Limpoe	</option>
                                                <option>	Kec. Sabbangparu	</option>
                                                <option>	Kec. Tempe	</option>
                                                <option>	Kec. Pammana	</option>
                                                <option>	Kec. Bola	</option>
                                                <option>	Kec. Takkalala	</option>
                                                <option>	Kec. Sajo Anging	</option>
                                                <option>	Kec. Majauleng	</option>
                                                <option>	Kec. Tanasitolo	</option>
                                                <option>	Kec. Belawa	</option>
                                                <option>	Kec. Maniang Pajo	</option>
                                                <option>	Kec. Keera	</option>
                                                <option>	Kec. Pitumpanua	</option>
                                                <option>	Kec. Gilireng	</option>
                                                <option>	Kec. Penrang	</option>
                                                <option>	Kec. Mariowiwawo	</option>
                                                <option>	Kec. Lalabata	</option>
                                                <option>	Kec. Liliriaja	</option>
                                                <option>	Kec. Lili Rilau	</option>
                                                <option>	Kec. Donri-Donri	</option>
                                                <option>	Kec. Mario Riawa	</option>
                                                <option>	Kec. Ganra	</option>
                                                <option>	Kec. Citta	</option>
                                                <option>	Kec. Bissappu	</option>
                                                <option>	Kec. Bantaeng	</option>
                                                <option>	Kec. Tompobulu	</option>
                                                <option>	Kec. Ulu Ere	</option>
                                                <option>	Kec. Ere Merasa	</option>
                                                <option>	Kec. Pa`jukukang	</option>
                                                <option>	Kec. Gantarang Keke	</option>
                                                <option>	Kec. Sinoa	</option>
                                                <option>	Kec. Gantarang	</option>
                                                <option>	Kec. Ujung Bulu	</option>
                                                <option>	Kec. Bontobahari	</option>
                                                <option>	Kec. Bontotiro	</option>
                                                <option>	Kec. Hero Lange-Lange	</option>
                                                <option>	Kec. Kajang	</option>
                                                <option>	Kec. Bulukumba	</option>
                                                <option>	Kec. Rilau Ale	</option>
                                                <option>	Kec. Kindang	</option>
                                                <option>	Kec. Ujung Loe	</option>
                                                <option>	Kec. Sinjai Barat	</option>
                                                <option>	Kec. Sinjai Borong	</option>
                                                <option>	Kec. Sinjai Selatan	</option>
                                                <option>	Kec. Tellu Limpoe	</option>
                                                <option>	Kec. Sinjai Timur	</option>
                                                <option>	Kec. Sinjai Tengah	</option>
                                                <option>	Kec. Sinjai Utara	</option>
                                                <option>	Kec. Bulupoddo	</option>
                                                <option>	Kec. Pulau Sembilan	</option>
                                                <option>	Kec. Pasimarannu	</option>
                                                <option>	Kec. Pasimassunggu	</option>
                                                <option>	Kec. Bontosikuyu	</option>
                                                <option>	Kec. Bontoharu	</option>
                                                <option>	Kec. Bontomatene	</option>
                                                <option>	Kec. Bontomanai	</option>
                                                <option>	Kec. Benteng	</option>
                                                <option>	Kec. Taka Bonerate	</option>
                                                <option>	Kec. Pasilambena	</option>
                                                <option>	Kec. Pasimasunggu Timur	</option>
                                                <option>	Kec. Buki	</option>
                                                <option>	Pasimasunggu	</option>
                                                <option>	Kec. Suppa	</option>
                                                <option>	Kec. Mattiro Sompe	</option>
                                                <option>	Kec. Mattiro Bulu	</option>
                                                <option>	Kec. Watang Sawitto	</option>
                                                <option>	Kec. Patampanua	</option>
                                                <option>	Kec. Cempa	</option>
                                                <option>	Kec. Duampanua	</option>
                                                <option>	Kec. Lembang	</option>
                                                <option>	Kec. Lanrisang	</option>
                                                <option>	Kec. Tiroang	</option>
                                                <option>	Kec. Paleteang	</option>
                                                <option>	Kec. Batulappa	</option>
                                                <option>	Kec. Panca Lautang	</option>
                                                <option>	Kec. Tellulimpoe	</option>
                                                <option>	Kec. Watangpulu	</option>
                                                <option>	Kec. Baranti	</option>
                                                <option>	Kec. Panca Rijang	</option>
                                                <option>	Kec. Maritengae	</option>
                                                <option>	Kec. Pitu Riawa	</option>
                                                <option>	Kec. Duapitue	</option>
                                                <option>	Kec. Kulo	</option>
                                                <option>	Kec. Pitu Riase	</option>
                                                <option>	Kec. Watang Sidenreng	</option>
                                                <option>	Kec. Maiwa	</option>
                                                <option>	Kec. Enrekang	</option>
                                                <option>	Kec. Barakka	</option>
                                                <option>	Kec. Anggeraja	</option>
                                                <option>	Kec. Alla	</option>
                                                <option>	Kec. Bungin	</option>
                                                <option>	Kec. Cendana	</option>
                                                <option>	Kec. Curio	</option>
                                                <option>	Kec. Malua	</option>
                                                <option>	Kec. Buntu Batu	</option>
                                                <option>	Kec. Masale	</option>
                                                <option>	Kec. Baroko	</option>
                                                <option>	Kec. Larompong	</option>
                                                <option>	Kec. Suli	</option>
                                                <option>	Kec. Belopa	</option>
                                                <option>	Kec. Bajo	</option>
                                                <option>	Kec. Bassesangtempe	</option>
                                                <option>	Kec. Buaponrang	</option>
                                                <option>	Kec. Bua	</option>
                                                <option>	Kec. Walenrang	</option>
                                                <option>	Kec. Lamasi	</option>
                                                <option>	Kec. Latimojong	</option>
                                                <option>	Kec. Larompong Selatan	</option>
                                                <option>	Kec. Kamanre	</option>
                                                <option>	Kec. Walenrang Barat	</option>
                                                <option>	Kec. Walenrang Utara	</option>
                                                <option>	Kec. Walenrang Timur	</option>
                                                <option>	Kec. Lamasi Timur	</option>
                                                <option>	Kec. Suli Barat	</option>
                                                <option>	Kec. Bajo Barat	</option>
                                                <option>	Kec. Ponrang Selatan	</option>
                                                <option>	Kec. Ponrang	</option>
                                                <option>	Kec. Bolopa Utara	</option>
                                                <option>	Kec. Bonggakaradeng	</option>
                                                <option>	Kec. Mengkendek	</option>
                                                <option>	Kec. Sangalla	</option>
                                                <option>	Kec. Makale	</option>
                                                <option>	Kec. Saluputti	</option>
                                                <option>	Kec. Simbuang	</option>
                                                <option>	Kec. Rantetayo	</option>
                                                <option>	Kec. Bittuang	</option>
                                                <option>	Kec. Ranomeeto	</option>
                                                <option>	Kec. Mappak	</option>
                                                <option>	Kec. Gadang Batu Silanan	</option>
                                                <option>	Kec. Sangala Selatan	</option>
                                                <option>	Kec. Sangala Utara	</option>
                                                <option>	Kec. Makale Selatan	</option>
                                                <option>	Kec. Makale Utara	</option>
                                                <option>	Kec. Rembon	</option>
                                                <option>	Kec. Masanda	</option>
                                                <option>	Kec. Malimbong Balepe	</option>
                                                <option>	Kec. Kurra	</option>
                                                <option>	Kec. Sabbang	</option>
                                                <option>	Kec. Baebunta	</option>
                                                <option>	Kec. Malangke	</option>
                                                <option>	Kec. Sukamaju	</option>
                                                <option>	Kec. Bone-Bone	</option>
                                                <option>	Kec. Massamba	</option>
                                                <option>	Kec. Limbong	</option>
                                                <option>	Kec. Mappedeceng	</option>
                                                <option>	Kec. Sekko	</option>
                                                <option>	Kec. Rampi	</option>
                                                <option>	Kec. Malangke Barat	</option>
                                                <option>	Tana Lili	</option>
                                                <option>	Kec. Burau	</option>
                                                <option>	Kec. Tomoni	</option>
                                                <option>	Kec. Wotu	</option>
                                                <option>	Kec. Malili	</option>
                                                <option>	Kec. Nuha	</option>
                                                <option>	Kec. Mangkutana	</option>
                                                <option>	Kec. Towuti	</option>
                                                <option>	Kec. Angkona	</option>
                                                <option>	Kec. Tomoni Timur	</option>
                                                <option>	Kec. Kalaena	</option>
                                                <option>	Kec. Wasuponda	</option>
                                                <option>	Kec. Awan Rante karua	</option>
                                                <option>	Kec. Balusu	</option>
                                                <option>	Kec. Bengkelekila	</option>
                                                <option>	Kec. Baruppu	</option>
                                                <option>	Kec. Buntao	</option>
                                                <option>	Kec. Kapala Pitu	</option>
                                                <option>	Kec. Kesu	</option>
                                                <option>	Kec. Nanggala	</option>
                                                <option>	Kec. Rantebua	</option>
                                                <option>	Kec. Rantepao	</option>
                                                <option>	Kec. Rindingallo	</option>
                                                <option>	Kec. Sa`dan	</option>
                                                <option>	Kec. Sanggalangi	</option>
                                                <option>	Kec. Sesean	</option>
                                                <option>	Kec. Sesean Suloara	</option>
                                                <option>	Kec. Sopai	</option>
                                                <option>	Kec. Tallunglipu	</option>
                                                <option>	Kec. Tikala	</option>
                                                <option>	Kec. Tondon	</option>
                                                <option>	Kec. Dende` Piongan Napo	</option>
                                                <option>	Kec. Buntu Pepasan	</option>
                                                <option>	Kec. Mariso	</option>
                                                <option>	Kec. Mamajang	</option>
                                                <option>	Kec. Tamalate	</option>
                                                <option>	Kec. Makasar	</option>
                                                <option>	Kec. Ujung Pandang	</option>
                                                <option>	Kec. Wajo	</option>
                                                <option>	Kec. Bontoala	</option>
                                                <option>	Kec. Ujung Tanah	</option>
                                                <option>	Kec. Tallo	</option>
                                                <option>	Kec. Panakukkang	</option>
                                                <option>	Kec. Biringkanaya	</option>
                                                <option>	Kec. Tamalanrea	</option>
                                                <option>	Kec. Rappocini	</option>
                                                <option>	Kec. Manggala	</option>
                                                <option>	Kec. Bacukiki	</option>
                                                <option>	Kec. Ujung	</option>
                                                <option>	Kec. Soreang	</option>
                                                <option>	Kec. Bacukiki Barat	</option>
                                                <option>	Kec. Wara	</option>
                                                <option>	Kec. Wara Utara	</option>
                                                <option>	Kec. Wara Selatan	</option>
                                                <option>	Kec. Telluwanua	</option>
                                                <option>	Kec. Wara Timur	</option>
                                                <option>	Kec. Wara Barat	</option>
                                                <option>	Kec. Sendana	</option>
                                                <option>	Kec. Mungkajang	</option>
                                                <option>	Kec. Bara	</option>
                                                <option>	Kec. Soropia	</option>
                                                <option>	Kec. Sampara	</option>
                                                <option>	Kec. Lambuya	</option>
                                                <option>	Kec. Pondidaha	</option>
                                                <option>	Kec. Wawotobi	</option>
                                                <option>	Kec. Unaaha	</option>
                                                <option>	Kec. Abuki	</option>
                                                <option>	Kec. BondoAla	</option>
                                                <option>	Kec. Uepai	</option>
                                                <option>	Kec. Wonggeduku	</option>
                                                <option>	Kec. Tongauna	</option>
                                                <option>	Kec. Latoma	</option>
                                                <option>	Kec. Besulutu	</option>
                                                <option>	Kec. Puriala	</option>
                                                <option>	Kec. Meluhu	</option>
                                                <option>	Kec. Amonggedo	</option>
                                                <option>	Kec. Wawonii Barat	</option>
                                                <option>	Kec. Wawonii Timur	</option>
                                                <option>	Kec. Wawonii Selatan	</option>
                                                <option>	Kec. Wawonii Utara	</option>
                                                <option>	Kec. Routa	</option>
                                                <option>	Kec. Anggaberi	</option>
                                                <option>	Kec. Wawoni Tengah	</option>
                                                <option>	Kec. Kapoiala	</option>
                                                <option>	Kec. Konawe	</option>
                                                <option>	Asinua	</option>
                                                <option>	Wawonii Tenggara	</option>
                                                <option>	Wawonii Timur Laut	</option>
                                                <option>	Lalonggasumeeto	</option>
                                                <option>	Onembute	</option>
                                                <option>	Kec. Tongkuno	</option>
                                                <option>	Kec. Parigi	</option>
                                                <option>	Kec. Kabawo	</option>
                                                <option>	Kec. Lawa	</option>
                                                <option>	Kec. Kusambi	</option>
                                                <option>	Kec. Katobu	</option>
                                                <option>	Kec. Napabalano	</option>
                                                <option>	Kec. Wakorumba Selatan	</option>
                                                <option>	Kec. Lohia	</option>
                                                <option>	Kec. Kontunaga	</option>
                                                <option>	Kec. Sawerigadi	</option>
                                                <option>	Kec. Maginti	</option>
                                                <option>	Kec. Kabangka	</option>
                                                <option>	Kec. Maligano	</option>
                                                <option>	Kec. Tiworo Tengah	</option>
                                                <option>	Kec. Barangka	</option>
                                                <option>	Kec. Watopute	</option>
                                                <option>	Kec. Batalaiworu	</option>
                                                <option>	Kec. Duruka	</option>
                                                <option>	Kec. Lasalepa	</option>
                                                <option>	Kec. Pasir Putih	</option>
                                                <option>	Kec. Tiworo Kepulauan	</option>
                                                <option>	Kec. Bone	</option>
                                                <option>	Kontu Kowuna	</option>
                                                <option>	Marobo	</option>
                                                <option>	Tongkuno Selatan	</option>
                                                <option>	Pasi Kolaga	</option>
                                                <option>	Batukara	</option>
                                                <option>	Wa Daga	</option>
                                                <option>	Napano Kusambi	</option>
                                                <option>	Towea	</option>
                                                <option>	Tiworo Selatan	</option>
                                                <option>	Tiworo Utara	</option>
                                                <option>	Kec. Lasalimu	</option>
                                                <option>	Kec. Pasar Wajo	</option>
                                                <option>	Kec. Sampolawa	</option>
                                                <option>	Kec. Batauga	</option>
                                                <option>	Kec. Kapontori	</option>
                                                <option>	Kec. Gu	</option>
                                                <option>	Kec. Lakudo	</option>
                                                <option>	Kec. Mawasangka	</option>
                                                <option>	Kec. Lasalimu Selatan	</option>
                                                <option>	Kec. Batu Atas	</option>
                                                <option>	Kec. Siompu	</option>
                                                <option>	Kec. Kadatua	</option>
                                                <option>	Kec. Mawasangka Timur	</option>
                                                <option>	Kec. Talaga Raya	</option>
                                                <option>	Kec. Mawasangka Tengah	</option>
                                                <option>	Kec. Sangia Wambulu	</option>
                                                <option>	Kec. Siontapia	</option>
                                                <option>	Kec. Wolowa	</option>
                                                <option>	Kec. Wabula	</option>
                                                <option>	Kec. Lapandewa	</option>
                                                <option>	Kec. Siompu Barat	</option>
                                                <option>	Kec. Watubangga	</option>
                                                <option>	Kec. Pomalaa	</option>
                                                <option>	Kec. Wundulako	</option>
                                                <option>	Kec. Ladongi	</option>
                                                <option>	Kec. Tirawuta	</option>
                                                <option>	Kec. Kolaka	</option>
                                                <option>	Kec. Wolo	</option>
                                                <option>	Kec. Mowewe	</option>
                                                <option>	Kec. Tanggetada	</option>
                                                <option>	Kec. Baula	</option>
                                                <option>	Kec. Lambandia	</option>
                                                <option>	Kec. Latambaga	</option>
                                                <option>	Kec. Samaturu	</option>
                                                <option>	Kec. Uluiwoi	</option>
                                                <option>	Kec. Tinondo	</option>
                                                <option>	Kec. Poli-Polia	</option>
                                                <option>	Kec. Lalolae	</option>
                                                <option>	Kec. Toari	</option>
                                                <option>	Kec. Polinggona	</option>
                                                <option>	Kec. Loela	</option>
                                                <option>	Tirawuta	</option>
                                                <option>	Ladongi	</option>
                                                <option>	Poli-polia	</option>
                                                <option>	Lambandia	</option>
                                                <option>	Uluiwoi	</option>
                                                <option>	Mowewe	</option>
                                                <option>	Kec. Tinanggea	</option>
                                                <option>	Kec. Palangga	</option>
                                                <option>	Kec. Konda	</option>
                                                <option>	Kec. Lainea	</option>
                                                <option>	Kec. Moramo	</option>
                                                <option>	Kec. Ranomeeto	</option>
                                                <option>	Kec. Landono	</option>
                                                <option>	Kec. Kolono	</option>
                                                <option>	Kec. Andolo	</option>
                                                <option>	Kec. Laonti	</option>
                                                <option>	Kec. Angata	</option>
                                                <option>	Kec. Lalembuu	</option>
                                                <option>	Kec. Buke	</option>
                                                <option>	Kec. Palangga Selatan	</option>
                                                <option>	Kec. Baito	</option>
                                                <option>	Kec. Laeya	</option>
                                                <option>	Kec. Moramo Utara	</option>
                                                <option>	Kec. Wolasi	</option>
                                                <option>	Kec. Ranomeeto Barat	</option>
                                                <option>	Kec. Mowila	</option>
                                                <option>	Kec. Benua	</option>
                                                <option>	Kec. Basala	</option>
                                                <option>	Kec. Binongko	</option>
                                                <option>	Kec. Tomia	</option>
                                                <option>	Kec. Kaledupa	</option>
                                                <option>	Kec. Wangi-Wangi	</option>
                                                <option>	Kec. Wangi-Wangi Selatan	</option>
                                                <option>	Kec. Kaledupa Selatan	</option>
                                                <option>	Kec. Tomia Timur	</option>
                                                <option>	Kec. Togo Binongko	</option>
                                                <option>	Kec. Kabaena	</option>
                                                <option>	Kec. Kabaena Timur	</option>
                                                <option>	Kec. Poleang	</option>
                                                <option>	Kec. Poleang Timur	</option>
                                                <option>	Kec. Rumbia	</option>
                                                <option>	Kec. Rarowatu	</option>
                                                <option>	Kec. Poleang Barat	</option>
                                                <option>	Kec. Mataelo	</option>
                                                <option>	Kec. Rarowatu Utara	</option>
                                                <option>	Kec. Poleang Utara	</option>
                                                <option>	Kec. Poleang Selatan	</option>
                                                <option>	Kec. Poleang Tenggara	</option>
                                                <option>	Kec. Kabaena Selatan	</option>
                                                <option>	Kec. Kabaena Barat	</option>
                                                <option>	Kec. Kabaena Utara	</option>
                                                <option>	Kec. Kabaena Tengah	</option>
                                                <option>	Kec. Kep. Masaloka Raya	</option>
                                                <option>	Kec. Rumbia Tengah	</option>
                                                <option>	Kec. Poleang Tengah	</option>
                                                <option>	Kec. Tatonuwu	</option>
                                                <option>	Kec. Lantari Jaya	</option>
                                                <option>	Kec. Mata Usu	</option>
                                                <option>	Kec. Lasusua	</option>
                                                <option>	Kec. Pakue	</option>
                                                <option>	Kec. Batu Putih	</option>
                                                <option>	Kec. Ranteangin	</option>
                                                <option>	Kec. Kodeoha	</option>
                                                <option>	Kec. Ngapa	</option>
                                                <option>	Kec. Wawo	</option>
                                                <option>	Kec. Lambai	</option>
                                                <option>	Kec. Watunohu	</option>
                                                <option>	Kec. Pakue Tengah	</option>
                                                <option>	Kec. Pakue Utara	</option>
                                                <option>	Kec. Porehu	</option>
                                                <option>	Kec. Katoi	</option>
                                                <option>	Kec. Tiwu	</option>
                                                <option>	Kec. Katoi	</option>
                                                <option>	Tolala	</option>
                                                <option>	Kec. Langkima	</option>
                                                <option>	Kec. Molawe	</option>
                                                <option>	Kec. Lembo	</option>
                                                <option>	Kec. Asera	</option>
                                                <option>	Kec. Wiwirano	</option>
                                                <option>	Kec. Lasolo	</option>
                                                <option>	Kec. Sawa	</option>
                                                <option>	Kec. Oheo	</option>
                                                <option>	Kec. Andowia	</option>
                                                <option>	Kec. Motui	</option>
                                                <option>	Kec. Kulisusu	</option>
                                                <option>	Kec. Kulisusu Barat	</option>
                                                <option>	Kec. Kulisusu Utara	</option>
                                                <option>	Kec. Kambowa	</option>
                                                <option>	Kec. Bonenugu	</option>
                                                <option>	Kec. Wakorumba Utara	</option>
                                                <option>	Wakorumba Utara	</option>
                                                <option>	Loea	</option>
                                                <option>	Lalolae	</option>
                                                <option>	Kec. Mandonga	</option>
                                                <option>	Kec. Poasia	</option>
                                                <option>	Kec. Kendari	</option>
                                                <option>	Kec. Baruga	</option>
                                                <option>	Kec. Kendari Barat	</option>
                                                <option>	Kec. Abeli	</option>
                                                <option>	Kec. Puwato	</option>
                                                <option>	Kec. Kadia	</option>
                                                <option>	Kec. Wua-Wua	</option>
                                                <option>	Kec. Kambu	</option>
                                                <option>	Kec. Betoambari	</option>
                                                <option>	Kec. Wolio	</option>
                                                <option>	Kec. Sorowalio	</option>
                                                <option>	Kec. Bungi	</option>
                                                <option>	Kec. Murhum	</option>
                                                <option>	Kec. Kokalukuna	</option>
                                                <option>	Kec. Lea-Lea	</option>
                                                <option>	Kec. Banda	</option>
                                                <option>	Kec. Tehoru	</option>
                                                <option>	Kec. Amahai	</option>
                                                <option>	Kec. Teon Nila Serua	</option>
                                                <option>	Kec. Saparua	</option>
                                                <option>	Kec. Pulau Haruku	</option>
                                                <option>	Kec. Salahutu	</option>
                                                <option>	Kec. Leihitu	</option>
                                                <option>	Kec. Seram Utara	</option>
                                                <option>	Kec. Masohi	</option>
                                                <option>	Kec. Nusa Laut	</option>
                                                <option>	Kec. Teluk Elpaputih	</option>
                                                <option>	Kec. Seram Utara Barat	</option>
                                                <option>	Kec. Leihitu Barat	</option>
                                                <option>	Telutih	</option>
                                                <option>	Seram Utara Timur Seti	</option>
                                                <option>	Seram Utara Timur Kobi	</option>
                                                <option>	Kec. Kei Kecil	</option>
                                                <option>	Kec. Kei Besar	</option>
                                                <option>	Kec. Kei Besar Selatan	</option>
                                                <option>	Kec. Kei Besar Utara Timur	</option>
                                                <option>	Kec. Kei Kecil Barat	</option>
                                                <option>	Kec. Kei Kecil Timur	</option>
                                                <option>	Kec. Air Buaya	</option>
                                                <option>	Kec. Waeapo	</option>
                                                <option>	Kec. Namlea	</option>
                                                <option>	Kec. Waplau	</option>
                                                <option>	Kec. Bata Baul	</option>
                                                <option>	Kec. Pulau-pulau Terselatan	</option>
                                                <option>	Kec. Pulau-Pulau Babar	</option>
                                                <option>	Kec. Tanimbar Selatan	</option>
                                                <option>	Kec. Tanimbar Utara	</option>
                                                <option>	Kec. Damer	</option>
                                                <option>	Kec. Wer Tamrian	</option>
                                                <option>	Kec. Wer Maktian	</option>
                                                <option>	Kec. Selaru	</option>
                                                <option>	Kec. Yaru	</option>
                                                <option>	Kec. Wuar Labobar	</option>
                                                <option>	Kec. Nirun Mas	</option>
                                                <option>	Kec. Kormomolin	</option>
                                                <option>	Kec. Molu Maru	</option>
                                                <option>	Kec. Kairatu	</option>
                                                <option>	Kec. Seram Barat	</option>
                                                <option>	Kec. Taniwel	</option>
                                                <option>	Kec. Waisala	</option>
                                                <option>	Huamual	</option>
                                                <option>	Inamosol	</option>
                                                <option>	Elpaputih	</option>
                                                <option>	Taniwel Timur	</option>
                                                <option>	Huamual Belakang	</option>
                                                <option>	Kepulauan Manipa	</option>
                                                <option>	Kairatu Barat	</option>
                                                <option>	Kec. Seram Timur	</option>
                                                <option>	Kec. Werinama	</option>
                                                <option>	Kec. Bula	</option>
                                                <option>	Kec. Pulau-pulau Gorom	</option>
                                                <option>	Kec. Wakate	</option>
                                                <option>	Kec. Tutuk Tolu	</option>
                                                <option>	Kec. Pulau-Pulau Aru	</option>
                                                <option>	Kec. Aru Tengah	</option>
                                                <option>	Kec. Aru Selatan	</option>
                                                <option>	Kec. Aru Selatan Timur	</option>
                                                <option>	Kec. Aru Tengah Timur	</option>
                                                <option>	Kec. Aru Tengah Selatan	</option>
                                                <option>	Kec. Aru Utara	</option>
                                                <option>	Kec. Batuley	</option>
                                                <option>	Kec. Sir-Sir	</option>
                                                <option>	Kec. Aru Selatan Utara	</option>
                                                <option>	Kec. Babar Timur	</option>
                                                <option>	Kec. Mdona Hiera	</option>
                                                <option>	Kec. Moa Lakor	</option>
                                                <option>	Kec. Pulau Letti	</option>
                                                <option>	Kec. Wetar	</option>
                                                <option>	Damer	</option>
                                                <option>	Pulau-pulau Babar	</option>
                                                <option>	Pulau-pulau Terselatan	</option>
                                                <option>	Kec. Ambalau	</option>
                                                <option>	Kec. Kepala Madan	</option>
                                                <option>	Kec. Leksula	</option>
                                                <option>	Kec. Namrole	</option>
                                                <option>	Kec. Waesama	</option>
                                                <option>	Kec. Nusaniwe	</option>
                                                <option>	Kec. Sirimau	</option>
                                                <option>	Kec. Teluk Ambon	</option>
                                                <option>	Kec. Baguala	</option>
                                                <option>	Kec. Lei Timur Selatan	</option>
                                                <option>	Kec. PP Kur Mangur	</option>
                                                <option>	Kec. Pulau Dullah Selatan	</option>
                                                <option>	Kec. Pulau Dullah Utara	</option>
                                                <option>	Kec. Tayando Tam	</option>
                                                <option>	Kec. Gerokgak	</option>
                                                <option>	Kec. Seririt	</option>
                                                <option>	Kec. Busungbiu	</option>
                                                <option>	Kec. Banjar	</option>
                                                <option>	Kec. Sukasada	</option>
                                                <option>	Kec. Buleleng	</option>
                                                <option>	Kec. Sawan	</option>
                                                <option>	Kec. Kubutambahan	</option>
                                                <option>	Kec. Tejakula	</option>
                                                <option>	Kec. Melaya	</option>
                                                <option>	Kec. Negara	</option>
                                                <option>	Kec. Mendoyo	</option>
                                                <option>	Kec. Pekutatan	</option>
                                                <option>	Kec. Jembrana	</option>
                                                <option>	Kec. Selemadeg	</option>
                                                <option>	Kec. Kerambitan	</option>
                                                <option>	Kec. Tabanan	</option>
                                                <option>	Kec. Kediri	</option>
                                                <option>	Kec. Marga	</option>
                                                <option>	Kec. Baturiti	</option>
                                                <option>	Kec. Penebel	</option>
                                                <option>	Kec. Pupuan	</option>
                                                <option>	Kec. Selemadeg Barat	</option>
                                                <option>	Kec. Selemadeg Timur	</option>
                                                <option>	Kec. Kuta Selatan	</option>
                                                <option>	Kec. Kuta	</option>
                                                <option>	Kec. Kuta Utara	</option>
                                                <option>	Kec. Mengwi	</option>
                                                <option>	Kec. Abiansemal	</option>
                                                <option>	Kec. Petang	</option>
                                                <option>	Kec. Sukawati	</option>
                                                <option>	Kec. Blahbatuh	</option>
                                                <option>	Kec. Gianyar	</option>
                                                <option>	Kec. Tampak siring	</option>
                                                <option>	Kec. Ubud	</option>
                                                <option>	Kec. Tegallalang	</option>
                                                <option>	Kec. Payangan	</option>
                                                <option>	Kec. Nusapenida	</option>
                                                <option>	Kec. Banjarangkan	</option>
                                                <option>	Kec. Klungkung	</option>
                                                <option>	Kec. Dawan	</option>
                                                <option>	Kec. Susut	</option>
                                                <option>	Kec. Bangli	</option>
                                                <option>	Kec. Tembuku	</option>
                                                <option>	Kec. Kintamani	</option>
                                                <option>	Kec. Rendang	</option>
                                                <option>	Kec. Sidemen	</option>
                                                <option>	Kec. Manggis	</option>
                                                <option>	Kec. Karang asem	</option>
                                                <option>	Kec. Abang	</option>
                                                <option>	Kec. Bebandem	</option>
                                                <option>	Kec. Selat	</option>
                                                <option>	Kec. Kubu	</option>
                                                <option>	Kec. Denpasar Selatan	</option>
                                                <option>	Kec. Denpasar Timur	</option>
                                                <option>	Kec. Denpasar Barat	</option>
                                                <option>	Kec. Denpasar Utara	</option>
                                                <option>	Kec. Sekotong Tengah	</option>
                                                <option>	Kec. Gerung	</option>
                                                <option>	Kec. Labuapi	</option>
                                                <option>	Kec. Kediri	</option>
                                                <option>	Kec. Narmada	</option>
                                                <option>	Kec. Gunung Sari	</option>
                                                <option>	Kec. Kuripan	</option>
                                                <option>	Kec. Lembar	</option>
                                                <option>	Kec. Batu Layar	</option>
                                                <option>	Kec. Lingsar	</option>
                                                <option>	Kec. Praya Barat	</option>
                                                <option>	Kec. Pujut	</option>
                                                <option>	Kec. Praya Timur	</option>
                                                <option>	Kec. Janapria	</option>
                                                <option>	Kec. Kopang	</option>
                                                <option>	Kec. Praya	</option>
                                                <option>	Kec. Jonggat	</option>
                                                <option>	Kec. Pringgarata	</option>
                                                <option>	Kec. Batukliang	</option>
                                                <option>	Kec. Batukliang Utara	</option>
                                                <option>	Kec. Praya Barat Daya	</option>
                                                <option>	Kec. Praya Tengah	</option>
                                                <option>	Kec. Keruak	</option>
                                                <option>	Kec. Sakra	</option>
                                                <option>	Kec. Terara	</option>
                                                <option>	Kec. Sikur	</option>
                                                <option>	Kec. Masbagik	</option>
                                                <option>	Kec. Sukamulia	</option>
                                                <option>	Kec. Selong	</option>
                                                <option>	Kec. Pringgabaya	</option>
                                                <option>	Kec. Aikmel	</option>
                                                <option>	Kec. Sambelia	</option>
                                                <option>	Kec. Labuhan Haji	</option>
                                                <option>	Kec. Suralaga	</option>
                                                <option>	Kec. Sakra Timur	</option>
                                                <option>	Kec. Sakra Barat	</option>
                                                <option>	Kec. Jerowaru	</option>
                                                <option>	Kec. Pringgasela	</option>
                                                <option>	Kec. Montong Gading	</option>
                                                <option>	Kec. Wanasaba	</option>
                                                <option>	Kec. Sembalun	</option>
                                                <option>	Kec. Suela	</option>
                                                <option>	Kec. Lunyuk	</option>
                                                <option>	Kec. Alas	</option>
                                                <option>	Kec. Batu Lanten	</option>
                                                <option>	Kec. Sumbawa	</option>
                                                <option>	Kec. Moyo Hilir	</option>
                                                <option>	Kec. Moyo Hulu	</option>
                                                <option>	Kec. Ropang	</option>
                                                <option>	Kec. Plampang	</option>
                                                <option>	Kec. Empang	</option>
                                                <option>	Kec. Labuhan Badas	</option>
                                                <option>	Kec. Alas Barat	</option>
                                                <option>	Kec. Labangka	</option>
                                                <option>	Kec. Unter Iwes	</option>
                                                <option>	Kec. Rhee	</option>
                                                <option>	Kec. Buer	</option>
                                                <option>	Kec. Moyo Utara	</option>
                                                <option>	Kec. Maronge	</option>
                                                <option>	Kec. Tarano	</option>
                                                <option>	Kec. Lopok	</option>
                                                <option>	Kec. Lenangguar	</option>
                                                <option>	Kec. Orong Telu	</option>
                                                <option>	Kec. Utan	</option>
                                                <option>	Kec. Lape	</option>
                                                <option>	Kec. Lantung	</option>
                                                <option>	Kec. Hu`u	</option>
                                                <option>	Kec. Dompu	</option>
                                                <option>	Kec. Woja	</option>
                                                <option>	Kec. Kilo	</option>
                                                <option>	Kec. Kempo	</option>
                                                <option>	Kec. Pekat	</option>
                                                <option>	Kec. Pajo	</option>
                                                <option>	Kec. Manggelewa	</option>
                                                <option>	Kec. Monta	</option>
                                                <option>	Kec. Bolo	</option>
                                                <option>	Kec. Woha	</option>
                                                <option>	Kec. Belo	</option>
                                                <option>	Kec. Wawo	</option>
                                                <option>	Kec. Sape	</option>
                                                <option>	Kec. Wera	</option>
                                                <option>	Kec. Donggo	</option>
                                                <option>	Kec. Sanggar	</option>
                                                <option>	Kec. Lambu	</option>
                                                <option>	Kec. Tambora	</option>
                                                <option>	Kec. Ambalawi	</option>
                                                <option>	Kec. Mada pangga	</option>
                                                <option>	Kec. Langgudu	</option>
                                                <option>	Kec. Soromandi	</option>
                                                <option>	Kec. Parado	</option>
                                                <option>	Kec. Lambitu	</option>
                                                <option>	Kec. Palibelo	</option>
                                                <option>	Kec. Jereweh	</option>
                                                <option>	Kec. Taliwang	</option>
                                                <option>	Kec. Seteluk	</option>
                                                <option>	Kec. Brang Rea	</option>
                                                <option>	Kec. Sekongkang	</option>
                                                <option>	Kec. Maluk	</option>
                                                <option>	Kec. Brang Ene	</option>
                                                <option>	Kec. Poto Tano	</option>
                                                <option>	Kec. Tanjung	</option>
                                                <option>	Kec. Gangga	</option>
                                                <option>	Kec. Bayan	</option>
                                                <option>	Kec. Pemenang	</option>
                                                <option>	Kec. Kayangan	</option>
                                                <option>	Kec. Ampenan	</option>
                                                <option>	Kec. Mataram	</option>
                                                <option>	Kec. Cakranegara	</option>
                                                <option>	Kec. Sekarbela	</option>
                                                <option>	Kec. Selaperang	</option>
                                                <option>	Kec. Sandubaya	</option>
                                                <option>	Kec. RasanaE Barat	</option>
                                                <option>	Kec. RasanaE Timur	</option>
                                                <option>	Kec. Asakota	</option>
                                                <option>	Kec. Raba	</option>
                                                <option>	Kec. Mpunda	</option>
                                                <option>	Kec. Raijua	</option>
                                                <option>	Kec. Sabu Barat	</option>
                                                <option>	Kec. Sabu Timur	</option>
                                                <option>	Kec. Semau	</option>
                                                <option>	Kec. Kupang Barat	</option>
                                                <option>	Kec. Kupang Tengah	</option>
                                                <option>	Kec. Amarasi	</option>
                                                <option>	Kec. Kupang Timur	</option>
                                                <option>	Kec. Sulamu	</option>
                                                <option>	Kec. Fatuleu	</option>
                                                <option>	Kec. Takari	</option>
                                                <option>	Kec. Amfoang Selatan	</option>
                                                <option>	Kec. Amfoang Utara	</option>
                                                <option>	Kec. Sabu Tengah	</option>
                                                <option>	Kec. Nekamese	</option>
                                                <option>	Kec. Amabi Oefeto Timur	</option>
                                                <option>	Kec. Amarasi Selatan	</option>
                                                <option>	Kec. Amarasi Timur	</option>
                                                <option>	Kec. Amarasi Barat	</option>
                                                <option>	Kec. Amfoang Barat Daya	</option>
                                                <option>	Kec. Amfoang Barat Laut	</option>
                                                <option>	Kec. Sabu Liae	</option>
                                                <option>	Kec. Hawu Mehara	</option>
                                                <option>	Kec. Semau Selatan	</option>
                                                <option>	Kec. Taebenu	</option>
                                                <option>	Kec. Amabi Oefeto	</option>
                                                <option>	Kec. Fatuleu Tengah	</option>
                                                <option>	Kec. Fatuleu Barat	</option>
                                                <option>	Kec. Amfoang Timur	</option>
                                                <option>	Kec. Amfoang Tengah	</option>
                                                <option>	Kec. Mollo Utara	</option>
                                                <option>	Kec. Mollo Selatan	</option>
                                                <option>	Kec. Kota Soe	</option>
                                                <option>	Kec. Amanuban Barat	</option>
                                                <option>	Kec. Amanuban Selatan	</option>
                                                <option>	Kec. Kuanfatu	</option>
                                                <option>	Kec. Amanuban Tengah	</option>
                                                <option>	Kec. Amanuban Timur	</option>
                                                <option>	Kec. Kie	</option>
                                                <option>	Kec. Amanatun Selatan	</option>
                                                <option>	Kec. Amanatun Utara	</option>
                                                <option>	Kec. Fatumnasi	</option>
                                                <option>	Kec. Polen	</option>
                                                <option>	Kec. BatuPutih	</option>
                                                <option>	Kec. Boking	</option>
                                                <option>	Kec. Noebana	</option>
                                                <option>	Kec. Nunkolo	</option>
                                                <option>	Kec. Kot`Olin	</option>
                                                <option>	Kec. Oenino	</option>
                                                <option>	Kec. Kolbano	</option>
                                                <option>	Kec. Kualin	</option>
                                                <option>	Kec. Toianas	</option>
                                                <option>	Kec. Mollo Barat	</option>
                                                <option>	Kec. Kok Baun	</option>
                                                <option>	Kec. Tobu	</option>
                                                <option>	Kec. Nunbena	</option>
                                                <option>	Kec. Mollo Tengah	</option>
                                                <option>	Kec. Kuatnana	</option>
                                                <option>	Kec. Noebeba	</option>
                                                <option>	Kec. Fautmolo	</option>
                                                <option>	Kec. Fatukopa	</option>
                                                <option>	Kec. Santian	</option>
                                                <option>	Kec. Miomafo Barat	</option>
                                                <option>	Kec. Miomafo Timur	</option>
                                                <option>	Kec. Kota Kefamenanu	</option>
                                                <option>	Kec. Insana	</option>
                                                <option>	Kec. Biboki Selatan	</option>
                                                <option>	Kec. Biboki Utara	</option>
                                                <option>	Kec. Noemuti	</option>
                                                <option>	Kec. Insana Utara	</option>
                                                <option>	Kec. Biboki Anleu	</option>
                                                <option>	Kec. Noemuti Timur	</option>
                                                <option>	Kec. Miomafo Tengah	</option>
                                                <option>	Kec. Musi	</option>
                                                <option>	Kec. Mutis	</option>
                                                <option>	Kec. Bikomi Selatan	</option>
                                                <option>	Kec. Bikomi Tengah	</option>
                                                <option>	Kec. Bikomi Nilulat	</option>
                                                <option>	Kec. Bikomi Utara	</option>
                                                <option>	Kec. Naibenu	</option>
                                                <option>	Kec. Insana Fafinesu	</option>
                                                <option>	Kec. Insana Barat	</option>
                                                <option>	Kec. Insana Tengah	</option>
                                                <option>	Kec. Biboki Tanpah	</option>
                                                <option>	Kec. Biboki Moenleu	</option>
                                                <option>	Kec. Biboki Feotleu	</option>
                                                <option>	Kec. Malaka Barat	</option>
                                                <option>	Kec. Malaka Tengah	</option>
                                                <option>	Kec. Malaka Timur	</option>
                                                <option>	Kec. Kobalima	</option>
                                                <option>	Kec. Tasifeto Barat	</option>
                                                <option>	Kec. Kota Atambua	</option>
                                                <option>	Kec. Tasifeto Timur	</option>
                                                <option>	Kec. Lamakmen	</option>
                                                <option>	Kec. Kakuluk Mesak	</option>
                                                <option>	Kec. Raihat	</option>
                                                <option>	Kec. Rinhat	</option>
                                                <option>	Kec. Sasita Mean	</option>
                                                <option>	Kec. Weliman	</option>
                                                <option>	Kec. Wewiku	</option>
                                                <option>	Kec. Rai Manuk	</option>
                                                <option>	Kec. Laen Manen	</option>
                                                <option>	Kec. Lasiolat	</option>
                                                <option>	Kec. Lamakmen Selatan	</option>
                                                <option>	Kec. Lo Kufeu	</option>
                                                <option>	Kec. Botin Leo Bele	</option>
                                                <option>	Kec. Atambua Barat	</option>
                                                <option>	Kec. Atambua Selatan	</option>
                                                <option>	Kec. Nanaet Duabesi	</option>
                                                <option>	Kec. Kobalima Timur	</option>
                                                <option>	Malaka Barat	</option>
                                                <option>	Malaka Timur	</option>
                                                <option>	Kobalima	</option>
                                                <option>	Sasitamean	</option>
                                                <option>	Laen Manen	</option>
                                                <option>	Io Kufeu	</option>
                                                <option>	Botin Leo Bele	</option>
                                                <option>	Kobalima Timur	</option>
                                                <option>	Rinhat	</option>
                                                <option>	Wewiku	</option>
                                                <option>	Weliman	</option>
                                                <option>	Kec. Alor Barat Daya	</option>
                                                <option>	Kec. Alor Selatan	</option>
                                                <option>	Kec. Alor Timur	</option>
                                                <option>	Kec. Teluk Mutiara	</option>
                                                <option>	Kec. Alor Barat Laut	</option>
                                                <option>	Kec. Pantar	</option>
                                                <option>	Kec. Alor Timur Laut	</option>
                                                <option>	Kec. Alor Tengah Utara	</option>
                                                <option>	Kec. Pantar Barat	</option>
                                                <option>	Kec. Pantar Timur	</option>
                                                <option>	Kec. Pantar Barat Laut	</option>
                                                <option>	Kec. Pantar Tengah	</option>
                                                <option>	Kec. Mataru	</option>
                                                <option>	Kec. Pureman	</option>
                                                <option>	Kec. Kabola	</option>
                                                <option>	Kec. Pulau Pura	</option>
                                                <option>	Kec. Lembur	</option>
                                                <option>	Kec. Wulang Gitang	</option>
                                                <option>	Kec. Tanjung Bunga	</option>
                                                <option>	Kec. Larantuka	</option>
                                                <option>	Kec. Solor Barat	</option>
                                                <option>	Kec. Solor Timur	</option>
                                                <option>	Kec. Adonara Barat	</option>
                                                <option>	Kec. Adonara Timur	</option>
                                                <option>	Kec. Titehena	</option>
                                                <option>	Kec. Ile Boleng	</option>
                                                <option>	Kec. Witihama	</option>
                                                <option>	Kec. Kelubagolit	</option>
                                                <option>	Kec. Wotan Ulumado	</option>
                                                <option>	Kec. Ile Mandiri	</option>
                                                <option>	Kec. Demon Pagong	</option>
                                                <option>	Kec. Lewolema	</option>
                                                <option>	Kec. Ilebura	</option>
                                                <option>	Kec. Adonara	</option>
                                                <option>	Kec. Adonara Tengah	</option>
                                                <option>	Kec. Solor Selatan	</option>
                                                <option>	Kec. Paga	</option>
                                                <option>	Kec. Lela	</option>
                                                <option>	Kec. Bola	</option>
                                                <option>	Kec. Talibura	</option>
                                                <option>	Kec. Kewapante	</option>
                                                <option>	Kec. Nelle	</option>
                                                <option>	Kec. Nitta	</option>
                                                <option>	Kec. Alok	</option>
                                                <option>	Kec. Mego	</option>
                                                <option>	Kec. Waigete	</option>
                                                <option>	Kec. Palue	</option>
                                                <option>	Kec. Waiblama	</option>
                                                <option>	Kec. Alok Barat	</option>
                                                <option>	Kec. Alok Timur	</option>
                                                <option>	Kec. Magependa	</option>
                                                <option>	Kec. Koting	</option>
                                                <option>	Kec. Tana Wawo	</option>
                                                <option>	Kec. Hewokloang	</option>
                                                <option>	Kec. Kangae	</option>
                                                <option>	Kec. Doreng	</option>
                                                <option>	Kec. Mapitara	</option>
                                                <option>	Kec. Nangapanda	</option>
                                                <option>	Kec. Ende	</option>
                                                <option>	Kec. Ende Selatan	</option>
                                                <option>	Kec. Ndona	</option>
                                                <option>	Kec. Wolowaru	</option>
                                                <option>	Kec. Maurole	</option>
                                                <option>	Kec. Detusoko	</option>
                                                <option>	Kec. Maukaro	</option>
                                                <option>	Kec. Wewaria	</option>
                                                <option>	Kec. Wolojita	</option>
                                                <option>	Kec. Pulau Ende	</option>
                                                <option>	Kec. Kota Baru	</option>
                                                <option>	Kec. Ndona Timur	</option>
                                                <option>	Kec. Kelimutu	</option>
                                                <option>	Kec. Lio Timur	</option>
                                                <option>	Kec. Detukeli	</option>
                                                <option>	Kec. Ndori	</option>
                                                <option>	Kec. Ende Utara	</option>
                                                <option>	Kec. Ende Tengah	</option>
                                                <option>	Kec. Ende Timur	</option>
                                                <option>	Kec. Lepembusu Kelisoke	</option>
                                                <option>	Kec. Aimere	</option>
                                                <option>	Kec. Bajawa	</option>
                                                <option>	Kec. Golewa	</option>
                                                <option>	Kec. Bajawa Utara	</option>
                                                <option>	Kec. Riung	</option>
                                                <option>	Kec. Riung Barat	</option>
                                                <option>	Kec. Soa	</option>
                                                <option>	Kec. Jerebuu	</option>
                                                <option>	Kec. Riung Selatan	</option>
                                                <option>	Kec. Satarmese	</option>
                                                <option>	Kec. Langke Rembong	</option>
                                                <option>	Kec. Ruteng	</option>
                                                <option>	Kec. Cibal	</option>
                                                <option>	Kec. Reok	</option>
                                                <option>	Kec. Wae Ri`I	</option>
                                                <option>	Kec. Satar Mese Barat	</option>
                                                <option>	Kec. Rahong Utara	</option>
                                                <option>	Kec. Lelak	</option>
                                                <option>	Kec. Lewa	</option>
                                                <option>	Kec. Tabundung	</option>
                                                <option>	Kec. Paberiwai	</option>
                                                <option>	Kec. Pahunga Lodu	</option>
                                                <option>	Kec. Rindi	</option>
                                                <option>	Kec. Pandawai	</option>
                                                <option>	Kec. Kota Waingapu	</option>
                                                <option>	Kec. Haharu	</option>
                                                <option>	Kec. Nggaha Ori Angu	</option>
                                                <option>	Kec. Karera	</option>
                                                <option>	Kec. Umalulu	</option>
                                                <option>	Kec. Kahaungu Eti	</option>
                                                <option>	Kec. Matawai La Pawu	</option>
                                                <option>	Kec. Pinu Pahar	</option>
                                                <option>	Kec. Wulla Waijelu	</option>
                                                <option>	Kec. Katala Hamu Lingu	</option>
                                                <option>	Kec. Mahu	</option>
                                                <option>	Kec. Ngadu Ngala	</option>
                                                <option>	Kec. Kambata Mapambuhang	</option>
                                                <option>	Kec. Kambera	</option>
                                                <option>	Kec. Kanatang	</option>
                                                <option>	Kec. Lewa Tidahu	</option>
                                                <option>	Kec. Loli	</option>
                                                <option>	Kec. Kota Waikabubak	</option>
                                                <option>	Kec. Wanokaka	</option>
                                                <option>	Kec. Lamboya	</option>
                                                <option>	Kec. Tana Righu	</option>
                                                <option>	Kec. Laboya Barat	</option>
                                                <option>	Kec. Naga Wutung	</option>
                                                <option>	Kec. Atadei	</option>
                                                <option>	Kec. Ile Ape	</option>
                                                <option>	Kec. Lebatukan	</option>
                                                <option>	Kec. Nubatukan	</option>
                                                <option>	Kec. Omesuri	</option>
                                                <option>	Kec. Buyasuri	</option>
                                                <option>	Kec. Wulandoni	</option>
                                                <option>	Kec. Ile Ape Timur	</option>
                                                <option>	Kec. Rote Barat Daya	</option>
                                                <option>	Kec. Rote Barat Laut	</option>
                                                <option>	Kec. Lobalain	</option>
                                                <option>	Kec. Rote Tengah	</option>
                                                <option>	Kec. Pantai Baru	</option>
                                                <option>	Kec. Rote Timur	</option>
                                                <option>	Kec. Rote Barat	</option>
                                                <option>	Kec. Rote Selatan	</option>
                                                <option>	Kec. Komodo	</option>
                                                <option>	Kec. Sano Nggoang	</option>
                                                <option>	Kec. Lembor	</option>
                                                <option>	Kec. Kuwus	</option>
                                                <option>	Kec. Macang Pacar	</option>
                                                <option>	Kec. Boleng	</option>
                                                <option>	Kec. Welak	</option>
                                                <option>	Kec. Ndoso	</option>
                                                <option>	Kec. Lembor Selatan	</option>
                                                <option>	Kec. Mbeliling	</option>
                                                <option>	Kec. Aesesa	</option>
                                                <option>	Kec. Boawae	</option>
                                                <option>	Kec. Keo Tengah	</option>
                                                <option>	Kec. Mauponggo	</option>
                                                <option>	Kec. Nangaroro	</option>
                                                <option>	Kec. Wolowae	</option>
                                                <option>	Kec. Aesesa Selatan	</option>
                                                <option>	Kec. Katiku Tana	</option>
                                                <option>	Kec. Mamboro	</option>
                                                <option>	Kec. Umbu Ratu Nggay	</option>
                                                <option>	Kec. Umbu Ratu Nggay Barat	</option>
                                                <option>	Kec. Katiku Tana Selatan	</option>
                                                <option>	Kec. Kodi	</option>
                                                <option>	Kec. Kodi Bangedo	</option>
                                                <option>	Kec. Loura	</option>
                                                <option>	Kec. Wewewa Barat	</option>
                                                <option>	Kec. Wewewa Selatan	</option>
                                                <option>	Kec. Wewewa Timur	</option>
                                                <option>	Kec. Wewewa Utara	</option>
                                                <option>	Kec. Kodi Utara	</option>
                                                <option>	Kec. Elar	</option>
                                                <option>	Kec. Kota Komba	</option>
                                                <option>	Kec. Lamba Leda	</option>
                                                <option>	Kec. Poco Ranaka	</option>
                                                <option>	Kec. Sambi Rampas	</option>
                                                <option>	Kec. Borong	</option>
                                                <option>	Hawu Mehara	</option>
                                                <option>	Sabu Tengah	</option>
                                                <option>	Raijua	</option>
                                                <option>	Sabu Liae	</option>
                                                <option>	Sabu Barat	</option>
                                                <option>	Sabu Timur	</option>
                                                <option>	Kec. Alak	</option>
                                                <option>	Kec. Maulafa	</option>
                                                <option>	Kec. Oebodo	</option>
                                                <option>	Kec. Kelapa Lima	</option>
                                                <option>	Kec. Kota Raja	</option>
                                                <option>	Kec. Kota Lama	</option>
                                                <option>	Kec. Kaureh	</option>
                                                <option>	Kec. Kemtuk	</option>
                                                <option>	Kec. Kemtuk Gresie	</option>
                                                <option>	Kec. Nimboran	</option>
                                                <option>	Kec. Nimbokrang	</option>
                                                <option>	Kec. Unurum Guay	</option>
                                                <option>	Kec. Demta	</option>
                                                <option>	Kec. Depapre	</option>
                                                <option>	Kec. Sentani Barat	</option>
                                                <option>	Kec. Sentani	</option>
                                                <option>	Kec. Sentani Timur	</option>
                                                <option>	Kec. Airu	</option>
                                                <option>	Kec. Yapsi	</option>
                                                <option>	Kec. Nimboran Timur/Namblong	</option>
                                                <option>	Kec. Waibu	</option>
                                                <option>	Kec. Ebungfau	</option>
                                                <option>	Kec. Numfor Barat	</option>
                                                <option>	Kec. Numfor Timur	</option>
                                                <option>	Kec. Padaido	</option>
                                                <option>	Kec. Biak Timur	</option>
                                                <option>	Kec. Biak Kota	</option>
                                                <option>	Kec. Samofa	</option>
                                                <option>	Kec. Yendidori	</option>
                                                <option>	Kec. Biak Utara	</option>
                                                <option>	Kec. Warsa	</option>
                                                <option>	Kec. Biak Barat	</option>
                                                <option>	Kec. Swandiwe	</option>
                                                <option>	Kec. Orkeri	</option>
                                                <option>	Kec. Bruyandori	</option>
                                                <option>	Kec. Poiru	</option>
                                                <option>	Kec. Ainando Padaido	</option>
                                                <option>	Kec. Oridek	</option>
                                                <option>	Kec. Andey	</option>
                                                <option>	Kec. Yawosi	</option>
                                                <option>	Kec. Bondifuar	</option>
                                                <option>	Kec. Yapen Timur	</option>
                                                <option>	Kec. Angkaisera	</option>
                                                <option>	Kec. Yapen Selatan	</option>
                                                <option>	Kec. Yapen Barat	</option>
                                                <option>	Kec. Poom	</option>
                                                <option>	Kec. Kosiwo	</option>
                                                <option>	Kec. Yapen Utara	</option>
                                                <option>	Kec. Raimbawi	</option>
                                                <option>	Kec. Teluk Ampimoi	</option>
                                                <option>	Kec. Kepulauan Ambai	</option>
                                                <option>	Kec. Wonawa	</option>
                                                <option>	Kec. Windesi	</option>
                                                <option>	Kec. Kimaam	</option>
                                                <option>	Kec. Okaba	</option>
                                                <option>	Kec. Kurik	</option>
                                                <option>	Kec. Merauke	</option>
                                                <option>	Kec. Muting	</option>
                                                <option>	Kec. Distrik Ulilin	</option>
                                                <option>	Kec. Semangga	</option>
                                                <option>	Kec. Tanah Miring	</option>
                                                <option>	Kec. Jagebob	</option>
                                                <option>	Kec. Sota	</option>
                                                <option>	Kec. Eligobel	</option>
                                                <option>	Kec. Naukenjerai	</option>
                                                <option>	Kec. Animha	</option>
                                                <option>	Kec. Malind	</option>
                                                <option>	Kec. Tubang	</option>
                                                <option>	Kec. Ngunti	</option>
                                                <option>	Kec. Kaptel	</option>
                                                <option>	Kec. Tabonji	</option>
                                                <option>	Kec. Waan	</option>
                                                <option>	Kec. Ilwayab	</option>
                                                <option>	Kec. Asologaima	</option>
                                                <option>	Kec. Kurulu	</option>
                                                <option>	Kec. Abenaho	</option>
                                                <option>	Kec. Walelagama	</option>
                                                <option>	Kec. Musatfak	</option>
                                                <option>	Kec. Asolokobal	</option>
                                                <option>	Kec. Pelebaga	</option>
                                                <option>	Kec. Yalengga	</option>
                                                <option>	Kec. Wamena	</option>
                                                <option>	Kec. Hubikosi	</option>
                                                <option>	Kec. Bolakme	</option>
                                                <option>	Kec. Wollo	</option>
                                                <option>	Kec. Uwapa	</option>
                                                <option>	Kec. Yaur	</option>
                                                <option>	Kec. Wanggar	</option>
                                                <option>	Kec. Nabire	</option>
                                                <option>	Kec. Napan	</option>
                                                <option>	Kec. Siriwo	</option>
                                                <option>	Kec. Teluk Umar	</option>
                                                <option>	Kec. Makimi	</option>
                                                <option>	Kec. Teluk Kimi	</option>
                                                <option>	Kec. Yarokibisay	</option>
                                                <option>	Kec. Nabire Barat	</option>
                                                <option>	Kec. Wapoga	</option>
                                                <option>	Dipha	</option>
                                                <option>	Yaro	</option>
                                                <option>	Kec. Paniai Timur	</option>
                                                <option>	Kec. Bibida	</option>
                                                <option>	Kec. Paniai Barat	</option>
                                                <option>	Kec. Bogoboida	</option>
                                                <option>	Kec. Yatamo	</option>
                                                <option>	Kec. Kebo	</option>
                                                <option>	Kec. Dumadama	</option>
                                                <option>	Kec. Ekadide	</option>
                                                <option>	Kec. Siriwo	</option>
                                                <option>	Aradide	</option>
                                                <option>	Kec. Fawi	</option>
                                                <option>	Kec. Mulia	</option>
                                                <option>	Kec. Ilu	</option>
                                                <option>	Kec. Mewoluk	</option>
                                                <option>	Kec. Yamo	</option>
                                                <option>	Kec. Torere	</option>
                                                <option>	Kec. Jigonikme	</option>
                                                <option>	Kec. Tingginambut	</option>
                                                <option>	Kec. Mimika Barat	</option>
                                                <option>	Kec. Mimika Timur	</option>
                                                <option>	Kec. Mimika Baru	</option>
                                                <option>	Kec. Agimuga	</option>
                                                <option>	Kec. Mimika Barat Jauh	</option>
                                                <option>	Kec. Mimika Barat Tengah	</option>
                                                <option>	Kec. Tembagapura	</option>
                                                <option>	Kec. Jila	</option>
                                                <option>	Kec. Jita	</option>
                                                <option>	Kec. Kuala Kencana	</option>
                                                <option>	Kec. Mimika Timur Tengah	</option>
                                                <option>	Kec. Mimika Timur Jauh	</option>
                                                <option>	Kec. Jair	</option>
                                                <option>	Kec. Mindiptana	</option>
                                                <option>	Kec. Mandobo	</option>
                                                <option>	Kec. Kouh	</option>
                                                <option>	Kec. Waropko	</option>
                                                <option>	Kec. Distrik Bomakia	</option>
                                                <option>	Kec. Subur	</option>
                                                <option>	Kec. Iniyandit	</option>
                                                <option>	Kec. Fofi	</option>
                                                <option>	Kec. Arimop	</option>
                                                <option>	Kec. Firiwage	</option>
                                                <option>	Kec. Manggelum	</option>
                                                <option>	Kec. Yaniruma	</option>
                                                <option>	Kec. Ambatkwi	</option>
                                                <option>	Kombut	</option>
                                                <option>	Kec. Nambioman Bapai	</option>
                                                <option>	Kec. Edera	</option>
                                                <option>	Kec. Obaa	</option>
                                                <option>	Kec. Haju	</option>
                                                <option>	Kec. Assue	</option>
                                                <option>	Kec. Citakmitak	</option>
                                                <option>	Kec. Minyamur	</option>
                                                <option>	Kec. Venaha	</option>
                                                <option>	Kec. Passue	</option>
                                                <option>	Kec. Kaibar	</option>
                                                <option>	Kec. Pantai Kasuari	</option>
                                                <option>	Kec. Fayit	</option>
                                                <option>	Kec. Atsy	</option>
                                                <option>	Kec. Suator	</option>
                                                <option>	Kec. Akat	</option>
                                                <option>	Kec. Agats	</option>
                                                <option>	Kec. Sawaerma	</option>
                                                <option>	Kec. Kurima	</option>
                                                <option>	Kec. Ninia	</option>
                                                <option>	Kec. Anggruk	</option>
                                                <option>	Kec. Dekai	</option>
                                                <option>	Kec. Obio	</option>
                                                <option>	Kec. Suru Suru	</option>
                                                <option>	Kec. Wusuma	</option>
                                                <option>	Kec. Amuma	</option>
                                                <option>	Kec. Wusaik	</option>
                                                <option>	Kec. Pasema	</option>
                                                <option>	Kec. Hogio	</option>
                                                <option>	Kec. Mogi	</option>
                                                <option>	Kec. Soba	</option>
                                                <option>	Kec. Werima	</option>
                                                <option>	Kec. Tangma	</option>
                                                <option>	Kec. Ukha	</option>
                                                <option>	Kec. Panggema	</option>
                                                <option>	Kec. Kosarek	</option>
                                                <option>	Kec. Nipsan	</option>
                                                <option>	Kec. Ubahak	</option>
                                                <option>	Kec. Pronggoli	</option>
                                                <option>	Kec. Walma	</option>
                                                <option>	Kec. Yahuliambut	</option>
                                                <option>	Kec. Puldama	</option>
                                                <option>	Kec. Hereapingi	</option>
                                                <option>	Kec. Ubahili	</option>
                                                <option>	Kec. Talambo	</option>
                                                <option>	Kec. Endomen	</option>
                                                <option>	Kec. Kona	</option>
                                                <option>	Kec. Dirwemna	</option>
                                                <option>	Kec. Holuwon	</option>
                                                <option>	Kec. Lolat	</option>
                                                <option>	Kec. Soloikma	</option>
                                                <option>	Kec. Sela	</option>
                                                <option>	Kec. Korupun	</option>
                                                <option>	Kec. Langda	</option>
                                                <option>	Kec. Bomela	</option>
                                                <option>	Kec. Suntamon	</option>
                                                <option>	Kec. Seredela	</option>
                                                <option>	Kec. Sobaham	</option>
                                                <option>	Kec. Kabianggama	</option>
                                                <option>	Kec. Kwelamdua	</option>
                                                <option>	Kec. Kwikma	</option>
                                                <option>	Kec. Hilipuk	</option>
                                                <option>	Kec. Duram	</option>
                                                <option>	Kec. Yogosem	</option>
                                                <option>	Kec. Kayo	</option>
                                                <option>	Kec. Sumo	</option>
                                                <option>	Kec. Silimo	</option>
                                                <option>	Kec. Samenage	</option>
                                                <option>	Kec. Nalca	</option>
                                                <option>	Seradala	</option>
                                                <option>	Kec. Okiwur	</option>
                                                <option>	Kec. Oksibil	</option>
                                                <option>	Kec. Borme	</option>
                                                <option>	Kec. Okbibab	</option>
                                                <option>	Kec. Kiwirok	</option>
                                                <option>	Kec. Batom	</option>
                                                <option>	Kec. Pepera	</option>
                                                <option>	Kec. Bime	</option>
                                                <option>	Kec. Aboy	</option>
                                                <option>	Kec. Kiwirok Timur	</option>
                                                <option>	Kec. Kawor	</option>
                                                <option>	Kec. Tarup	</option>
                                                <option>	Kec. Alemsom	</option>
                                                <option>	Kec. Serambakon	</option>
                                                <option>	Kec. Kalomdom	</option>
                                                <option>	Kec. Oksop	</option>
                                                <option>	Kec. Epumek	</option>
                                                <option>	Kec. Weime	</option>
                                                <option>	Kec. Okbab	</option>
                                                <option>	Kec. Teiraplu	</option>
                                                <option>	Kec. Sopsebang	</option>
                                                <option>	Kec. Hokhika	</option>
                                                <option>	Kec.Oklip	</option>
                                                <option>	Kec. Oksamol	</option>
                                                <option>	Kec. Bemta	</option>
                                                <option>	Okbape	</option>
                                                <option>	Ok Aom	</option>
                                                <option>	Awinbon	</option>
                                                <option>	Batani	</option>
                                                <option>	Murkim	</option>
                                                <option>	Mofinop	</option>
                                                <option>	Jetfa	</option>
                                                <option>	Nongme	</option>
                                                <option>	Pamek	</option>
                                                <option>	Iwur	</option>
                                                <option>	Okbemtau	</option>
                                                <option>	Oksebang	</option>
                                                <option>	Kec. Kanggime	</option>
                                                <option>	Kec. Karubaga	</option>
                                                <option>	Kec. Bokondini	</option>
                                                <option>	Kec. Kembu	</option>
                                                <option>	Kec. Goyage	</option>
                                                <option>	Kec. Kubu	</option>
                                                <option>	Kec. Geya	</option>
                                                <option>	Kec. Numba	</option>
                                                <option>	Kec. Dundu	</option>
                                                <option>	Kec. Gudage	</option>
                                                <option>	Kec. Timori	</option>
                                                <option>	Kec. Konda	</option>
                                                <option>	Kec. Nelawi	</option>
                                                <option>	Kec. Kuari	</option>
                                                <option>	Kec. Bokoneri	</option>
                                                <option>	Kec. Bewani	</option>
                                                <option>	Kec. Komboneri	</option>
                                                <option>	Kec. Nabunage	</option>
                                                <option>	Kec. Wakuo	</option>
                                                <option>	Kec. Nunggawi	</option>
                                                <option>	Kec. Woniki	</option>
                                                <option>	Kec. Wunin	</option>
                                                <option>	Kec. Wina	</option>
                                                <option>	Kec. Panaga	</option>
                                                <option>	Kec. Poganeri	</option>
                                                <option>	Kec. Dow	</option>
                                                <option>	Kec. Wari/Taiyeve	</option>
                                                <option>	Kec. Umagi	</option>
                                                <option>	Kec. Gilungbandu	</option>
                                                <option>	Kec. Yuneri	</option>
                                                <option>	Kec. Taginire	</option>
                                                <option>	Kec. Egiam	</option>
                                                <option>	Kec. Air Garam	</option>
                                                <option>	Gika	</option>
                                                <option>	Telenggeme	</option>
                                                <option>	Anawi	</option>
                                                <option>	Wenam	</option>
                                                <option>	Wugi	</option>
                                                <option>	Danime	</option>
                                                <option>	Tagime	</option>
                                                <option>	tidak ada	</option>
                                                <option>	Aweku	</option>
                                                <option>	Bogonuk	</option>
                                                <option>	Li Anogomma	</option>
                                                <option>	Biuk	</option>
                                                <option>	Yuko	</option>
                                                <option>	Kamboneri	</option>
                                                <option>	Kec. Pantai Timur	</option>
                                                <option>	Kec. Bonggo	</option>
                                                <option>	Kec. Tor Atas	</option>
                                                <option>	Kec. Sarmi	</option>
                                                <option>	Kec. Pantai Barat	</option>
                                                <option>	Kec. Pantai Timur Bagian Barat	</option>
                                                <option>	Kec. Bonggo Timur	</option>
                                                <option>	Kec. Sarmi Timur	</option>
                                                <option>	Kec. Sarmi Barat	</option>
                                                <option>	Kec. Apawert Hulu	</option>
                                                <option>	Sarmi Selatan	</option>
                                                <option>	Kec. Web	</option>
                                                <option>	Kec. Senggi	</option>
                                                <option>	Kec. Waris	</option>
                                                <option>	Kec. Arso	</option>
                                                <option>	Kec. Skamto	</option>
                                                <option>	Kec. Towe Hitam	</option>
                                                <option>	Kec. Arso Timur	</option>
                                                <option>	Kec. Waropen Bawah	</option>
                                                <option>	Kec. Masirei	</option>
                                                <option>	Kec. Inggerus	</option>
                                                <option>	Kec. Ureifaisei	</option>
                                                <option>	Kec. Risei Sayati	</option>
                                                <option>	Kec. Kirihi	</option>
                                                <option>	Wapoga	</option>
                                                <option>	Kec. Supiori Selatan	</option>
                                                <option>	Kec. Yenggarbun	</option>
                                                <option>	Kec. Supiori Timur	</option>
                                                <option>	Kec. Kepulauan Aruri	</option>
                                                <option>	Kec. Supiori Barat	</option>
                                                <option>	Supiori Utara	</option>
                                                <option>	Kec. Waropen Atas	</option>
                                                <option>	Kec. Mamberamo Ilir	</option>
                                                <option>	Kec. Mamberamo Tengah	</option>
                                                <option>	Kec. Mamberamo Tengah Timur	</option>
                                                <option>	Kec. Rufaer	</option>
                                                <option>	Kec. Mamberamo Ulu	</option>
                                                <option>	Kec. Benuki	</option>
                                                <option>	Kec. Sawai	</option>
                                                <option>	Mamberamo Hulu	</option>
                                                <option>	Mamberamo Hilir	</option>
                                                <option>	Kec. Wosak	</option>
                                                <option>	Kec. Kenyam	</option>
                                                <option>	Kec. Geselma	</option>
                                                <option>	Kec. Mapenduma	</option>
                                                <option>	Kec. Mugi	</option>
                                                <option>	Kec. Yigi	</option>
                                                <option>	Kec. Mbuwa	</option>
                                                <option>	Kec. Gearek	</option>
                                                <option>	Kec. Makki	</option>
                                                <option>	Kec. Pirime	</option>
                                                <option>	Kec. Tiom	</option>
                                                <option>	Kec. Balingga	</option>
                                                <option>	Kec. Kuyawage	</option>
                                                <option>	Kec. Malagaineri	</option>
                                                <option>	Kec. Tiomneri	</option>
                                                <option>	Kec. Dimba	</option>
                                                <option>	Kec. Gamelia	</option>
                                                <option>	Kec. Poga	</option>
                                                <option>	Kec. Kobakma	</option>
                                                <option>	Kec. Ilugwa	</option>
                                                <option>	Kec. Kelila	</option>
                                                <option>	Kec. Eragayam	</option>
                                                <option>	Kec. Megambilis	</option>
                                                <option>	Kec. Welarek	</option>
                                                <option>	Kec. Apalapsili	</option>
                                                <option>	Kec. Abenaho	</option>
                                                <option>	Kec. Elelim	</option>
                                                <option>	Kec. Benawa	</option>
                                                <option>	Kec. Agadugume	</option>
                                                <option>	Kec. Gome	</option>
                                                <option>	Kec. Ilaga	</option>
                                                <option>	Kec. Sinak	</option>
                                                <option>	Kec. Pogoma	</option>
                                                <option>	Kec. Wangbe	</option>
                                                <option>	Kec. Beoga	</option>
                                                <option>	Kec. Doufo	</option>
                                                <option>	Kec. Piyaiye	</option>
                                                <option>	Kec. Mapia Barat	</option>
                                                <option>	Kec. Mapia	</option>
                                                <option>	Kec. Dogiyai	</option>
                                                <option>	Kec. Kamu Selatan	</option>
                                                <option>	Kec. Kamu	</option>
                                                <option>	Kec. Mapia Tengah	</option>
                                                <option>	Kec. Kamu Tmur	</option>
                                                <option>	Kec. Kamu Utara	</option>
                                                <option>	Kec. Sukikai Selatan	</option>
                                                <option>	Kec. Tigi	</option>
                                                <option>	Kec. Tigi Barat	</option>
                                                <option>	Kec. Tigi Timur	</option>
                                                <option>	Kec. Bowobado	</option>
                                                <option>	Kec. Kapiraya	</option>
                                                <option>	Kec. Sugapa	</option>
                                                <option>	Kec. Hitadipa	</option>
                                                <option>	Kec. Homeyo	</option>
                                                <option>	Kec. Biandoga	</option>
                                                <option>	Kec. Wandai	</option>
                                                <option>	Kec. Agisiga	</option>
                                                <option>	Sugapa*	</option>
                                                <option>	Hitadipa*	</option>
                                                <option>	Homeyo*	</option>
                                                <option>	Biandoga*	</option>
                                                <option>	Wandai*	</option>
                                                <option>	Agisiga*	</option>
                                                <option>	Kec. Muara Tami	</option>
                                                <option>	Kec. Abepura	</option>
                                                <option>	Kec. Jayapura Selatan	</option>
                                                <option>	Kec. Jayapura Utara	</option>
                                                <option>	Kec. Heram	</option>
                                                <option>	Kotaraja	</option>
                                                <option>	Kec. Enggano	</option>
                                                <option>	Kec. Kerkap	</option>
                                                <option>	Kec. Arga Makmur	</option>
                                                <option>	Kec. Lais	</option>
                                                <option>	Kec. Padang Jaya	</option>
                                                <option>	Kec. Ketahun	</option>
                                                <option>	Kec. Putri Hijau	</option>
                                                <option>	Kec. Air Napal	</option>
                                                <option>	Kec. Air Besi	</option>
                                                <option>	Kec. Batik Nau	</option>
                                                <option>	Kec. Giri Mulia	</option>
                                                <option>	Kec. Napal Putih	</option>
                                                <option>	Hulu Palik	</option>
                                                <option>	Air Padang	</option>
                                                <option>	Kec. Kota Padang	</option>
                                                <option>	Kec. Padang Ulang Tanding	</option>
                                                <option>	Kec. Curup	</option>
                                                <option>	Kec. Sindang Kelingi	</option>
                                                <option>	Kec. Bermani Ulu	</option>
                                                <option>	Kec. Selupu Rejang	</option>
                                                <option>	Kec. Sindang Beliti Ilir	</option>
                                                <option>	Kec. Bindu Riang	</option>
                                                <option>	Kec. Sindang Beliti Ulu	</option>
                                                <option>	Kec. Sindang Dataran	</option>
                                                <option>	Kec. Curup Selatan	</option>
                                                <option>	Kec. Curup Tengah	</option>
                                                <option>	Kec. Bermani Ulu Raya	</option>
                                                <option>	Kec. Curup Utara	</option>
                                                <option>	Kec. Curup Timur	</option>
                                                <option>	Kec. Manna	</option>
                                                <option>	Kec. Seginim	</option>
                                                <option>	Kec. Pino	</option>
                                                <option>	Kec. Kota Manna	</option>
                                                <option>	Kec. Pinoraya	</option>
                                                <option>	Kec. Kedurang	</option>
                                                <option>	Kec. Bunga Mas	</option>
                                                <option>	Kec. Pasar Manna	</option>
                                                <option>	Kec. Kedurang Ilir	</option>
                                                <option>	Kec. Air Nipis	</option>
                                                <option>	Kec. Ulu Manna	</option>
                                                <option>	Kec. Muko-Muko Selatan	</option>
                                                <option>	Kec. Teras Terunjam	</option>
                                                <option>	Kec. Muko-Muko Utara	</option>
                                                <option>	Kec. Pondok Suguh	</option>
                                                <option>	Kec. Lubuk Pinang	</option>
                                                <option>	Kec. Air Rami	</option>
                                                <option>	Kec. Malin Deman	</option>
                                                <option>	Kec. Sungai Rumbai	</option>
                                                <option>	Kec. Teramang Jaya	</option>
                                                <option>	Kec. Penarik	</option>
                                                <option>	Kec. Selagan Raya	</option>
                                                <option>	Kec. Air Dikit	</option>
                                                <option>	Kec. XIV Koto	</option>
                                                <option>	Kec. Air Manjunto	</option>
                                                <option>	Kec. V Koto	</option>
                                                <option>	Kota Mukomuko	</option>
                                                <option>	Ipuh	</option>
                                                <option>	Kec. Kepahiang	</option>
                                                <option>	Kec. Bermani Ilir	</option>
                                                <option>	Kec. Tebat Karai	</option>
                                                <option>	Kec. Ujan Mas	</option>
                                                <option>	Kec. Muara Kemumu	</option>
                                                <option>	Kec. Seberang Musi	</option>
                                                <option>	Kec. Kaba Wetan	</option>
                                                <option>	Kec. Merigi	</option>
                                                <option>	Kec. Lebong Selatan	</option>
                                                <option>	Kec. Lebong Utara	</option>
                                                <option>	Kec. Rimbo Pegadang	</option>
                                                <option>	Kec. Lebong Tengah	</option>
                                                <option>	Kec. Lebong Atas	</option>
                                                <option>	Topos	</option>
                                                <option>	Bingin Kuning	</option>
                                                <option>	Lebong Sakti	</option>
                                                <option>	Pelabai	</option>
                                                <option>	Amen	</option>
                                                <option>	Uram Jaya	</option>
                                                <option>	Pinang Belapis	</option>
                                                <option>	Kec. Kaur Selatan	</option>
                                                <option>	Kec. Kaur Tengah	</option>
                                                <option>	Kec. Kaur Utara	</option>
                                                <option>	Kec. Maje	</option>
                                                <option>	Kec. Nasal	</option>
                                                <option>	Kec. Kinal	</option>
                                                <option>	Kec. Tanjung Kemuning	</option>
                                                <option>	Kec. Muara Tetap	</option>
                                                <option>	Kec. Luas	</option>
                                                <option>	Kec. Muara Sahung	</option>
                                                <option>	Kec. Semidang Gumai	</option>
                                                <option>	Kec. Kelam Tengah	</option>
                                                <option>	Kec. Padang Guci Hilir	</option>
                                                <option>	Kec. Padang Guci Ulu	</option>
                                                <option>	Kec. Lungkang Kule	</option>
                                                <option>	Kec. Talo	</option>
                                                <option>	Kec. Seluma	</option>
                                                <option>	Kec. Sukaraja	</option>
                                                <option>	Kec. Semidang Alas Maras	</option>
                                                <option>	Kec. Semidang Alas	</option>
                                                <option>	Kec. Ilir Talo	</option>
                                                <option>	Kec. Talo Kecil	</option>
                                                <option>	Kec. Ulu Talo	</option>
                                                <option>	Kec. Seluma Selatan	</option>
                                                <option>	Kec. Seluma Barat	</option>
                                                <option>	Kec. Seluma Timur	</option>
                                                <option>	Kec. Seluma Utara	</option>
                                                <option>	Kec. Air Periukan	</option>
                                                <option>	Kec. Lubuk Sandi	</option>
                                                <option>	Kec. Talang Empat	</option>
                                                <option>	Kec. Karang Tinggi	</option>
                                                <option>	Kec. Taba Penanjung	</option>
                                                <option>	Kec. Pagar Jati	</option>
                                                <option>	Kec. Pondok Kelapa	</option>
                                                <option>	Kec. Pematang Tiga	</option>
                                                <option>	Kec. Merigi Kelindang	</option>
                                                <option>	Kec. Merigi Sakti	</option>
                                                <option>	Kec. Pondok Kubang	</option>
                                                <option>	Kec. Bang Haji	</option>
                                                <option>	Kec. Selebar	</option>
                                                <option>	Kec. Gading Cempaka	</option>
                                                <option>	Kec. Teluk Segara	</option>
                                                <option>	Kec. Muara Bangkahulu	</option>
                                                <option>	Kec. Kampung Melayu	</option>
                                                <option>	Kec. Ratu Agung	</option>
                                                <option>	Kec. Ratu Samban	</option>
                                                <option>	Kec. Sungai Serut	</option>
                                                <option>	Kec. Singaran Pati	</option>
                                                <option>	Kec. Weda	</option>
                                                <option>	Kec. Pulau Gebe	</option>
                                                <option>	Kec. Patani	</option>
                                                <option>	Kec. Weda Utara	</option>
                                                <option>	Kec. Weda Selatan	</option>
                                                <option>	Kec. Patani Utara	</option>
                                                <option>	Weda Tengah	</option>
                                                <option>	Patani Barat	</option>
                                                <option>	Kec. Sahu	</option>
                                                <option>	Kec. Ibu	</option>
                                                <option>	Kec. Loloda	</option>
                                                <option>	Kec. Jailolo Selatan	</option>
                                                <option>	Kec. Jailolo	</option>
                                                <option>	Kec. Sahu Timur	</option>
                                                <option>	Kec. Ibu Selatan	</option>
                                                <option>	Kec. Ibu Utara	</option>
                                                <option>	Jailolo Timur	</option>
                                                <option>	Kec. Tobelo	</option>
                                                <option>	Kec. Tobelo Selatan	</option>
                                                <option>	Kec. Kao	</option>
                                                <option>	Kec. Galela	</option>
                                                <option>	Kec. Loloda Utara	</option>
                                                <option>	Kec. Malifut	</option>
                                                <option>	Kec. Tobelo Utara	</option>
                                                <option>	Kec. Tobelo Tengah	</option>
                                                <option>	Kec. Tobelo Timur	</option>
                                                <option>	Kec. Tobelo Barat	</option>
                                                <option>	Kec. Galela Barat	</option>
                                                <option>	Kec. Galela Selatan	</option>
                                                <option>	Kec. Galela Utara	</option>
                                                <option>	Kec. Loloda Kepulauan	</option>
                                                <option>	Kec. Kao Utara	</option>
                                                <option>	Kec. Kao Barat	</option>
                                                <option>	Kec. Kao Teluk	</option>
                                                <option>	Kec. Bacan Timur	</option>
                                                <option>	Kec. Bacan Barat	</option>
                                                <option>	Kec. Bacan	</option>
                                                <option>	Kec. Obi	</option>
                                                <option>	Kec. Gane Barat	</option>
                                                <option>	Kec. Gane Timur	</option>
                                                <option>	Kec. Kayoa	</option>
                                                <option>	Kec. Pulau Makian	</option>
                                                <option>	Kec. Obi Selatan	</option>
                                                <option>	Kec. Obi Barat	</option>
                                                <option>	Kec. Obi Timur	</option>
                                                <option>	Kec. Obi Utara	</option>
                                                <option>	Kec. Mandioli Selatan	</option>
                                                <option>	Kec. Mandioli Utara	</option>
                                                <option>	Kec. Bancan Selatan	</option>
                                                <option>	Kec. Batang Lomang	</option>
                                                <option>	Kec. Bacan Timur Selatan	</option>
                                                <option>	Kec. Bacan Timur Tengah	</option>
                                                <option>	Kec. Kasiruta Barat	</option>
                                                <option>	Kec. Kasiruta Timur	</option>
                                                <option>	Kec. Bacan Barat Utara	</option>
                                                <option>	Kec. Kayoa Barat	</option>
                                                <option>	Kec. Kayoa Selatan	</option>
                                                <option>	Kec. Kayoa Utara	</option>
                                                <option>	Kec. Makian Barat	</option>
                                                <option>	Kec. Gane Selatan	</option>
                                                <option>	Kec. Gane Utara	</option>
                                                <option>	Kec. Kepulauan Joronga	</option>
                                                <option>	Kec. Gane Timur Tengah	</option>
                                                <option>	Kec. Gane Timur Selatan	</option>
                                                <option>	Gane Barat Selatan	</option>
                                                <option>	Gane Barat Utara	</option>
                                                <option>	Kec. Maba Selatan	</option>
                                                <option>	Kec. Wasile Selatan	</option>
                                                <option>	Kec. Wasile	</option>
                                                <option>	Kec. Maba	</option>
                                                <option>	Kec. Wasile Tengah	</option>
                                                <option>	Kec. Wasile Utara	</option>
                                                <option>	Kec. Wasile Timur	</option>
                                                <option>	Kec. Maba Tengah	</option>
                                                <option>	Kec. Maba Utara	</option>
                                                <option>	Kec. Kota Maba	</option>
                                                <option>	Kec. Sanana	</option>
                                                <option>	Kec. Sula Besi Barat	</option>
                                                <option>	Kec. Mangoli Timur	</option>
                                                <option>	Kec. Taliabu Barat	</option>
                                                <option>	Kec. Taliabu Timur	</option>
                                                <option>	Kec. Mangoli Barat	</option>
                                                <option>	Kec. Sula Besi Tengah	</option>
                                                <option>	Kec. Sula Besi Timur	</option>
                                                <option>	Kec. Mangoli Tengah	</option>
                                                <option>	Kec. Mangoli Utara Timur	</option>
                                                <option>	Kec. Mangoli Utara	</option>
                                                <option>	Kec. Mangoli Selatan	</option>
                                                <option>	Kec. Taliabu Timur Selatan	</option>
                                                <option>	Kec. Taliabu Utara	</option>
                                                <option>	Kec. Sula Besi Selatan	</option>
                                                <option>	Kec. Sanana Utara	</option>
                                                <option>	Kec. Taliabu Barat Laut	</option>
                                                <option>	Kec. Lede	</option>
                                                <option>	Kec. Taliabu Selatan	</option>
                                                <option>	Mangoli Tengah	</option>
                                                <option>	Taliabu Utara	</option>
                                                <option>	Taliabu Timur Selatan	</option>
                                                <option>	Taliabu -Timur	</option>
                                                <option>	Taliabu Selatan	</option>
                                                <option>	Taliabu Barat Laut	</option>
                                                <option>	Taliabu Barat	</option>
                                                <option>	Lede	</option>
                                                <option>	Tabona	</option>
                                                <option>	Kec. Morotai Selatan Barat	</option>
                                                <option>	Kec. Morotai Selatan	</option>
                                                <option>	Kec. Morotai Utara	</option>
                                                <option>	Kec. Morotai Jaya	</option>
                                                <option>	Kec. Morotai Timur	</option>
                                                <option>	Kec. Pulau Ternate	</option>
                                                <option>	Kec. Ternate Selatan	</option>
                                                <option>	Kec. Ternate Utara	</option>
                                                <option>	Kec. Moti	</option>
                                                <option>	Kec. Pulau Batang Dua	</option>
                                                <option>	Kec. Ternate Tengah	</option>
                                                <option>	Kec. Pulau Hiri	</option>
                                                <option>	Kec. Tidore Selatan	</option>
                                                <option>	Kec. Tidore Utara	</option>
                                                <option>	Kec. Oba	</option>
                                                <option>	Kec. Oba Utara	</option>
                                                <option>	Kec. Oba Tengah	</option>
                                                <option>	Kec. Oba Selatan	</option>
                                                <option>	Kec. Tidore	</option>
                                                <option>	Kec. Tidore Timur	</option>
                                                <option>	Kec. Sumur	</option>
                                                <option>	Kec. Cimanggu	</option>
                                                <option>	Kec. Cibaliung	</option>
                                                <option>	Kec. Cikeusik	</option>
                                                <option>	Kec. Cigeulis	</option>
                                                <option>	Kec. Panimbang	</option>
                                                <option>	Kec. Munjul	</option>
                                                <option>	Kec. Picung	</option>
                                                <option>	Kec. Bojong	</option>
                                                <option>	Kec. Saketi	</option>
                                                <option>	Kec. Pagelaran	</option>
                                                <option>	Kec. Labuan	</option>
                                                <option>	Kec. Jiput	</option>
                                                <option>	Kec. Menes	</option>
                                                <option>	Kec. Mandalawangi	</option>
                                                <option>	Kec. Cimanuk	</option>
                                                <option>	Kec. Banjar	</option>
                                                <option>	Kec. Pandeglang	</option>
                                                <option>	Kec. Cadasari	</option>
                                                <option>	Kec. Angsana	</option>
                                                <option>	Kec. Karang Tanjung	</option>
                                                <option>	Kec. Kaduhejo	</option>
                                                <option>	Kec. Cikedal	</option>
                                                <option>	Kec. Cipeucang	</option>
                                                <option>	Kec. Cisata	</option>
                                                <option>	Kec. Patia	</option>
                                                <option>	Kec. Carita	</option>
                                                <option>	Kec. Sukaresmi	</option>
                                                <option>	Kec. Mekarjaya	</option>
                                                <option>	Kec. Sindangresmi	</option>
                                                <option>	Kec. Pulosari	</option>
                                                <option>	Kec. Koroncong	</option>
                                                <option>	Kec. Cibitung	</option>
                                                <option>	Kec. Majasari	</option>
                                                <option>	Kec. Sobang	</option>
                                                <option>	Kec. Malingping	</option>
                                                <option>	Kec. Panggarangan	</option>
                                                <option>	Kec. Bayah	</option>
                                                <option>	Kec. Cibeber	</option>
                                                <option>	Kec. Cijaku	</option>
                                                <option>	Kec. Banjarsari	</option>
                                                <option>	Kec. Cileles	</option>
                                                <option>	Kec. Gunung kencana	</option>
                                                <option>	Kec. Bojongmanik	</option>
                                                <option>	Kec. Leuwidamar	</option>
                                                <option>	Kec. Muncang	</option>
                                                <option>	Kec. Cipanas	</option>
                                                <option>	Kec. Sajira	</option>
                                                <option>	Kec. Cimarga	</option>
                                                <option>	Kec. Cikulur	</option>
                                                <option>	Kec. Warung gunung	</option>
                                                <option>	Kec. Cibadak	</option>
                                                <option>	Kec. Rangkasbitung	</option>
                                                <option>	Kec. Maja	</option>
                                                <option>	Kec. Curug bitung	</option>
                                                <option>	Kec. Sobang	</option>
                                                <option>	Kec. Wanasalam	</option>
                                                <option>	Kec. Cilograng	</option>
                                                <option>	Kec. Cihara	</option>
                                                <option>	Kec. Cigemblong	</option>
                                                <option>	Kec. Cirinten	</option>
                                                <option>	Kec. Lebakgedong	</option>
                                                <option>	Kec. Karanganyar	</option>
                                                <option>	Kec. Cisoka	</option>
                                                <option>	Kec. Tiga raksa	</option>
                                                <option>	Kec. Cikupa	</option>
                                                <option>	Kec. Panongan	</option>
                                                <option>	Kec. Curug	</option>
                                                <option>	Kec. Legok	</option>
                                                <option>	Kec. Pagedangan	</option>
                                                <option>	Kec. Pasar Kemis	</option>
                                                <option>	Kec. Balaraja	</option>
                                                <option>	Kec. Kresek	</option>
                                                <option>	Kec. Kronjo	</option>
                                                <option>	Kec. Mauk	</option>
                                                <option>	Kec. Rajeg	</option>
                                                <option>	Kec. Sepatan	</option>
                                                <option>	Kec. Pakuhaji	</option>
                                                <option>	Kec. Teluk naga	</option>
                                                <option>	Kec. Kosambi	</option>
                                                <option>	Kec. Jayanti	</option>
                                                <option>	Kec. Jambe	</option>
                                                <option>	Kec. Cisauk	</option>
                                                <option>	Kec. Kemeri	</option>
                                                <option>	Kec. Sukadiri	</option>
                                                <option>	Kec. Sukamulya	</option>
                                                <option>	Kec. Kelapa Dua	</option>
                                                <option>	Kec. Sindang Jaya	</option>
                                                <option>	Kec. Sepatan Timur	</option>
                                                <option>	Kec. Solear	</option>
                                                <option>	Kec. Gunung Kaler	</option>
                                                <option>	Kec. Mekar Baru	</option>
                                                <option>	Kec. Cinangka	</option>
                                                <option>	Kec. Padarincang	</option>
                                                <option>	Kec. Ciomas	</option>
                                                <option>	Kec. Pabuaran	</option>
                                                <option>	Kec. Baros	</option>
                                                <option>	Kec. Petir	</option>
                                                <option>	Kec. Cikeusal	</option>
                                                <option>	Kec. Pamarayan	</option>
                                                <option>	Kec. Jawilan	</option>
                                                <option>	Kec. Kopo	</option>
                                                <option>	Kec. Cikande	</option>
                                                <option>	Kec. Kragilan	</option>
                                                <option>	Kec. Serang	</option>
                                                <option>	Kec. Waringinkurung	</option>
                                                <option>	Kec. Mancak	</option>
                                                <option>	Kec. Anyar	</option>
                                                <option>	Kec. Bojonegara	</option>
                                                <option>	Kec. Kramatwatu	</option>
                                                <option>	Kec. Ciruas	</option>
                                                <option>	Kec. Pontang	</option>
                                                <option>	Kec. Carenang	</option>
                                                <option>	Kec. Tirtayasa	</option>
                                                <option>	Kec. Tunjung Teja	</option>
                                                <option>	Kec. Kibin	</option>
                                                <option>	Kec. Pulo Ampel	</option>
                                                <option>	Kec. Binuang	</option>
                                                <option>	Kec. Tanara	</option>
                                                <option>	Kec. Gunung Sari	</option>
                                                <option>	Kec. Bandung	</option>
                                                <option>	Kec. Ciwandan	</option>
                                                <option>	Kec. Pulomerak	</option>
                                                <option>	Kec. Cilegon	</option>
                                                <option>	Kec. Cibeber	</option>
                                                <option>	Kec. Gerogol	</option>
                                                <option>	Kec. Purwakarta	</option>
                                                <option>	Kec. Jombang	</option>
                                                <option>	Kec. Citangkil	</option>
                                                <option>	Kec. Ciledug	</option>
                                                <option>	Kec. Cipondoh	</option>
                                                <option>	Kec. Tangerang	</option>
                                                <option>	Kec. Jati uwung	</option>
                                                <option>	Kec. Batuceper	</option>
                                                <option>	Kec. Benda	</option>
                                                <option>	Kec. Larangan	</option>
                                                <option>	Kec. Karang Tengah	</option>
                                                <option>	Kec. Pinang	</option>
                                                <option>	Kec. Karawaci	</option>
                                                <option>	Kec. Cibodas	</option>
                                                <option>	Kec. Periuk	</option>
                                                <option>	Kec. Neglasari	</option>
                                                <option>	Kec. Cipocok Jaya	</option>
                                                <option>	Kec. Curug	</option>
                                                <option>	Kec. Kasemen	</option>
                                                <option>	Kec. Taktakan	</option>
                                                <option>	Kec. Walantaka	</option>
                                                <option>	Serang	</option>
                                                <option>	Kec. Ciputat	</option>
                                                <option>	Kec. Ciputat Timur	</option>
                                                <option>	Kec. Pamulang	</option>
                                                <option>	Kec. Pondok Aren	</option>
                                                <option>	Kec. Serpong	</option>
                                                <option>	Kec. Serpong Utara	</option>
                                                <option>	Kec. Setu	</option>
                                                <option>	Kec. Mendo Barat	</option>
                                                <option>	Kec. Merawang	</option>
                                                <option>	Kec. Sungai Liat	</option>
                                                <option>	Kec. Belinyu	</option>
                                                <option>	Kec. Puding Besar	</option>
                                                <option>	Kec. Bakam	</option>
                                                <option>	Kec. Pemali	</option>
                                                <option>	Kec. Riau Silip	</option>
                                                <option>	Kec. Membalong	</option>
                                                <option>	Kec. Tanjung Pandan	</option>
                                                <option>	Kec. Sijuk	</option>
                                                <option>	Kec. Badau	</option>
                                                <option>	Kec. Selat Nasik	</option>
                                                <option>	Kec. Air Gegas	</option>
                                                <option>	Kec. Koba	</option>
                                                <option>	Kec. Pangkalan Baru	</option>
                                                <option>	Kec. Sungai Selan	</option>
                                                <option>	Kec. Simpang Katis	</option>
                                                <option>	Kec. Lubuk Besar	</option>
                                                <option>	Kec. Namang	</option>
                                                <option>	Kec. Kelapa	</option>
                                                <option>	Kec. Tempilang	</option>
                                                <option>	Kec. Mentok	</option>
                                                <option>	Kec. Simpang Teritip	</option>
                                                <option>	Kec. Jebus	</option>
                                                <option>	Kec. Parittiga	</option>
                                                <option>	Kec. Payung	</option>
                                                <option>	Kec. Simpang Rimba	</option>
                                                <option>	Kec. Toboali	</option>
                                                <option>	Kec. Lepar Pongok	</option>
                                                <option>	Kec. Pulau Besar	</option>
                                                <option>	Kec. Tukak Sadai	</option>
                                                <option>	Air Gegas	</option>
                                                <option>	Kec. Dendang	</option>
                                                <option>	Kec. Gantung	</option>
                                                <option>	Kec. Manggar	</option>
                                                <option>	Kec. Kelapa Kampit	</option>
                                                <option>	Kec. Damar	</option>
                                                <option>	Kec. Simpang Renggiang	</option>
                                                <option>	Kec. Simpang Pesak	</option>
                                                <option>	Kec. Rangkui	</option>
                                                <option>	Kec. Bukit Intan	</option>
                                                <option>	Kec. Pangkal Balam	</option>
                                                <option>	Kec. Taman Sari	</option>
                                                <option>	Kec. Gerunggang	</option>
                                                <option>	Girimaya	</option>
                                                <option>	Kec. Tilamuta	</option>
                                                <option>	Kec. Paguyaman	</option>
                                                <option>	Kec. Mananggu	</option>
                                                <option>	Kec. Dulupi	</option>
                                                <option>	Kec. Wonosari	</option>
                                                <option>	Kec. Botumoita	</option>
                                                <option>	Kec. Paguyaman Pantai	</option>
                                                <option>	Kec. Batudaa Pantai	</option>
                                                <option>	Kec. Batudaa	</option>
                                                <option>	Kec. Tibawa	</option>
                                                <option>	Kec. Boliyohuto	</option>
                                                <option>	Kec. Limboto	</option>
                                                <option>	Kec. Telaga	</option>
                                                <option>	Kec. Bongomeme	</option>
                                                <option>	Kec. Pulubala	</option>
                                                <option>	Kec. Tolangohula	</option>
                                                <option>	Kec. Mootilango	</option>
                                                <option>	Kec. Telaga Biru	</option>
                                                <option>	Kec. Limboto Barat	</option>
                                                <option>	Kec. Biluhu	</option>
                                                <option>	Kec. Tabongo	</option>
                                                <option>	Kec. Asparaga	</option>
                                                <option>	Kec. Tilango	</option>
                                                <option>	Kec. Telaga Jaya	</option>
                                                <option>	Kec. Bilato	</option>
                                                <option>	Bilato	</option>
                                                <option>	Kec. Popayato	</option>
                                                <option>	Kec. Marisa	</option>
                                                <option>	Kec. Paguat	</option>
                                                <option>	Kec. Lemito	</option>
                                                <option>	Kec. Randangan	</option>
                                                <option>	Kec. Patilanggio	</option>
                                                <option>	Kec. Taluditi	</option>
                                                <option>	Kec. Popayato Barat	</option>
                                                <option>	Kec. Popayato Timur	</option>
                                                <option>	Kec. Wanggarasi	</option>
                                                <option>	Kec. Buntulia	</option>
                                                <option>	Kec. Duhiadaa	</option>
                                                <option>	Kec. Dengilo	</option>
                                                <option>	Kec. Tapa	</option>
                                                <option>	Kec. Kabila	</option>
                                                <option>	Kec. Suwawa	</option>
                                                <option>	Kec. Bonepantai	</option>
                                                <option>	Kec. Bulango Utara	</option>
                                                <option>	Kec. Tilongkabila	</option>
                                                <option>	Kec. Botupingge	</option>
                                                <option>	Kec. Kabila Bone	</option>
                                                <option>	Kec. Bone	</option>
                                                <option>	Kec. Bone Raya	</option>
                                                <option>	Kec. Bulango Selatan	</option>
                                                <option>	Kec. Bulango Timur	</option>
                                                <option>	Kec. Bulango Ulu	</option>
                                                <option>	Kec. Suwawa Selatan	</option>
                                                <option>	Kec. Suwawa Timur	</option>
                                                <option>	Kec. Suwawa Tengah	</option>
                                                <option>	Kec. Bulawa	</option>
                                                <option>	Kec. Anggrek	</option>
                                                <option>	Kec. Atinggola	</option>
                                                <option>	Kec. Kwandang	</option>
                                                <option>	Kec. Sumalata	</option>
                                                <option>	Kec. Tolinggula	</option>
                                                <option>	Kec. Gentuma Raya	</option>
                                                <option>	Tomilito	</option>
                                                <option>	Ponelo Kepulauan	</option>
                                                <option>	Monano	</option>
                                                <option>	Biau	</option>
                                                <option>	Kec. Kota Barat	</option>
                                                <option>	Kec. Kota Selatan	</option>
                                                <option>	Kec. Kota Utara	</option>
                                                <option>	Kec. Kota Timur	</option>
                                                <option>	Kec. Dungingi	</option>
                                                <option>	Kec. Kota Tengah	</option>
                                                <option>	Kec. Sipatana	</option>
                                                <option>	Kec. Dumbo Raya	</option>
                                                <option>	Kec. Hulonthalangi	</option>
                                                <option>	Kec. Bintan Utara	</option>
                                                <option>	Kec. Gunung Kijang	</option>
                                                <option>	Kec. Tambelan	</option>
                                                <option>	Kec. Teluk Bintan	</option>
                                                <option>	Kec. Teluk Sebong	</option>
                                                <option>	Kec. Toapaya	</option>
                                                <option>	Kec. Mantang	</option>
                                                <option>	Kec. Bintan Pesisir	</option>
                                                <option>	Kec. Seri Kuala Loban	</option>
                                                <option>	Kec. Bintan Timur	</option>
                                                <option>	Kec. Moro	</option>
                                                <option>	Kec. Kundur	</option>
                                                <option>	Kec. Karimun	</option>
                                                <option>	Kec. Meral	</option>
                                                <option>	Kec. Tebing	</option>
                                                <option>	Kec. Buru	</option>
                                                <option>	Kec. Kundur Utara	</option>
                                                <option>	Kec. Kundur Barat	</option>
                                                <option>	Kec. Durai	</option>
                                                <option>	Kec. Midai	</option>
                                                <option>	Kec. Bunguran Barat	</option>
                                                <option>	Kec. Bunguran Timur	</option>
                                                <option>	Kec. Serasan	</option>
                                                <option>	Kec. Subi	</option>
                                                <option>	Kec. Bunguran Utara	</option>
                                                <option>	Kec. Pulau Laut	</option>
                                                <option>	Kec. Pulau Tiga	</option>
                                                <option>	Kec. Bunguran Timur Laut	</option>
                                                <option>	Kec. Bunguran Tengah	</option>
                                                <option>	Kec. Bunguran Selatan	</option>
                                                <option>	Kec. Serasan Timur	</option>
                                                <option>	Kec. Singkep	</option>
                                                <option>	Kec. Lingga	</option>
                                                <option>	Kec. Senayang	</option>
                                                <option>	Kec. Singkep Barat	</option>
                                                <option>	Kec. Lingga Utara	</option>
                                                <option>	Kec. Jemaja	</option>
                                                <option>	Kec. Jemaja Timur	</option>
                                                <option>	Kec. Siantan	</option>
                                                <option>	Kec. Palmatak	</option>
                                                <option>	Kec. Siantan Selatan	</option>
                                                <option>	Kec. Siantan Timur	</option>
                                                <option>	Kec. Siantan Tengah	</option>
                                                <option>	Kec. Belakang Padang	</option>
                                                <option>	Kec. Sekupang	</option>
                                                <option>	Kec. Sei Beduk	</option>
                                                <option>	Kec. Bulang	</option>
                                                <option>	Kec. Lubuk Baja	</option>
                                                <option>	Kec. Batu Ampar	</option>
                                                <option>	Kec. Nongsa	</option>
                                                <option>	Kec. Galang	</option>
                                                <option>	Kec. Bengkong	</option>
                                                <option>	Kec. Batam Kota	</option>
                                                <option>	Kec. Sagulung	</option>
                                                <option>	Kec. Batu Aji	</option>
                                                <option>	Kec. Tanjung Pinang Barat	</option>
                                                <option>	Kec. Tanjung Pinang Timur	</option>
                                                <option>	Kec. Bukit Bestari	</option>
                                                <option>	Kec. Tanjung Pinang Kota	</option>
                                                <option>	Kec. Fak-Fak Timur	</option>
                                                <option>	Kec. Karas	</option>
                                                <option>	Kec. Fak-Fak	</option>
                                                <option>	Kec. Fak-Fak Tengah	</option>
                                                <option>	Kec. Fak-Fak Barat	</option>
                                                <option>	Kec. Kokas	</option>
                                                <option>	Kec. Teluk Patipi	</option>
                                                <option>	Kec. Kramongmongga	</option>
                                                <option>	Kec. Bomberay	</option>
                                                <option>	Kec. Buruway	</option>
                                                <option>	Kec. Teluk Arguni (Atas)	</option>
                                                <option>	Kec. Teluk Arguni Bawah	</option>
                                                <option>	Kec. Kaimana	</option>
                                                <option>	Kec. Kambrau	</option>
                                                <option>	Kec. Teluk Etna	</option>
                                                <option>	Kec. Yamor	</option>
                                                <option>	Kec. Naikere	</option>
                                                <option>	Kec. Wondiboy	</option>
                                                <option>	Kec. Rasiei	</option>
                                                <option>	Kec. Kuri Wamesa	</option>
                                                <option>	Kec. Wasior	</option>
                                                <option>	Kec. Teluk Duairi	</option>
                                                <option>	Kec. Roon	</option>
                                                <option>	Kec. Windesi	</option>
                                                <option>	Kec. Wamesa	</option>
                                                <option>	Kec. Roswar	</option>
                                                <option>	Kec. Rumberpon	</option>
                                                <option>	Kec. Soug Jaya	</option>
                                                <option>	Kec. Nikiwar	</option>
                                                <option>	Sough Wepu	</option>
                                                <option>	Kec. Irorutu/Fafuwar	</option>
                                                <option>	Kec. Babo	</option>
                                                <option>	Kec. Sumuri	</option>
                                                <option>	Kec. Aroba	</option>
                                                <option>	Kec. Kaitaro	</option>
                                                <option>	Kec. Kuri	</option>
                                                <option>	Kec. Idoor	</option>
                                                <option>	Kec. Bintuni	</option>
                                                <option>	Kec. Manimeri	</option>
                                                <option>	Kec. Tuhiba	</option>
                                                <option>	Kec. Dataran Beimes	</option>
                                                <option>	Kec. Tembuni	</option>
                                                <option>	Kec. Aranday	</option>
                                                <option>	Kec. Komundan	</option>
                                                <option>	Kec. Tomu	</option>
                                                <option>	Kec. Weriagar	</option>
                                                <option>	Kec. Moskona Selatan	</option>
                                                <option>	Kec. Meyado	</option>
                                                <option>	Kec. Moskona Barat	</option>
                                                <option>	Kec. Merdey	</option>
                                                <option>	Kec. Biscoop	</option>
                                                <option>	Kec. Masyeta	</option>
                                                <option>	Kec. Moskona Utara	</option>
                                                <option>	Kec. Moskona Timur	</option>
                                                <option>	Wamesa	</option>
                                                <option>	Kec. Ransiki	</option>
                                                <option>	Kec. Momi Waren	</option>
                                                <option>	Kec. Nenei	</option>
                                                <option>	Kec. Sururay	</option>
                                                <option>	Kec. Anggi	</option>
                                                <option>	Kec. Taige	</option>
                                                <option>	Kec. Membey	</option>
                                                <option>	Kec. Oransbari	</option>
                                                <option>	Kec. Warmare	</option>
                                                <option>	Kec. Prafi	</option>
                                                <option>	Kec. Menyambouw	</option>
                                                <option>	Kec. Catubouw	</option>
                                                <option>	Kec. Manokwari Barat	</option>
                                                <option>	Kec. Manokwari Timur	</option>
                                                <option>	Kec. Manokwari Utara	</option>
                                                <option>	Kec. Manokwari Selatan	</option>
                                                <option>	Kec. Testega	</option>
                                                <option>	Kec. Tanah Rubu	</option>
                                                <option>	Kec. Kebar	</option>
                                                <option>	Kec. Senopi	</option>
                                                <option>	Kec. Amberbaken	</option>
                                                <option>	Kec. Murbani/Arfu	</option>
                                                <option>	Kec. Masni	</option>
                                                <option>	Kec. Sidey	</option>
                                                <option>	Kec. Tahosta	</option>
                                                <option>	Kec. Didohu	</option>
                                                <option>	Kec. Dataran Isim	</option>
                                                <option>	Kec. Anggi Gida	</option>
                                                <option>	Kec. Hingk	</option>
                                                <option>	Neney	</option>
                                                <option>	Momi - Waren	</option>
                                                <option>	Tohota	</option>
                                                <option>	Taige	</option>
                                                <option>	Membey	</option>
                                                <option>	Anggi Gida	</option>
                                                <option>	Didohu	</option>
                                                <option>	Dataran Isim	</option>
                                                <option>	Catubouw	</option>
                                                <option>	Hink	</option>
                                                <option>	Ransiki	</option>
                                                <option>	Kec. Inanwatan	</option>
                                                <option>	Kec. Kokoda	</option>
                                                <option>	Kec. Metemeini Kais	</option>
                                                <option>	Kec. Moswaren	</option>
                                                <option>	Kec. Seremuk	</option>
                                                <option>	Kec. Wayer	</option>
                                                <option>	Kec. Sawiat	</option>
                                                <option>	Kec. Kais	</option>
                                                <option>	Kec. Konda	</option>
                                                <option>	Kec. Kokoda Utara	</option>
                                                <option>	Kec. Saifi	</option>
                                                <option>	Kec. Fokour	</option>
                                                <option>	Kec. Teminabuan	</option>
                                                <option>	Kec. Moraid	</option>
                                                <option>	Kec. Makbon	</option>
                                                <option>	Kec. Beraur	</option>
                                                <option>	Kec. Klamono	</option>
                                                <option>	Kec. Salawati	</option>
                                                <option>	Kec. Manyamuk	</option>
                                                <option>	Kec. Seget	</option>
                                                <option>	Kec. Segun	</option>
                                                <option>	Kec. Salawati Selatan	</option>
                                                <option>	Kec. Aimas	</option>
                                                <option>	Kec. Sayosa	</option>
                                                <option>	Kec. Klabot	</option>
                                                <option>	Kec. Klawak	</option>
                                                <option>	Kec. Maudus	</option>
                                                <option>	Kec. Mariat	</option>
                                                <option>	Kec. Klayili	</option>
                                                <option>	Kec. Klaso	</option>
                                                <option>	Kec. Moisegen	</option>
                                                <option>	Kec. Misool Selatan	</option>
                                                <option>	Kec. Misool (Misool Utara)	</option>
                                                <option>	Kec. Kofiau	</option>
                                                <option>	Kec. Misool Timur	</option>
                                                <option>	Kec. Salawati Utara	</option>
                                                <option>	Kec. Waigeo Selatan	</option>
                                                <option>	Kec. Teluk Mayalibit	</option>
                                                <option>	Kec. Meos Mansar	</option>
                                                <option>	Kec. Waigeo Barat	</option>
                                                <option>	Kec. Waigeo Utara	</option>
                                                <option>	Kec. Kepulauan Ayau	</option>
                                                <option>	Kec. Waigeo Timur	</option>
                                                <option>	Kec. Warwarbomi	</option>
                                                <option>	Kec. Waigeo Barat Kepulauan	</option>
                                                <option>	Kec. Misool Barat	</option>
                                                <option>	Kec. Kepulauan Sembilan	</option>
                                                <option>	Kec. Kota Waisai	</option>
                                                <option>	Kec. Tiplol Mayalibit	</option>
                                                <option>	Kec. Batanta Utara	</option>
                                                <option>	Kec. Salawati Barat	</option>
                                                <option>	Kec. Salawati Tengah	</option>
                                                <option>	Kec. Supnin	</option>
                                                <option>	Kec. Ayau	</option>
                                                <option>	Kec. Batanta Selatan	</option>
                                                <option>	Selat Sagawin	</option>
                                                <option>	Kec. Fef	</option>
                                                <option>	Kec. Miyah	</option>
                                                <option>	Kec. Yembun	</option>
                                                <option>	Kec. Kwoor	</option>
                                                <option>	Kec. Sausapor	</option>
                                                <option>	Kec. Abun	</option>
                                                <option>	Kec. Aifat	</option>
                                                <option>	Kec. Aifat Utara	</option>
                                                <option>	Kec. Aifat Timur	</option>
                                                <option>	Kec. Aifat Selatan	</option>
                                                <option>	Kec. Aitinyo Barat	</option>
                                                <option>	Kec. Aitinyo	</option>
                                                <option>	Kec. Aitinyo Utara	</option>
                                                <option>	Kec. Ayamaru	</option>
                                                <option>	Kec. Ayamaru Utara	</option>
                                                <option>	Kec. Ayamaru Timur	</option>
                                                <option>	Kec. Mare	</option>
                                                <option>	Aitinyo Tengah	</option>
                                                <option>	Aifat Timur Selatan	</option>
                                                <option>	Aitinyo Raya	</option>
                                                <option>	Ayamaru Timur Selatan	</option>
                                                <option>	Aifat Timur Tengah	</option>
                                                <option>	Aifat Timur Jauh	</option>
                                                <option>	Ayamaru Jaya	</option>
                                                <option>	Kec. Sorong Timur	</option>
                                                <option>	Kec. Sorong	</option>
                                                <option>	Kec. Klablim	</option>
                                                <option>	Kec. Klawalu	</option>
                                                <option>	Kec. Giwu	</option>
                                                <option>	Kec. Klamana	</option>
                                                <option>	Kec. Tapalang	</option>
                                                <option>	Kec. Mamuju	</option>
                                                <option>	Kec. Kalukku	</option>
                                                <option>	Kec. Kalumpang	</option>
                                                <option>	Kec. Budong-Budong	</option>
                                                <option>	Kec. Tapalang Barat	</option>
                                                <option>	Kec. Papalang	</option>
                                                <option>	Kec. Sampaga	</option>
                                                <option>	Kec. Pangale	</option>
                                                <option>	Kec. Tommo	</option>
                                                <option>	Kec. Bonehau	</option>
                                                <option>	Kec. Topoyo	</option>
                                                <option>	Kec. Tobadak	</option>
                                                <option>	Kec. Karossa	</option>
                                                <option>	Kec. Simboro Kepulauan	</option>
                                                <option>	Kec. Kep. Bala Balakang	</option>
                                                <option>	Topoyo	</option>
                                                <option>	Budong-budong	</option>
                                                <option>	Pangale	</option>
                                                <option>	Kec. Pasangkayu	</option>
                                                <option>	Kec. Sarudu	</option>
                                                <option>	Kec. Baras	</option>
                                                <option>	Kec. Bambalamutu	</option>
                                                <option>	Kec. Dapurang	</option>
                                                <option>	Kec. Duripoku	</option>
                                                <option>	Kec. Bulu Taba	</option>
                                                <option>	Kec. Lariang	</option>
                                                <option>	Kec. Tikke Raya	</option>
                                                <option>	Kec. Pedongga	</option>
                                                <option>	Kec. Bambaira	</option>
                                                <option>	Kec. Sarjo	</option>
                                                <option>	Karossa	</option>
                                                <option>	Simboro Dan Kepulauan	</option>
                                                <option>	Tobadak	</option>
                                                <option>	Kec. Tinambung	</option>
                                                <option>	Kec. Tutallu	</option>
                                                <option>	Kec. Campalagian	</option>
                                                <option>	Kec. Wonomulyo	</option>
                                                <option>	Kec. Polewali	</option>
                                                <option>	Kec. Limboro	</option>
                                                <option>	Kec. Balanipa	</option>
                                                <option>	Kec. Luyo	</option>
                                                <option>	Kec. Allu	</option>
                                                <option>	Kec. Mapili	</option>
                                                <option>	Kec. Matakali	</option>
                                                <option>	Kec. Binuang	</option>
                                                <option>	Kec. Anreapi	</option>
                                                <option>	Kec. Tapango	</option>
                                                <option>	Kec. Matangnga	</option>
                                                <option>	Kec. Bulo	</option>
                                                <option>	Tutar	</option>
                                                <option>	Kec. Sumarorong	</option>
                                                <option>	Kec. Pana	</option>
                                                <option>	Kec. Mamasa	</option>
                                                <option>	Kec. Mambi	</option>
                                                <option>	Kec. Tabulahan	</option>
                                                <option>	Kec. Tabang	</option>
                                                <option>	Kec. Messawa	</option>
                                                <option>	Kec. Sesenapadang	</option>
                                                <option>	Kec. Tandukalua	</option>
                                                <option>	Kec. Aralle	</option>
                                                <option>	Kec. Nosu	</option>
                                                <option>	Kec. Bambang	</option>
                                                <option>	Kec. Balla	</option>
                                                <option>	Kec. Tawalian	</option>
                                                <option>	Kec. Rantebulahan Timur	</option>
                                                <option>	Kec. Buntumalangka	</option>
                                                <option>	Kec. Mehalaan	</option>
                                                <option>	Kec. Pamboang	</option>
                                                <option>	Kec. Ulumunda	</option>
                                                <option>	Kec. Tammerodo Sendana	</option>
                                                <option>	Kec. Tubo Sendana	</option>
                                                <option>	Kec. Banggai	</option>
                                                <option>	Kec. Sendana	</option>
                                                <option>	Kec. Malunda	</option>
                                                <option>	Kec. Banggai Timur	</option>
                                                <option>	Kec. Cigombong	</option>

                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="mb-3">
                                        <label for="kelurahan" class="form-label">Kelurahan</label>
                                            <select id="kelurahan" class="form-select">
                                                <option disabled selected>Pilih Kelurahan</option>
                                            </select>
                                        </div>
                                    </div>
                                </div> <!-- row -->
                                <div class="mb-3">
                                    <label for="alamat-lengkap" class="form-label">Alamat lengkap</label>
                                    <textarea class="form-control" id="alamat-lengkap" rows="5" name="state" placeholder="Jl.wangun jaya"></textarea>
                                </div>
                                <hr>
                                {{-- <div class="form-check">
                                    <input type="checkbox" id="shipping" class="form-check-input">
                                    <label for="shipping" class="form-check-label">COD <span class="text-muted">(Cash on delivery)</span></label>
                                </div> --}}
                                </form>
                                <div class="form-check" data-bs-toggle="collapse" href="#collapseExample" role="button" aria-expanded="true" aria-controls="collapseExample">
                                    <input type="checkbox" id="saveti" class="form-check-input">
                                    <label for="saveti" class="form-check-label">Transfer Bank</label>
                                </div>
                                <hr>
                                <div class="collapse" id="collapseExample">
                                    <div class="card card-body card-stretch mb-6">
                                    <h4>Payment</h4>

                                <div class="form-check">
                                    <input type="radio" id="cc" class="form-check-input">
                                    <label for="cc" class="form-check-label">Credit Card</label>
                                </div>

                                <div class="form-check">
                                    <input type="radio" id="dc" class="form-check-input">
                                    <label for="dc" class="form-check-label">Debit Card</label>
                                </div>

                                <div class="form-check">
                                    <input type="radio" id="paypal" class="form-check-input">
                                    <label for="paypal" class="form-check-label">Paypal</label>
                                </div>

                                <div class="row mt-3">
                                    <div class="col-md-6">
                                        <div class="mb-3">
                                            <label for="card-name" class="form-label">Name on card</label>
                                            <input type="text" id="card-name" class="form-control">
                                            <small class="text-muted">
                                                Full name as dislayed on card
                                            </small>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="mb-3">
                                            <label for="card-number" class="form-label">Credit card number</label>
                                            <input type="text" id="card-number" class="form-control">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="mb-3">
                                            <label for="expirationr" class="form-label">Expiration</label>
                                            <input type="text" id="expiration" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="mb-3">
                                            <label for="cvv" class="form-label">CVV</label>
                                            <input type="text" id="cvv" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-5">
                            <h4 class="d-flex justify-content-between">Your Cart<span class="badge bg-primary rounded-circle">3</span></h4>
                            <hr>
                            <ul class="list-group">
                                <li class="list-group-item d-flex align-items-center justify-content-between">
                                    <div class="symbol symbol-50 symbol-3 flex-shrink-0 mr-4">
                                        <div class="d-flex flex-column">
                                            <div class="symbol-label" style="background-image: url('assets/media/products/27.jpg')"></div>
                                        </div>
                                    </div>
                                    <div class="d-flex flex-column flex-grow-1 my-lg-0 my-2 pr-3">
                                        <a class="text-muted-75 font-weight-bolder">Darius the Great</a>
                                        <small class="text-muted font-weight-bolder">all</small>
                                    </div>
                                    <div class="text-muted">
                                        Rp 10.000
                                    </div>
                                </li>
                                <li class="list-group-item d-flex align-items-center justify-content-between">
                                    <div class="symbol symbol-50 symbol-3 flex-shrink-0 mr-4">
                                        <div class="d-flex flex-column">
                                            <div class="symbol-label" style="background-image: url('assets/media/products/27.jpg')"></div>
                                        </div>
                                    </div>
                                    <div class="d-flex flex-column flex-grow-1 my-lg-0 my-2 pr-3">
                                        <a class="text-muted-75 font-weight-bolder">Darius the Great</a>
                                        <small class="text-muted font-weight-bolder">all</small>
                                    </div>
                                    <div class="text-muted">
                                        Rp 10.000
                                    </div>
                                </li>
                                <li class="list-group-item d-flex align-items-center justify-content-between">
                                    <div class="symbol symbol-50 symbol-3 flex-shrink-0 mr-4">
                                        <div class="d-flex flex-column">
                                            <div class="symbol-label" style="background-image: url('assets/media/products/27.jpg')"></div>
                                        </div>
                                    </div>
                                    <div class="d-flex flex-column flex-grow-1 my-lg-0 my-2 pr-3">
                                        <a class="text-muted-75 font-weight-bolder">Darius the Great</a>
                                        <small class="text-muted font-weight-bolder">all</small>
                                    </div>
                                    <div class="text-muted">
                                        Rp 10.000
                                    </div>
                                </li>
                                {{-- <li class="list-group-item d-flex align-items-center justify-content-between">
                                    <div>
                                         <h6 class="my-0">Product Name</h6>
                                         <small class="text-muted">Product description</small>
                                    </div>

                                    <div class="text-muted">
                                        20.000
                                    </div>
                                </li>
                                <li class="list-group-item d-flex align-items-center justify-content-between">
                                    <div>
                                         <h6 class="my-0">Product Name</h6>
                                         <small class="text-muted">Product description</small>
                                    </div>

                                    <div class="text-muted">
                                        35.000
                                    </div>
                                </li> --}}
                                <li class="list-group-item d-flex align-items-center justify-content-between">
                                    <div>
                                         <h6 class="text-muted-75 font-weight-bolder my-0">Total Harga</h6>
                                    </div>

                                    <div class="text-muted-75 font-weight-bolder">
                                     <strong>Rp 35.000</strong>
                                    </div>
                                </li>
                            </ul>
                            <button type="submit" class="mt-1 btn btn-primary btn-block">checkout</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <div class="footer bg-white py-4 d-flex flex-lg-column mt-3 bg-footer" id="kt_footer">
        <div class="kt-container kt-container--fluid">
          <div class="kt-footer__copyright">
            <footer class="footer text-center">2022 © HEALTHYSHOP</footer>
          </div>
        </div>
    </div>
                <script>var HOST_URL = "https://preview.keenthemes.com/metronic/theme/html/tools/preview";</script>
                <!--begin::Global Config(global config for global JS scripts)-->
                <script>var KTAppSettings = { "breakpoints": { "sm": 576, "md": 768, "lg": 992, "xl": 1200, "xxl": 1400 }, "colors": { "theme": { "base": { "white": "#ffffff", "primary": "#3699FF", "secondary": "#E5EAEE", "success": "#1BC5BD", "info": "#8950FC", "warning": "#FFA800", "danger": "#F64E60", "light": "#E4E6EF", "dark": "#181C32" }, "light": { "white": "#ffffff", "primary": "#E1F0FF", "secondary": "#EBEDF3", "success": "#C9F7F5", "info": "#EEE5FF", "warning": "#FFF4DE", "danger": "#FFE2E5", "light": "#F3F6F9", "dark": "#D6D6E0" }, "inverse": { "white": "#ffffff", "primary": "#ffffff", "secondary": "#3F4254", "success": "#ffffff", "info": "#ffffff", "warning": "#ffffff", "danger": "#ffffff", "light": "#464E5F", "dark": "#ffffff" } }, "gray": { "gray-100": "#F3F6F9", "gray-200": "#EBEDF3", "gray-300": "#E4E6EF", "gray-400": "#D1D3E0", "gray-500": "#B5B5C3", "gray-600": "#7E8299", "gray-700": "#5E6278", "gray-800": "#3F4254", "gray-900": "#181C32" } }, "font-family": "Poppins" };</script>
                <!--end::Global Config-->
                <!--begin::Global Theme Bundle(used by all pages)-->
                <script src="assets/plugins/global/plugins.bundle.js"></script>
                <script src="assets/plugins/custom/prismjs/prismjs.bundle.js"></script>
                <script src="assets/js/scripts.bundle.js"></script>
                <!--end::Global Theme Bundle-->
                <!--begin::Page Scripts(used by this page)-->
                <script src="assets/js/pages/custom/ecommerce/checkout.js"></script>
            <script src="{{ asset('assets/plugins/bootstrap/dist/js/bootstrap.bundle.min.js') }}"></script>
        </body>

        </html>
