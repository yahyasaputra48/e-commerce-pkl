<nav class="navbar navbar-expand-lg bg-light fixed-top py-4 shadow-sm">
    <div class="container">
      <a class="navbar-brand text-start" href="{{ Route('user.index') }}"><h4>Healty<span>Shop</span></h4></a>
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarNav">
        <div class="input-group mx-auto" style="width: 700px;">
            <input type="text" class="form-control" placeholder="Cari" aria-label="Recipient's username" aria-describedby="button-addon2">
            <button class="btn btn-outline-primary" type="button" id="button-addon2">
                <i class='bx bx-search'></i>
            </button>
          </div>
          <li class="nav-item me-5">
            <a class="nav-link" href="{{ Route('cart') }}">
                <i class='bx bx-cart-alt'></i>
                <span class=" badge text-bg-primary rounded-circle position-absolute"></span>
            </a>
          </li>
          @guest
          <li class="nav-item">
            <a class="nav-link btn-second me-1" href="{{ Route('login') }}">Masuk</a>
          </li>
          @if (Route::has('register'))
          <li class="nav-item">
            <a class="nav-link btn-first" href="{{ Route('register') }}">Daftar</a>
          </li>
          @endif
          @else
          <li class="dropdown dropstart">
            <a href="#" class="d-block link-dark text-decoration-none dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false">
                <img src="{{asset('image/'.Auth::user()->image) }}" alt="mdo" width="32" height="32" class="rounded-circle">
            </a>
            <ul class="dropdown-menu">
              <li><a class="dropdown-item" href="{{ Route('user.edit', Auth::user()->id) }}">Akun Saya</a></li>
              <li><a class="dropdown-item" href="#">Pesanan Saya</a></li>
              <li><hr class="dropdown-divider" /></li>
              <form class="dropdown-item" id="logout-form" action="{{ Route('logout')}}" method="POST">
                @csrf
                <button type="submit" class="btn btn-sm btn-secondary" onclick="return confirm('Apakah Anda yakin?')">Sign Out</button>
              </form>
            </ul>
          </li>
          @endguest
      </div>
    </div>
  </nav>
