<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link href='https://unpkg.com/boxicons@2.1.2/css/boxicons.min.css' rel='stylesheet'>
    <link rel="stylesheet" href="{{ asset('assets/plugins/bootstrap/dist/css/bootstrap.min.css')}}" />
    <link rel="stylesheet" href="{{ asset('assets/style/main.css') }}" />
    <link rel="shortcut icon" href="{{ asset('assets/media/logos/faviconhealthy.png') }}" />
    <title>HOME | HEALTY SHOP</title>
</head>
<body>
    <nav class="navbar navbar-expand-lg bg-light fixed-top py-4 shadow-sm">
        <div class="container-fluid">
          <a class="navbar-brand" href="#">Healty<span>Shop</span></a>
          <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarNav">
            <div class="input-group mx-auto" style="width: 700px;">
                <input type="text" class="form-control" placeholder="Cari" aria-label="Recipient's username" aria-describedby="button-addon2">
                <button class="btn btn-outline-primary" type="button" id="button-addon2">
                    <i class='bx bx-search'></i>
                </button>
              </div>
              <ul class="navbar-nav me-3"></ul>
                <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle me-3" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Kategori
              </a>
              <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                <li><a class="dropdown-item" href="#">Makanan</a></li>
                <li><a class="dropdown-item" href="#">Minuman</a></li>
                <li><a class="dropdown-item" href="#">Fashion</a></li>
              </ul>
              <li class="nav-item me-5">
                <a class="nav-link" href="#">
                    <i class='bx bx-cart-alt'></i>
                    <span class=" badge text-bg-primary rounded-circle position-absolute">1</span>
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link btn-second me-1" href="login">Masuk</a>
              </li>
              <li class="nav-item">
                <a class="nav-link btn-first" href="#">Daftar</a>
              </li>
          </div>
        </div>
      </nav>

      <!-- POPULAR SECTION -->
      <div class="container menu-wrapper">
        <section class="popular">
            <div class="container">
              <div class="row align-items-center">
                <div class="col-6">
                  <h4>Popular Now</h4>
                </div>
                <div class="col-6 text-end">
                  <a href="#" class="btn-first">Lihat Semua...</a>
                </div>
              </div>
              {{-- <div class="row row-card mt-3 align-items-stretch"> --}}
              <div class="row row justify-content-starf mx-auto ">
                <div class="col-lg-3 col-6">
                <div class="card p-3 p-sm-3" style="width: 15rem;">
                    <img src="assets/media/products/24.png" class="img-fluid-m-auto" alt="...">
                    <div class="card-body mt-3 p-0">
                      <h6 class="card-title">Martabak Keju Kurma</h6>
                      <p class="ratting">
                      <i class="bx bxs-star text-warning"></i>
                      <i class="bx bxs-star text-warning"></i>
                      <i class="bx bxs-star text-warning"></i>
                      <i class="bx bxs-star text-warning"></i>
                      <i class="bx bxs-star-half text-warning"></i>
                      <p class="card-text">Rp 200.000</p>
                          <a href="#" class="btn btn-primary mt-2">Beli</a>
                          <a href="detail.html" class="btn btn-danger mt-2"><i class="bx bx-cart-alt"></i></a>
                    </div>
                  </div>
                </div>
                <div class="col-lg-3 col-6">
                <div class="card p-3 p-sm-3" style="width: 15rem;">
                    <img src="assets/media/products/23.jpeg" class="img-fluid-m-auto" alt="...">
                    <div class="card-body mt-3 p-0">
                      <h6 class="card-title">Martabak telor</h6>
                      <p class="ratting">
                      <i class="bx bxs-star text-warning"></i>
                      <i class="bx bxs-star text-warning"></i>
                      <i class="bx bxs-star text-warning"></i>
                      <i class="bx bxs-star text-warning"></i>
                      <i class="bx bxs-star-half text-warning"></i>
                      <p class="card-text">Rp 200.000</p>
                          <a href="#" class="btn btn-primary mt-2">Beli</a>
                          <a href="detail.html" class="btn btn-danger mt-2"><i class="bx bx-cart-alt"></i></a>
                    </div>
                  </div>
                </div>
                <div class="col-lg-3 col-6">
                <div class="card p-3 p-sm-3" style="width: 15rem;">
                    <img src="assets/media/products/25.jpeg" class="img-fluid-m-auto" alt="...">
                    <div class="card-body mt-3 p-0">
                      <h6 class="card-title">Nasi Goreng</h6>
                      <p class="ratting">
                      <i class="bx bxs-star text-warning"></i>
                      <i class="bx bxs-star text-warning"></i>
                      <i class="bx bxs-star text-warning"></i>
                      <i class="bx bxs-star text-warning"></i>
                      <i class="bx bxs-star-half text-warning"></i>
                      <p class="card-text">Rp 200.000</p>
                          <a href="#" class="btn btn-primary mt-2">Beli</a>
                          <a href="detail.html" class="btn btn-danger mt-2"><i class="bx bx-cart-alt"></i></a>
                    </div>
                  </div>
                </div>
                <div class="col-lg-3 col-6">
                <div class="card p-3 p-sm-3" style="width: 15rem;">
                    <img src="assets/media/products/26.jpeg" class="img-fluid-m-auto" alt="...">
                    <div class="card-body mt-3 p-0">
                      <h6 class="card-title">Pempek</h6>
                      <p class="ratting">
                      <i class="bx bxs-star text-warning"></i>
                      <i class="bx bxs-star text-warning"></i>
                      <i class="bx bxs-star text-warning"></i>
                      <i class="bx bxs-star text-warning"></i>
                      <i class="bx bxs-star-half text-warning"></i><br>
                      <p class="card-text">Rp 200.000</p>
                          <a href="#" class="btn btn-primary mt-2">Beli</a>
                          <a href="detail.html" class="btn btn-danger mt-2"><i class="bx bx-cart-alt"></i></a>
                    </div>
                  </div>
                </div>
                  </div>
                {{-- <div class="col-lg-3 col-6">
                  <div class="card p-3 p-sm-3">
                    <img src="assets/media/products/Martabak.png" class="img-fluid m-auto" alt="" />
                    <div class="card-body mt-4 p-0">
                      <h6 class="title">Martabak Keju kurma</h6>
                      <div class="rating">
                        <i class="bx bxs-star"></i>
                        <i class="bx bxs-star"></i>
                        <i class="bx bxs-star"></i>
                        <i class="bx bxs-star"></i>
                        <i class="bx bxs-star"></i>
                      </div>
                        <p class="m-0 price">Rp 200.000</p>
                        <div class="cart d-flex align-items-center mt-4 justify-content-between">
                          <a href="#" class="category badge text-bg-primary">Beli</a>
                          <a href="detail.html" class="btn-cart"><i class="bx bx-cart-alt"></i></a>
                      </div>
                    </div>
                  </div> --}}

                {{-- <div class="col-lg-3 col-6">
                    <div class="card p-3 p-sm-3">
                      <img src="assets/media/products/Martabak telor.jpeg" class="img-fluid m-auto" alt="" />
                      <div class="card-body mt-4 p-0">
                        <h6 class="title">Martabak Telor</h6>
                        <div class="rating">
                          <i class="bx bxs-star"></i>
                          <i class="bx bxs-star"></i>
                          <i class="bx bxs-star"></i>
                          <i class="bx bxs-star"></i>
                          <i class="bx bxs-star"></i>
                        </div>
                        <p class="m-0 price">Rp 240.000</p>
                        <div class="cart d-flex align-items-center mt-4 justify-content-between">
                           <a href="#" class="category badge text-bg-primary">Beli</a>
                           <a href="detail.html" class="btn-cart">
                            <i class="bx bx-cart-alt"></i></a>
                        </div>
                      </div>
                    </div>
                  </div>
                <div class="col-lg-3 col-6">
                    <div class="card p-3 p-sm-3">
                      <img src="assets/media/products/Nasi goreng.jpeg" class="img-fluid m-auto" alt="" />
                      <div class="card-body mt-4 p-0">
                        <h6 class="title">Nasi Goreng Palembang</h6>
                        <div class="rating">
                          <i class="bx bxs-star"></i>
                          <i class="bx bxs-star"></i>
                          <i class="bx bxs-star"></i>
                          <i class="bx bxs-star"></i>
                          <i class="bx bxs-star"></i>
                        </div>
                        <p class="m-0 price">Rp 240.000</p>
                        <div class="cart d-flex align-items-center mt-4 justify-content-between">
                           <a href="#" class="category badge text-bg-primary">Beli</a>
                           <a href="detail.html" class="btn-cart">
                            <i class="bx bx-cart-alt"></i></a>
                        </div>
                      </div>
                    </div>
                  </div>
                <div class="col-lg-3 col-6">
                    <div class="card p-3 p-sm-3">
                      <img src="assets/media/products/Pempek.jpeg" class="img-fluid m-auto" alt="" />
                      <div class="card-body mt-4 p-0">
                        <h6 class="title">Pempek</h6>
                        <div class="rating">
                          <i class="bx bxs-star"></i>
                          <i class="bx bxs-star"></i>
                          <i class="bx bxs-star"></i>
                          <i class="bx bxs-star"></i>
                          <i class="bx bxs-star"></i>
                        </div>
                        <p class="m-0 price">Rp 240.000</p>
                        <div class="cart d-flex align-items-center mt-4 justify-content-between">
                           <a href="#" class="category badge text-bg-primary">Beli</a>
                           <a href="detail.html" class="btn-cart">
                            <i class="bx bx-cart-alt"></i></a>
                        </div>
                      </div>
                    </div>
                  </div> --}}
      <!-- <div class="container menu-wrapper fixed-top">
        <div class="menu">
            <a class="nav-link active" href="#">All Kategori</a>
          </div>
      </div> -->


    <script src="{{ asset('assets/plugins/bootstrap/dist/js/bootstrap.bundle.min.js') }}"></script>
</body>
</html>
