<?php

namespace App\Exports;

use App\Models\Order;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ReportOrderUserExport implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Order::all();
    }
    public function headings(): array
    {
        return [
            'No',
            'Tanggal',
            'Nama Customer',
            'Nama Product',
            'Kategori',
            'Pesanan',
            'Jumlah Terjual',
            'Total',
        ];
    }
}
