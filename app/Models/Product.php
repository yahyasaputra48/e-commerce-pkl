<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use HasFactory;
    protected $fillable = [
        'menu_category_id',
        'name',
        'price',
        'stock',
        'description',
        'image',
        'unit',
    ];

    use SoftDeletes;
    protected $dates = ['deleted_at'];

}
