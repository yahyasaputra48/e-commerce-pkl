<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;
    protected $fillable = [
        'user_id', 'order_number', 'order_date', 'status', 'total', 'proof_of_payment'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
