<?php

namespace App\Http\Controllers;

use App\Exports\ReportOrderExport;
use App\Exports\ReportOrderUserExport;
use App\Exports\ReportProductExport;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Controllers\Controller;
use App\Models\Order;
use Illuminate\Http\Request;

class ReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function reportorder()
    {
        return view('pages.admin.report.order');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function reportproduct()
    {
        $orders = Order::all();
        return view('pages.admin.report.product', compact('orders'));
    }

    public function reportproductexport(){
        return Excel::download(new ReportProductExport, 'ReportProduct.xlsx');
    }

    public function reportuser()
    {
        $orders = Order::all();
        return view('pages.admin.report.user', compact('orders'));
    }

    public function reportuserexport(){
        return Excel::download(new ReportOrderUserExport, 'Reportuser.xlsx');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
