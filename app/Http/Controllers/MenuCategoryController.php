<?php

namespace App\Http\Controllers;

use App\Models\MenuCategory;
use App\Models\Product;
use Illuminate\Http\Request;

class MenuCategoryController extends Controller
{
    public function index(){
        $categories = MenuCategory::latest()->paginate(5);
        $products  = Product::all();
        return view('pages.admin.categories.index', compact('categories', 'products'));
    }

   public function create(){
    return view('pages.admin.categories.create');
   }

   public function store(Request $request)
   {
    $this->validate($request, [
        'name' => 'required',
    ]);
    $input = $request->all();
    if($request->file('image')){
        $file= $request->file('image');
        $filename= date('YmdHis').'.'.$file->getClientOriginalExtension();
        $file-> move(public_path('/category'), $filename);
        $input['image']= $filename;
    }
       $result = MenuCategory::create($input);

       if ($result) return redirect()->route('admin.categories.index')->with('success', 'Berhasil Menyimpan !');
   }
 
   public function edit($id)
   {
    $category = MenuCategory::findorfail($id);
    return view('pages.admin.categories.edit', compact('category'));
   }

   public function update(Request $request , $id)
   {
    $category = MenuCategory::findorfail($id);
    $input = $request->all();

    $category->update($input);

    return redirect()->route('admin.categories.index')->with('success', 'Data Berhasil Di update');
   }

   public function destroy(MenuCategory $category)
   {

    $category->delete();

    return redirect()->route('admin.categories.index')->with('success', 'Data Berhasil Di Delete');
   }
}
