<?php

namespace App\Http\Controllers;

use App\Models\MenuCategory;
use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::all();
        $menu_categories = MenuCategory::get();
        return view ('pages.admin.product.index', compact('products', 'menu_categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $menu_categories = MenuCategory::get();
        return view ('pages.admin.product.create', compact('menu_categories'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'menu_category_id' => 'required',
            'name' => 'required',
            'price' => 'required',
            'stock' => 'required',
            'description' => 'required',
            'unit' => 'required',
        ]);

        $input = $request->all();
        if($request->file('image')){
            $file= $request->file('image');
            $filename= date('YmdHis').'.'.$file->getClientOriginalExtension();
            $file-> move(public_path('/image'), $filename);
            $input['image']= $filename;
        }

        $result = Product::create($input);


        if ($result) return redirect()->route('admin.product.index')->with('success', 'Berhasil Menyimpan !');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::findorfail($id);
        $menu_categories = MenuCategory::get();
        return view ('pages.admin.product.edit', compact('menu_categories', 'product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $product = Product::findorfail($id);
        $input = $request->all();

        $product->update($input);
        return redirect()->route('admin.product.index')->with('success', 'Berhasil Menyimpan !');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $product->delete();

        return redirect()->route('admin.product.index')->with('success', 'Berhasil Hapus !');
    }
}
