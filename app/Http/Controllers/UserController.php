<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use PhpParser\Node\Expr\FuncCall;

class UserController extends Controller
{
    public function index(){
        $users = User::latest()->paginate(7);
        return view('pages.admin.user.index', compact('users'));
    }

   public function create(){
    return view('pages.admin.user.create');
   }

   public function store(Request $request)
   {

    $this->validate($request, [
        'name' => 'required',
        'email' => 'required',
        'password' => 'required',
        'phone' => 'required',
        'address' => 'required',
        'birth_place' => 'required',
        'birth_date' => 'required',
        'jk' => 'requeried',
    ]);

    $input = $request->all();
    if($request->file('image')){
        $file= $request->file('image');
        $filename= date('YmdHis').'.'.$file->getClientOriginalExtension();
        $file-> move(public_path('/image'), $filename);
        $input['image']= $filename;
    }

    $input['password'] = Hash::make($request->password);

    $result = User::create($input);
    $result->assignRole('user');

    if ($result) return redirect()->route('admin.user.index')->with('success', 'Data berhasil di tambahkan');

    return redirect()->back();
   }

   public function edit($id)
   {
    $user = User::findorfail($id);
    return view('pages.admin.user.edit', compact('user'));
   }

   public function update(Request $request, $id)
   {
    $user = User::findorfail($id);
    $input = $request->all();
    if($request->file('image')){
        $file= $request->file('image');
        $filename= date('YmdHis').'.'.$file->getClientOriginalExtension();
        $file-> move(public_path('/image'), $filename);
        $input['image']= $filename;
    }

    $user->update($input);

    return redirect()->route('admin.user.index')->with('success', 'Data berhasil di update');
    // return redirect()->back();
   }

   public function destroy(User $user)
   {

    $user->delete();

    return redirect()->route('admin.user.index')->with('success', 'Data berhasil di delete');
   }
}
