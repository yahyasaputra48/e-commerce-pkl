<?php

namespace App\Http\controllers;

use App\Models\Categories;
use Illuminate\Http\Request;
use PhpParser\Node\Expr\FuncCall;

class CategoriesController extends Controller
{
    public function index(){
        $categories = Categories::latest()->paginate(5);
        return view('pages.categories.index', compact('categories'));
    }

   public function create(){
    return view('pages.categories.create');
   }

   public function store(Request $request)
   {
       $request->validate([
        'nama' => 'required',
       ]);

       Categories::create($request->all());

       return redirect()->route('admin.categories.index')->with('success', 'Berhasil Menyimpan !');
   }

   public function edit($id)
   {
    $categories = Categories::findorfail($id);
    return view('pages.categories.edit', compact('categories'));
   }

   public function update(Request $request, $id)
   {
    $categories = Categories::findorfail($id);
    $input = $request->all();

    $categories->update($input);

    return redirect()->route('admin.categories.index')->with('success', 'Data Berhasil Di update');
   }

   public function destroy(Categories $categories)
   {
    $categories->delete();

    return redirect()->route('admin.categories.index')->with('success', 'Data Berhasil Di Delete');
   }
}
