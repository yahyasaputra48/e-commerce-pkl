<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;

class DetailController extends Controller
{
    public function index()
    {
        $products = Product::all();
        return view('pages.customers.product-detail', compact('products'));
    }

    public function show($id)
    {
        $products = Product::findorfail($id);
        return view('pages.customers.product-detail', compact('products'));
    }
}
