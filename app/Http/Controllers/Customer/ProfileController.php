<?php

namespace App\Http\Controllers\Customer;

use App\Models\User;
use App\Http\Controllers\Controller;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    public function index()
    {
        // $users = User::latest()->paginate(7);S
        $user = Auth::user();
        $products = Product::all();
        return view('pages.customers.home', compact('user', 'products'));
    }

    public function edit($id)
    {
        $user = User::findorfail($id);
        return view('pages.customers.profile', compact('user'));
    }

    public function update(Request $request, $id)
    {
        $input = $request->all();
        $user = User::findorfail($id);

        // $img = $request->file('image');
        // $image = date('YmdHis') . '.' . $img->getClientOriginalExtension();
        // $path = '/upload/images';
        // $destinationPath = public_path($path);
        // $img->move($destinationPath, $path);
        // $input['image'] = $path.'/'.$image;

        if($request->file('image')){
            $file= $request->file('image');
            $filename= date('YmdHis').'.'.$file->getClientOriginalExtension();
            $file-> move(public_path('/image'), $filename);
            $input['image']= $filename;
        }
        // dd($input);
        $user->update($input);

        return redirect()->back();
    }
}
