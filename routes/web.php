<?php

use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\Auth\ResetPasswordController;
use App\Http\Controllers\CartController;
use App\Http\Controllers\Customer\CheckoutController;
use App\Http\Controllers\Customer\ProfileController;
use App\Http\Controllers\Customer\CustomerController;
use App\Http\Controllers\Customer\DetailController;
use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\MenuCategoryController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\ReportController;
use App\Http\Controllers\ChartController;
use App\Http\Controllers\PesananController;
use Doctrine\DBAL\Schema\Index;
use GuzzleHttp\Psr7\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('admin-page', function() {
//     return '';
// })->middleware('role:admin')->name('admin.page');

// Route::get('user-page', function() {
//     return 'Halaman untuk User';
// })->middleware('role:user')->name('user.page');
Route::get('/cart', function () {
    return view('cart');
});
// Route::get('/login', function () {
    Route::get('/login', function() {
    return redirect()->route('login');
});
// Route::post('/register', [RegisterController::class, 'create'])->name('register');
// Route::get('/register', function() {
    //     return view('auth.register');
    // });

    Route::get('/resetpassword', [ResetPasswordController::class, 'index'])->name('reset.password');
    Route::put('/resetpassword', [ResetPasswordController::class, 'update'])->name('reset.update');

    // Route::get('/', [CustomerController::class, 'index'])->name('landingPage');

    //Guest
    Route::get('/', [CustomerController::class, 'index']);
    Route::get('product/detail/{id}', [CustomerController::class, 'show'])->name('detail.product');
    // Route::get('customer/cart', [CartController::class, 'index'])->name('cart');


    Auth::routes();
    Route::group(['prefix'=>'admin/','as'=>'admin.'], function(){
        Route::middleware('role:admin')->group(function (){
            Route::get('dashboard', [HomeController::class, 'index'])->name('dashboard');
            Route::resource('user', UserController::class);
            Route::resource('categories', MenuCategoryController::class);
            Route::resource('product', ProductController::class);
            Route::get('reportorder', [ReportController::class, 'reportorder'])->name('reportorder');
            Route::get('exportorder', [ReportController::class, 'reportorder'])->name('exportorder');
            Route::get('reportproduct', [ReportController::class, 'reportproduct'])->name('reportproduct');
            Route::get('exportproduct', [ReportController::class, 'reportproductexport'])->name('exportproduct');
            Route::get('reportuser', [ReportController::class, 'reportuser'])->name('reportuser');
            Route::get('exportuser', [ReportController::class, 'reportuserexport'])->name('exportuser');
            Route::get('list', [OrderController::class, 'index'])->name('order');
            Route::get('list/detail/{id}', [OrderController::class, 'show'])->name('orderdetail');
            Route::get('grafik', [ChartController::class, 'index'])->name('grafik.barchart');
            Route::get('list/create', [OrderController::class, 'create'])->name('ordercreate');
        });
    });

    Route::group(['prefix'=>'user/', 'as'=>'user.'], function(){
        Route::middleware('role:user')->group(function () {
            Route::get('/', [CustomerController::class, 'index']);
            Route::get('customer', [ProfileController::class, 'index'])->name('index');
            Route::get('customer/edit/{id}', [ProfileController::class, 'edit'])->name('edit');
            Route::put('customer/update/{id}', [ProfileController::class, 'update'])->name('update');
            Route::get('customer/cart', [CartController::class, 'index'])->name('cart');
            Route::post('customer/cart', [CartController::class, 'store'])->name('cart.store');
            Route::get('customer/checkout', [CheckoutController::class, 'index'])->name('checkout');
            Route::get('customer/details/{id}', [DetailController::class, 'show'])->name('details');
    });
    Route::get('customer/pesanan', [PesananController::class, 'index'])->name('pesanan');
    });

